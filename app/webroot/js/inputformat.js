$(function(){

    ctrlDisabled();

    // 商材カテゴリ変更時
    $(document).on('change', '#product_category', function(e) {
        var productCategoryId = $(this).val();
        location.href = '/input_formats/' + getCurrentStartMonth() + '/' + productCategoryId;
    });

    // アップロード　ファイル選択時
    $(document).on('change', 'input[name="csv"]', function(e){
        e.preventDefault();

        var target = e.target;
        var files = target.files;
        var reader = new FileReader();

        reader.readAsText(files[0], 'Shift_JIS');

        // ファイル読み込み時
        reader.onload = function(){

            // POST
            var formData = new FormData();
            var csvFiles = files;
            var productCategoryId = $('#product_category').val();
            $.each(csvFiles, function (i, csvFile) {
                formData.append('csvFile', csvFile);
            });
            formData.append('productCategory', productCategoryId);
            formData.append('month', getCurrentStartMonth());
            $.ajax({
                url: '/api/input_formats/input_table',
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                beforeSend: function () {
                    $.blockUI({
                        message: '処理中です。しばらくお待ちください。'
                    });
                },
            }).done(function (response) {
                if (response.result == 'OK') {
                    showSuccessMessage('データインポートを完了しました。');
                } else {
                    showErrorMessage('データインポートに失敗しました。');
                }
                // 3秒後に自動遷移
                setInterval(function () {
                    location.href = '/input_formats/' + getCurrentStartMonth() + '/' + productCategoryId;
                }, 3000);
            }).fail(function (xhr, response, error) {
                console.log(xhr)
                showErrorMessage('データインポートに失敗しました。[' + xhr.responseText + ']');
            }).always(function () {
                $.unblockUI();
            });

        };


    });

    $(window).on('load', function() {
        var columnLength = $('table thead tr td').length

        if (columnLength > 50) {
            $('table').width(6000);
        } else if (columnLength > 30) {
            $('table').width(4000);
        } else if (columnLength > 10) {
            $('table').width(2000);
        }
    });

    $('input[name="pageNo"]').on('change', function() {

        var pageNo = $(this).val();
        var totalPage = parseInt($('#totalPage').text());
        if (pageNo < 1 || pageNo > totalPage) {
            alert('ページ指定が不正です');
            return;
        }

        var productCategoryId = $('#product_category').val();
        var offset = (parseInt($(this).val()) -1) * 100;
        var limit = parseInt($('input[name="limit"]').val());
        location.href = '/input_formats/' + getCurrentStartMonth() + '/' + productCategoryId + '?offset=' + offset + '&limit=' + limit;
    });

    // 前へ
    $('#prev').on('click', function(){
        var productCategoryId = $('#product_category').val();
        var offset = parseInt($('input[name="offset"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var page = parseInt($('input[name="page"]').val());
        var item = parseInt($('input[name="item"]').val());
        var count = parseInt($('input[name="count"]').val());
        page--;
        offset -= limit;

        if ((page*item) <= count ) {
            $('#next').prop('disabled', false);
        }
        location.href = '/input_formats/' + getCurrentStartMonth() + '/' + productCategoryId + '?offset=' + offset + '&limit=' + limit;
    });

    // 次へ
    $('#next').on('click', function(){
        var productCategoryId = $('#product_category').val();
        var offset = parseInt($('input[name="offset"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var page = parseInt($('input[name="page"]').val());
        page++;
        offset += limit;

        if (page != 1) {
            $('#prev').prop('disabled', false);
        }
        location.href = '/input_formats/' + getCurrentStartMonth() + '/' + productCategoryId + '?offset=' + offset + '&limit=' + limit;
    });

    // 全選択ボタン押下
    $('#allSelect').on('click', function () {
        $('table input[type="checkbox"]').prop('checked', true);
    });

    // 全解除ボタン押下
    $('#allCancel').on('click', function () {
        $('table input[type="checkbox"]').prop('checked', false);
    });

    // 削除ボタン押下
    $('#delete').on('click', function () {
        // チェックされているか
        var checked = $('table input[type="checkbox"]:checked');
        if (checked.length == 0) {
            alert('チェックされていません。');
            return;
        }
        if(!window.confirm('選択されているデータを削除します。よろしいですか？')){
          return;
        }

        var id = '';
        $('table tbody tr').each(function() {
            var checked = $(this).find('input[type="checkbox"]').prop('checked');
            if (!checked) return;

            id += $(this).find('input[name="id"]').val() + ',';
        });

        var productCategoryId = $('#product_category').val();

        var url = '/api/input_formats';
        var param = {
            id: id,
            productCategoryId: productCategoryId
        };
        var request = Common.delete(url, param);
        request.done(function(response){

            if (response.result == 'OK') {
                showSuccessMessage('獲得データの削除を完了しました。');
                setTimeout(function() {
                    location.href = '/input_formats/' + getCurrentStartMonth() + '/' + productCategoryId;
                }, 3000);
            } else {
                showErrorMessage('獲得データの削除に失敗しました。[' + response.message + ']');
            }

        }).fail(function (xhr, response, error) {
            showErrorMessage(xhr.responseText);
        });

    });

    function ctrlDisabled() {
        var offset = parseInt($('input[name="offset"]').val());
        var page = parseInt($('input[name="page"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var count = parseInt($('input[name="count"]').val());
        if (page <= 1) {
            $('#prev').prop('disabled', true);
        }
        if ((page*limit) >= count || (offset+limit) == count) {
            $('#next').prop('disabled', true);
        }
    }

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
            month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    function showSuccessMessage(message) {
        var message = $('<div class="alert alert-success">' + message + '</div>');
        $('#message').html(message);
    }

    function showErrorMessage(message) {
        var message = $('<div class="alert alert-danger">' + message + '</div>');
        $('#message').html(message);
    }

    // Override
    Common.onChangeYear = function(){
        var productCategoryId = $('#product_category').val();
        location.href = '/input_formats/' + getCurrentStartMonth() + '/' + productCategoryId;
    }

    // Override
    Common.onChangeMonth = function(month){
        var productCategoryId = $('#product_category').val();
        location.href = '/input_formats/' + getCurrentStartMonth(month) + '/' + productCategoryId;
    }
});
