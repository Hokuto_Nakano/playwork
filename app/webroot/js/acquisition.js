
function setProduct(product_id, product_name){
  document.getElementById('product_id').value = product_id;
  document.getElementById('product_name').innerHTML = product_name;
}

function getAcquisition(id){
  getModal_('/partial/acquisitions/' + id);
}

function getModal_(url){
  var request = Common.getHtml(url);
  request.success(function(response){
    $('#acquisitionModal').html(response);
    $('#acquisitionModal').modal();
  });
  request.error(function(error){
    console.log(JSON.stringify(error));
  });
  request.done(function(response){
    //console.log(JSON.stringify(response));
  });
}

function registAcquisition(){
  var url = '/api/acquisitions';
  var id = document.getElementById('id').value;
  if(id > 0){
    url = url + '/' + id
  }
  var data = $('form').serialize();
  regist_(url, data);
}

function regist_(url, data){
  var request = Common.post(url, data);
  request.success(function(response){
    if(response.result == 'OK'){
      location.href = '/acquisitions';
    }
    else{
      document.getElementById('error').innerHTML = response.message;
    }
  });
  request.error(function(error){
    document.getElementById('error').innerHTML = '登録に失敗しました。';
  });
  request.done(function(response){
    //console.log(JSON.stringify(response));
  });
}

function deleteAcquisition(id){
    if(!id) return;
    var url = '/api/acquisitions/' + id;
    var jqXHR = Common.delete(url);
    jqXHR.done(function(response){
        if(response.result == 'OK'){
            location.href = '/acquisitions';
        }else{
            console.log(response.message);
        }
    }).fail(function(error){
        console.log(error);
    });
    return false;
};
