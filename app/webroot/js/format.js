$(function(){
    var currentPageConst = {
        necessaryColumns: 'necessaryColumns',
        acquisitions: 'acquisitions',
        status: 'status',
        plan: 'plan',
        countItems: 'countItems',
        identifyUser: 'identifyUser'
    };

    var captionText = {
        necessaryColumns: '取り込みたいデータを設定してください',
        acquisition: '獲得区分が入力されているCSVカラムを設定してください',
        status: '契約状況を設定してください',
        plan: '商材プランを設定してください',
        countItem: 'カウント項目をどのカラムでいつカウントするか設定してください',
        identifyUser: 'それぞれのカウントをどのスタッフに計上するか設定してください'
    }

    $('#year, #month').on('change', function() {
        location.href = '/formats/' + getCurrentStartMonth();
    });

    // モーダルが開いたときに実行
    $('#formatModal').on('shown.bs.modal', function() {
        // カウント項目を取得しておく
        var url = '/api/formats/count_items';
        var jqXHR = Common.getJson(url, {});
        var countItems = [];
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                _.each(response.data, function(data) {
                    var id = data.CountItem.id;
                    var itemName = data.CountItem.item_name;
                    var countItem = {};
                    countItem[id] = itemName;
                    countItems.push(countItem);
                });
                localStorage.setItem('countItems', JSON.stringify(countItems));
            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });

        // 選択した商材と年月でテーブルマップが存在するかチェック
        checkExist();

    });

    $('#registBtn').on('click', function(){
        $('#message').hide();
        var url = '/partial/formats/new';
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#formatModal').html(response);
            $('#formatModal').modal();

            setCaptionText(captionText.necessaryColumns);
            bindoOnChangeProductCategory();
            bindOnClose();
            bindPrevButton();

            $('#next').on('click', function(e){
                // 現在位置取得
                var currentPage = localStorage.getItem('currentPage');

                // データ登録
                if(currentPage == currentPageConst.identifyUser){

                    var conditions = [];
                    $('#staff_table table.table tbody').each(function(){
                        var input = $(this).find('input').serializeArray();
                        var select = $(this).find('select').serializeArray();
                        conditions.push(_.union(input, select));
                    });
                    localStorage.setItem('condition2', JSON.stringify(conditions));

                    var data = {
                        csvColumns: localStorage.getItem('csvColumns'),
                        data1: localStorage.getItem('data1'),
                        condition1: localStorage.getItem('condition1'),
                        condition2: localStorage.getItem('condition2'),
                        acquisitions: localStorage.getItem('acquisitions'),
                        statusData: localStorage.getItem('statusData'),
                        planData: localStorage.getItem('planData'),
                        categoryId: $('#modal_product_categories').val()
                    };
                    var url = '/api/formats/' + getCurrentStartMonth();
                    var param = {
                        data: data
                    };
                    var request = Common.post(url, param);
                    request.done(function(response){
                        if(response.result == 'OK'){
                            location.href = '/formats/' + getCurrentStartMonth();
                            // ローカルストレージ削除
                            localStorage.clear();
                        }
                    }).fail(function(xhr, response, error){
                        console.log(xhr);
                        console.log(response);
                        console.log(error);
                    });
                    return false;
                }

                if(currentPage == currentPageConst.countItems){
                    // 現在位置保存
                    localStorage.setItem('currentPage', currentPageConst.identifyUser);
                    $('#page').text('6/6 page');
                    setCaptionText(captionText.identifyUser);
                    $(this).text('登録');
                    // カウント項目へ戻る
                    $('#count_table').fadeOut();
                    // データ保存
                    var countItemData = [];
                    $('#count_table table.table tbody').each(function(){
                        var input = $(this).find('input').serializeArray();
                        var select = $(this).find('select').serializeArray();
                        countItemData.push(_.union(input, select));
                    });
                    // ユーザーとCSVカラムの紐付け
                    var storageCsvColumns = JSON.parse(localStorage.getItem('csvColumns'));
                    var csvColumns = [];
                    for (var i = 0; i < storageCsvColumns.length; i++){
                        var obj = {};
                        obj[i.toString()] = storageCsvColumns[i];
                        csvColumns.push(obj);
                    }
                    var data1 = JSON.parse(localStorage.getItem('data1'));

                    // 画面表示に使用する値
                    var countItemLabels = [];
                    var choiceItemKeys = [];
                    // 表示するカウント項目のラベルを特定
                    for(var a = 0; a < countItemData.length; a++){

                        for(var b = 0; b < countItemData[a].length; b++) {
                            if (countItemData[a][b]['name'] != 'count_items') {
                                continue;
                            }
                            var choiceItemKey = countItemData[a][b]['value'];
                            var countItemArray = getCountItems();

                            for (var c = 0; c < countItemArray.length; c++) {
                                var key = Object.keys(countItemArray[c]);
                                if (key == choiceItemKey) {
                                    var countItemLabel = countItemArray[c][key];
                                    countItemLabels.push(countItemLabel);
                                    choiceItemKeys.push(choiceItemKey);
                                }
                            }
                        }
                    }
                    var templateParam = {
                        staffCountConditionIds: [],
                        countItemLabels: countItemLabels,
                        choiceItemKeys: choiceItemKeys,
                        columnNames: data1.columnNames,
                        userIdColumnNames: [],
                        userNameColumnNames: [],
                        staffConditions: [],
                        itemNames: ['']
                    };
                    var url = '/formats/stafftable';
                    var request = Common.getHtml(url);
                    request.done(function (response) {
                        var template = _.template(response);
                        var contents = template(templateParam);
                        var html = $(contents).last().find('table.table')
                        $('#staff_table').html(html);
                        $('#staff_table').fadeIn();
                    }).fail(function () {
                        showErrorMessage('コンテンツ取得に失敗しました。');
                    });
                    $('#staff_table').fadeIn();
                    var condition1 = countItemData;
                    localStorage.setItem('condition1', JSON.stringify(condition1));
                    return false;
                }

                // 商材プラン
                if(currentPage == currentPageConst.plan){
                    // 現在位置の保存
                    localStorage.setItem('currentPage', currentPageConst.countItems);
                    $('#page').text('5/6 page');
                    setCaptionText(captionText.countItem);

                    // 前画面のデータ保存
                    var planData = [];
                    $('#plan_table table.table tbody tr').each(function(){
                        var input = $(this).find('input').serializeArray();
                        var select = $(this).find('select').serializeArray();
                        var plan = {};
                        if (!_.isEmpty(input) && !_.isEmpty(select)) {
                            var planValue = $(this).find('span').first().text();
                            plan['name'] = 'plan';
                            plan['value'] = planValue;
                            planData.push(_.union(input, select, [plan]));
                        }
                    });
                    localStorage.setItem('planData', JSON.stringify(planData));

                    $('#plan_table').fadeOut();

                    var data1 = JSON.parse(localStorage.getItem('data1'));
                    var columnNames = data1.columnNames;

                    var templateParam = {
                        countItems: getCountItems(),
                        columnNames: data1.columnNames,
                        dateColumnNames: [],
                        dataColumnNames: [],
                        comparisonConditions: [],
                        comparisonStrings: [],
                        itemNames: ['']
                    };

                    var url = '/formats/counttable';
                    var request = Common.getHtml(url);
                    request.done(function(response) {
                        var template = _.template(response);
                        var contents = template(templateParam);
                        var html = $(contents).last().find('table.table')
                        $('#count_table').html(html);
                        $('#count_table').fadeIn();
                    }).fail(function() {
                        showErrorMessage('コンテンツ取得に失敗しました。');
                    });

                    // 表示
                    $('#count_table').fadeIn();

                    // 追加ボタン押下時
                    $(document).off('click','.add-row-btn, .add-identify-user-row-btn');
                    $(document).on('click','.add-row-btn, .add-identify-user-row-btn', function(e){
                        var className = e.target.className;
                        // かつ
                        var and;
                        if (className.indexOf('add-row-btn') != -1) {
                            and = ['','','','','かつ','',''];
                        } else {
                            and = ['','','','かつ',''];
                        }
                        var row = getRow(and);
                        var rowArray;
                        if(className.indexOf('add-row-btn') != -1){
                            $(e.target).parents('tbody').append(row);
                            // カウント項目を表示
                            rowArray = ['','','','',
                            getDropDown(columnNames, 'csv_column'),
                            getDropDown(getConditions(), 'conditions'),
                            getInput({
                                id: 'compare',
                                name: 'compare',
                                type: 'text'
                            }),
                            getButton({type:'button', 'class':'del-row-btn',text:'×'})];
                            row = getRow(rowArray);
                            $(e.target).parents('tbody').append(row);
                        }
                        if(className.indexOf('add-identify-user-row-btn') != -1){
                            $(e.target).parents('tbody').append(row);
                            // CSVカラム、ユーザー属性を表示
                            rowArray = ['','','',
                            getDropDown(getConditions(), 'conditions'),
                            getDropDown(getStaff(), 'staff'),
                            getButton({type:'button', 'class':'del-row-btn',text:'×'})];
                            row = getRow(rowArray);
                            $(e.target).parents('tbody').append(row);
                        }
                    });
                    // 削除ボタン押下時
                    $(document).off('click','.del-row-btn');
                    $(document).on('click','.del-row-btn', function(e){
                        var index = $(this).parent().parent().index();
                        $(this).parents('tbody').find('tr:eq(' + (index-1) +')').remove();
                        $(this).parents('tbody').find('tr:eq(' + (index-1) +')').remove();
                    });
                    // 新規テーブル追加ボタン押下時
                    $(document).off('click','.add-countItem-btn, .add-identify-user-btn');
                    $(document).on('click','.add-countItem-btn, .add-identify-user-btn', function(e){
                        var className = e.target.className;
                        var tbody;
                        var rowArray;
                        var row;
                        if(className.indexOf('add-countItem-btn') != -1){
                            tbody = getTbody();
                            rowArray = [
                                '',
                                getDropDown(getCountItems(), 'count_items'),
                                getDropDown(columnNames, 'recorded_date'),
                                getButton({type:'button', 'class':'add-row-btn',text:'＋'}),
                                getDropDown(columnNames, 'csv_column'),
                                getDropDown(getConditions(), 'conditions' ),
                                getInput({
                                    id: 'compare',
                                    name: 'compare',
                                    type: 'text'
                                }),
                                getButton({type:'button', 'class':'del-count-btn',text:'×'}),
                                ];
                            row = getRow(rowArray);
                            tbody.append(row);
                            $(e.target).parents('table.table').append(tbody);
                        }
                        if(className.indexOf('add-identify-user-btn') != -1){
                            tbody = getTbody();
                            // ユーザーとCSVカラムの紐付け
                            var storageCsvColumns = JSON.parse(localStorage.getItem('csvColumns'));
                            var csvColumns = [];
                            for (var i = 0; i < storageCsvColumns.length; i++){
                                var obj = {};
                                obj[i.toString()] = storageCsvColumns[i];
                                csvColumns.push(obj);
                            }
                            rowArray = [
                                '',
                                getDropDown(csvColumns, 'identify-user'),
                                getButton({type:'button', 'class':'add-identify-user-row-btn',text:'＋'}),
                                getDropDown(getStaff(), 'staff'),
                                getDropDown(getConditions(), 'conditions'),
                                getInput({
                                    id: 'compare',
                                    name: 'compare',
                                    type: 'text'
                                }),
                                getButton({type:'button', 'class':'del-count-btn',text:'×'}),
                                ];
                            row = getRow(rowArray);
                            tbody.append(row);
                            $(e.target).parents('table.table').append(tbody);
                        }
                    });
                    // 追加テーブル削除ボタン押下時
                    $(document).off('click','.del-count-btn');
                    $(document).on('click','.del-count-btn', function(e){
                        $(this).parents('tbody').remove();
                    });
                }

                // プラン名
                if(currentPage == currentPageConst.status){
                    // 現在位置の保存
                    localStorage.setItem('currentPage', currentPageConst.plan);
                    $('#page').text('4/6 page');
                    setCaptionText(captionText.plan);

                    // 前画面のデータ保存
                    var statusData = [];
                    $('#status_table table.table tbody tr').each(function(){
                        var input = $(this).find('input').serializeArray();
                        var select = $(this).find('select').serializeArray();
                        var status = {};
                        if (!_.isEmpty(input) && !_.isEmpty(select)) {
                            var statusValue = $(this).find('div').first().text();
                            status['name'] = 'status';
                            status['value'] = statusValue;
                            statusData.push(_.union(input, select, [status]));
                        }
                    });
                    localStorage.setItem('statusData', JSON.stringify(statusData));

                    $('#status_table').fadeOut();

                    // 商材プラン名を取得する
                    var url = '/api/formats/plans';
                    var param = {
                        startMonth: getCurrentStartMonth(),
                        productCategoryId: $('#modal_product_categories').val()
                    };
                    var jqXHR = Common.getJson(url, param);
                    var planNames = [];
                    jqXHR.done(function(response){
                        if(response.result == 'OK'){
                            _.each(response.data, function(data) {
                                var productDetail = {};
                                productDetail[data['Product']['id']] = data['Product']['product_detail_name'];
                                var productFamily = {};
                                productFamily[data['ProductFamily']['id']] = data['ProductFamily']['name'] + '(' + data['ProductFamily']['short_name'] +')';

                                var planName = {
                                    productDetail, productFamily
                                };
                                planNames.push(planName);
                            });
                            localStorage.setItem('planNames', JSON.stringify(planNames));
                        }
                        var data1 = JSON.parse(localStorage.getItem('data1'));
                        var templateParam = {
                            planNames: planNames,
                            columnNames: data1.columnNames,
                            productCsvColumnNames: [],
                            comparisonConditions: [],
                            comparisonStrings: [],
                            updateFlg: false
                        };
                        var url = '/formats/plantable';
                        var request = Common.getHtml(url);
                        request.done(function(response) {
                            var template = _.template(response);
                            var contents = template(templateParam);
                            var html = $(contents).last().find('table.table')
                            $('#plan_table').html(html);
                            $('#plan_table').fadeIn();
                        }).fail(function() {
                            showErrorMessage('コンテンツ取得に失敗しました。');
                        });
                        // 表示
                        $('#plan_table').fadeIn();

                        // 行追加の制御
                        // 追加ボタン押下時
                        $(document).off('click','.addPlanConditionBtn');
                        $(document).on('click','.addPlanConditionBtn', function(e){
                            // かつ
                            var and = ['','','','かつ','',''];
                            var row = getRow(and);
                            var rowArray;
                            $(e.target).parents('tbody').append(row);
                            // カウント項目を表示
                            rowArray = ['','','',
                                getDropDown(data1.columnNames, 'csv_column'),
                                getDropDown(getConditions(), 'conditions'),
                                getInput({
                                    id: 'compare',
                                    name: 'compare',
                                    type: 'text'
                                }),
                                getButton({type:'button', 'class':'del-row-btn',text:'×'})];
                            row = getRow(rowArray);
                            $(e.target).parents('tbody').append(row);
                        });
                        // 削除ボタン押下時
                        $(document).off('click','.del-row-btn');
                        $(document).on('click','.del-row-btn', function(e){
                            var index = $(this).parent().parent().index();
                            $(this).parents('tbody').find('tr:eq(' + (index-1) +')').remove();
                            $(this).parents('tbody').find('tr:eq(' + (index-1) +')').remove();
                        });

                    }).fail(function(xhr, response, error){
                        console.log(xhr.responseText);
                    });
                }

                // 獲得区分で次へボタン押下
                if(currentPage == currentPageConst.acquisitions){
                    // 現在位置の保存
                    localStorage.setItem('currentPage', currentPageConst.status);
                    $('#page').text('3/6 page');
                    setCaptionText(captionText.status);

                    // 全画面を閉じる
                    $('#acquisition_table').fadeOut();

                    // 獲得区分のデータを保存
                    var acquisitions = [];
                    $('#acquisition_table table.table tbody tr').each(function(){
                        var input = $(this).find('input').serializeArray();
                        var select = $(this).find('select').serializeArray();
                        var type = $(this).find('span').first().text();
                        var acquisitionType = {
                            name: 'type',
                            value: type
                        };
                        acquisitions.push(_.union(input, select, acquisitionType));
                    });
                    localStorage.setItem('acquisitions', JSON.stringify(acquisitions));

                    // 画面表示用のデータ
                    var labels = [
                        {
                            label: '契約',
                            value: 'agreement',
                            message: '契約の時は獲得件数+1'

                        },{
                            label: 'キャンセル',
                            value: 'cancel',
                            message: 'キャンセルの時は獲得件数±0'
                        },{
                            label: '解約',
                            value: 'cancellation',
                            message: '解約の時は獲得件数-1'
                        }
                    ];

                    var data1 = JSON.parse(localStorage.getItem('data1'));
                    var templateParam = {
                        labels: labels,
                        columnNames: data1.columnNames,
                        productCsvColumnNames: [],
                        comparisonConditions: [],
                        comparisonStrings: [],
                        updateFlg: false
                    };

                    var url = '/formats/statustable';
                    var request = Common.getHtml(url);
                    request.done(function(response) {
                        var template = _.template(response);
                        var contents = template(templateParam);
                        var html = $(contents).last().find('table.table')
                        $('#status_table').html(html);
                        $('#status_table').fadeIn();
                        bindYesOrNo();
                    }).fail(function() {
                        showErrorMessage('コンテンツ取得に失敗しました。');
                    });
                    // 表示
                    $('#status_table').fadeIn();
                }

                if(currentPage == currentPageConst.necessaryColumns) {

                    // 現在位置の保存
                    localStorage.setItem('currentPage', currentPageConst.acquisitions);
                    $('#prev').show();
                    $('#csv_table').fadeOut();
                    $('#modal_product_categories').css('visibility','hidden');
                    $('input[type="file"]').hide();
                    $('#page').text('2/6 page');
                    setCaptionText(captionText.acquisition);

                    // チェックしたカラムを取得
                    var columnNames = [];
                    var dataTypes = [];
                    var uniqs = [];
                    var nameCollectings = [];
                    $('#csv_table tbody input:checked').each(function(){
                        var name = $(this).attr('name');
                        if(name.indexOf('row') != -1){
                            var row = name.replace('row','');
                            var column = $('#csv_table tbody tr:eq(' + row +') td:eq(3)').text();
                            var data = $('#csv_table tbody tr:eq(' + row +') td:eq(4) select[name="dataType"]').val();
                            var columnName = {};
                            columnName[row] = column;
                            var dataType = {
                                row: data
                            };
                            columnNames.push(columnName);
                            dataTypes.push(dataType);
                        }
                        if(name.indexOf('uniq') != -1){
                            var uniqRow = name.replace('uniq','');
                            var uniqColumn = $('#csv_table tbody tr:eq(' + uniqRow +') td:eq(3)').text();
                            var uniqData = {
                                uniqRow: uniqRow,
                                uniqColumn: uniqColumn
                            };
                            uniqs.push(uniqData);
                        }
                        if(name.indexOf('nameCollecting') != -1){
                            var nameCollectingRow = name.replace('nameCollecting','');
                            var nameCollectingColumn = $('#csv_table tbody tr:eq(' + nameCollectingRow +') td:eq(3)').text();
                            var nameCollectingData = {
                                nameCollectingRow: nameCollectingRow,
                                nameCollectingColumn: nameCollectingColumn
                            };
                            nameCollectings.push(nameCollectingData);
                        }
                    });

                    // ユニークキーのチェックは必須
                    if (_.isEmpty(uniqs)) {
                        alert('ユニークキーのチェックは必須です。');
                        $('#prev').click();
                        return false;
                    }
                    // 名寄せキーのチェックは1つのみ
                    if (nameCollectings.length != 1) {
                        alert('名寄せキーを1つチェックしてください。');
                        $('#prev').click();
                        return false;
                    }

                    // 選択されたカラム順にキーを変更
                    var clone = _.clone(columnNames);
                    columnNames = [];
                    _.each(clone, function(data, i) {
                        var obj = {};
                        obj[i] = _.values(data)[0];
                        columnNames.push(obj);
                    });

                    // データ保存
                    var data1 = {
                        columnNames: columnNames,
                        uniqKeys: uniqs,
                        nameCollectingKeys: nameCollectings,
                        dataTypes: dataTypes
                    };
                    localStorage.setItem('data1', JSON.stringify(data1));

                    // 獲得区分のデータを取得
                    var categoryId = $('#modal_product_categories').val();
                    var url = '/api/formats/' + categoryId;
                    var request = Common.getJson(url);
                    request.done(function(response) {
                        // 獲得区分のラベル
                        var acquisitionTypes = [];
                        _.each(response.data, function (data) {
                            var type = {};
                            type[data.AcquisitionType.id] = data.AcquisitionType.acquisition_type;
                            acquisitionTypes.push(type);
                        });

                        var templateParam = {
                            acquisitionTypes: acquisitionTypes,
                            columnNames: columnNames,
                            productCsvColumnNames: [],
                            comparisonConditions: [],
                            comparisonStrings: []
                        };

                        var url = '/formats/acquisitiontable';
                        var request = Common.getHtml(url);
                        request.done(function(response) {
                            var template = _.template(response);
                            var contents = template(templateParam);
                            var html = $(contents).last().find('table.table')
                            $('#acquisition_table').html(html);
                            $('#acquisition_table').fadeIn();
                        }).fail(function() {
                            showErrorMessage('コンテンツ取得に失敗しました。');
                        });

                    }).fail(function (data) {
                        console.log(data);
                    });
                }
                return false;
            });

        });
    });

    // アップロード　ファイル選択時
    $(document).on('change','input[name="file"]', function(e){
        var target = e.target;
        var files = target.files;
        var reader = new FileReader();
        // ファイル読み込み時
        reader.onload = function(){
            var header = reader.result.split(/\r\n|\r|\n/)[0];
            // ダブルクォーテーションが付加されている場合は削除しておく
            header = header.replace(/"/g, '');
            var headerNames = header.split(',');

            // 現在位置の保存とCSVカラム一覧の保存
            localStorage.setItem('currentPage', 'necessaryColumns');
            localStorage.setItem('csvColumns', JSON.stringify(headerNames));

            // テンプレートで使用する値
            var templateParam = {
                headerNames: headerNames,
                commonKeyColumns: '',
                nameCollecting: '',
                dataTypes: []
            };

            var url = '/formats/csvtable';
            var request = Common.getHtml(url);
            request.done(function(response) {
                var template = _.template(response);
                var contents = template(templateParam);
                var html = $(contents).last().find('table.table')
                $('#csv_table').html(html);
            }).fail(function() {
                showErrorMessage('コンテンツ取得に失敗しました。[csvtable]');
            });

            if (!_.isEmpty($('#modal-message').text())) return;
            // 次へボタンを有効化
            $('#next').prop('disabled', false);
        };
        reader.readAsText(files[0], 'Shift_JIS');
    });

    // データをクリックした場合
    $('#data table.table tbody tr').on('click', function() {

        // モーダルダイアログを表示
        var url = '/partial/formats/update';
        var request = Common.getHtml(url);
        var tableMapId = $(this).find('td:first').text();
        request.done(function(response) {
            $('#formatModal').html(response);
            $('#formatModal').modal();
            setCaptionText(captionText.necessaryColumns);
            bindOnClose();
            bindPrevButton();
            bindDisabledButton(tableMapId);

            url = '/api/formats/update';
            var param = {
                tableMapId: tableMapId
            };
            var req = Common.getJson(url, param);
            req.done(function (response) {
                // csvtableを取得
                url = '/formats/csvtable';
                // 1画面目
                var csvColumnNames = [];
                var commonKeyColumns = '';
                var nameCollecting = '';
                var dataTypes = [];
                // 2画面目
                var acquisitionTypes = [];
                var comparisonConditions = [];
                var comparisonStrings = [];
                var productCsvColumnNames = [];
                // 3画面目
                var contractStatuses = [];
                var statusComparisonConditions = [];
                var statusComparisonStrings = [];
                var statusCsvColumnNames = [];
                // 4画面目
                var planNames = [];
                var planComparisonConditions = [];
                var planComparisonStrings = [];
                var planCsvColumnNames = [];
                // 5画面目
                var dateColumnNames = [];
                var dataColumnNames = [];
                var countItemComparisonConditions = [];
                var countItemComparisonStrings = [];
                var acquisitionTypeColumnNames = [];
                var itemNames = [];
                // 6画面目
                var userIdColumnNames = [];
                var userNameColumnNames = [];
                var staffConditions = [];
                var staffCountConditionIds = [];
                // ID系
                var productCsvColumnMapIds = [];
                var productCsvAcquisitionMapIds = [];
                var productCsvStatusMapIds = [];
                var productCsvPlanMapIds = [];
                var countConditionIds = [];
                var staffConditionIds = [];
                var categoryId;
                _.each(response.data, function(data) {

                    // ProductCsvTableMap
                    if (Object.keys(data)[0] === 'ProductCsvTableMap') {
                        categoryId = data['ProductCsvTableMap']['product_category_id'];
                        commonKeyColumns = data['ProductCsvTableMap']['common_key_columns'];
                        nameCollecting = data['ProductCsvTableMap']['name_collecting'];
                    }

                    if (Array.isArray(data)) {
                        _.each(data, function(object) {
                            if (_.indexOf(Object.keys(object), 'ProductCsvColumnMap') != -1) {
                                var productCsvColumnMapId = object['ProductCsvColumnMap']['id'];
                                var csvColumnName = object['ProductCsvColumnMap']['product_csv_column_name'];
                                var dataType = object['ProductCsvColumnMap']['data_type'];
                                productCsvColumnMapIds.push(productCsvColumnMapId);
                                csvColumnNames.push(csvColumnName);
                                dataTypes.push(dataType);
                            }
                            if (_.indexOf(Object.keys(object), 'AcquisitionType') != -1)  {
                                var id = object['AcquisitionType']['id'];
                                var acquisitionType = object['AcquisitionType']['acquisition_type'];
                                var type = {};
                                type[id] = acquisitionType;
                                acquisitionTypes.push(type);
                            }
                            if (_.indexOf(Object.keys(object), 'ProductCsvAcquisitionMap') != -1) {
                                var productCsvAcquisitionMapId = object['ProductCsvAcquisitionMap']['id'];
                                var comparisonCondition = object['ProductCsvAcquisitionMap']['comparison_condition'];
                                var comparisonString = object['ProductCsvAcquisitionMap']['comparison_string'];
                                var productCsvColumnName = object['ProductCsvAcquisitionMap']['product_csv_column_name'];
                                productCsvAcquisitionMapIds.push(productCsvAcquisitionMapId);
                                comparisonConditions.push(comparisonCondition);
                                comparisonStrings.push(comparisonString);
                                productCsvColumnNames.push(productCsvColumnName);
                            }
                            if (_.indexOf(Object.keys(object), 'ProductCsvStatusMap') != -1) {
                                var productCsvStatusMapId = object['ProductCsvStatusMap']['id'];
                                var contractStatus = object['ProductCsvStatusMap']['contract_status'];
                                var comparisonCondition = object['ProductCsvStatusMap']['comparison_condition'];
                                var comparisonString = object['ProductCsvStatusMap']['comparison_string'];
                                var productCsvColumnName = object['ProductCsvStatusMap']['product_csv_column_name'];
                                productCsvStatusMapIds.push(productCsvStatusMapId);
                                contractStatuses.push(contractStatus);
                                statusComparisonConditions.push(comparisonCondition);
                                statusComparisonStrings.push(comparisonString);
                                statusCsvColumnNames.push(productCsvColumnName);
                            }
                            if (_.indexOf(Object.keys(object), 'ProductCsvPlanMap') != -1 || _.indexOf(Object.keys(object), 'Product') != -1 || _.indexOf(Object.keys(object), 'ProductFamily') != -1) {
                                var productCsvPlanMapId = object['ProductCsvPlanMap']['id'];
                                var productDetail = {};
                                productDetail[object['Product']['id']] = object['Product']['product_detail_name'];
                                var productFamily = {};
                                productFamily[object['ProductFamily']['id']] = object['ProductFamily']['name'] + '(' + object['ProductFamily']['short_name'] +')';

                                var planName = {
                                    productDetail, productFamily
                                };
                                var comparisonCondition = object['ProductCsvPlanMap']['comparison_condition'];
                                var comparisonString = object['ProductCsvPlanMap']['comparison_string'];
                                var productCsvColumnName = object['ProductCsvPlanMap']['product_csv_column_name'];
                                productCsvPlanMapIds.push(productCsvPlanMapId);
                                planNames.push(planName);
                                planComparisonConditions.push(comparisonCondition);
                                planComparisonStrings.push(comparisonString);
                                planCsvColumnNames.push(productCsvColumnName);
                            }
                            if (_.indexOf(Object.keys(object), 'CountCondition') != -1) {
                                var countConditionId = object['CountCondition']['id'];
                                var dateColumnName = object['CountCondition']['date_column_name'];
                                var dataColumnName = object['CountCondition']['data_column_name'];
                                var comparisonCondition = object['CountCondition']['comparison_condition'];
                                var comparisonString = object['CountCondition']['comparison_string'];
                                var acquisitionTypeColumnName = object['CountCondition']['acquisition_type_column_name'];
                                countConditionIds.push(countConditionId);
                                dateColumnNames.push(dateColumnName);
                                dataColumnNames.push(dataColumnName);
                                countItemComparisonConditions.push(comparisonCondition);
                                countItemComparisonStrings.push(comparisonString);
                                acquisitionTypeColumnNames.push(acquisitionTypeColumnName);
                            }
                            if (_.indexOf(Object.keys(object), 'StaffCondition') != -1) {
                                var staffConditionId = object['StaffCondition']['id'];
                                var staffCountConditionId = object['StaffCondition']['count_condition_id'];
                                var comparisonCondition = object['StaffCondition']['comparison_condition'];
                                var userIdColumnName = object['StaffCondition']['user_id_column_name'];
                                var userNameColumnName = object['StaffCondition']['user_name_column_name'];
                                staffConditionIds.push(staffConditionId);
                                staffCountConditionIds.push(staffCountConditionId);
                                staffConditions.push(comparisonCondition);
                                userIdColumnNames.push(userIdColumnName);
                                userNameColumnNames.push(userNameColumnName);
                            }
                            if (_.indexOf(Object.keys(object), 'CountItem') != -1) {
                                var itemName = object['CountItem']['item_name'];
                                itemNames.push(itemName);
                            }

                        })
                    }
                });

                var templateParam = {
                    headerNames: csvColumnNames,
                    commonKeyColumns: commonKeyColumns,
                    nameCollecting: nameCollecting,
                    dataTypes: dataTypes
                }
                var reqCsvTable = Common.getHtml(url);
                reqCsvTable.done(function(response) {
                    var template = _.template(response);
                    var contents = template(templateParam);
                    var html = $(contents).last().find('table.table')
                    $('#csv_table').html(html);

                    localStorage.setItem('currentPage', 'necessaryColumns');
                    localStorage.setItem('csvColumns', JSON.stringify(csvColumnNames));

                    // 次へボタンを有効化
                    $('#next').prop('disabled', false);

                    // 次へボタン押下時
                    $('#next').on('click', function () {

                        var currentPage = localStorage.getItem('currentPage');

                        // データ更新
                        if(currentPage == currentPageConst.identifyUser){

                            var conditions = [];
                            $('#staff_table table.table tbody').each(function(){
                                var input = $(this).find('input').serializeArray();
                                var select = $(this).find('select').serializeArray();
                                conditions.push(_.union(input, select));
                            });
                            localStorage.setItem('condition2', JSON.stringify(conditions));

                            var data = {
                                csvColumns: localStorage.getItem('csvColumns'),
                                data1: localStorage.getItem('data1'),
                                condition1: localStorage.getItem('condition1'),
                                condition2: localStorage.getItem('condition2'),
                                acquisitions: localStorage.getItem('acquisitions'),
                                statusData: localStorage.getItem('statusData'),
                                planData: localStorage.getItem('planData'),
                                categoryId: categoryId,
                                productCsvTableMapId: tableMapId,
                                productCsvColumnMapIds: productCsvColumnMapIds,
                                productCsvStatusMapIds: productCsvStatusMapIds,
                                productCsvAcquisitionMapIds: productCsvAcquisitionMapIds,
                                productCsvPlanMapIds: productCsvPlanMapIds,
                                countConditionIds: countConditionIds,
                                staffConditionIds: staffConditionIds
                            };
                            var url = '/api/formats/' + getCurrentStartMonth();
                            var param = {
                                data: data
                            };
                            var request = Common.post(url, param);
                            request.done(function(response){
                                if(response.result == 'OK'){
                                    location.href = '/formats/' + getCurrentStartMonth();
                                    // ローカルストレージ削除
                                    localStorage.clear();
                                    localStorage.clear();
                                }
                            }).fail(function(xhr, response, error){
                                console.log(xhr);
                                console.log(response);
                                console.log(error);
                            });
                            return false;
                        }

                        if(currentPage == currentPageConst.countItems){
                            // 現在位置保存
                            localStorage.setItem('currentPage', currentPageConst.identifyUser);
                            $('#page').text('6/6 page');
                            setCaptionText(captionText.identifyUser);
                            $(this).text('登録');
                            // カウント項目へ戻る
                            $('#count_table').fadeOut();
                            // データ保存
                            var countItemData = [];
                            $('#count_table table.table tbody').each(function(){
                                var input = $(this).find('input').serializeArray();
                                var select = $(this).find('select').serializeArray();
                                countItemData.push(_.union(input, select));
                            });
                            // ユーザーとCSVカラムの紐付け
                            var storageCsvColumns = JSON.parse(localStorage.getItem('csvColumns'));
                            var csvColumns = [];
                            for (var i = 0; i < storageCsvColumns.length; i++){
                                var obj = {};
                                obj[i.toString()] = storageCsvColumns[i];
                                csvColumns.push(obj);
                            }
                            var data1 = JSON.parse(localStorage.getItem('data1'));

                            // 画面表示に使用する値
                            var countItemLabels = [];
                            var choiceItemKeys = [];
                            // 表示するカウント項目のラベルを特定
                            for(var a = 0; a < countItemData.length; a++){

                                for(var b = 0; b < countItemData[a].length; b++) {
                                    if (countItemData[a][b]['name'] != 'count_items') {
                                        continue;
                                    }
                                    var choiceItemKey = countItemData[a][b]['value'];
                                    var countItemArray = getCountItems();

                                    for (var c = 0; c < countItemArray.length; c++) {
                                        var key = Object.keys(countItemArray[c]);
                                        if (key == choiceItemKey) {
                                            var countItemLabel = countItemArray[c][key];
                                            countItemLabels.push(countItemLabel);
                                            choiceItemKeys.push(choiceItemKey);
                                        }
                                    }
                                }
                            }
                            var templateParam = {
                                staffCountConditionIds: staffCountConditionIds,
                                countItemLabels: countItemLabels,
                                choiceItemKeys: choiceItemKeys,
                                columnNames: data1.columnNames,
                                userIdColumnNames: userIdColumnNames,
                                userNameColumnNames: userNameColumnNames,
                                staffConditions: staffConditions,
                                itemNames: itemNames
                            };
                            var url = '/formats/stafftable';
                            var request = Common.getHtml(url);
                            request.done(function (response) {
                                var template = _.template(response);
                                var contents = template(templateParam);
                                var html = $(contents).last().find('table.table')
                                $('#staff_table').html(html);
                                $('#staff_table').fadeIn();
                            }).fail(function () {
                                showErrorMessage('コンテンツ取得に失敗しました。');
                            });
                            $('#staff_table').fadeIn();
                            var condition1 = countItemData;
                            localStorage.setItem('condition1', JSON.stringify(condition1));
                            return false;
                        }

                        // 契約、キャンセル、解約
                        if(currentPage == currentPageConst.plan){
                            // 現在位置の保存
                            localStorage.setItem('currentPage', currentPageConst.countItems);
                            $('#page').text('5/6 page');
                            setCaptionText(captionText.countItem);

                            // 前画面のデータ保存
                            var planData = [];
                            $('#plan_table table.table tbody tr').each(function(){
                                var input = $(this).find('input').serializeArray();
                                var select = $(this).find('select').serializeArray();
                                var plan = {};
                                if (!_.isEmpty(input) && !_.isEmpty(select)) {
                                    var planValue = $(this).find('span').first().text();
                                    plan['name'] = 'plan';
                                    plan['value'] = planValue;
                                    planData.push(_.union(input, select, [plan]));
                                }
                            });
                            localStorage.setItem('planData', JSON.stringify(planData));

                            $('#plan_table').fadeOut();
                            var data1 = JSON.parse(localStorage.getItem('data1'));
                            var columnNames = data1.columnNames;

                            var templateParam = {
                                countItems: getCountItems(),
                                columnNames: data1.columnNames,
                                dateColumnNames: dateColumnNames,
                                dataColumnNames: dataColumnNames,
                                comparisonConditions: countItemComparisonConditions,
                                comparisonStrings: countItemComparisonStrings,
                                itemNames: itemNames
                            };

                            var url = '/formats/counttable';
                            var request = Common.getHtml(url);
                            request.done(function(response) {
                                var template = _.template(response);
                                var contents = template(templateParam);
                                var html = $(contents).last().find('table.table');
                                $('#count_table').html(html);
                                $('#count_table').fadeIn();
                            }).fail(function() {
                                showErrorMessage('コンテンツ取得に失敗しました。[counttable]');
                            });

                            // 表示
                            $('#count_table').fadeIn();

                            // 追加ボタン押下時
                            $(document).off('click','.add-row-btn, .add-identify-user-row-btn');
                            $(document).on('click','.add-row-btn, .add-identify-user-row-btn', function(e){
                                var className = e.target.className;
                                // かつ
                                var and;
                                if (className.indexOf('add-row-btn') != -1) {
                                    and = ['','','','','かつ','',''];
                                } else {
                                    and = ['','','','かつ',''];
                                }
                                var row = getRow(and);
                                var rowArray;
                                if(className.indexOf('add-row-btn') != -1){
                                    $(e.target).parents('tbody').append(row);
                                    // カウント項目を表示
                                    rowArray = ['','','','',
                                        getDropDown(columnNames, 'csv_column'),
                                        getDropDown(getConditions(), 'conditions'),
                                        getInput({
                                            id: 'compare',
                                            name: 'compare',
                                            type: 'text'
                                        }),
                                        getButton({type:'button', 'class':'del-row-btn',text:'×'})];
                                    row = getRow(rowArray);
                                    $(e.target).parents('tbody').append(row);
                                }
                                if(className.indexOf('add-identify-user-row-btn') != -1){
                                    $(e.target).parents('tbody').append(row);
                                    // CSVカラム、ユーザー属性を表示
                                    rowArray = ['','','',
                                        getDropDown(getConditions(), 'conditions'),
                                        getDropDown(getStaff(), 'staff'),
                                        getButton({type:'button', 'class':'del-row-btn',text:'×'})];
                                    row = getRow(rowArray);
                                    $(e.target).parents('tbody').append(row);
                                }
                            });
                            // 削除ボタン押下時
                            $(document).off('click','.del-row-btn');
                            $(document).on('click','.del-row-btn', function(e){
                                var index = $(this).parent().parent().index();
                                $(this).parents('tbody').find('tr:eq(' + (index-1) +')').remove();
                                $(this).parents('tbody').find('tr:eq(' + (index-1) +')').remove();
                            });
                            // 新規テーブル追加ボタン押下時
                            $(document).off('click','.add-countItem-btn, .add-identify-user-btn');
                            $(document).on('click','.add-countItem-btn, .add-identify-user-btn', function(e){
                                var className = e.target.className;
                                var tbody;
                                var rowArray;
                                var row;
                                if(className.indexOf('add-countItem-btn') != -1){
                                    tbody = getTbody();
                                    rowArray = [
                                        '',
                                        getDropDown(getCountItems(), 'count_items'),
                                        getDropDown(columnNames, 'recorded_date'),
                                        getButton({type:'button', 'class':'add-row-btn',text:'＋'}),
                                        getDropDown(columnNames, 'csv_column'),
                                        getDropDown(getConditions(), 'conditions' ),
                                        getInput({
                                            id: 'compare',
                                            name: 'compare',
                                            type: 'text'
                                        }),
                                        getButton({type:'button', 'class':'del-count-btn',text:'×'}),
                                    ];
                                    row = getRow(rowArray);
                                    tbody.append(row);
                                    $(e.target).parents('table.table').append(tbody);
                                }
                                if(className.indexOf('add-identify-user-btn') != -1){
                                    tbody = getTbody();
                                    // ユーザーとCSVカラムの紐付け
                                    var storageCsvColumns = JSON.parse(localStorage.getItem('csvColumns'));
                                    var csvColumns = [];
                                    for (var i = 0; i < storageCsvColumns.length; i++){
                                        var obj = {};
                                        obj[i.toString()] = storageCsvColumns[i];
                                        csvColumns.push(obj);
                                    }
                                    rowArray = [
                                        '',
                                        getDropDown(csvColumns, 'identify-user'),
                                        getButton({type:'button', 'class':'add-identify-user-row-btn',text:'＋'}),
                                        getDropDown(getStaff(), 'staff'),
                                        getDropDown(getConditions(), 'conditions'),
                                        getInput({
                                            id: 'compare',
                                            name: 'compare',
                                            type: 'text'
                                        }),
                                        getButton({type:'button', 'class':'del-count-btn',text:'×'}),
                                    ];
                                    row = getRow(rowArray);
                                    tbody.append(row);
                                    $(e.target).parents('table.table').append(tbody);
                                }
                            });
                            // 追加テーブル削除ボタン押下時
                            $(document).off('click','.del-count-btn');
                            $(document).on('click','.del-count-btn', function(e){
                                $(this).parents('tbody').remove();
                            });
                        }

                        // プラン名
                        if(currentPage == currentPageConst.status){
                            // 現在位置の保存
                            localStorage.setItem('currentPage', currentPageConst.plan);
                            $('#page').text('4/6 page');
                            setCaptionText(captionText.plan);

                            // 前画面のデータ保存
                            var statusData = [];
                            $('#status_table table.table tbody tr').each(function(){
                                var input = $(this).find('input').serializeArray();
                                var select = $(this).find('select').serializeArray();
                                var status = {};
                                if (!_.isEmpty(input) && !_.isEmpty(select)) {
                                    var statusValue = $(this).find('div').first().text();
                                    status['name'] = 'status';
                                    status['value'] = statusValue;
                                    statusData.push(_.union(input, select, [status]));
                                }
                            });
                            localStorage.setItem('statusData', JSON.stringify(statusData));

                            $('#status_table').fadeOut();

                            var data1 = JSON.parse(localStorage.getItem('data1'));
                            var templateParam = {
                                planNames: planNames,
                                columnNames: data1.columnNames,
                                productCsvColumnNames: planCsvColumnNames,
                                comparisonConditions: planComparisonConditions,
                                comparisonStrings: planComparisonStrings,
                                updateFlg: true
                            };
                            var url = '/formats/plantable';
                            var request = Common.getHtml(url);
                            request.done(function(response) {
                                var template = _.template(response);
                                var contents = template(templateParam);
                                var html = $(contents).last().find('table.table')
                                $('#plan_table').html(html);
                                $('#plan_table').fadeIn();
                            }).fail(function() {
                                showErrorMessage('コンテンツ取得に失敗しました。');
                            });
                            // 表示
                            //$('#plan_table').fadeIn();

                            // 行追加の制御
                            // 追加ボタン押下時
                            $(document).off('click','.addPlanConditionBtn');
                            $(document).on('click','.addPlanConditionBtn', function(e){
                                // かつ
                                var and = ['','','','かつ','',''];
                                var row = getRow(and);
                                var rowArray;
                                $(e.target).parents('tbody').append(row);
                                // カウント項目を表示
                                rowArray = ['','','',
                                    getDropDown(data1.columnNames, 'csv_column'),
                                    getDropDown(getConditions(), 'conditions'),
                                    getInput({
                                        id: 'compare',
                                        name: 'compare',
                                        type: 'text'
                                    }),
                                    getButton({type:'button', 'class':'del-row-btn',text:'×'})];
                                row = getRow(rowArray);
                                $(e.target).parents('tbody').append(row);
                            });
                            // 削除ボタン押下時
                            $(document).off('click','.del-row-btn');
                            $(document).on('click','.del-row-btn', function(e){
                                var index = $(this).parent().parent().index();
                                $(this).parents('tbody').find('tr:eq(' + (index-1) +')').remove();
                                $(this).parents('tbody').find('tr:eq(' + (index-1) +')').remove();
                            });

                        }

                        // 契約状況画面へ遷移する（獲得区分で次へボタン押下）
                        if(currentPage == currentPageConst.acquisitions){
                            // 現在位置の保存
                            localStorage.setItem('currentPage', currentPageConst.status);
                            $('#page').text('3/6 page');
                            setCaptionText(captionText.status);

                            // 全画面を閉じる
                            $('#acquisition_table').fadeOut();

                            // 獲得区分のデータを保存
                            var acquisitions = [];
                            $('#acquisition_table table.table tbody tr').each(function(){
                                var input = $(this).find('input').serializeArray();
                                var select = $(this).find('select').serializeArray();
                                var type = $(this).find('span').first().text();
                                var acquisitionType = {
                                    name: 'type',
                                    value: type
                                };
                                acquisitions.push(_.union(input, select, acquisitionType));
                            });
                            localStorage.setItem('acquisitions', JSON.stringify(acquisitions));

                            // 画面表示用のデータ
                            var labels = [
                                {
                                    label: '契約',
                                    value: 'agreement',
                                    message: '契約の時は獲得件数+1'

                                },{
                                    label: 'キャンセル',
                                    value: 'cancel',
                                    message: 'キャンセルの時は獲得件数±0'
                                },{
                                    label: '解約',
                                    value: 'cancellation',
                                    message: '解約の時は獲得件数-1'
                                }
                            ];

                            var data1 = JSON.parse(localStorage.getItem('data1'));
                            var templateParam = {
                                labels: labels,
                                columnNames: data1.columnNames,
                                productCsvColumnNames: statusCsvColumnNames,
                                comparisonConditions: statusComparisonConditions,
                                comparisonStrings: statusComparisonStrings,
                                updateFlg: true
                            };

                            var url = '/formats/statustable';
                            var request = Common.getHtml(url);
                            request.done(function(response) {
                                var template = _.template(response);
                                var contents = template(templateParam);
                                var html = $(contents).last().find('table.table')
                                $('#status_table').html(html);
                                $('#status_table').fadeIn();
                                bindYesOrNo();
                            }).fail(function() {
                                showErrorMessage('コンテンツ取得に失敗しました。[statustable]');
                            });
                            // 表示
                            $('#status_table').fadeIn();
                        }

                        // 必要
                        if(currentPage == currentPageConst.necessaryColumns) {

                            // 現在位置の保存
                            localStorage.setItem('currentPage', currentPageConst.acquisitions);
                            $('#prev').show();
                            $('#csv_table').fadeOut();
                            $('#page').text('2/6 page');
                            setCaptionText(captionText.acquisition);
                            // チェックしたカラムを取得
                            var columnNames = [];
                            var dataTypes = [];
                            var uniqs = [];
                            var nameCollectings = [];
                            $('#csv_table tbody input:checked').each(function () {
                                var name = $(this).attr('name');
                                if (name.indexOf('row') != -1) {
                                    var row = name.replace('row', '');
                                    var column = $('#csv_table tbody tr:eq(' + row + ') td:eq(3)').text();
                                    var data = $('#csv_table tbody tr:eq(' + row + ') td:eq(4) select[name="dataType"]').val();
                                    var columnName = {};
                                    columnName[row] = column;
                                    var dataType = {
                                        row: data
                                    };
                                    columnNames.push(columnName);
                                    dataTypes.push(dataType);
                                }
                                if (name.indexOf('uniq') != -1) {
                                    var uniqRow = name.replace('uniq', '');
                                    var uniqColumn = $('#csv_table tbody tr:eq(' + uniqRow + ') td:eq(3)').text();
                                    var uniqData = {
                                        uniqRow: uniqRow,
                                        uniqColumn: uniqColumn
                                    };
                                    uniqs.push(uniqData);
                                }
                                if(name.indexOf('nameCollecting') != -1){
                                    var nameCollectingRow = name.replace('nameCollecting','');
                                    var nameCollectingColumn = $('#csv_table tbody tr:eq(' + nameCollectingRow +') td:eq(3)').text();
                                    var nameCollectingData = {
                                        nameCollectingRow: nameCollectingRow,
                                        nameCollectingColumn: nameCollectingColumn
                                    };
                                    nameCollectings.push(nameCollectingData);
                                }
                            });

                            // ユニークキーのチェックは必須
                            if (_.isEmpty(uniqs)) {
                                alert('ユニークキーのチェックは必須です。');
                                $('#prev').click();
                                return false;
                            }
                            // 名寄せキーのチェックは1つのみ
                            if (nameCollectings.length != 1) {
                                alert('名寄せキーを1つチェックしてください。');
                                $('#prev').click();
                                return false;
                            }

                            // 選択されたカラム順にキーを変更
                            var clone = _.clone(columnNames);
                            columnNames = [];
                            _.each(clone, function(data, i) {
                                var obj = {};
                                obj[i] = _.values(data)[0];
                                columnNames.push(obj);
                            });

                            // データ保存
                            var data1 = {
                                columnNames: columnNames,
                                uniqKeys: uniqs,
                                nameCollectingKeys: nameCollectings,
                                dataTypes: dataTypes
                            };
                            localStorage.setItem('data1', JSON.stringify(data1));
                            // 獲得区分のデータを取得
                            var templateParam = {
                                acquisitionTypes: acquisitionTypes,
                                columnNames: columnNames,
                                productCsvColumnNames: productCsvColumnNames,
                                comparisonConditions: comparisonConditions,
                                comparisonStrings: comparisonStrings
                            };
                            var url = '/formats/acquisitiontable';
                            var request = Common.getHtml(url);
                            request.done(function (response) {
                                var template = _.template(response);
                                var contents = template(templateParam);
                                var html = $(contents).last().find('table.table')
                                $('#acquisition_table').html(html);
                                $('#acquisition_table').fadeIn();
                            }).fail(function () {
                                showErrorMessage('コンテンツ取得に失敗しました。[acquisitiontable]');
                            });
                        }

                    });

                }).fail(function() {
                    showErrorMessage('コンテンツ取得に失敗しました。[csvtable]');
                });
            }).fail(function (xhr, response, error) {
                console.log(xhr);
                console.log(response);
                console.log(error);
            });

        }).fail(function (xhr, response, error) {
            console.log(xhr);
            console.log(response);
            console.log(error);
        });

    });

    function bindDisabledButton(tableMapId) {
        $('#disabled').on('click', function(e){

            if(!window.confirm('フォーマット履歴を無効にします。よろしいですか？')) return false;

            var url = '/api/formats/disabled';
            var param = {
                tableMapId: tableMapId
            }
            var request = Common.post(url, param);
            request.done(function (response) {
                location.href = '/formats/' + getCurrentStartMonth();
            }).fail(function (xhr, response, error) {
                console.log(xhr);
                console.log(response);
                console.log(error);
            });
        });
    }

    function bindPrevButton () {
        $('#prev').on('click', function(e){
            $('#next').prop('disabled', false);
            var currentPage = localStorage.getItem('currentPage');
            if(currentPage == currentPageConst.acquisitions) {
                $('#csv_table').fadeIn();
                $('#acquisition_table').fadeOut();
                $('#modal_product_categories').css('visibility','visible');
                $('input[type="file"]').show();
                $('#page').text('1/6 page');
                setCaptionText(captionText.necessaryColumns);
                $('#prev').hide();
                localStorage.setItem('currentPage', currentPageConst.necessaryColumns);
            } else if(currentPage == currentPageConst.status) {
                $('#acquisition_table').fadeIn();
                $('#status_table').fadeOut();
                setCaptionText(captionText.acquisition);
                $('#page').text('2/6 page');
                localStorage.setItem('currentPage', currentPageConst.acquisitions);
            } else if(currentPage == currentPageConst.plan) {
                $('#status_table').fadeIn();
                $('#plan_table').fadeOut();
                $('#page').text('3/6 page');
                setCaptionText(captionText.status);
                localStorage.setItem('currentPage', currentPageConst.status);
            } else if(currentPage == currentPageConst.countItems) {
                $('#plan_table').fadeIn();
                $('#count_table').fadeOut();
                $('#page').text('4/6 page');
                setCaptionText(captionText.plan);
                localStorage.setItem('currentPage', currentPageConst.plan);
            } else {
                $('#count_table').fadeIn();
                $('#staff_table').fadeOut();
                $('#next').text('次へ');
                $('#page').text('5/6 page');
                setCaptionText(captionText.countItem);
                localStorage.setItem('currentPage', currentPageConst.countItems);
            }
            return false;
        });
    }

    function bindoOnChangeProductCategory() {
        $('#modal_product_categories').on('change', function () {
            checkExist();
        });
    }

    function bindYesOrNo() {
        $('input[name="yesOrNo"]').on('change', function(e) {
            var checked = $(this).prop('checked');
            if (!checked) {
                $(e.target).parent().parent().find('#compare').css('background-color', '#f5f5f5');
                $(e.target).parent().parent().find('input:hidden').prop('disabled', true);
                $(e.target).parent().parent().find('#compare').prop('disabled', true);
                $(e.target).parent().parent().find('select').prop('disabled', true);
            } else {
                $(e.target).parent().parent().find('#compare').css('background-color', '');
                $(e.target).parent().parent().find('input:hidden').prop('disabled', false);
                $(e.target).parent().parent().find('#compare').prop('disabled', false);
                $(e.target).parent().parent().find('select').prop('disabled', false);
            }
            console.log(checked);
        });
    }

    function bindOnClose() {
        $('#close').on('click', function(){
            $('#formatModal').modal('hide');
        });
    }

    function getCountItems(){
        var countItems = JSON.parse(localStorage.getItem('countItems'));
        if (_.isEmpty(countItems)) {
            console.log('countItems is undefined.');
        }
        return countItems;
    }

    function getDropDown(values, id) {
        var select = $('<select></select>', {
            'id': id,
            'name': id,
            'class': 'form-control'
        });

        _.each(values, function(data){
            var option = $('<option value="'+ Object.keys(data)[0] + '">' + data[Object.keys(data)] +  '</option>');
            select.append(option);
        })
        return select;
    }

    function getTbody(){
        return $('<tbody></tbody>');
    }

    function getRow(array){
        var tr = $('<tr></tr>');
        for(var i = 0; i < array.length; i++) {
            var td = $('<td></td>').append(array[i]);
            tr.append(td);
        }
        return tr;
    }

    function getConditions(){
        return [{indexOf:'が次の文字を含むとき'},{equal:'が次の文字と一致するとき'},
        {notIndexOf:'が次の文字を含めないとき'},{notEqual:'が次の文字と異なるとき'}];
    }

    function getInput(attr){
        var input = $('<input/>', {
            id: attr.id,
            name: attr.name,
            type: attr.type,
            'class': attr.class
        });
        return input;
    }

    function getButton(attr) {
        return $('<button></button>',{
            'id': attr.id,
            'type' : attr.type,
            'class': attr.class,
            'text': attr.text
        });
    }

    function getStaff() {
        return [{staffNo:'社員番号'},{name:'氏名'}];
    }

    function setCaptionText(captionText) {
        $('#captionText').text(captionText);
    }

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
            month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    function clickableRow() {
        $(document).on('click', '#data table.table tbody tr', function(){
            if(!window.confirm('フォーマット履歴を削除します。よろしいですか？')) return false;

            var id = $(this).find('td:first').text();

            var url = '/api/formats/' + id;
            var request = Common.delete(url);
            request.done(function (response) {
                if(response.result == 'OK'){
                    location.href = '/formats/' + getCurrentStartMonth();
                }else{
                    console.log(response.message);
                }
            }).fail(function (response) {
                console.log(response);
            });
        });
    }


    function checkExist() {

        $('#modal-message').empty();

        var param = {
            startMonth: getCurrentStartMonth(),
            productCategoryId: $('#modal_product_categories').val()
        };
        var url = '/api/formats/check_exist';
        var request = Common.getJson(url, param);
        request.done(function (response) {

            if (response.result == 'OK') {
                $('#next').prop('disabled', false);
            } else {
                $('#modal-message').text('既にデータが存在しています');
                $('#next').prop('disabled', true);
            }
        }).fail(function (response) {
            console.log(response);
        });
    }

    function showSuccessMessage(message) {
        var message = $('<div class="alert alert-success">' + message + '</div>');
        $('#modal-message').html(message);
    }

    function showErrorMessage(message) {
        var message = $('<div class="alert alert-danger">' + message + '</div>');
        $('#modal-message').html(message);
    }

    // Override
    Common.onChangeYear = function(){
        location.href = '/formats/' + getCurrentStartMonth();
    }

    // Override
    Common.onChangeMonth = function(month){
        location.href = '/formats/' + getCurrentStartMonth(month);
    }
});
