$(function(){

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
          month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    // Override
    Common.onChangeYear = function(){
        location.href = '/ranking_supports/' + getCurrentStartMonth();
    }

    // Override
    Common.onChangeMonth = function(month){
        location.href = '/ranking_supports/' + getCurrentStartMonth(month);
    }

});
