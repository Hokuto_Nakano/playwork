$(function(){

    // 前へボタン、次へボタン無効化制御
    ctrlDisabled();

    // 一覧表示更新
    $('#year, #month').on('change', function () {
        location.href = '/pays/' + getCurrentStartMonth();
    });

    $(document).on('click', 'table.table tbody tr', function(){
        var id = $(this).find('input[name="id"]').val();
        location.href = '/pay_details/' + id + '/' + getCurrentStartMonth();
    });

    // アップロード　ファイル選択時
    $(document).on('change', 'input[name="csv"]', function(e){
        e.preventDefault();
        var formData = new FormData();
        formData.append('month', getCurrentStartMonth());
        var csvFiles = this.files;
        $.each(csvFiles, function(i, csvFile){
            formData.append('csvFile', csvFile);
        });
        $.ajax({
            url: '/api/pays/import',
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function(response) {
            if (response.result == 'OK') {
                showSuccessMessage('データインポートを完了しました。');
                // 3秒後に自動遷移
                setInterval(function () {
                    location.href = '/pays/' + getCurrentStartMonth();
                }, 3000);
            } else {
                showErrorMessage('データのインポートに失敗しました。<br>' + response.message);
            }
        }).fail(function(xhr, response, error) {
            console.log(xhr)
            showErrorMessage('データインポートに失敗しました。');
        });
    });

    // 前へ
    $('#prev').on('click', function(){
        var offset = parseInt($('input[name="offset"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var page = parseInt($('input[name="page"]').val());
        var item = parseInt($('input[name="item"]').val());
        var count = parseInt($('input[name="count"]').val());
        page--;
        offset -= limit;

        if ((page*item) <= count ) {
            $('#next').prop('disabled', false);
        }
        location.href = '/pays/' + getCurrentStartMonth() + '?offset=' + offset + '&limit=' + limit;
    });

    // 次へ
    $('#next').on('click', function(){
        var offset = parseInt($('input[name="offset"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var page = parseInt($('input[name="page"]').val());
        page++;
        offset += limit;

        if (page != 1) {
            $('#prev').prop('disabled', false);
        }
        location.href = '/pays/' + getCurrentStartMonth() + '?offset=' + offset + '&limit=' + limit;
    });

    function ctrlDisabled() {
        var offset = parseInt($('input[name="offset"]').val());
        var page = parseInt($('input[name="page"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var count = parseInt($('input[name="count"]').val());
        if (page <= 1) {
            $('#prev').prop('disabled', true);
        }
        if ((page*limit) >= count || (offset+limit) == count) {
            $('#next').prop('disabled', true);
        }
    }

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
          month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    function showSuccessMessage(message) {
        var message = $('<div class="alert alert-success">' + message + '</div>');
        $('#message').html(message);
    }

    function showErrorMessage(message) {
        var message = $('<div class="alert alert-danger">' + message + '</div>');
        $('#message').html(message);
    }

    // Override
    Common.onChangeYear = function(){
        location.href = '/pays/' + getCurrentStartMonth();
    }

    // Override
    Common.onChangeMonth = function(month){
        location.href = '/pays/' + getCurrentStartMonth(month);
    }

});
