(function(global){
  "use strict;"

  function BaseCtrl(){};
  BaseCtrl.prototype.showDivisionModal = showDivisionModal;
  BaseCtrl.prototype.showAreaModal = showAreaModal;
  BaseCtrl.prototype.showSectionModal = showSectionModal;
  BaseCtrl.prototype.regist = registBase;
  BaseCtrl.prototype.delete = deleteBase;
  BaseCtrl.prototype.getModal_ = getModal_;

  function showDivisionModal(id){
    this.getModal_('/partial/divisions/' + id);
  }
  function showAreaModal(id){
    this.getModal_('/partial/areas/' + id);
  }
  function showSectionModal(id){
    this.getModal_('/partial/sections/' + id);
  }

  function registBase(url, data){
    var request = Common.post(url, data);
    request.success(function(response){
      if(response.result == 'OK'){
        location.href = '/bases';
      }
      else{
        document.getElementById('error').innerHTML = response.message;
      }
    });
    request.error(function(error){
      document.getElementById('error').innerHTML = '登録に失敗しました。';
    });
    request.done(function(response){
    });
  }

  function deleteBase(url){
      var jqXHR = Common.delete(url);
      jqXHR.done(function(response){
          if(response.result == 'OK'){
              location.href = '/bases';
          }else{
              document.getElementById('error').innerHTML = response.message;
          }
      }).fail(function(error){
          console.log(error);
      });
  };

  function getModal_(url){
    var request = Common.getHtml(url);
    request.success(function(response){
      $('#baseModal').html(response);
      $('#baseModal').modal();
    });
    request.error(function(error){
      console.log(JSON.stringify(error));
    });
    request.done(function(response){
    });
  }

  if ("process" in global) {
    module["exports"] = BaseCtrl;
  }
  global["BaseCtrl"] = BaseCtrl;

}((this || 0).self || global));
