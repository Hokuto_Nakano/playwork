$(function(){

    // 年月を入力した場合
    $('#year, #month').on('change', function() {
        location.href = '/attendances/' + getCurrentStartMonth();
    });

    // アップロード　ファイル選択時
    $(document).on('change','input[name="file"]', function(e){
        var target = e.target;
        var files = target.files;
        var reader = new FileReader();
        // ファイル読み込み時
        reader.onload = function(){
            var csvText = reader.result.split(/\r\n|\r|\n/);

            // メインデータ読み込み
            var csvHeader = csvText[0].split(',');
            var csvData = [];
            for(var i = 1; i < csvText.length; i++) {
                var data = csvText[i].split(',');
                csvData.push(data);
            }

            // CSVカラム、勤怠データ登録
            $.ajax({
                url: '/api/attendances/reg',
                type: 'post',
                data: {
                    csvHeader: csvHeader,
                    csvData: csvData,
                    startMonth: getCurrentStartMonth()
                },
                dataType: 'json',
                beforeSend: function () {
                    $.blockUI({
                        message: '処理中です。しばらくお待ちください。'
                    });
                }
            }).done(function(response) {
                if(response.result == 'OK'){
                    showSuccessMessage('勤怠データの登録/更新を完了しました。');

                    // 3秒後に自動遷移
                    setInterval(function () {
                        location.href = '/attendances/' + getCurrentStartMonth();
                    }, 3000);
                } else {
                    if(_.isArray(response.message)) {
                        var href = '/attendances/' + getCurrentStartMonth();
                        var errorMessage = '<a href="' + href + '">画面再読み込み</a><br>';
                        _.each(response.message, function(message) {
                            errorMessage += message + '<br>';
                        });
                        showSuccessMessage(errorMessage);
                    } else {
                        var href = '/attendances/' + getCurrentStartMonth();
                        var errorMessage = '<a href="' + href + '">画面再読み込み</a><br>';
                        showSuccessMessage(errorMessage + response.message);
                    }
                }
            }).fail(function(data, error) {
                console.log(data);
                console.log(error);
                showErrorMessage('勤怠データの登録/更新に失敗しました。');
            }).always(function () {
                $.unblockUI();
            });

        };
        reader.readAsText(files[0], 'Shift_JIS');
    });

    $('#updateBtn').on('click', function () {
        var attendances = [];

        $('#attendance-data table.table tbody tr').each(function() {
            var staffId = $(this).find('td:first').text();
            var attendanceDate = $(this).find('td').eq(2).text();
            var inputAttendanceDatetime = $(this).find('input[name="inputAttendanceDatetime"]').val();
            var inputLeaveDatetime = $(this).find('input[name="inputLeaveDatetime"]').val();
            var outputAttendanceDatetime = $(this).find('input[name="outputAttendanceDatetime"]').val();
            var outputLeaveDatetime = $(this).find('input[name="outputLeaveDatetime"]').val();
            var overTime = $(this).find('input[name="over_time"]').val();

            if (inputAttendanceDatetime === '00:00:00' && inputLeaveDatetime === '00:00:00' &&
                outputAttendanceDatetime === '00:00:00' && outputLeaveDatetime === '00:00:00') {
                return true;
            }

            var obj = {
                staffId: staffId,
                attendanceDate: attendanceDate,
                inputAttendanceDatetime: inputAttendanceDatetime,
                inputLeaveDatetime: inputLeaveDatetime,
                outputAttendanceDatetime: outputAttendanceDatetime,
                outputLeaveDatetime: outputLeaveDatetime,
                overTime: overTime
            }
            attendances.push(obj);
        });

        $.ajax({
            url: '/api/attendances/update',
            type: 'post',
            data: {
                attendances: attendances,
                startMonth: getCurrentStartMonth()
            },
            dataType: 'json',
            beforeSend: function () {
                $.blockUI({
                    message: '処理中です。しばらくお待ちください。'
                });
            }
        }).done(function(response) {
            if (response.result == 'OK') {
                showSuccessMessage('勤怠データを更新しました。');
                setTimeout(function () {
                    location.href = '/attendances/' + getCurrentStartMonth();
                }, 3000);
            } else {
                showErrorMessage('勤怠データの更新に失敗しました。');
            }
        }).fail(function (xhr, response, error) {
            console.log(xhr);
            console.log(response);
            console.log(error);
            showErrorMessage('勤怠データの更新に失敗しました。');
        }).always(function () {
            $.unblockUI();
        });
    });

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
            month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    function showSuccessMessage(message) {
        var message = $('<div class="alert alert-success">' + message + '</div>');
        $('#message').html(message);
    }

    function showErrorMessage(message) {
        var message = $('<div class="alert alert-danger">' + message + '</div>');
        $('#message').html(message);
    }

    // Override
    Common.onChangeYear = function(){
        location.href = '/attendances/' + getCurrentStartMonth();
    }

    // Override
    Common.onChangeMonth = function(month){
        location.href = '/attendances/' + getCurrentStartMonth(month);
    }

});
