$(function(){

    init();

    $('#insert').on('click', function(){
        var url = '/partial/companies/new';
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#companyModal').html(response);
            $('#companyModal').modal();
            $('#registBtn').on('click', function(){
                url = '/api/companies';
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/companies';
                    }else{
                        $('#error').html(response.message);
                    }
                }).fail(function(response){
                    $('#error').html(response);
                });
                return false;
            });
        });
    });

    $(document).on('click', '.update', function(){
        var id = $(this).parent().parent().find('td:first').text();

        var url = '/partial/companies/' + id;
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#companyModal').html(response);
            $('#companyModal').modal();
            $('#idArea').show();
            $('#registBtn').on('click', function(){
                url = '/api/companies';
                if(id > 0){
                    url = url + '/' + id;
                }
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/companies';
                    }else{
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        });
    });

    $(document).on('click', '.delete', function(){
        var id = $(this).parent().parent().find('td:first').text();
        var url = '/api/companies';
        if(id) {
            url += '/' + id;
        }
        var jqXHR = Common.delete(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                location.href = '/companies';
            }
        }).fail(function(response){
            console.log(response);
        });
        return false;
    });

    function init(){
        var url = '/api/companies';
        var jqXHR = Common.getJson(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                _.each(response.data, function (data) {
                    var id = data['Company']['id'];
                    var name = data['Company']['name'];
                    var html = getHtml(id, name);
                    $('tbody').append(html);
                });

            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });
    }

    function getHtml(id, name){
        var html = '<tr>' +
                    '<td>' + id +'</td>' +
                    '<td>' + name + '</td>' +
                    '<td>' +
                    '<button type="button" class="btn btn-primary update" data-toggle="modal">更新</button>' +
                    '<span>&nbsp;</span>' +
                    '<button type="submit" class="delete btn btn-primary">削除</button>' +
                    '</td>' +
                    '</tr>';
        return html;
    }
});
