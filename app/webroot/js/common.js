(function(global){
  "use strict;"

  var Common = function(){};
  Common.prototype.startLoading = startLoading;
  Common.prototype.endLoading = endLoading;
  Common.prototype.getJson = getJson;
  Common.prototype.getHtml = getHtml;
  Common.prototype.post = post;
  Common.prototype.delete = delete_;
  Common.prototype.getThisYear = getThisYear;
  Common.prototype.getThisMonth = getThisMonth;
  Common.prototype.onChangeYear = onChangeYear;
  Common.prototype.onChangeMonth = onChangeMonth;
  Common.prototype.getCurrentYYYYMM = getCurrentYYYYMM;

  function startLoading(){
    document.getElementById('loading').style.display = "block";
  }

  function endLoading(){
    document.getElementById('loading').style.display = "none";
  }

  function getJson(url, param) {
    var jqXHR = $.ajax({
      type: 'GET',
      url: url,
      dataType: 'json',
      data: param
    });
    return jqXHR;
  }

  function getHtml(url, param) {
    var jqXHR = $.ajax({
      type: 'GET',
      url: url,
      data: param
    });
    return jqXHR;
  }

  function post(url, param) {
    var jqXHR = $.ajax({
      type: 'POST',
      url: url,
      dataType: 'json',
      data: param
    });
    return jqXHR;
  }

  function delete_(url, param) {
    var jqXHR = $.ajax({
      type: 'DELETE',
      url: url,
      dataType: 'json',
      data: param
    });
    return jqXHR;
  }

  function getThisYear(){
    var d = new Date();
    return d.getFullYear();
  }

  function getThisMonth(){
    var d = new Date();
    return d.getMonth() + 1;
  }

  function onChangeYear(){
    throw new Error("not implemented");
  }

  function onChangeMonth(month){
    throw new Error("not implemented");
  }


  function getCurrentYYYYMM(){
    if(!(document.getElementById('year') && document.getElementById('month'))){
      return null;
    }

    var yyyy = document.getElementById('year').value;
    var mm = ('0' + document.getElementById('month').value).slice(-2);
    return yyyy + mm;
  }


  if ("process" in global) {
    module["exports"] = new Common();
  }
  global["Common"] = new Common();

}((this || 0).self || global));
