function login(){
  document.getElementById('error').innerHTML = '';

  var data = $('form').serialize();
  var request = Common.post('/api/session', data);
  request.success(function(response){
    console.log(response.result);
    if(response.result == 'OK'){
      location.href = '/';
    }
  });
  request.error(function(error){
    console.log(JSON.stringify(error));
    document.getElementById('error').innerHTML = 'ログインIDかパスワードが間違っています';
  });
  request.done(function(response){
    console.log(JSON.stringify(response));
  });
}