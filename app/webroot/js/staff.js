$(function(){

    // 前へボタン、次へボタン無効化制御
    ctrlDisabled();

    // 一覧表示更新
    $('#year, #month').on('change', function () {
        location.href = '/staffs/' + getCurrentStartMonth();
    });

    $('#insert').on('click', function(){
        var url = '/partial/staffs/new';
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#staffModal').html(response);
            $('#staffModal').modal();
            bindOnChange();
            var year = $('#year').val();
            var month = $('#month').val();

            // カレンダー
            $('#hire_int_date').datepicker({
                language: 'ja',
                format: 'yyyymmdd'
            });

            $('#registBtn').on('click', function(){
                // 入力値チェック
                if(!isValidParameter()) {
                    return false;
                }

                var url = '/api/staffs';
                var id = $('#staff_id').val();
                if(id > 0){
                    url = url + '/' + id;
                }
                $('#start_month').val(getCurrentStartMonth());
                var param = $('form').serializeArray();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/staffs/' + $('#start_month').val();
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        }).fail(function(error){
            console.log(error);
        });
    });

    $(document).on('click', '#registBtn', function(){
        var index = $('.reg').index(this);
        var id = $('#id' + index).text();

        var url = '/partial/staffs/' + id;
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#staffModal').html(response);
            $('#staffModal').modal();
            $('.modal-title').text('スタッフ履歴登録');
            // 名前は空にしておく
            $('#name').val('');
            $('#registBtn').on('click', function(){
                // 入力値チェック
                if(!isValidParameter()) {
                    return false;
                }
                url = '/api/staffs/reg';
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/staffs';
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        });
    });

    $(document).on('click', 'table.table tbody tr', function(){
        var id = $(this).find('.id').text();

        var url = '/partial/staffs/' + id;
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#staffModal').html(response);
            $('#staffModal').modal();

            bindOnChange();
            bindDisabledBtn();
            // カレンダー
            $('#hire_int_date').datepicker({
                language: 'ja',
                format: 'yyyymmdd'
            });
            // DBレコードの適用月
            var startMonth = $('#start_month').val();
            // タイトル及びボタン名
            if (getCurrentStartMonth() === startMonth) {
                $('.modal-title').text('スタッフ履歴更新');
                $('#registBtn').text('更新');
            } else {
                $('.modal-title').text('スタッフ履歴登録');
                $('#registBtn').text('登録');
            }
            var year = $('#year').val();
            var month = $('#month').val();
            $('span.lead').text(year + '年'+ month +'月');

            $('#registBtn').on('click', function(){
                // 入力値チェック
                if(!isValidParameter()) {
                    return false;
                }
                url = '/api/staffs';
                if(id > 0){
                    url = url + '/' + id;
                }

                var param = _.union($('form').serializeArray(), [{name: 'req_start_month', value: getCurrentStartMonth()}]);
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/staffs/' + getCurrentStartMonth();
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        });
    });

    $(document).on('click', '#deleteBtn', function(){
        if(!window.confirm('スタッフ履歴を削除します。よろしいですか？')) return false;

        var id = $('#history_id').val();
        var url = '/api/staffs';
        if(id) {
            url += '/' + id;
        }
        var jqXHR = Common.delete(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                location.href = '/staffs';
            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });
    });

    // 前へ
    $('#prev').on('click', function(){
        var offset = parseInt($('input[name="offset"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var page = parseInt($('input[name="page"]').val());
        var item = parseInt($('input[name="item"]').val());
        var count = parseInt($('input[name="count"]').val());
        page--;
        offset -= limit;

        if ((page*item) <= count ) {
            $('#next').prop('disabled', false);
        }
        location.href = '/staffs/' + getCurrentStartMonth() + '?offset=' + offset + '&limit=' + limit;
    });

    // 次へ
    $('#next').on('click', function(){
        var offset = parseInt($('input[name="offset"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var page = parseInt($('input[name="page"]').val());
        page++;
        offset += limit;

        if (page != 1) {
            $('#prev').prop('disabled', false);
        }
        location.href = '/staffs/' + getCurrentStartMonth() + '?offset=' + offset + '&limit=' + limit;
    });

    // アップロード　ファイル選択時
    $(document).on('change','input[name="file"]', function(e){
        var target = e.target;
        var files = target.files;
        var reader = new FileReader();
        //clearMessage();
        // ファイル読み込み時
        reader.onload = function(){
            var csvText = reader.result.split(/\r\n|\r|\n/);

            // メインデータ読み込み
            var csvHeader = csvText[0].split(',');
            var csvData = [];
            for(var i = 1; i < csvText.length; i++) {
                var data = csvText[i].split(',');
                csvData.push(data);
            }

            // CSVカラム、スタッフデータ
            var data = {
                csvHeader: csvHeader,
                csvData: csvData,
                startMonth: getCurrentStartMonth()
            };
            var url = '/api/staffs/import';

            var request = Common.post(url, data);
            request.done(function(response) {
                if (response.result == 'OK') {
                    showSuccessMessage('スタッフデータをインポートしました。');

                    setTimeout(function() {
                        location.href = '/staffs/' + getCurrentStartMonth();
                    }, 3000);
                } else {
                    showErrorMessage('スタッフデータのインポートに失敗しました。[' + response.message + ']');
                }
            }).fail(function(xhr,response, error) {
                showErrorMessage('スタッフデータのインポートに失敗しました。');
                console.log(xhr);
                console.log(response);
                console.log(error);
            });

        };
        reader.readAsText(files[0], 'Shift_JIS');
    });

    function bindDisabledBtn() {
        $('#disabledBtn').on('click', function (e) {

            if(!window.confirm('スタッフ履歴を無効にします。よろしいですか？')) return false;

            var url = '/api/staffs/disabled';
            var historyId = $('#history_id').val();
            var param = {
                historyId: historyId
            }
            var request = Common.post(url, param);
            request.done(function (response) {
                console.log(response);
            }).fail(function (xhr, response, error) {
                console.log(xhr);
                console.log(response);
                console.log(error);
            });
        });
    }

    function bindOnChange () {
        // 事業部を変更した際、エリアの候補を変更する
        $('#division_id').on('change', function () {
            var url = '/api/staffs/get_area';
            var param = {
                divisionId: $('#division_id').val()
            };
            var request = Common.getJson(url, param);
            request.done(function (response) {
                $('#area_id option').remove();

                _.each(response.data, function(data) {
                    var option = createOption();
                    option.text(data.Area.name).val(data.Area.id);
                    $('#area_id').append(option);
                });
                // エリアが存在しなければ部署も存在しない
                if ($('#area_id option').length == 0) {
                    $('#section_id option').remove();
                    // chromeだと候補がないのにラベルだけ残る
                    // focusすると消える
                    $('#area_id').focus();
                    $('#section_id').focus();
                    $('#division_id').focus();
                } else {
                    // 1つしかエリアがなければそのIDで部署の候補も表示する
                    areaOnChange();
                }
            }).fail(function (xhr, response, error) {
                console.log(xhr);
                console.log(response);
                console.log(error);
            })
        });
        // 事業部を変更した際、エリアの候補を変更する
        $('#area_id').on('change', function () {
            areaOnChange();
        });
    }

    function areaOnChange() {
        var url = '/api/staffs/get_section';
        var param = {
            areaId: $('#area_id').val()
        };
        var request = Common.getJson(url, param);
        request.done(function (response) {
            $('#section_id option').remove();
            _.each(response.data, function(data) {
                var option = createOption();
                option.text(data.Section.name).val(data.Section.id);
                $('#section_id').append(option);
            });
            // chromeだと候補がないのにラベルだけ残る
            // focusすると消える
            $('#section_id').focus();
            $('#area_id').focus();
        }).fail(function (xhr, response, error) {
            console.log(xhr);
            console.log(response);
            console.log(error);
        })
    }

    function createOption() {
        return $('<option></option>');
    }


    function ctrlDisabled() {
        var offset = parseInt($('input[name="offset"]').val());
        var page = parseInt($('input[name="page"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var count = parseInt($('input[name="count"]').val());
        if (page <= 1) {
            $('#prev').prop('disabled', true);
        }
        if ((page*limit) >= count || (offset+limit) == count) {
            $('#next').prop('disabled', true);
        }
    }

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
          month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    function isValidParameter() {

        if(($('#admin_flg').val() || !$('#update_flg').val()) && $('#password').val().length < 8){
          alert('パスワードは8文字以上です');
          return false;
        }

        if(($('#admin_flg').val() && $('#update_flg').val()) && $('#password').val() != $('#password_confirm').val()){
          alert('「パスワード確認」が正しくありません。再度入力してください。');
          return false;
        }

        if (checkEmpty($('#staff_no')) || checkEmpty($('#login_id')) ||
            checkEmpty($('#password')) || checkEmpty($('#outside_attendance_id')) ||
            checkEmpty($('#division_id')) || checkEmpty($('#area_id')) ||
            checkEmpty($('#section_id')) || checkEmpty($('#job_type_id')) ||
            checkEmpty($('#staff_type_id')) || checkEmpty($('#call_center_id')) ||
            checkEmpty($('#shift_type_id')) || checkEmpty($('#name'))) {
            $('#error').html('入社日以外の全ての項目を入力してください。');
            return false;
        }
        return true;

    }

    function checkEmpty(element) {

        var id = $(element).attr('id');
        if (!id) {
            var updateFlg = $('#update_flg').val();
            if (updateFlg) {
                return false;
            }
        }

        var value = $(element).val();
        if(!value) {
            return true;
        }
        return false;
    }

    function showSuccessMessage(message) {
        var message = $('<div class="alert alert-success">' + message + '</div>');
        $('#message').html(message);
    }

    function showErrorMessage(message) {
        var message = $('<div class="alert alert-danger">' + message + '</div>');
        $('#message').html(message);
    }

    // Override
    Common.onChangeYear = function(){
        location.href = '/staffs/' + getCurrentStartMonth();
    }

    // Override
    Common.onChangeMonth = function(month){
        location.href = '/staffs/' + getCurrentStartMonth(month);
    }

});
