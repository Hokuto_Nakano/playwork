(function(global){
    "use strict;"

    function IncentiveCtrl(){};
    IncentiveCtrl.prototype.showModal = showModal;
    IncentiveCtrl.prototype.regist = registIncentive;
    IncentiveCtrl.prototype.delete = deleteIncentive;
    IncentiveCtrl.prototype.selectTerm = selectTerm;

    function showRule(id){
        var yyyy = document.getElementById('year').value;
        var mm = ('0' + document.getElementById('month').value).slice(-2);
        location.href = '/incentive_rules/' + id + '/' + yyyy + mm;
    }

    function showModal(id){
        var request = Common.getHtml('/partial/incentives/' + id);
        request.success(function(response){
            $('#incentiveModal').html(response);
            $('#incentiveModal').modal();
        });
        request.error(function(error){
            console.log(JSON.stringify(error));
        });
        request.done(function(response){
        });
    }

    function registIncentive(){
        var url = '/api/incentives';
        var id = document.getElementById('incentive_id').value;
        if(id > 0){
            url = url + '/' + id
        }
        var data = _.union($('form').serializeArray(), [{
            name: 'start_month',
            value: $('#year').val() + ('0' + $('#month').val()).slice(-2)
        }]);
        var request = Common.post(url, data);
        request.success(function(response){
            if(response.result == 'OK'){
                var yyyy = document.getElementById('year').value;
                var month = ('0' + document.getElementById('month').value).slice(-2);
                var mm = ('0' + month).slice(-2);
                location.href = '/incentives/' + yyyy + mm;
            }
            else{
                document.getElementById('error').innerHTML = response.message;
            }
        });
        request.error(function(error){
            document.getElementById('error').innerHTML = '登録に失敗しました。';
        });
        request.done(function(response){
        });
    }

    function deleteIncentive(id){
        if(!id) return;
        if(!window.confirm('インセンティブルールを無効化します。この操作は元に戻せません。よろしいですか？')) return;
        var url = '/api/incentives/' + id;
        var request = Common.delete(url);
        request.done(function(response){
            if(response.result == 'OK'){
                location.href = '/incentives';
            }else{
                document.getElementById('error').innerHTML = response.message;
            }
        }).fail(function(error){
            console.log(error);
        });
    }

    function selectTerm(){
        var obj = document.getElementById('incentive_term');
        if(obj.options[obj.selectedIndex].value == 'rank'){
            document.getElementById('js-ranking_range').style.display = '';
        }else{
            document.getElementById('js-ranking_range').style.display = 'none';
        }
    }

    if ("process" in global) {
        module["exports"] = new IncentiveCtrl();
    }
    global["incentiveCtrl"] = new IncentiveCtrl();

    // Override
    Common.onChangeYear = function(){
        getList();
    }

    // Override
    Common.onChangeMonth = function(month){
        getList(month);
    }

    function getList(month){
        var yyyy = document.getElementById('year').value;
        if(!month){
            month = ('0' + document.getElementById('month').value).slice(-2);
        }
        var mm = ('0' + month).slice(-2)
        location.href = '/incentives/' + yyyy + mm;
    }

}((this || 0).self || global));
