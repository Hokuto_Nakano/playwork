$(function(){

  document.getElementById('new').onclick = function(){
    getModal_('/partial/count_items/new');
  };

  $(document).on('click', '.update', function(){
    getModal_('/partial/count_items/' + this.id);
  });

  function getModal_(url){
    var request = Common.getHtml(url);
    request.success(function(response){
      $('#countItemModal').html(response);
      $('#countItemModal').modal();
    });
    request.error(function(error){
      console.log(JSON.stringify(error));
    });
    request.done(function(response){
      document.getElementById('countitemForm').onsubmit = function(){
        regist();
        return false;
      };
      if(removeElm = document.getElementById('remove')){
        removeElm.onclick = function(){
          remove();
        };
      }
    });
  }

  function regist(){
    var id = document.getElementById('item_id').value;
    var url = '/api/count_items/' + id;
    var param = $('form').serialize();
    var request = Common.post(url, param);
    request.done(function(response){
      if(response.result == 'OK'){
        location.href = '/count_items';
      }else{
        console.log(response.message);
        $('#error').html(response.message);
      }
    }).fail(function(error){
      console.log(error);
      $('#error').html(error);
    });
  }

  function remove(){
    var id = document.getElementById('item_id').value;
    var url = '/api/count_items/' + id;
    var request = Common.delete(url);
    request.done(function(response){
        if(response.result == 'OK'){
            location.href = '/count_items';
        }else{
            console.log(response.message);
        }
    }).fail(function(error){
        console.log(error);
    });
  }

});
