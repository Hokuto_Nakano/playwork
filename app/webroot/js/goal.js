$(function(){

   $('button').on('click', function (e) {
       var divisionId = $('input[name="divisionId[]"]').serializeArray()
       var areaId = $('input[name="areaId[]"]').serializeArray()
       var sectionId = $('input[name="sectionId[]"]').serializeArray();
       var goalCount = $('input[name="goalCount[]"]').serializeArray();
       var goalBp = $('input[name="goalBp[]"]').serializeArray();
       var param = {
           divisionId: divisionId,
           areaId: areaId,
           sectionId: sectionId,
           goalCount: goalCount,
           goalBp: goalBp,
           month: getCurrentStartMonth()
       }

       var url = '/api/goals';
       var request = Common.post(url, param);
       request.done(function(response) {
           if(response.result == 'OK'){
               showSuccessMessage('目標設定を完了しました。');

               // 3秒後に自動遷移
               setInterval(function () {
                   location.href = '/goals/' + getCurrentStartMonth();
               }, 3000);
           }
       }).fail(function(data, error) {
           console.log(data);
           console.log(error);
           showErrorMessage('目標設定に失敗しました。');
       });

   });

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
            month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    function showSuccessMessage(message) {
        var message = $('<div class="alert alert-success">' + message + '</div>');
        $('#message').html(message);
    }

    function showErrorMessage(message) {
        var message = $('<div class="alert alert-danger">' + message + '</div>');
        $('#message').html(message);
    }

    // Override
    Common.onChangeYear = function(){
        location.href = '/goals/' + getCurrentStartMonth();
    }

    // Override
    Common.onChangeMonth = function(month){
        location.href = '/goals/' + getCurrentStartMonth(month);
    }
});
