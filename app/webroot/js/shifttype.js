$(function(){

    init();

    $('#insert').on('click', function(){
        var url = '/partial/shift_types/new';
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#shifttypeModal').html(response);
            $('#shifttypeModal').modal();
            $('#registBtn').on('click', function(){
                var url = '/api/shift_types';
                var id = $('#shifttype_id').val();
                if(id > 0){
                    url = url + '/' + id;
                }
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/shift_types';
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        }).fail(function(error){
            console.log(error);
        });
    });

    $(document).on('click', '.update', function(){
        var index = $('.update').index(this);
        var id = $('#id' + index).text();

        var url = '/partial/shift_types/' + id;
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#shifttypeModal').html(response);
            $('#shifttypeModal').modal();
            $('#form-first').show();
            $('#registBtn').on('click', function(){
                url = '/api/shift_types';
                if(id > 0){
                    url = url + '/' + id;
                }
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/shift_types';
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        });
    });

    $(document).on('click', '.delete', function(){
        var index = $('.delete').index(this);
        var id = $('#id' + index).text();
        var url = '/api/shift_types';
        if(id) {
            url += '/' + id;
        }
        var jqXHR = Common.delete(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                location.href = '/shift_types';
            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });
        return false;
    });

    $(document).on('click','#form-third .dropdown .dropdown-menu li a', function(){
        var label = $(this).text();
        $('#is_break_label').text(label);
        if(label == '休憩なし') {
            $('#is_break').val(0);
        } else {
            $('#is_break').val(1);
        }
    });

    function init(){
        var url = '/api/shift_types';
        var jqXHR = Common.getJson(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                console.log(response);
                for(var i = 0; i < response.data.length; i++){
                    var id = response.data[i].ShiftType.id;
                    var type = response.data[i].ShiftType.type;
                    var is_break = response.data[i].ShiftType.is_break;
                    var is_break_label;
                    if(is_break == true) {
                        is_break_label = '休憩あり';
                    } else {
                        is_break_label = '休憩なし';
                    }
                    var html = getHtml(i, id, type, is_break_label);
                    $('tbody').append(html);
                }

            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });
    }

    function getHtml(row, id, type, is_break){
        var html = '<tr>' +
                    '<td>' +
                        '<span id="id' + row + '">' + id +'</span>'  +
                    '</td>' +
                    '<td>' +
                        '<span id="type' + row +'">' + type +'</span>' +
                    '</td>' +
                    '<td>' +
                        '<span id="is_break' + row +'">' + is_break +'</span>' +
                    '</td>' +
                    '<td>' +
                        '<button type="button" class="btn btn-primary update" data-toggle="modal">更新</button>' +
                        '<span>&nbsp;</span>' +
                        '<button type="submit" class="delete btn btn-primary">削除</button>' +
                    '</td>' +
                    '</tr>';
        return html;
    }
});
