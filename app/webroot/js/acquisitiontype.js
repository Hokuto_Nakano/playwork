(function(global){
  "use strict;"

  function AcquisitionTypeCtrl(categories){
    this.categories = categories;
  }
  AcquisitionTypeCtrl.prototype.showModal = showModal;
  AcquisitionTypeCtrl.prototype.getList = getList;
  AcquisitionTypeCtrl.prototype.selectCategory = selectCategory;
  AcquisitionTypeCtrl.prototype.regist = registAcquisitionType;
  AcquisitionTypeCtrl.prototype.delete = deleteAcquisitionType;
  AcquisitionTypeCtrl.prototype.getModal_ = getModal_;

  function showModal(id){
    id = !isFinite(id) ? 'new' : id;
    var yyyy = document.getElementById('year').value;
    var mm = ('0' + document.getElementById('month').value).slice(-2);
    this.getModal_('/partial/acquisition_types/' + id + '/' + yyyy + mm);
  }

  function getModal_(url){
    var request = Common.getHtml(url);
    request.success(function(response){
      $('#acquisitionTypeModal').html(response);
      $('#acquisitionTypeModal').modal();
    });
    request.error(function(error){
      console.log(JSON.stringify(error));
    });
    request.done(function(response){
    });
  }

  function getList(month){
    var yyyy = document.getElementById('year').value;
    if(!month){
      month = ('0' + document.getElementById('month').value).slice(-2);
    }
    var mm = ('0' + month).slice(-2)
    location.href = '/acquisition_types/' + yyyy + mm;
  }

  function selectCategory(categoryId){
    var i = 0;
    while(i<this.categories.length){
      if(categoryId == this.categories[i].ProductCategory.id){
        var category = this.categories[i].ProductCategory;
        document.getElementById('category_id').value = category.id;
        document.getElementById('category_name').innerHTML = category.name;
        break;
      }
      i = i + 1;
    }
  }

  function registAcquisitionType(){
    var url = '/api/acquisition_types';
    var id = document.getElementById('acquisition_type_id').value;
    if(id > 0){
      url = url + '/' + id
    }
    var data = $('form').serialize();
    var request = Common.post(url, data);
    request.success(function(response){
      if(response.result == 'OK'){
        var yyyy = document.getElementById('year').value;
        var mm = ('0' + document.getElementById('month').value).slice(-2);
        location.href = '/acquisition_types/' + yyyy + mm;
      }
      else{
        document.getElementById('error').innerHTML = response.message;
      }
    });
    request.error(function(error){
      console.log(JSON.stringify(error));
      document.getElementById('error').innerHTML = '登録に失敗しました。';
    });
    request.done(function(response){
    });
  }

  function deleteAcquisitionType(id){
    if(!id) return;
    if(!window.confirm('獲得区分を削除します。元に戻せませんがよろしいですか？')) return;
    var url = '/api/acquisition_types/' + id;
    var jqXHR = Common.delete(url);
    jqXHR.done(function(response){
      if(response.result == 'OK'){
        location.href = '/acquisition_types';
      }else{
        document.getElementById('error').innerHTML = response.message;
      }
    }).fail(function(error){
      console.log(error);
    });
  }

  if ("process" in global) {
    module["exports"] = AcquisitionTypeCtrl;
  }
  global["AcquisitionTypeCtrl"] = AcquisitionTypeCtrl;

  // Override
  Common.onChangeYear = function(){
    getList();
  }

  // Override
  Common.onChangeMonth = function(month){
    getList(month);
  }

}((this || 0).self || global));
