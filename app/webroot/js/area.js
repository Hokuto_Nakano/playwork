(function(global){
  'use strict;'

  function AreaCtrl(){}
  AreaCtrl.prototype.regist = registArea;
  AreaCtrl.prototype.delete = deleteArea;

  function registArea(){
    var url = '/api/areas';
    var id = document.getElementById('area_id').value;
    if(id > 0){
      url = url + '/' + id
    }
    var data = $('form').serialize();
    baseCtrl.regist(url, data);
  }

  function deleteArea(id){
      if(!id) return;
      if(!window.confirm('エリアを削除します。よろしいですか？')) return;
      var url = '/api/areas/' + id;
      baseCtrl.delete(url);
      return false;
  };

  if ('process' in global) {
    module['exports'] = AreaCtrl;
  }
  global['AreaCtrl'] = AreaCtrl;

}((this || 0).self || global));
