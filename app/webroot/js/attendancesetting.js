$(function(){

    $('#settingBtn').on('click', function(){

        // 存在チェックして存在していれば更新
        var url = '/api/attendances_settings';
        var request = Common.getJson(url);
        request.done(function(data){

            if(data.response) {
                var id = data.response['AttendanceCalc'].id;
                if (id > 0) {
                    // 更新
                    url = '/partial/attendances_settings/' + id;
                }
            } else {
                // 新規登録
                url = '/partial/attendances_settings/new';
            }

            // 新規登録か更新
            request = Common.getHtml(url);
            request.done(function(response) {
                $('#attendanceModal').html(response);
                $('#attendanceModal').modal();
                clearMessage();
                $('#registBtn').on('click', function(){
                    var param = $('form').serialize();
                    var id = $('input[name="id"]').val();
                    url = '/api/attendances_settings';
                    if(id > 0) {
                        url = '/api/attendances_settings/' + id;
                    }
                    var req = Common.post(url, param);
                    req.done(function(response){
                        if(response.result == 'OK'){
                            showSuccessMessage('勤怠設定を完了しました。');
                            $('#attendanceModal').modal('hide');
                        }else{
                            console.log(response.message);
                            showErrorMessage('勤怠設定に失敗しました。');
                        }
                        // 3秒後に自動遷移
                        setInterval(function () {
                            location.href = '/attendances_settings';
                        }, 3000);
                    }).fail(function(error){
                        console.log(error);
                        showErrorMessage('勤怠設定に失敗しました。');
                    });
                    return false;
                });
            }).fail(function(error){
                console.log(error);
            });

        }).fail(function (error) {
            console.log(error);
        });
    });

    // アップロード　ファイル選択時
    $(document).on('change','input[name="file"]', function(e){
        var target = e.target;
        var files = target.files;
        var reader = new FileReader();
        clearMessage();
        // ファイル読み込み時
        reader.onload = function(){
            // 0:だこっくんID, 1:氏名, 2:年月日, 3:出勤時刻, 4:退勤時刻, 5:出勤時刻（計算後）,
            // 6:退勤時刻（計算後）, 7:差異, 8:曜日, 9:休憩時間, 10:実稼働時間
            // 11:シフト区分, 12:アポラン, 13:区分, 14:部署, 15:エリア
            var csvText = reader.result.split(/\r\n|\r|\n/);
            var header = csvText[0].split(',');

            var csvColumns = [];
            for(var i = 0; i < header.length; i++) {
                var obj = _.object(['name'], [header[i]])
                csvColumns.push(_.extend(obj, {key: 'column_' + i}));
            }
            // テンプレートで使用するデータ
            var headers =  {
                csvColumns: csvColumns,
                csvMaps: getCsvMaps()
            };
            localStorage.setItem('headers', JSON.stringify(headers));
            var url = '/attendances_settings/contents';

            var request = Common.getHtml(url);
            request.done(function(response) {
                var template = _.template(response);
                var contents = template(headers);
                var html = $(contents).last().find('#mapping_contents');
                $('#contents').html(html);
            }).fail(function() {
                showErrorMessage('コンテンツ取得に失敗しました。');
            });

        };
        reader.readAsText(files[0], 'Shift_JIS');
    });

    // 登録ボタン押下時
    $(document).on('click', '#regBtn', function(){

        var data = {
            input: $('#mapping_contents select').serializeArray(),
            headers: JSON.parse(localStorage.getItem('headers'))
        }
        // 外部システムユーザーIDと氏名が選択されていない場合
        var isUserId = false;
        var isName = false;
        _.each(data.input, function(data){
            var value = data['value'];
            if (_.isEqual(value, 'other_id_column')) {
                isUserId = true;
            }
            if (_.isEqual(value, 'name_column')) {
                isName = true;
            }
        });
        if (!isUserId || !isName) {
            showErrorMessage('外部ユーザーIDと氏名は選択必須です。');
            return;
        }

        var url = '/api/attendances_settings/headers';
        var param = {
            data: data
        };
        var request = Common.post(url, param);
        request.done(function(response) {
            if (response.result == 'OK') {
                if (response.data.status == 'update') {
                    showSuccessMessage('勤怠フォーマットの更新を完了しました。');
                } else {
                    showSuccessMessage('勤怠フォーマットの登録を完了しました。');
                }
                localStorage.clear();
                // 3秒後に自動遷移
                setInterval(function () {
                    location.href = '/attendances_settings';
                }, 3000);
            }
        }).fail(function(xhr, response, error) {
            showErrorMessage('勤怠フォーマットの登録/更新に失敗しました。');
        });

        return false;
    });

    function getCsvMaps() {
        var labels = ['', '外部勤怠システムID','氏名', '日付', '出勤日', '退勤日', '出勤時刻', '退勤時刻', '出勤日時', '退勤日時', '実働時間', '休憩時間', '休憩入り時刻', '休憩戻り時刻'];
        var values = ['none', 'other_id_column','name_column','date_column', 'attendance_date_column', 'leave_date_column', 'attendance_time_column', 'leave_time_column', 'attendance_datetime_column', 'leave_datetime_column', 'work_hours_column', 'break_hours_column', 'break_in_time_column', 'break_out_time_column'];
        var csvMaps = [];
        for(var i = 0; i < labels.length; i++) {
            csvMaps.push(_.object([values[i]], [labels[i]]));
        }
        return csvMaps;
    }

    function showSuccessMessage(message) {
        var message = $('<div class="alert alert-success">' + message + '</div>');
        $('#message').html(message);
    }

    function showErrorMessage(message) {
        var message = $('<div class="alert alert-danger">' + message + '</div>');
        $('#message').html(message);
    }

    function clearMessage(message) {
        $('#message').empty();
    }

});
