(function(global){
  'use strict;'

  function SectionCtrl(divisions){
    this.divisions = divisions;
  }

  SectionCtrl.prototype.regist = registSection;
  SectionCtrl.prototype.delete = deleteSection;

  SectionCtrl.prototype.selectDivision = selectDivision;
  SectionCtrl.prototype.selectArea = selectArea;
  SectionCtrl.prototype.setOldDivisionName = setOldDivisionName;
  SectionCtrl.prototype.onChangeDivisionName = onChangeDivisionName;
  SectionCtrl.prototype.setOldAreaName = setOldAreaName;
  SectionCtrl.prototype.onChangeAreaName = onChangeAreaName;

  SectionCtrl.prototype.setAreas_ = setAreas_;
  SectionCtrl.prototype.setDivisionId_ = setDivisionId_;
  SectionCtrl.prototype.setDivisionName_ = setDivisionName_;
  SectionCtrl.prototype.setDivisionShortName_ = setDivisionShortName_;
  SectionCtrl.prototype.setAreaId_ = setAreaId_;
  SectionCtrl.prototype.setAreaName_ = setAreaName_;
  SectionCtrl.prototype.setAreaShortName_ = setAreaShortName_;

  function registSection(){
    var url = '/api/sections';
    var id = document.getElementById('section_id').value;
    if(id > 0){
      url = url + '/' + id
    }
    var data = $('form').serialize();
    baseCtrl.regist(url, data);
  }

  function deleteSection(id){
      if(!id) return;
      if(!window.confirm('部署を削除します。よろしいですか？')) return;
      var url = '/api/sections/' + id;
      baseCtrl.delete(url);
      return false;
  };

  function selectDivision(divisionId){
    var i = 0;
    while(i<this.divisions.length){
      if(divisionId == this.divisions[i].Division.id){
        var division = this.divisions[i].Division;
        this.setDivisionId_(division.id);
        this.setDivisionName_(division.name);
        this.setDivisionShortName_(division.short_name);
        break;
      }
      i = i + 1;
    }

    this.setAreas_(divisionId);
  }

  function selectArea(areaId, areaName, areaShortName){
    this.setAreaId_(areaId);
    this.setAreaName_(areaName);
    this.setAreaShortName_(areaShortName);
  }

  function setOldDivisionName(divisionName){
    this.divisionName = divisionName;
  }

  function onChangeDivisionName(name){
    if(this.divisionName == name){ return; }

    document.getElementById('division_short_name').removeAttribute('disabled');
    document.getElementById('area_short_name').removeAttribute('disabled');

    var divisionId = 0;
    var i = 0;
    while(i<this.divisions.length){
      if(name == this.divisions[i].Division.name){
        divisionId = this.divisions[i].Division.id;
        this.setDivisionShortName_(this.divisions[i].Division.short_name);
        break;
      }
      i = i + 1;
    }

    this.setDivisionId_(divisionId);
    this.setAreas_(divisionId);
  }

  function setOldAreaName(areaName){
    this.areaName = areaName;
  }

  function onChangeAreaName(name){
    if(this.areaName == name){ return; }

    document.getElementById('area_short_name').removeAttribute('disabled');

    var areaId = 0;
    var i = 0;
    while(i<this.areas.length){
      if(name == this.areas[i].name){
        familyId = this.areas[i].id;
        this.setAreaShortName_(this.areas[i].short_name);
        break;
      }
      i = i + 1;
    }

    this.setAreaId_(areaId);
  }

  function setAreas_(divisionId){
    var selectAreas = document.getElementById('selectAreas');
    if(!selectAreas) return;

    if(divisionId == 0){
      this.setAreaId_(0);
      selectAreas.innerHTML = '';
      return;
    }

    this.areas = [];
    var i = 0;
    while(i<this.divisions.length){
      if(divisionId == this.divisions[i].Division.id){
        this.areas = this.divisions[i].Area;
        break;
      }
      i = i + 1;
    }

    this.setAreaId_(0);
    this.setAreaName_('');
    this.setAreaShortName_('');
    selectAreas.innerHTML = '';

    for(var i=0; i<this.areas.length; i++){
      if(i == 0){
        this.selectArea(this.areas[i].id, this.areas[i].name, this.areas[i].short_name)
      }
      selectAreas.innerHTML += "<li><a href=\"javascript:void(0)\" onclick=\"sectionCtrl.selectArea('" + this.areas[i].id + "', '" + this.areas[i].name + "', '" + this.areas[i].short_name + "');\" >" + this.areas[i].name + "</a></li>";
    }
  }

  function setDivisionId_(divisionId){
    document.getElementById('division_id').value = divisionId;
  }

  function setDivisionName_(divisionName){
    var element = document.getElementById('division_name');
    if(element.getAttribute('value') != null){
      element.value = divisionName;
    }else{
      element.innerHTML = divisionName;
    }
  }

  function setDivisionShortName_(divisionShortName){
    document.getElementById('division_short_name').value = divisionShortName;
    document.getElementById('division_short_name').setAttribute('disabled', 'true');
  }

  function setAreaId_(areaId){
    document.getElementById('area_id').value = areaId;
  }

  function setAreaName_(areaName){
    var element = document.getElementById('area_name');
    if(element.getAttribute('value') != null){
      element.value = areaName;
    }else{
      element.innerHTML = areaName;
    }
  }

  function setAreaShortName_(areaShortName){
    document.getElementById('area_short_name').value = areaShortName;
    if(document.getElementById('area_id').value > 0){
      document.getElementById('area_short_name').setAttribute('disabled', 'true');
    }
    else{
      document.getElementById('area_short_name').removeAttribute('disabled');
    }
  }

  if ('process' in global) {
    module['exports'] = SectionCtrl;
  }
  global['SectionCtrl'] = SectionCtrl;

}((this || 0).self || global));