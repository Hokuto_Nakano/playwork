$(function () {
    function getYYYYMM (month) {
        var yyyy = document.getElementById('year').value;
        if (!month) {
            month = ('0' + document.getElementById('month').value).slice(-2);
        }
        var mm = ('0' + month).slice(-2);
        return yyyy + mm;
    }

    function getList (month) {
        var divisionId = document.getElementById('divisionId').value;
        var areaId = document.getElementById('areaId').value;
        var sectionId = document.getElementById('sectionId').value;
        location.href = '/outputs/' + getYYYYMM(month) + '/' + divisionId + '/' + areaId + '/' + sectionId +'/';
    }

    $('#divisionId').on('change', function () {
        location.href = '/outputs/' + getYYYYMM() + '/' + this.value + '/';
    });

    $('#areaId').on('change', function () {
        var divisionId = document.getElementById('divisionId').value;
        location.href = '/outputs/' + getYYYYMM() + '/' + divisionId + '/' + this.value + '/';
    });

    $('#sectionId').on('change', function () {
        var divisionId = document.getElementById('divisionId').value;
        var areaId = document.getElementById('areaId').value;
        location.href = '/outputs/' + getYYYYMM() + '/' + divisionId + '/' + areaId + '/' + this.value +'/';
    });
    $(document).on('click', '.download', function () {
        var divisionId = document.getElementById('divisionId').value;

        if (this.id == 'profit_losses') {
            var areaId = document.getElementById('areaId').value;
            var sectionId = document.getElementById('sectionId').value;
            location.href = '/' + this.id + '/excel/' + getYYYYMM() + '/?division_id=' + divisionId + '&area_id=' + areaId + '&section_id=' + sectionId;
            return;
        }

        location.href = '/' + this.id + '/excel/' + getYYYYMM() + '/' + divisionId;
    });
    // Override
    Common.onChangeYear = function () {
        getList();
    }
    // Override
    Common.onChangeMonth = function (month) {
        getList(month);
    }
});
