(function(global){
  'use strict;'

  function ProductCategoryCtrl(){}
  ProductCategoryCtrl.prototype.regist = registProductCategory;
  ProductCategoryCtrl.prototype.delete = deleteProductCategory;

  function registProductCategory(){
    var url = '/api/product_categories';
    var id = document.getElementById('product_category_id').value;
    if(id > 0){
      url = url + '/' + id
    }
    var data = $('form').serialize();
    productCtrl.regist(url, data);
  }

  function deleteProductCategory(id){
      if(!id) return;
      if(!window.confirm('商品カテゴリを削除します。よろしいですか？')) return;
      var url = '/api/product_categories/' + id;
      productCtrl.delete(url);
      return false;
  };

  if ('process' in global) {
    module['exports'] = ProductCategoryCtrl;
  }
  global['ProductCategoryCtrl'] = ProductCategoryCtrl;

}((this || 0).self || global));
