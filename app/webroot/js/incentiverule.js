(function(global){
    "use strict;"

    function IncentiveRuleCtrl(){};
    IncentiveRuleCtrl.prototype.update = updateIncentiveRule;
    IncentiveRuleCtrl.prototype.delete = deleteIncentive;
    IncentiveRuleCtrl.prototype.selectTerm = selectTerm;
    IncentiveRuleCtrl.prototype.addRow = addRow;
    IncentiveRuleCtrl.prototype.deleteRow = deleteRow;
    IncentiveRuleCtrl.prototype.reflect = reflectRank;
    IncentiveRuleCtrl.prototype.changeDivision = changeDivision;
    IncentiveRuleCtrl.prototype.changeArea = changeArea;
    IncentiveRuleCtrl.prototype.changeSection = changeSection;

    function updateIncentiveRule(){

        if (!checkTargets()){
            alert('社員区分または職種区分が重複して選択されています');
            return;
        }

        if (!checkRanks()) {
            alert('インセンティブ定義に反映してからインセンティブルールを登録してください');
            return;
        }

        var url = '/api/incentive_rules';
        var id = document.getElementById('incentive_id').value;
        var yyyy = document.getElementById('year').value;
        var mm = document.getElementById('month').value;
        if(id > 0){
            url = url + '/' + id + '/' + yyyy + mm;
        }
        var data = $('form').serialize();
        var request = Common.post(url, data);
        request.success(function(response){
            if(response.result == 'OK'){

                var divisionId = response.data.divisionId;
                var areaId = response.data.areaId;
                var sectionId = response.data.sectionId;

                location.href = '/incentive_rules/' + id + '/' + yyyy + mm + '/' + divisionId + '/'+ areaId + '/' + sectionId;
            }
            else{
                document.getElementById('error').innerHTML = response.message;
            }
        });
        request.error(function(error){
            document.getElementById('error').innerHTML = '更新に失敗しました。';
        });
        request.done(function(response){
        });
    }

    function deleteIncentive(id){
        if(!id) return;
        if(!window.confirm('インセンティブルールを無効化します。この操作は元に戻せません。よろしいですか？')) return;
        var url = '/api/incentive_count_rules/' + id;
        var request = Common.delete(url);
        request.done(function(response){
            if(response.result == 'OK'){
                location.href = '/incentive_rules';
            }else{
                document.getElementById('error').innerHTML = response.message;
            }
        }).fail(function(error){
            console.log(error);
        });
    }

    function selectTerm(){
        var obj = document.getElementById('incentive_term');
        if(obj.options[obj.selectedIndex].value == 'rank'){
            document.getElementById('js-ranking_range').style.display = '';
        }else{
            document.getElementById('js-ranking_range').style.display = 'none';
        }
    }

    function addRow(target){
        var table = document.getElementById(target + '_rules');
        var lastRule = table.children[table.children.length-1];
        var newRule = lastRule.cloneNode(true);
        table.appendChild(newRule);
        sortRules(target);
    }

    function deleteRow(target, id){
        var table = document.getElementById(target + '_rules');
        var children = table.children;
        var length = children.length;
        if(length == 1) return;

        for(var i = 0; i < length; i++){
            if(id == children[i].id){
                table.removeChild(children[i]);
                break;
            }
        }

        sortRules(target);
    }

    function sortRules(target){
        var table = document.getElementById(target + '_rules');
        for(var i = 0, children = table.children, length = children.length; i < children.length; i++){
            children[i].id = target + '_' + i;
        }
    }

    if ("process" in global) {
        module["exports"] = new IncentiveRuleCtrl();
    }
    global["incentiveRuleCtrl"] = new IncentiveRuleCtrl();

    // Override
    Common.onChangeYear = function(){
        getList();
    }

    // Override
    Common.onChangeMonth = function(month){
        getList(month);
    }

    function getList(month){
        var id = document.getElementById('incentive_id').value;
        location.href = '/incentive_rules/' + id + '/' + getYYMM(month);
    }

    function reflectRank() {

        // ランキングの場合
        var rank = 0;

        var targetTypes = $('tbody#pay_rules tr td select[name="target[]"]');
        if (targetTypes.length > 0) {
            $('tr#count_ranks').html('<th></th>');
        } else {
            $('tr#count_ranks').html('');
        }

        // 件数の場合
        if ($('tbody#count_rules').length > 0) {
            var ranks = [];
            var targetTypes = $('tbody#pay_rules tr td select[name="target[]"]');
            if (targetTypes.length > 0) {
                $('tr#count_ranks').html('<th></th>');
            } else {
                $('tr#count_ranks').html('');
            }
            $('tbody#count_rules tr').each(function(i, data) {
                var rank = $(data).find('input[name="rank[]"]').val();
                $('tr#count_ranks').append('<th>' + rank + '</th>');
                ranks.push(rank);
            });

            rank = ranks.length;
        } else {

            // ランキングの場合
            rank = $('input[name="ranking"]').val();

            // hidden項目
            $('div#hiddenInput input').remove();

            for (var i = 0; i < rank; i++) {
                $('tr#count_ranks').append('<th>' + (i+1) + '位</th>');
                $('div#hiddenInput').append('<input id="rank[]" name="rank[]" type="hidden" value="' + (i+1) + '位">');
            }
        }

        // インセンティブ定義のインセンティブ額のところ
        $('tbody#pay_rules tr').each(function(i, row) {

            // ランクの行数が増えた場合
            var inputLength = $(row).find('input').length;
            while (inputLength < rank) {
                var tdClone = $('input[name="pays[]"]:first').parent('td').clone();
                tdClone.find('input').val(0);
                tdClone.insertBefore($(row).find('td:last'));
                inputLength++;
            }

            // ランクの行数が減った場合
            $(row).find('input').each(function(j, input) {
                if (j > rank-1) {
                    $(input).parent('td').remove();
                    return;
                }
            });
        });
    }

    function changeDivision() {
        var incentiveId = $('input[name="incentive_id"]').val();
        var divisionId = $('#divisionId').val();
        location.href = '/incentive_rules/' + incentiveId + '/' + getYYMM() + '/' + divisionId;
    };

    function changeArea() {
        var incentiveId = $('input[name="incentive_id"]').val();
        var divisionId = $('#divisionId').val();
        var areaId = $('#areaId').val();
        location.href = '/incentive_rules/' + incentiveId + '/' + getYYMM() + '/' + divisionId + '/' + areaId;
    }

    function changeSection() {
        var incentiveId = $('input[name="incentive_id"]').val();
        var divisionId = $('#divisionId').val();
        var areaId = $('#areaId').val();
        var sectionId = $('#sectionId').val();
        location.href = '/incentive_rules/' + incentiveId + '/' + getYYMM() + '/' + divisionId + '/' + areaId + '/' + sectionId;
    }

    function checkTargets() {
        var targets = $('form select[name="target[]"').serializeArray();

        if (targets.length < 1) return true;

        var group = _.groupBy(targets, function (data) {
            return data.value;
        });
        var duplicated = _.pick(group, function (data) {
            return data.length > 1;
        });
        if (!_.isEmpty(duplicated)) {
            return false;
        }
        return true;
    }

    function checkRanks () {

        var topRank = $('input[name="ranking"]').val();

        if ($('tbody#count_rules tr').length > 0) {
            // 件数の場合
            topRank = $('tbody#count_rules tr').length;
        }

        var bottomRank = $('tr#count_ranks th:not(:empty)').length;
        return topRank == bottomRank ? true : false;
    }

    function getYYMM(month) {
        var yyyy = document.getElementById('year').value;
        if(!month){
            month = ('0' + document.getElementById('month').value).slice(-2);
        }
        var mm = ('0' + month).slice(-2);
        return yyyy + mm;
    }

}((this || 0).self || global));
