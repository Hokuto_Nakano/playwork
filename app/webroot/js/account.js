$(function(){

    $('button').on('click', function (e) {
        e.preventDefault();

        var loginId = $('input[name="loginId"]').val();
        var password = $('input[name="password"]').val();

        if(!checkPassword(password)) return;

        var url = '/api/account';
        var param = $('input').serialize();
        var request = Common.post(url, param);
        request.done(function(response){
            if(response.result == 'OK'){
                showSuccessMessage('更新完了しました。');
                // 3秒後に自動遷移
                setInterval(function () {
                    location.href = '/account/';
                }, 3000);
            }else{
                showErrorMessage(response.message);
            }
        }).fail(function(xhr, response, error){
            showErrorMessage('更新失敗しました。');
            console.log(xhr.responseText);
        });
    });

    function showSuccessMessage(message) {
        var message = $('<div class="alert alert-success">' + message + '</div>');
        $('#message').html(message);
    }

    function showErrorMessage(message) {
        var message = $('<div class="alert alert-danger">' + message + '</div>');
        $('#message').html(message);
    }

    function checkPassword (password) {

        if (password.length < 8) {
            alert('パスワードは8文字以上です');
            return false;
        }

        var result = password.match(/^[a-zA-Z0-9!-/:-@¥[-`{-~]+$/);
        if(!result) {
            alert('パスワードは半角英数字と記号のみ使用可能です');
            return false;
        }
        return true;
    }
});
