$(function(){

    ctrlDisabled();

    // 前へ
    $('#prev').on('click', function(){
        var offset = parseInt($('input[name="offset"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var page = parseInt($('input[name="page"]').val());
        var item = parseInt($('input[name="item"]').val());
        var count = parseInt($('input[name="count"]').val());
        page--;
        offset -= limit;

        if ((page*item) <= count ) {
            $('#next').prop('disabled', false);
        }
        location.href = location.pathname + '?offset=' + offset + '&limit=' + limit;
    });

    // 次へ
    $('#next').on('click', function(){
        var offset = parseInt($('input[name="offset"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var page = parseInt($('input[name="page"]').val());
        page++;
        offset += limit;

        if (page != 1) {
            $('#prev').prop('disabled', false);
        }
        location.href = location.pathname + '?offset=' + offset + '&limit=' + limit;
    });

    function ctrlDisabled() {
        var offset = parseInt($('input[name="offset"]').val());
        var page = parseInt($('input[name="page"]').val());
        var limit = parseInt($('input[name="limit"]').val());
        var count = parseInt($('input[name="count"]').val());
        if (page <= 1) {
            $('#prev').prop('disabled', true);
        }
        if ((page*limit) >= count || (offset+limit) == count) {
            $('#next').prop('disabled', true);
        }
    }

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
          month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    $('#divisionId').on('change', function () {
        var divisionId = $(this).val();
        var isMonitor = $('#isMonitor').val();
        location.href = '/ranking_ap_overhangs/' + getCurrentStartMonth() + '/' + divisionId + '/' + isMonitor;
    });

    // Override
    Common.onChangeYear = function(){
        var divisionId = $('#divisionId').val();
        var isMonitor = $('#isMonitor').val();
        location.href = '/ranking_ap_overhangs/' + getCurrentStartMonth() + '/' + divisionId + '/' + isMonitor;
    }

    // Override
    Common.onChangeMonth = function(month){
        var divisionId = $('#divisionId').val();
        var isMonitor = $('#isMonitor').val();
        location.href = '/ranking_ap_overhangs/' + getCurrentStartMonth(month) + '/' + divisionId + '/' + isMonitor;
    }

});
