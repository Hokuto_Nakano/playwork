(function(global){
  "use strict;"

  function ProductDetailCtrl(categories){
    this.categories = categories;
  }
  ProductDetailCtrl.prototype.regist = registProductDetail
  ProductDetailCtrl.prototype.delete = deleteProductDetail;

  ProductDetailCtrl.prototype.selectCategory = selectCategory;
  ProductDetailCtrl.prototype.selectFamily = selectFamily;
  ProductDetailCtrl.prototype.setOldCategoryName = setOldCategoryName;
  ProductDetailCtrl.prototype.onChangeCategoryName = onChangeCategoryName;
  ProductDetailCtrl.prototype.setOldFamilyName = setOldFamilyName;
  ProductDetailCtrl.prototype.onChangeFamilyName = onChangeFamilyName;

  ProductDetailCtrl.prototype.setFamilies_ = setFamilies_;
  ProductDetailCtrl.prototype.setCategoryId_ = setCategoryId_;
  ProductDetailCtrl.prototype.setCategoryName_ = setCategoryName_;
  ProductDetailCtrl.prototype.setCategoryShortName_ = setCategoryShortName_;
  ProductDetailCtrl.prototype.setFamilyId_ = setFamilyId_;
  ProductDetailCtrl.prototype.setFamilyName_ = setFamilyName_;
  ProductDetailCtrl.prototype.setFamilyShortName_ = setFamilyShortName_;

  function registProductDetail(){
    var url = '/api/product_details';
    var id = document.getElementById('product_detail_id').value;
    if(id > 0){
      url = url + '/' + id
    }
    var data = $('form').serialize();
    productCtrl.regist(url, data);
  }

  function deleteProductDetail(id){
      if(!id) return;
      if(!window.confirm('商品を削除します。よろしいですか？')) return;
      var url = '/api/product_details/' + id;
      productCtrl.delete(url);
      return false;
  };

  function selectCategory(categoryId){
    var i = 0;
    while(i<this.categories.length){
      if(categoryId == this.categories[i].ProductCategory.id){
        var category = this.categories[i].ProductCategory;
        this.setCategoryId_(category.id);
        this.setCategoryName_(category.name);
        this.setCategoryShortName_(category.short_name);
        break;
      }
      i = i + 1;
    }

    this.setFamilies_(categoryId);
  }

  function selectFamily(familyId, familyName, familyShortName){
    this.setFamilyId_(familyId);
    this.setFamilyName_(familyName);
    this.setFamilyShortName_(familyShortName);
  }

  function setOldCategoryName(categoryName){
    this.categoryName = categoryName;
  }

  function onChangeCategoryName(keyCode, name){
    if(this.categoryName == name){ return; }

    document.getElementById('product_category_short_name').removeAttribute('disabled');
    document.getElementById('product_family_short_name').removeAttribute('disabled');

    var categoryId = 0;
    var i = 0;
    while(i<this.categories.length){
      if(name == this.categories[i].ProductCategory.name){
        categoryId = this.categories[i].ProductCategory.id;
        this.setCategoryShortName_(this.categories[i].ProductCategory.short_name);
        break;
      }
      i = i + 1;
    }

    this.setCategoryId_(categoryId);
    this.setFamilies_(categoryId);
  }

  function setOldFamilyName(familyName){
    this.familyName = familyName;
  }

  function onChangeFamilyName(name){
    if(this.familyName == name){ return; }

    document.getElementById('product_family_short_name').removeAttribute('disabled');

    var familyId = 0;
    var i = 0;
    while(i<this.families.length){
      if(name == this.families[i].name){
        familyId = this.families[i].id;
        this.setFamilyShortName_(this.families[i].short_name);
        break;
      }
      i = i + 1;
    }

    this.setFamilyId_(familyId);
  }

  function setFamilies_(categoryId){
    var selectFamilies = document.getElementById('selectFamilies');
    if(!selectFamilies) return;

    if(categoryId == 0){
      this.setFamilyId_(0);
      selectFamilies.innerHTML = '';
      return;
    }

    this.families = [];
    var i = 0;
    while(i<this.categories.length){
      if(categoryId == this.categories[i].ProductCategory.id){
        this.families = this.categories[i].ProductFamily;
        break;
      }
      i = i + 1;
    }

    this.setFamilyId_(0);
    this.setFamilyName_('');
    this.setFamilyShortName_('');
    selectFamilies.innerHTML = '';

    for(var i=0; i<this.families.length; i++){
      if(i == 0){
        this.selectFamily(this.families[i].id, this.families[i].name, this.families[i].short_name)
      }
      selectFamilies.innerHTML += "<li><a href=\"javascript:void(0)\" onclick=\"detailCtrl.selectFamily('" + this.families[i].id + "', '" + this.families[i].name + "', '" + this.families[i].short_name + "');\" >" + this.families[i].name + "</a></li>";
    }
  }

  function setCategoryId_(categoryId){
    document.getElementById('product_category_id').value = categoryId;
  }

  function setCategoryName_(categoryName){
    var element = document.getElementById('product_category_name');
    if(element.getAttribute('value') != null){
      element.value = categoryName;
    }else{
      element.innerHTML = categoryName;
    }
  }

  function setCategoryShortName_(categoryShortName){
    document.getElementById('product_category_short_name').value = categoryShortName;
    document.getElementById('product_category_short_name').setAttribute('disabled', 'true');
  }

  function setFamilyId_(familyId){
    document.getElementById('product_family_id').value = familyId;
  }

  function setFamilyName_(familyName){
    var element = document.getElementById('product_family_name');
    if(element.getAttribute('value') != null){
      element.value = familyName;
    }else{
      element.innerHTML = familyName;
    }
  }

  function setFamilyShortName_(familyShortName){
    document.getElementById('product_family_short_name').value = familyShortName;
    if(document.getElementById('product_family_id').value > 0){
      document.getElementById('product_family_short_name').setAttribute('disabled', 'true');
    }
    else{
      document.getElementById('product_family_short_name').removeAttribute('disabled');
    }
  }

  if ("process" in global) {
    module["exports"] = ProductDetailCtrl;
  }
  global["ProductDetailCtrl"] = ProductDetailCtrl;

}((this || 0).self || global));
