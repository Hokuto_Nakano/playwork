(function(global){
  "use strict;"

  function ProductCtrl(){};
  ProductCtrl.prototype.showCategoryModal = showCategoryModal;
  ProductCtrl.prototype.showFamilyModal = showFamilyModal;
  ProductCtrl.prototype.showDetailModal = showDetailModal;
  ProductCtrl.prototype.getList = getList;
  ProductCtrl.prototype.regist = registProduct;
  ProductCtrl.prototype.delete = deleteProduct;
  ProductCtrl.prototype.getModal_ = getModal_;

  function showCategoryModal(id){
    this.getModal_('/partial/product_categories/' + id);
  }
  function showFamilyModal(id){
    this.getModal_('/partial/product_families/' + id);
  }
  function showDetailModal(id){
    var yyyy = document.getElementById('year').value;
    var mm = ('0' + document.getElementById('month').value).slice(-2);
    this.getModal_('/partial/product_details/' + id + '/' + yyyy + mm);
  }

  function getList(month){
    var yyyy = document.getElementById('year').value;
    if(!month){
      month = ('0' + document.getElementById('month').value).slice(-2);
    }
    var mm = ('0' + month).slice(-2)
    location.href = '/products/' + yyyy + mm;
  }

  function registProduct(url, data){
    console.log(JSON.stringify(data));
    var request = Common.post(url, data);
    request.success(function(response){
      if(response.result == 'OK'){
        var yyyy = document.getElementById('year').value;
        var mm = ('0' + document.getElementById('month').value).slice(-2);
        location.href = '/products/' + yyyy + mm;
      }
      else{
        document.getElementById('error').innerHTML = response.message;
      }
    });
    request.error(function(error){
      console.log(JSON.stringify(error));
      document.getElementById('error').innerHTML = '登録に失敗しました。';
    });
    request.done(function(response){
    });
  }

  function deleteProduct(url){
    var jqXHR = Common.delete(url);
    jqXHR.done(function(response){
      if(response.result == 'OK'){
        location.href = '/products';
      }else{
        document.getElementById('error').innerHTML = response.message;
      }
    }).fail(function(error){
      console.log(error);
    });
  }

  function getModal_(url){
    var request = Common.getHtml(url);
    request.success(function(response){
      $('#productModal').html(response);
      $('#productModal').modal();
    });
    request.error(function(error){
      console.log(JSON.stringify(error));
    });
    request.done(function(response){
    });
  }

  if ("process" in global) {
    module["exports"] = new ProductCtrl();
  }
  global["productCtrl"] = new ProductCtrl();

  // Override
  Common.onChangeYear = function(){
    getList();
  }

  // Override
  Common.onChangeMonth = function(month){
    getList(month);
  }

}((this || 0).self || global));
