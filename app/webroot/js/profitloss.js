$(function(){

  $('#new').on('click', function () {
    getModal_('/partial/profit_losses/new/' + Common.getCurrentYYYYMM());
  });

  $(document).on('click', '.update', function(){
    getModal_('/partial/profit_losses/' + this.id + '/' + Common.getCurrentYYYYMM());
  });

  $('#division_id').on('change', function () {
    $('#area_id').val('0');
    $('#section_id').val('0');
    $('#division_form').submit();
  });

  $('#area_id').on('change', function () {
    $('#section_id').val('0');
    $('#division_form').submit();
  });

  $('#section_id').on('change', function () {
    $('#division_form').submit();
  });

  function getModal_(url){
    var data = $('#division_form').serialize();
    var request = Common.getHtml(url, data);
    request.success(function(response){
      $('#plModal').html(response);
      $('#plModal').modal();
    });
    request.error(function(error){
      console.log(JSON.stringify(error));
    });
    request.done(function(response){
      document.getElementById('plvalueForm').onsubmit = function(){
        regist();
        return false;
      };
      if(removeElm = document.getElementById('remove')){
        removeElm.onclick = function(){
          remove();
        };
      }
    });
  }

  function regist(){
    var id = document.getElementById('plvalue_id').value;
    var url = '/api/profit_losses/' + Common.getCurrentYYYYMM() + '/' + id;
    var param = $('form').serialize();
    var request = Common.post(url, param);
    request.done(function(response){
      if(response.result == 'OK'){
        location.href = '/profit_losses/' + Common.getCurrentYYYYMM() + '?division_id=' + $('#division_id').val() + '&area_id=' + $('#area_id').val() + '&section_id=' + $('#section_id').val() + '&update=1';
      }else{
        console.log(response.message);
        $('#error').html(response.message);
      }
    }).fail(function(error){
      console.log(error);
      $('#error').html(error);
    });
  }

  function remove(){
    var id = document.getElementById('plvalue_id').value;
    var url = '/api/profit_losses/' + Common.getCurrentYYYYMM() + '/' + id;
    var request = Common.delete(url);
    request.done(function(response){
        if(response.result == 'OK'){
          location.href = '/profit_losses/' + Common.getCurrentYYYYMM() + '?division_id=' + $('#division_id').val() + '&area_id=' + $('#area_id').val() + '&section_id=' + $('#section_id').val() + '&update=1';
          //location.href = '/profit_losses/' + Common.getCurrentYYYYMM();
        }else{
            console.log(response.message);
        }
    }).fail(function(error){
        console.log(error);
    });
  }

  function getList(month){
    var yyyy = document.getElementById('year').value;
    if(!month){
      month = ('0' + document.getElementById('month').value).slice(-2);
    }
    var mm = ('0' + month).slice(-2);
    $('form').attr('action', '/profit_losses/' + yyyy + mm);
    $('form').submit();
  }

  // Override
  Common.onChangeYear = function(){
    getList();
  }

  // Override
  Common.onChangeMonth = function(month){
    getList(month);
  }
});
