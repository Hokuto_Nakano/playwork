$(function(){

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
          month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    $('#divisionId').on('change', function () {
        var divisionId = $(this).val();
        location.href = '/ranking_ap_breakdowns/' + getCurrentStartMonth() + '/' + divisionId;
    });

    // Override
    Common.onChangeYear = function(){
        location.href = '/ranking_ap_breakdowns/' + getCurrentStartMonth();
    }

    // Override
    Common.onChangeMonth = function(month){
        location.href = '/ranking_ap_breakdowns/' + getCurrentStartMonth(month);
    }

});
