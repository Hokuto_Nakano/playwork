(function(global){
  'use strict;'

  function DivisionCtrl(){}
  DivisionCtrl.prototype.regist = registDivision;
  DivisionCtrl.prototype.delete = deleteDivision;

  function registDivision(){
    var url = '/api/divisions';
    var id = document.getElementById('division_id').value;
    if(id > 0){
      url = url + '/' + id
    }
    var data = $('form').serialize();
    baseCtrl.regist(url, data);
  }

  function deleteDivision(id){
      if(!id) return;
      if(!window.confirm('事業部を削除します。よろしいですか？')) return;
      var url = '/api/divisions/' + id;
      baseCtrl.delete(url);
      return false;
  };

  if ('process' in global) {
    module['exports'] = DivisionCtrl;
  }
  global['DivisionCtrl'] = DivisionCtrl;

}((this || 0).self || global));
