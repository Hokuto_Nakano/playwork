$(function(){

    //init();

    $('#insert').on('click', function(){
        var url = '/products/partial/new';
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#productModal').html(response);
            $('#productModal').modal();
            $('#registBtn').on('click', function(){
                var url = '/products/api/';
                var id = $('#product_id').val();
                if(id > 0){
                    url = url + '/' + id;
                }
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/products';
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        }).fail(function(error){
            console.log(error);
        });
    });

    $(document).on('click', '.update', function(){
        var index = $('.update').index(this);
        var id = $('#id' + index).text();

        var url = '/products/partial/' + id;
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#productModal').html(response);
            $('#productModal').modal();
            $('#form-first').show();
            $('#registBtn').on('click', function(){
                url = '/products/api/';
                if(id > 0){
                    url = url + '/' + id;
                }
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/products';
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        });
    });

    $(document).on('click', '.delete', function(){
        var index = $('.delete').index(this);
        var id = $('#id' + index).text();
        var url = '/products/api';
        if(id) {
            url += '/' + id;
        }
        var jqXHR = Common.delete(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                location.href = '/products';
            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });
        return false;
    });

    // function init(){
    //     var url = '/products/api';
    //     var jqXHR = Common.getJson(url, {});
    //     jqXHR.done(function(response){
    //         if(response.result == 'OK'){
    //             console.log(response);
    //             for(var i = 0; i < response.data.length; i++){
    //                 var id = response.data[i].Product.id;
    //                 var name = response.data[i].Product.name;
    //                 var shortName = response.data[i].Product.short_name;
    //                 var html = getHtml(i, id, name, shortName);
    //                 $('tbody').append(html);
    //             }
    // 
    //         }else{
    //             console.log(response.message);
    //         }
    //     }).fail(function(error){
    //         console.log(error);
    //     });
    // }
    // 
    // function getHtml(row, id, name, shortName){
    //     var html = '<tr>' +
    //                 '<td>' +
    //                     '<span id="id' + row + '">' + id +'</span>'  +
    //                 '</td>' +
    //                 '<td>' +
    //                     '<span id="name' + row +'">' + name +'</span>' +
    //                 '</td>' +
    //                 '<td>' +
    //                     '<span id="shortName' + row +'">' + shortName +'</span>' +
    //                 '</td>' +
    //                 '<td>' +
    //                     '<button type="button" class="btn btn-primary update" data-toggle="modal">更新</button>' +
    //                     '<span>&nbsp;</span>' +
    //                     '<button type="submit" class="delete btn btn-danger">削除</button>' +
    //                 '</td>' +
    //                 '</tr>';
    //     return html;
    // }
});
