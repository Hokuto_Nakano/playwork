$(function(){

  document.getElementById('new').onclick = function(){
    getModal_('/partial/pl_items/new/' + Common.getCurrentYYYYMM());
  };

  $(document).on('click', '.update', function(){
    getModal_('/partial/pl_items/' + this.id);
  });

  function getList(month){
    var yyyy = document.getElementById('year').value;
    if(!month){
      month = ('0' + document.getElementById('month').value).slice(-2);
    }
    var mm = ('0' + month).slice(-2);
    location.href = '/pl_items/' + yyyy + mm;
  }

  function getModal_(url){
    var request = Common.getHtml(url);
    request.success(function(response){
      $('#plItemModal').html(response);
      $('#plItemModal').modal();
    });
    request.error(function(error){
      console.log(JSON.stringify(error));
    });
    request.done(function(response){
      document.getElementById('plitemForm').onsubmit = function(){
        regist();
        return false;
      };
      if(removeElm = document.getElementById('remove')){
        removeElm.onclick = function(){
          remove();
        };
      }
    });
  }

  function regist(){
    var id = document.getElementById('plitem_id').value;
    var url = '/api/pl_items/' + id;
    var param = $('form').serialize();
    var request = Common.post(url, param);
    request.done(function(response){
      if(response.result == 'OK'){
        location.href = '/pl_items/' + Common.getCurrentYYYYMM();
      }else{
        console.log(response.message);
        $('#error').html(response.message);
      }
    }).fail(function(error){
      console.log(error);
      $('#error').html(error);
    });
  }

  function remove(){
    var id = document.getElementById('plitem_id').value;
    var url = '/api/pl_items/' + id;
    var request = Common.delete(url);
    request.done(function(response){
        if(response.result == 'OK'){
            location.href = '/pl_items/' + Common.getCurrentYYYYMM();
        }else{
            console.log(response.message);
        }
    }).fail(function(error){
        console.log(error);
    });
  }

  // Override
  Common.onChangeYear = function(){
    getList();
  }

  // Override
  Common.onChangeMonth = function(month){
    getList(month);
  }
});
