(function(global){
  'use strict;'

  function ProductFamilyCtrl(){}
  ProductFamilyCtrl.prototype.regist = registProductFamily;
  ProductFamilyCtrl.prototype.delete = deleteProductFamily;

  function registProductFamily(){
    var url = '/api/product_families';
    var id = document.getElementById('product_family_id').value;
    if(id > 0){
      url = url + '/' + id
    }
    var data = $('form').serialize();
    productCtrl.regist(url, data);
  }

  function deleteProductFamily(id){
      if(!id) return;
      if(!window.confirm('事業部を削除します。よろしいですか？')) return;
      var url = '/api/product_families/' + id;
      productCtrl.delete(url);
      return false;
  };

  if ('process' in global) {
    module['exports'] = ProductFamilyCtrl;
  }
  global['ProductFamilyCtrl'] = ProductFamilyCtrl;

}((this || 0).self || global));
