$(function(){

    init();

    $('#insert').on('click', function(){
        var url = '/partial/job_types/new';
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#jobtypeModal').html(response);
            $('#jobtypeModal').modal();
            $('#registBtn').on('click', function(){
                var url = '/api/job_types';
                var id = $('#jobtype_id').val();
                if(id > 0){
                    url = url + '/' + id;
                }
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/job_types';
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        }).fail(function(error){
            console.log(error);
        });
    });

    $(document).on('click', '.update', function(){
        var index = $('.update').index(this);
        var id = $('#id' + index).text();

        var url = '/partial/job_types/' + id;
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#jobtypeModal').html(response);
            $('#jobtypeModal').modal();
            $('#form-first').show();
            $('#registBtn').on('click', function(){
                url = '/api/job_types';
                if(id > 0){
                    url = url + '/' + id;
                }
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/job_types';
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        });
    });

    $(document).on('click', '.delete', function(){
        var index = $('.delete').index(this);
        var id = $('#id' + index).text();
        var url = '/api/job_types';
        if(id) {
            url += '/' + id;
        }
        var jqXHR = Common.delete(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                location.href = '/job_types';
            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });
        return false;
    });

    function init(){
        var url = '/api/job_types';
        var jqXHR = Common.getJson(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                console.log(response);
                for(var i = 0; i < response.data.length; i++){
                    var id = response.data[i].JobType.id;
                    var type = response.data[i].JobType.type;
                    var html = getHtml(i, id, type);
                    $('tbody').append(html);
                }

            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });
    }

    function getHtml(row, id, type){
        var html = '<tr>' +
                    '<td>' +
                        '<span id="id' + row + '">' + id +'</span>'  +
                    '</td>' +
                    '<td>' +
                        '<span id="type' + row +'">' + type +'</span>' +
                    '</td>' +
                    '<td>' +
                        '<button type="button" class="btn btn-primary update" data-toggle="modal">更新</button>' +
                        '<span>&nbsp;</span>' +
                        '<button type="submit" class="delete btn btn-primary">削除</button>' +
                    '</td>' +
                    '</tr>';
        return html;
    }
});
