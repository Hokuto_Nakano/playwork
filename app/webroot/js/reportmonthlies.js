$(function(){

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
            month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    $('#divisionId').on('change', function () {
        var divisionId = $(this).val();
        location.href = '/report_monthlies/' + getCurrentStartMonth() + '/' + divisionId;
    });

    $(window).on('load', function () {
        var colSize = $('#colSize').attr('colspan');

        if (colSize > 50) {
            $('table').width(5000);
        } else if (colSize > 30) {
            $('table').width(4000);
        } else if (colSize > 20) {
            $('table').width(3000);
        } else if (colSize > 10) {
            $('table').width(2000);
        }
    });

    // Override
    Common.onChangeYear = function(){
        location.href = '/report_monthlies/' + getCurrentStartMonth();
    }

    // Override
    Common.onChangeMonth = function(month){
        location.href = '/report_monthlies/' + getCurrentStartMonth(month);
    }

});
