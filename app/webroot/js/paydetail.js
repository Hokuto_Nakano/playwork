$(function(){

    $('button').on('click', function (e) {
        e.preventDefault();
        var historyId = $('input[name="staffHistoryId"]').val();
        var bp = $('input[name="bp"]').val();
        var incentive = $('input[name="incentive"]').val();
        var url = '/api/pay_details';
        var param = {
            staffHistoryId: historyId,
            bp: bp,
            incentive: incentive,
            startMonth: getCurrentStartMonth()
        };
        var request = Common.post(url, param);
        request.done(function(response){
            if(response.result == 'OK'){
                showSuccessMessage('更新完了しました。');
                // 3秒後に自動遷移
                setInterval(function () {
                    location.href = '/pay_details/' + historyId + '/' + getCurrentStartMonth();
                }, 3000);
            }else{
                showErrorMessage(response.message);
            }
        }).fail(function(xhr, response, error){
            showErrorMessage('更新失敗しました。');
            console.log(xhr.responseText);
        });
    });

    function getCurrentStartMonth(month) {
        var year = $('#year').val();
        if(!month){
          month = $('#month').val();
        }
        return year + ('0' + month).slice(-2);
    }

    function showSuccessMessage(message) {
        var message = $('<div class="alert alert-success">' + message + '</div>');
        $('#message').html(message);
    }

    function showErrorMessage(message) {
        var message = $('<div class="alert alert-danger">' + message + '</div>');
        $('#message').html(message);
    }

    // Override
    Common.onChangeYear = function(){
        var historyId = $('input[name="staffHistoryId"]').val();
        location.href = '/pay_details/' + historyId + '/' + getCurrentStartMonth();
    }

    // Override
    Common.onChangeMonth = function(month){
        var historyId = $('input[name="staffHistoryId"]').val();
        location.href = '/pay_details/' + historyId + '/' + getCurrentStartMonth(month);
    }

});
