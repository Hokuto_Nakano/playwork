$(function(){

    init();

    $('#insert').on('click', function(){
        var url = '/partial/call_centers/new';
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#callcenterModal').html(response);
            $('#callcenterModal').modal();
            $('#registBtn').on('click', function(){
                url = '/api/call_centers/';
                var id = $('#callcenter_id').val();
                if(id > 0){
                    url = url + '/' + id;
                }
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/call_centers';
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        });
    });

    $(document).on('click', '.update', function(){
        var index = $('.update').index(this);
        var id = $('#id' + index).text();

        var url = '/partial/call_centers/' + id;
        var request = Common.getHtml(url);
        request.done(function(response){
            $('#callcenterModal').html(response);
            $('#callcenterModal').modal();
            $('#form-first').show();
            $('#registBtn').on('click', function(){
                url = '/api/call_centers';
                if(id > 0){
                    url = url + '/' + id;
                }
                var param = $('form').serialize();
                var request = Common.post(url, param);
                request.done(function(response){
                    if(response.result == 'OK'){
                        location.href = '/call_centers';
                    }else{
                        console.log(response.message);
                        $('#error').html(response.message);
                    }
                }).fail(function(error){
                    console.log(error);
                    $('#error').html(error);
                });
                return false;
            });
        });
    });

    $(document).on('click', '.delete', function(){
        var index = $('.delete').index(this);
        var id = $('#id' + index).text();
        var url = '/api/call_centers';
        if(id) {
            url += '/' + id;
        }
        var jqXHR = Common.delete(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                location.href = '/call_centers';
            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });
        return false;
    });

    function init(){
        var url = '/api/call_centers';
        var jqXHR = Common.getJson(url, {});
        jqXHR.done(function(response){
            if(response.result == 'OK'){
                console.log(response);
                for(var i = 0; i < response.data.length; i++){
                    var id = response.data[i].CallCenter.id;
                    var name = response.data[i].CallCenter.name;
                    var html = getHtml(i, id, name);
                    $('tbody').append(html);
                }

            }else{
                console.log(response.message);
            }
        }).fail(function(error){
            console.log(error);
        });
    }

    function getHtml(row, id, name){
        var html = '<tr>' +
                    '<td>' +
                        '<span id="id' + row + '">' + id +'</span>'  +
                    '</td>' +
                    '<td>' +
                    '<span id="name' + row +'">' + name +'</span>' +
                    '</td>' +
                    '<td>' +
                    '<button type="button" class="btn btn-primary update" data-toggle="modal">更新</button>' +
                    '<span>&nbsp;</span>' +
                    '<button type="submit" class="delete btn btn-primary">削除</button>' +
                    '</td>' +
                    '</tr>';
        return html;
    }
});
