<?php
	
	include('./common/env.php');
	include('./login_manager.php');
	
	
	extract($_POST);
	
	
	
	if ($proc_type == "upload") {

		begin();	

		$error_flag			= "";
		$db_error_flag		= "";
		$msg				= "";
		
		$result_string		= "";
		
		$save_estate_name 		= "";
		$save_estate_address 	= "";
		
		$data_file				= $_FILES['data_file']['name'];
		if ($data_file != "") {
			if ($_FILES['data_file']['size'] > 10000000) {
				$error_file_name		= "<font color=red>ファイルのサイズが10MBを超えています。</font>";
				$error_flag				= "*";
				echo "Error.. $error_file_name<br>";
			}
			else {
				$lines 			= file($_FILES['data_file']['tmp_name']);
				//$lines			= move_uploaded_file($_FILES['data_file']['tmp_name']);
				
				//echo "Data... ";
				//print_r($lines);/
			}
		}		
					
		foreach ($lines as $line_num => $csv_line) {
			//echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br />\n";
			//if ($line_num > 10) break;
			
			if ($line_num < 1) continue;
			
			$i				= $line_num;
			
			/*
			NEW...
			物件ジョブ,物件ステータス,物件ID,物件名,物件住所,部屋番号,専有面積,築年数_年,築年数_月,構造,沿線,駅,総戸数,管理会社,分譲会社,抵当権者,ローン残額,情報取得日,物件備考,顧客ID,顧客名,顧客郵便番号,顧客住所1,顧客住所2,顧客住所3,顧客TEL1,顧客TEL2,顧客TEL3,顧客備考,顧客勤務先,顧客属性,DM反響ステータス,見込み取得日,PMステータス,賃料,管理費,修繕積立金,賃貸情報備考,顧客ステータス
			,,10920122,新中野スカイレジテル,東京都中野区本町5-35-11,804,14,1992,04,,,新中野,40,,,,,,,00002602,安藤昭三,195-0051,東京都,町田市真光寺町789-19,,0427347128,,,20130910ﾚ14ﾚ16ﾚ,,,,,,,,,,
			,,10920122,新中野スカイレジテル,東京都中野区本町5-35-11,305,14,1992,04,,,新中野,40,,,,,,,00003587,伊藤毅,153-0061,東京都,目黒区中目黒4-12-7,403,0337933448,,,20130910ﾚ14ﾚ16ﾚ260424ﾚ0511不通,,,,,,,,,,
			,,10920122,新中野スカイレジテル,東京都中野区本町5-35-11,703,14,1992,04,,,新中野,40,,,,,,,00005050,井関健男,134-0087,東京都,江戸川区清新町1-1-24,301,0338789426,,,不通,,,,,,,,,,

			1	物件ジョブ      job
			2       物件ステータス       status_code
			3       物件ID               id
			4       物件名               name
			5       物件住所             address
			6       部屋番号             room_no
			7       専有面積             area
			8       築年数_年            built_date_year
			9       築年数_月            built_date_month
			10      構造                 structure
			11      沿線                 railroad_name
			12      駅                   station_name
			13      総戸数               room_count
			14      管理会社             management_company_name
			15      分譲会社             sell_company_name
			16      抵当権者             mortgagee
			17      ローン残額           loan_remnant
			18      情報取得日           get_date
			19      物件備考             remarks

			20      顧客ID               customer_id
			21      顧客名               name
			22      顧客郵便番号         zip
			23      顧客住所1            prefecture
			24      顧客住所2            address_1
			25      顧客住所3            address_2
			26      顧客TEL1             tel_1
			27      顧客TEL2             tel_2
			28      顧客TEL3             tel_3
			29      顧客備考             remarks
			30      顧客勤務先           company_name
			31      顧客属性             evaluate_string
			32      DM反響ステータス     dm_status
			33      見込み取得日         expect_date
			34      PMステータス         pm_status
			35      賃料                 fee_rent
			36      管理費               fee_management
			37      修繕積立金           fee_correct
			38      賃貸情報備考         rent_remarks
			39		顧客ステータス       call_status

			*/
			
			
			//echo "csv_line : " . $csv_line . "<br>";
			
			$csv_line			= mb_convert_encoding($csv_line, "UTF-8", "SJIS");
			
			$items				= @explode(",", str_replace(" ", "", $csv_line));
			
			$estate_id			= array_shift($items);
			
			if ($items[3] == "") continue;
			
			$error_flag			= "";
			
			//$line_result		= "$i. " . $csv_line . "<br>";
			
			/*
			if (count($items) != 38) {
				$error_flag				= "*";
				$line_result			.= "       => 項目数が違います。<br>";
			}
			*/

			/*
			// 取引ID
			if (is_numeric($items[1]) == false || strlen($items[1]) != 8) {
				$error_flag			= "*";
				$line_result			.= "       => 取引IDを正常に入力してください。<br>";
			}					
			// 物件ステータス
			if ($items[55] == "") {
				$error_flag			= "*";
				$line_result			.= "       => ステータスを正常に入力してください。<br>";
			}
			else {
				$status_code			= "";
				for ($j = 0; $j < count($array_status); $j++) {
					if ($array_status[$j] == $items[55]) {
						$status_code		= $j;
						break;
					}
				}						
				if ($status_code == "") {
					$error_flag				= "*";
					$line_result			.= "       => ステータスを正常に入力してください。<br>";
				}
			}
			
			// 物件名
			if ($items[3] == "") {
				$error_flag				= "*";
				$line_result			.= "       => 物件名を正常に入力してください。<br>";
			}
			// 築年数_年,築年数_月,
			if (checkdate($items[8], 1, $items[7]) == false) {
				$error_flag			= "*";
				$line_result			.= "       => 築年数_年,築年数_月を正常に入力してください。<br>";
			}				
			*/

			
			if ($error_flag == "") {
				//echo "line_num : $line_num ...<br>";

				if ($items[2] == "") {
					$id_mgt			= "";
					
					$sql = "SELECT e.id_mgt ";
					$sql .= "FROM estate_query e ";
					$sql .= "WHERE e.name = '" . str_replace("'", "''", $items[3]) . "' ";
					$sql .= "AND e.address = '" . str_replace("'", "''", $items[4]) . "' ";
					$sql .= $sql_sub;
					//echo "SQL : $sql<br>";
					$rs 			= mysql_query($sql);
					while ($item = mysql_fetch_array($rs)) {
						$id_mgt			= $item["id_mgt"];
					}
					
					if ($id_mgt == "") {
						$sql = "SELECT e.id_mgt ";
						$sql .= "FROM estate_query e ";
						$sql .= "WHERE e.active_flag = '1' ";
						$sql .= "ORDER BY e.id_mgt DESC ";
						$sql .= "LIMIT 0, 1 ";
						$sql .= $sql_sub;
						//echo "SQL : $sql<br>";
						$rs 			= mysql_query($sql);
						while ($item = mysql_fetch_array($rs)) {
							$id_mgt			= $item["id_mgt"] + 1;
						}
					}
					
					$items[2] 		= $id_mgt;
					
					//echo "     id_mgt : $id_mgt ...<br>";
				}
							
				// 検索用のテーブル
				if ($estate_id == "") {
					//echo "     process : INSERT INTO estate_query ...<br>";
					
					$sql = "INSERT INTO estate_query (";
					$sql .= "id_mgt, ";
					$sql .= "job, ";
					$sql .= "status_code, ";
					$sql .= "name, ";
					$sql .= "address, ";
					$sql .= "room_no, ";
					$sql .= "area, ";
					$sql .= "built_date, ";
					$sql .= "structure, ";
					$sql .= "railroad_name, ";
					$sql .= "station_name, ";
					$sql .= "room_count, ";
					$sql .= "management_company_name, ";
					$sql .= "sell_company_name, ";
					$sql .= "mortgagee, ";
					$sql .= "loan_remnant, ";
					$sql .= "get_date, ";
					$sql .= "remarks, ";
					$sql .= "customer_id, ";
					$sql .= "customer_name, ";
					$sql .= "customer_zip, ";
					$sql .= "customer_prefecture, ";
					$sql .= "customer_address_1, ";
					$sql .= "customer_address_2, ";
					$sql .= "customer_tel_1, ";
					$sql .= "customer_tel_2, ";
					$sql .= "customer_tel_3, ";
					$sql .= "customer_remarks, ";
					$sql .= "customer_company_name, ";
					$sql .= "customer_evaluate_string, ";
					$sql .= "customer_dm_status, ";
					$sql .= "customer_expect_date, ";
					$sql .= "customer_call_status, ";
					$sql .= "pm_status, ";
					$sql .= "fee_rent, ";
					$sql .= "fee_management, ";
					$sql .= "fee_correct, ";
					$sql .= "rent_remarks, ";
					$sql .= "active_flag, ";
					$sql .= "reg_date, ";
					$sql .= "upd_date) VALUES ('";
					$sql .= $items[2]	. "', '";		// 物件ID
					$sql .= $items[0]	. "', '";		// 物件ジョブ
					$sql .= $items[1]	. "', '";		// 物件ステータス
					$sql .= str_replace("'", "''", $items[3])	. "', '";		// 物件名,
					$sql .= str_replace("'", "''", $items[4])	. "', '";		// 物件住所
					$sql .= $items[5]	. "', '";		// 部屋番号,
					$sql .= $items[6]	. "', '";		// 専有面積,
					$sql .= $items[7]	. "/" . $items[8]  . "/01', '";		// 築年数_年,
					$sql .= $items[9]	. "', '";		// 構造						
					$sql .= $items[10]	. "', '";		// 沿線
					$sql .= $items[11]	. "', '";		// 駅
					$sql .= $items[12]	. "', '";		// 総戸数,
					$sql .= $items[13]	. "', '";		// 管理会社
					$sql .= $items[14]	. "', '";		// 分譲会社
					$sql .= $items[15]	. "', '";		// 抵当権者
					$sql .= $items[16]	. "', '";		// ローン残額
					$sql .= $items[17]	. "', '";		// 情報取得日
					$sql .= $items[18]	. "', '";		// 物件備考
					$sql .= $items[19]	. "', '";		// 顧客ID
					$sql .= $items[20]	. "', '";		// 顧客名
					$sql .= $items[21]	. "', '";		// 顧客郵便番号
					$sql .= $items[22]	. "', '";		// 顧客住所1
					$sql .= str_replace("'", "''", $items[23])	. "', '";		// 顧客住所2
					$sql .= str_replace("'", "''", $items[24])	. "', '";		// 顧客住所3
					$sql .= $items[25]	. "', '";		// 顧客TEL1
					$sql .= $items[26]	. "', '";		// 顧客TEL2
					$sql .= $items[27]	. "', '";		// 顧客TEL3	
					$sql .= $items[28]	. "', '";		// 顧客備考								
					$sql .= $items[29]	. "', '";		// 勤務先
					$sql .= $items[30]	. "', '";		// 顧客属性
					$sql .= $items[31]	. "', '";		// DM反響ステータス
					$sql .= $items[32]	. "', '";		// 見込み取得日
					$sql .= $items[38]	. "', '";		// 顧客ステータス
					$sql .= $items[33]	. "', '";		// PMステータス
					$sql .= $items[34]	. "', '";		// 賃料
					$sql .= $items[35]	. "', '";		// 管理費							
					$sql .= $items[36]	. "', '";		// 修繕積立金
					$sql .= $items[37]	. "', '";		// 賃貸情報備考
					$sql .= "1', ";
					$sql .= "CURRENT_TIMESTAMP, ";
					$sql .= "CURRENT_TIMESTAMP) ";
					$rs 	= mysql_query($sql);
					//echo "<br>SQL : $sql<br>";
					if (!$rs) {
						$error_flag				= "*";
						$db_error_flag 			= "*";
						echo "[$i] $sql      => ";
						echo mysql_errno($dbh) . ": " . mysql_error($dbh) . "\n<br>";
					}						
						
					if ($items[19] != "") {
						// 顧客情報があれば先に登録処理		
						//$customer_id			= 0;
						//$staff_id				= 0;
											
						$sql = "INSERT INTO customer (";
						$sql .= "id, ";
						$sql .= "name, ";
						$sql .= "zip, ";
						$sql .= "prefecture, ";
						$sql .= "address_1, ";
						$sql .= "address_2, ";
						$sql .= "tel_1, ";
						$sql .= "tel_2, ";
						$sql .= "tel_3, ";
						$sql .= "remarks, ";
						$sql .= "company_name, ";
						$sql .= "evaluate_string, ";
						$sql .= "dm_status, ";
						$sql .= "expect_date, ";
						$sql .= "call_status, ";
						$sql .= "active_flag, ";
						$sql .= "reg_date, ";
						$sql .= "upd_date) VALUES ('";
						$sql .= $items[19]	. "', '";		// 顧客ID
						$sql .= $items[20]	. "', '";		// 顧客名
						$sql .= $items[21]	. "', '";		// 顧客郵便番号
						$sql .= $items[22]	. "', '";		// 顧客住所1
						$sql .= $items[23]	. "', '";		// 顧客住所2
						$sql .= $items[24]	. "', '";		// 顧客住所3
						$sql .= $items[25]	. "', '";		// 顧客TEL1
						$sql .= $items[26]	. "', '";		// 顧客TEL2
						$sql .= $items[27]	. "', '";		// 顧客TEL3	
						$sql .= $items[28]	. "', '";		// 顧客備考								
						$sql .= $items[29]	. "', '";		// 勤務先
						$sql .= $items[30]	. "', '";		// 顧客属性
						$sql .= $items[31]	. "', '";		// DM反響ステータス
						$sql .= $items[32]	. "', '";		// 見込み取得日
						$sql .= $items[38]	. "', '";		// 顧客ステータス
						$sql .= "1', ";
						$sql .= "CURRENT_TIMESTAMP, ";
						$sql .= "CURRENT_TIMESTAMP) ";
						$rs 	= mysql_query($sql);
						//echo "SQL : $sql<br>";
						/*
						if (!$rs) {
							$error_flag				= "*";
							$db_error_flag 			= "*";
							echo "[$i] $sql      => DB処理が異常終了しました。<br>";
						}						
						else {
							//$customer_id			= mysql_insert_id();
						}
						*/
					}
					
					if ($save_estate_name != $items[3] || $save_estate_address != $items[4]) {
						
						$common_id			= "";
						
						$sql = "SELECT id FROM estate_common ";
						$sql .= "WHERE name = '" . $items[3] . "' ";
						$sql .= "AND address = '" . $items[4] . "' ";
						$rs 			= mysql_query($sql);
						while ($item = @mysql_fetch_array($rs)) {
							$common_id  				= $item['id'];
						}
						
						$save_estate_name 		= $items[3];
						$save_estate_address 	= $items[4];
						
						if ($common_id == "") {
							$sql = "INSERT INTO estate_common (";
							$sql .= "name, ";
							$sql .= "address, ";
							$sql .= "remarks, ";
							$sql .= "active_flag, ";
							$sql .= "reg_date, ";
							$sql .= "upd_date) VALUES ('";
							$sql .= str_replace("'", "''", $items[3])	. "', '";		// 物件名,
							$sql .= str_replace("'", "''", $items[4])	. "', ";		// 物件住所
							$sql .= "NULL, '";
							$sql .= "1', ";
							$sql .= "CURRENT_TIMESTAMP, ";
							$sql .= "CURRENT_TIMESTAMP) ";
							$rs 	= mysql_query($sql);
							//echo "SQL : $sql<br>";
							if (!$rs) {
								$error_flag				= "*";
								$db_error_flag 			= "*";
								echo "[$i] $sql      => DB処理が異常終了しました。<br>";
							}		
							else {
								$common_id			= mysql_insert_id();
							}
						}
					
					}
					
					//echo "     process : INSERT INTO estate ...<br>";

					$sql = "INSERT INTO estate (";
					$sql .= "id_mgt, ";
					$sql .= "customer_id, ";
					$sql .= "common_id, ";
					$sql .= "job, ";
					$sql .= "status_code, ";
					$sql .= "name, ";
					$sql .= "address, ";
					$sql .= "room_no, ";
					$sql .= "area, ";
					$sql .= "built_date, ";
					$sql .= "structure, ";
					$sql .= "railroad_name, ";
					$sql .= "station_name, ";
					$sql .= "room_count, ";
					$sql .= "management_company_name, ";
					$sql .= "sell_company_name, ";
					$sql .= "mortgagee, ";
					$sql .= "loan_remnant, ";
					$sql .= "get_date, ";
					$sql .= "pm_status, ";
					$sql .= "fee_rent, ";
					$sql .= "fee_management, ";
					$sql .= "fee_correct, ";
					$sql .= "rent_remarks, ";
					$sql .= "remarks, ";
					$sql .= "active_flag, ";
					$sql .= "reg_date, ";
					$sql .= "upd_date) VALUES ('";
					$sql .= $items[2]	. "', '";		// 物件ID
					$sql .= $items[19]	. "', '";		// 顧客ID
					$sql .= $common_id	. "', '";		// 共通ID
					$sql .= $items[0]	. "', '";		// 物件ジョブ
					$sql .= $items[1]	. "', '";		// 物件ステータス
					$sql .= str_replace("'", "''", $items[3])	. "', '";		// 物件名,
					$sql .= str_replace("'", "''", $items[4])	. "', '";		// 物件住所
					$sql .= $items[5]	. "', '";		// 部屋番号,
					$sql .= $items[6]	. "', '";		// 専有面積,
					$sql .= $items[7]	. "/" . $items[8]  . "/01', '";		// 築年数_年,
					$sql .= $items[9]	. "', '";		// 構造						
					$sql .= $items[10]	. "', '";		// 沿線
					$sql .= $items[11]	. "', '";		// 駅
					$sql .= $items[12]	. "', '";		// 総戸数,
					$sql .= $items[13]	. "', '";		// 管理会社
					$sql .= $items[14]	. "', '";		// 分譲会社
					$sql .= $items[15]	. "', '";		// 抵当権者
					$sql .= $items[16]	. "', '";		// ローン残額
					$sql .= $items[17]	. "', '";		// 情報取得日
					$sql .= $items[33]	. "', '";		// PMステータス
					$sql .= $items[34]	. "', '";		// 賃料
					$sql .= $items[35]	. "', '";		// 管理費							
					$sql .= $items[36]	. "', '";		// 修繕積立金
					$sql .= $items[37]	. "', '";		// 賃貸情報備考
					$sql .= $items[18]	. "', '";		// 物件備考
					$sql .= "1', ";
					$sql .= "CURRENT_TIMESTAMP, ";
					$sql .= "CURRENT_TIMESTAMP) ";
					$rs 	= mysql_query($sql);
					//echo "SQL : $sql<br>";
					if (!$rs) {
						$error_flag				= "*";
						$db_error_flag 			= "*";
						echo "[$i] $sql      => DB処理が異常終了しました。<br>";
					}		
				}
				else {
					//echo "     process : UPDATE estate_query ...<br>";
					
					$sql = "UPDATE estate_query SET ";
					$sql .= "id_mgt				= '" . $items[2]	. "', ";		// 物件ID                               
					$sql .= "job                    		= '" . $items[0]	. "', ";		// 物件ジョブ                           
					$sql .= "status_code            		= '" . $items[1]	. "', ";		// 物件ステータス                       
					$sql .= "name                   		= '" . str_replace("'", "''", $items[3])	. "', ";		// 物件名,      
					$sql .= "address                		= '" . str_replace("'", "''", $items[4])	. "', ";		// 物件住所     
					$sql .= "room_no                		= '" . $items[5]	. "', ";		// 部屋番号,                            
					$sql .= "area                   		= '" . $items[6]	. "', ";		// 専有面積,                            
					$sql .= "built_date             		= '" . $items[7]	. "/" . $items[8]  . "/01', ";		// 築年数_年,       
					$sql .= "structure              		= '" . $items[9]	. "', ";		// 構造						            
					$sql .= "railroad_name          		= '" . $items[10]	. "', ";		// 沿線                                 
					$sql .= "station_name           		= '" . $items[11]	. "', ";		// 駅                                   
					$sql .= "room_count             		= '" . $items[12]	. "', ";		// 総戸数,                              
					$sql .= "management_company_name		= '" . $items[13]	. "', ";		// 管理会社                             
					$sql .= "sell_company_name      		= '" . $items[14]	. "', ";		// 分譲会社                             
					$sql .= "mortgagee              		= '" . $items[15]	. "', ";		// 抵当権者                             
					$sql .= "loan_remnant           		= '" . $items[16]	. "', ";		// ローン残額                           
					$sql .= "get_date               		= '" . $items[17]	. "', ";		// 情報取得日                           
					$sql .= "remarks                		= '" . $items[18]	. "', ";		// 物件備考                             
					$sql .= "customer_id                	= '" . $items[19]	. "', ";		// 顧客ID                               
					$sql .= "customer_name              	= '" . $items[20]	. "', ";		// 顧客名                               
					$sql .= "customer_zip               	= '" . $items[21]	. "', ";		// 顧客郵便番号                         
					$sql .= "customer_prefecture        	= '" . $items[22]	. "', ";		// 顧客住所1                            
					$sql .= "customer_address_1         	= '" . str_replace("'", "''", $items[23])	. "', ";		// 顧客住所2    
					$sql .= "customer_address_2         	= '" . str_replace("'", "''", $items[24])	. "', ";		// 顧客住所3    
					$sql .= "customer_tel_1             	= '" . $items[25]	. "', ";		// 顧客TEL1                             
					$sql .= "customer_tel_2             	= '" . $items[26]	. "', ";		// 顧客TEL2                             
					$sql .= "customer_tel_3             	= '" . $items[27]	. "', ";		// 顧客TEL3	                            
					$sql .= "customer_remarks           	= '" . $items[28]	. "', ";		// 顧客備考								
					$sql .= "customer_company_name      	= '" . $items[29]	. "', ";		// 勤務先                               
					$sql .= "customer_evaluate_string   	= '" . $items[30]	. "', ";		// 顧客属性                             
					$sql .= "customer_dm_status         	= '" . $items[31]	. "', ";		// DM反響ステータス                     
					$sql .= "customer_expect_date       	= '" . $items[32]	. "', ";		// 見込み取得日                         
					$sql .= "customer_call_status       	= '" . $items[38]	. "', ";		// 見込み取得日                         
					$sql .= "pm_status                  	= '" . $items[33]	. "', ";		// PMステータス                         
					$sql .= "fee_rent                   	= '" . $items[34]	. "', ";		// 賃料                                 
					$sql .= "fee_management             	= '" . $items[35]	. "', ";		// 管理費							    
					$sql .= "fee_correct                	= '" . $items[36]	. "', ";		// 修繕積立金                           
					$sql .= "rent_remarks               	= '" . $items[37]	. "', ";		// 顧客ステータス                         
					$sql .= "upd_date                   	= CURRENT_TIMESTAMP  ";                                              
					$sql .= "WHERE id 			= '$estate_id' ";                
					$rs 	= mysql_query($sql);
					//echo "<br>SQL : $sql<br>";
					if (!$rs) {
						$error_flag				= "*";
						$db_error_flag 			= "*";
						echo "[$i] $sql      => ";
						echo mysql_errno($dbh) . ": " . mysql_error($dbh) . "\n<br>";
					}						
					
					$sql = "SELECT e.id ";
					$sql .= "FROM estate e ";
					$sql .= "WHERE e.name = '" . str_replace("'", "''", $items[3]) . "' ";
					$sql .= "AND e.address = '" . str_replace("'", "''", $items[4]) . "' ";
					$sql .= $sql_sub;
					//echo "SQL : $sql<br>";
					$rs 			= mysql_query($sql);
					while ($item = mysql_fetch_array($rs)) {
						$estate_id			= $item["id"];
					}
					
					//echo "     process : UPDATE estate ...<br>";
					
					$sql = "UPDATE estate SET  ";
					$sql .= "id_mgt				= '" . $items[2]	. "', ";		// 物件ID                           
					$sql .= "customer_id                	= '" . $items[19]	. "', ";		// 顧客ID                           
					$sql .= "common_id                  	= '" . $common_id	. "', ";		// 共通ID                           
					$sql .= "job                        	= '" . $items[0]	. "', ";		// 物件ジョブ                       
					$sql .= "status_code                	= '" . $items[1]	. "', ";		// 物件ステータス                   
					$sql .= "name                       	= '" . str_replace("'", "''", $items[3])	. "', ";		// 物件名,  
					$sql .= "address                    	= '" . str_replace("'", "''", $items[4])	. "', ";		// 物件住所 
					$sql .= "room_no                    	= '" . $items[5]	. "', ";		// 部屋番号,                        
					$sql .= "area                       	= '" . $items[6]	. "', ";		// 専有面積,                        
					$sql .= "built_date                 	= '" . $items[7]	. "/" . $items[8]  . "/01', ";		// 築年数_年,   
					$sql .= "structure                  	= '" . $items[9]	. "', ";		// 構造						        
					$sql .= "railroad_name              	= '" . $items[10]	. "', ";		// 沿線                             
					$sql .= "station_name               	= '" . $items[11]	. "', ";		// 駅                               
					$sql .= "room_count                 	= '" . $items[12]	. "', ";		// 総戸数,                          
					$sql .= "management_company_name    	= '" . $items[13]	. "', ";		// 管理会社                         
					$sql .= "sell_company_name          	= '" . $items[14]	. "', ";		// 分譲会社                         
					$sql .= "mortgagee                  	= '" . $items[15]	. "', ";		// 抵当権者                         
					$sql .= "loan_remnant               	= '" . $items[16]	. "', ";		// ローン残額                       
					$sql .= "get_date                   	= '" . $items[17]	. "', ";		// 情報取得日                       
					$sql .= "pm_status                  	= '" . $items[33]	. "', ";		// PMステータス                     
					$sql .= "fee_rent                   	= '" . $items[34]	. "', ";		// 賃料                             
					$sql .= "fee_management             	= '" . $items[35]	. "', ";		// 管理費							
					$sql .= "fee_correct                	= '" . $items[36]	. "', ";		// 修繕積立金                       
					$sql .= "rent_remarks               	= '" . $items[37]	. "', ";		// 賃貸情報備考                     
					$sql .= "remarks                    	= '" . $items[18]	. "', ";		// 物件備考                         
					$sql .= "upd_date                   	= CURRENT_TIMESTAMP  ";                                              
					$sql .= "WHERE id 			= '$estate_id' ";                                              
					$rs 	= mysql_query($sql);
					//echo "<br>SQL : $sql<br>";
					if (!$rs) {
						$error_flag				= "*";
						$db_error_flag 			= "*";
						echo "[$i] $sql      => ";
						echo mysql_errno($dbh) . ": " . mysql_error($dbh) . "\n<br>";
					}
				}				
			}
			
			if ($error_flag　== "*") {
				$result_string		.= $line_result;
			}
		}
		
		foreach ($array_prefecture_id as $key => $value) {
			$sql = "UPDATE estate SET prefecture_id = '$key' WHERE address LIKE '$value%'";
			$rs 	= mysql_query($sql);
			
			$sql = "UPDATE estate_common SET prefecture_id = '$key' WHERE address LIKE '$value%'";
			$rs 	= mysql_query($sql);
			
			$sql = "UPDATE estate_query SET prefecture_id = '$key' WHERE address LIKE '$value%'";
			$rs 	= mysql_query($sql);
		}	
		
		if ($db_error_flag == "") {
			commit();
			$msg			= "CSVアップロードが正常に完了しました。";
		}
		else {
			// Error
			rollback();
			$msg			= "<font style='color:#ff0000;'>CSVアップロードが異常終了しました。11</font>";
		}		
		
		echo "result : $msg ...<br>";
	}
	
	
	
	$search_name_type		= $_POST['search_name_type'];

	$sort_item_order    	= $_GET['order'];
	$sort_item_seq       	= $_GET['seq'];
		
	$page        		= $_GET['page'];
	
	$list_type        		= $_GET['type'];
	$view_count	       	= $_GET['view'];
	
	if ($view_count != "") {
		$_SESSION['view_count'] 			= $view_count;
		$page					= $_SESSION['page'];
	}		
		
	if ($sort_item_order == "" && $sort_item_seq == "") {
		$_SESSION['sort_item_order'] 	= 2;
		$_SESSION['sort_item_seq'] 		= 0;
	}		
	else {
		$_SESSION['sort_item_order'] 	= $sort_item_order;
		$_SESSION['sort_item_seq'] 		= $sort_item_seq;
		$page					= $_SESSION['page'];
	}
	
	if ($page == "") {
		$page  			= 1;
					
		$sql_sub		= "";
		$_SESSION['sql_sub'] 		= "";
				
		if ($search_more != "") {
			foreach ($array_prefecture_id as $key => $value) {
				if ($search_more == $value) {
					$prefecture_id			= $key;
					break;
				}
			}			
			$sql_sub		.= "AND (e.id = '" . $search_more . "' ";
			$sql_sub		.= "	  OR e.name LIKE '%" . $search_more . "%' ";
			$sql_sub		.= "	  OR e.room_no = '" . $search_more . "' ";
			$sql_sub		.= "	  OR c.name LIKE '%" . $search_more . "%' ";
			$sql_sub		.= "	  OR e.prefecture_id = '" . $prefecture_id . "' ";
			$sql_sub		.= "	  OR e.address LIKE '%" . $search_more . "%' ";
			$sql_sub		.= ") ";
		}

		if ($search_job != "") {
			$sql_sub		.= "AND e.job = '" . $search_job . "' ";
		}
		if ($search_estate_id != "") {
			$sql_sub		.= "AND e.id_mgt = '" . $search_estate_id . "' ";
		}
		if ($search_perspective != "") {
			$sql_sub		.= "AND e.perspective_id = '" . $search_perspective . "' ";
		}
		if ($search_estate_status != "") {
			$sql_sub		.= "AND e.estate_status = '" . $search_estate_status . "' ";
		}
		if ($search_name != "") {
			if ($search_name_type == "1") {
				$sql_sub		.= "AND e.name LIKE '%" . $search_name . "%' ";
			}
			else {
				$sql_sub		.= "AND e.name = '" . $search_name . "' ";
			}
		}
		if ($search_prefecture != "") {
			$sql_sub		.= "AND e.prefecture_id = '" . $search_prefecture . "' ";
		}
		if ($search_address != "") {
			$sql_sub		.= "AND e.address LIKE '%" . $search_address . "%' ";
		}
		if ($search_built_year_from != "" && $search_built_month_from != "") {
			$sql_sub		.= "AND e.built_date >= '" . $search_built_year_from . "/" . $search_built_month_from . "/01' ";
		}
		elseif ($search_built_year_from != "") {
			$sql_sub		.= "AND e.built_date >= '" . $search_built_year_from . "/01/01' ";
		}
		if ($search_built_year_to != "" && $search_built_month_to != "") {
			$sql_sub		.= "AND e.built_date <= '" . $search_built_year_to . "/" . $search_built_month_to . "/01' ";
		}
		elseif ($search_built_year_to != "") {
			$sql_sub		.= "AND e.built_date <= '" . $search_built_year_to . "/12/01' ";
		}
		if ($search_room_count_from != "") {
			$sql_sub		.= "AND e.room_count >= '" . $search_room_count_from . "' ";
		}
		if ($search_room_count_to != "") {
			$sql_sub		.= "AND e.room_count <= '" . $search_room_count_to . "' ";
		}
		if ($search_area_from != "") {
			$sql_sub		.= "AND cast(e.area as decimal(7,2)) >= '" . $search_area_from . "' ";
		}
		if ($search_area_to != "") {
			$sql_sub		.= "AND cast(e.area as decimal(7,2)) <= '" . $search_area_to . "' ";
		}
		if ($search_status != "") {
			$sql_sub		.= "AND e.status = '" . $search_status . "' ";
		}
		if ($search_structure != "") {
			$sql_sub		.= "AND e.structure = '" . $search_structure . "' ";
		}		
		if ($search_management_company_name != "") {
			$sql_sub		.= "AND e.management_company_name LIKE '%" . $search_management_company_name . "%' ";
		}
		if ($search_sell_company_name != "") {
			$sql_sub		.= "AND e.sell_company_name LIKE '%" . $search_sell_company_name . "%' ";
		}
		if ($search_get_date_year_from != "" && $search_get_date_month_from != "") {
			$sql_sub		.= "AND e.get_date >= DATE('" . date("Y/m/d", mktime(0,0,0,$search_get_date_month_from, 1, $search_get_date_year_from)) . "') ";
		}
		elseif ($search_get_date_year_from != "") {
			$sql_sub		.= "AND e.get_date >= DATE('" . $search_get_date_year_from . "/01/01') ";
		}
		if ($search_get_date_year_to != "" && $search_get_date_month_to != "") {
			$sql_sub		.= "AND e.get_date <= DATE('" . date("Y/m/d", mktime(0,0,0,$search_get_date_month_to + 1, 0, $search_get_date_year_to)) . "') ";
		}
		elseif ($search_get_date_year_to != "") {
			$sql_sub		.= "AND e.get_date <= DATE('" . $search_get_date_year_to . "/12/31') ";
		}
		if ($search_pm_status != "") {
			$sql_sub		.= "AND c.pm_status = '" . $search_pm_status . "' ";
		}
		if ($search_fee_rent_from != "") {
			$sql_sub		.= "AND cast(c.fee_rent as int) >= '" . $search_fee_rent_from . "' ";
		}
		if ($search_fee_rent_to != "") {
			$sql_sub		.= "AND cast(c.fee_rent as int) <= '" . $search_fee_rent_to . "' ";
		}
		if ($search_fee_management_from != "") {
			$sql_sub		.= "AND cast(c.fee_management as int) >= '" . $search_fee_management_from . "' ";
		}
		if ($search_fee_management_to != "") {
			$sql_sub		.= "AND cast(c.fee_management as int) <= '" . $search_fee_management_to . "' ";
		}
		if ($search_fee_correct_from != "") {
			$sql_sub		.= "AND cast(c.fee_correct as int) >= '" . $search_fee_correct_from . "' ";
		}
		if ($search_fee_correct_to != "") {
			$sql_sub		.= "AND cast(c.fee_correct as int) <= '" . $search_fee_correct_to . "' ";
		}
		if ($search_rent_remarks != "") {
			$sql_sub		.= "AND c.rent_remarks LIKE '%" . $search_rent_remarks . "%' ";
		}
		if ($search_customer_id != "") {
			$sql_sub		.= "AND e.customer_id = '" . $search_customer_id . "' ";
		}
		if ($search_customer_name != "") {
			$sql_sub		.= "AND c.name LIKE '%" . $search_customer_name . "%' ";
		}
		if ($search_customer_zip != "") {
			$sql_sub		.= "AND c.zip = '" . $search_customer_zip . "' ";
		}
		if ($search_customer_tel1 != "") {
			$sql_sub		.= "AND c.tel_1 LIKE '%" . $search_customer_tel1 . "%' ";
		}
		if ($search_customer_tel2 != "") {
			$sql_sub		.= "AND c.tel_2 LIKE '%" . $search_customer_tel2 . "%' ";
		}
		if ($search_customer_tel3 != "") {
			$sql_sub		.= "AND c.tel_3 LIKE '%" . $search_customer_tel3 . "%' ";
		}
		if ($search_customer_prefecture_id != "") {
			$sql_sub		.= "AND c.prefecture = '" . $search_customer_prefecture_id . "' ";
		}
		if ($search_customer_address != "") {
			$sql_sub		.= "AND c.address_1 LIKE '%" . $search_customer_address . "%' ";
		}
		if ($search_customer_address_2 != "") {
			$sql_sub		.= "AND c.address_2 LIKE '%" . $search_customer_address_2 . "%' ";
		}
		if ($search_customer_type != "") {
			$sql_sub		.= "AND c.customer_type = '" . $search_customer_type . "' ";
		}
		if ($search_dm_status != "") {
			$sql_sub		.= "AND c.dm_status = '" . $search_dm_status . "' ";
		}
		if ($search_customer_remarks != "") {
			$sql_sub		.= "AND c.remarks LIKE '%" . $search_customer_remarks . "%' ";
		}
		
		$_SESSION['search_job']								= $search_job;
		$_SESSION['search_estate_id']                   	= $search_estate_id;
		$_SESSION['search_perspective']                 	= $search_perspective;
		$_SESSION['search_name']                        	= $search_name;
		$_SESSION['search_name_type']                   	= $search_name_type;
		$_SESSION['search_prefecture']                  	= $search_prefecture;
		$_SESSION['search_address']                     	= $search_address;
		$_SESSION['search_built_year_from']             	= $search_built_year_from;
		$_SESSION['search_built_month_from']            	= $search_built_month_from;
		$_SESSION['search_built_year_to']               	= $search_built_year_to;
		$_SESSION['search_built_month_to']              	= $search_built_month_to;
		$_SESSION['search_room_count_from']             	= $search_room_count_from;
		$_SESSION['search_room_count_to']               	= $search_room_count_to;
		$_SESSION['search_area_from']                   	= $search_area_from;
		$_SESSION['search_area_to']                     	= $search_area_to;
		$_SESSION['search_status']                      	= $search_status;
		$_SESSION['search_structure']                   	= $search_structure;
		$_SESSION['search_management_company_name']     	= $search_management_company_name;
		$_SESSION['search_sell_company_name']           	= $search_sell_company_name;
		$_SESSION['search_get_date_year_from']          	= $search_get_date_year_from;
		$_SESSION['search_get_date_month_from']         	= $search_get_date_month_from;
		$_SESSION['search_get_date_year_to']            	= $search_get_date_year_to;
		$_SESSION['search_get_date_month_to']           	= $search_get_date_month_to;
		$_SESSION['search_pm_status']                   	= $search_pm_status;
		$_SESSION['search_fee_rent_from']               	= $search_fee_rent_from;
		$_SESSION['search_fee_rent_to']                 	= $search_fee_rent_to;
		$_SESSION['search_fee_management_from']         	= $search_fee_management_from;
		$_SESSION['search_fee_management_to']           	= $search_fee_management_to;
		$_SESSION['search_fee_correct_from']            	= $search_fee_correct_from;
		$_SESSION['search_fee_correct_to']              	= $search_fee_correct_to;
		$_SESSION['search_rent_remarks']                	= $search_rent_remarks;
		$_SESSION['search_customer_id']                 	= $search_customer_id;
		$_SESSION['search_customer_name']               	= $search_customer_name;
		$_SESSION['search_customer_zip']                	= $search_customer_zip;
		$_SESSION['search_customer_tel1']                	= $search_customer_tel1;
		$_SESSION['search_customer_tel2']                	= $search_customer_tel2;
		$_SESSION['search_customer_tel3']                	= $search_customer_tel3;
		$_SESSION['search_customer_prefecture_id']      	= $search_customer_prefecture_id;
		$_SESSION['search_customer_address']            	= $search_customer_address;
		$_SESSION['search_customer_address_2']          	= $search_customer_address_2;
		$_SESSION['search_customer_type']               	= $search_customer_type;
		$_SESSION['search_dm_status']                   	= $search_dm_status;
		$_SESSION['search_customer_remarks']            	= $search_customer_remarks;

		$_SESSION['sql_sub']				= $sql_sub;
	}
	else {
		$search_job								= $_SESSION['search_job'];
		$search_estate_id                        = $_SESSION['search_estate_id'];
		$search_perspective                      = $_SESSION['search_perspective'];
		$search_name                             = $_SESSION['search_name'];
		$search_name_type                        = $_SESSION['search_name_type'];
		$search_prefecture                       = $_SESSION['search_prefecture'];
		$search_address                          = $_SESSION['search_address'];
		$search_built_year_from                  = $_SESSION['search_built_year_from'];
		$search_built_month_from                 = $_SESSION['search_built_month_from'];
		$search_built_year_to                    = $_SESSION['search_built_year_to'];
		$search_built_month_to                   = $_SESSION['search_built_month_to'];
		$search_room_count_from                  = $_SESSION['search_room_count_from'];
		$search_room_count_to                    = $_SESSION['search_room_count_to'];
		$search_area_from                        = $_SESSION['search_area_from'];
		$search_area_to                          = $_SESSION['search_area_to'];
		$search_status                           = $_SESSION['search_status'];
		$search_structure                        = $_SESSION['search_structure'];
		$search_management_company_name          = $_SESSION['search_management_company_name'];
		$search_sell_company_name                = $_SESSION['search_sell_company_name'];
		$search_get_date_year_from               = $_SESSION['search_get_date_year_from'];
		$search_get_date_month_from              = $_SESSION['search_get_date_month_from'];
		$search_get_date_year_to                 = $_SESSION['search_get_date_year_to'];
		$search_get_date_month_to                = $_SESSION['search_get_date_month_to'];
		$search_pm_status                        = $_SESSION['search_pm_status'];
		$search_fee_rent_from                    = $_SESSION['search_fee_rent_from'];
		$search_fee_rent_to                      = $_SESSION['search_fee_rent_to'];
		$search_fee_management_from              = $_SESSION['search_fee_management_from'];
		$search_fee_management_to                = $_SESSION['search_fee_management_to'];
		$search_fee_correct_from                 = $_SESSION['search_fee_correct_from'];
		$search_fee_correct_to                   = $_SESSION['search_fee_correct_to'];
		$search_rent_remarks                     = $_SESSION['search_rent_remarks'];
		$search_customer_id                      = $_SESSION['search_customer_id'];
		$search_customer_name                    = $_SESSION['search_customer_name'];
		$search_customer_zip                     = $_SESSION['search_customer_zip'];
		$search_customer_tel1 					 = $_SESSION['search_customer_tel1'];
		$search_customer_tel2 					 = $_SESSION['search_customer_tel2'];
		$search_customer_tel3 					 = $_SESSION['search_customer_tel3'];
		$search_customer_prefecture_id           = $_SESSION['search_customer_prefecture_id'];
		$search_customer_address                 = $_SESSION['search_customer_address'];
		$search_customer_address_2               = $_SESSION['search_customer_address_2'];
		$search_customer_type                    = $_SESSION['search_customer_type'];
		$search_dm_status                        = $_SESSION['search_dm_status'];
		$search_customer_remarks                 = $_SESSION['search_customer_remarks'];

		$sql_sub           			= $_SESSION['sql_sub'];
	}
	
	$_SESSION['page']						= $page;

	$view_count			= $_SESSION['view_count'];
	if ($view_count == "") {
		$view_count			= 100;
	}	
	
	$sort_item_seq		= $_SESSION['sort_item_seq'];
	$sort_item_order	= $_SESSION['sort_item_order'];

	$array_sort_items		= array("e.name","e.room_no","c.name","e.prefecture_id","e.address");
	$array_item_signal		= array();
	$array_item_link		= array();
	
	
	$sort_string			= "ORDER BY e.id ASC ";
	
	for ($i = 0; $i < count($array_sort_items); $i++) {
		if ($sort_item_seq != "" && $i == $sort_item_seq) {
			if ($sort_item_order == 1) {
				$sort_string			= "ORDER BY " . $array_sort_items[$sort_item_seq] . " ASC ";
				$array_item_signal[$i]	= "▲";
				$array_item_link[$i]	= "order=2&seq=$i";
			}
			else {
				$sort_string			= "ORDER BY " . $array_sort_items[$sort_item_seq] . " DESC ";
				$array_item_signal[$i]	= "▼";
				$array_item_link[$i]	= "order=1&seq=$i";
			}
		}
		else {
			$array_item_signal[$i]		= "◆";
			$array_item_link[$i]		= "order=1&seq=$i";
		}		
	}
	
	$offset	= $view_count * ($page - 1);
	
	$sql_sub_query			= str_replace("c.", "e.customer_", $sql_sub);

	//$sql_sub			= str_replace("c.", "e.customer_", $sql_sub);


	$array_staff_id		= array();
	
	$sql = "SELECT s.* ";
	$sql .= "FROM staff s ";
	$sql .= "WHERE s.active_flag = '1' ";
	$sql .= "ORDER BY s.id ";
	//echo "SQL : $sql<br>";
	$rs 			= mysql_query($sql);
	while ($item = @mysql_fetch_array($rs)) {
		$db_staff_id	   				= $item['id'];
		$db_staff_name  				= $item['name'];	
		
		$array_staff_id[$db_staff_id]	= $db_staff_name;
	}


	$sql = "SELECT COUNT(*) cnter ";
	$sql .= "FROM estate e ";
	$sql .= "LEFT JOIN customer c ON e.customer_id = c.id ";
	$sql .= "WHERE e.active_flag = '1' ";
	$sql .= $sql_sub;
	//$sql .= "AND (c.call_status IS NULL OR c.call_status NOT IN(5,6,7,8,9)) ";
	// Update 2014/11/04
	//$sql .= "AND (c.call_status IS NULL OR c.call_status NOT IN(5,6,7,8,9,10)) ";
	//$sql .= "AND c.prohibit_flag IS NULL ";
	//$sql .= "AND c.mybox_flag IS NULL ";
	//echo "SQL : $sql<br>";
	$rs 			= @mysql_query($sql);
	$items			= @mysql_fetch_array($rs);
	$total_count	= $items["cnter"];
	
	$_SESSION['save_sql_sub']		= $sql_sub;
	
	$display_list_limit		= $view_count;
	$offset			= ($page - 1) * $display_list_limit;
	
	$pageing_contents		= "";
	pageing_link_build2("mgt_list.php");
						
?>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Tel Force</title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="./assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="./assets/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="./assets/css/jquery-ui.min.css" />
		<link rel="stylesheet" href="./assets/css/datepicker.css" />
		<link rel="stylesheet" href="./assets/css/bootstrap-datetimepicker.css" />
		<link rel="stylesheet" href="./assets/css/chosen.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="./assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="./assets/css/ace.min.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="./assets/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="./assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="./assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="./assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="./assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="./assets/js/html5shiv.js"></script>
		<script src="./assets/js/respond.min.js"></script>
		<![endif]-->
			
	<script type="text/javascript">
	
	setInterval (
		function () {
			// alert("Hello")
			//document.getElementById("lbl_recall_string").innerHTML = "";
			xmlhttpPost("recall_check.php");
		}, 30000);

//	setInterval (
//		function () {
//			// alert("Hello")
//			document.getElementById("lbl_recall_string").innerHTML = "**********";
//			xmlhttpPost("recall_check.php");
//		}, 60000);
			
	function xmlhttpPost(strURL) {
	    var xmlHttpReq = false;
	    var self = this;
	    // Mozilla/Safari
	    if (window.XMLHttpRequest) {
	        self.xmlHttpReq = new XMLHttpRequest();
	    }
	    // IE
	    else if (window.ActiveXObject) {
	        self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    self.xmlHttpReq.open('POST', strURL, true);
	    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    self.xmlHttpReq.onreadystatechange = function() {
	        if (self.xmlHttpReq.readyState == 4) {
	            //window.location=document.getElementById('recall_show').href;
	            
	            var rc = self.xmlHttpReq.responseText;
	            if (rc == "##") return false;
	            updatepage(rc);
	            document.getElementById("recall_show").click();
	        }
	    }
	    self.xmlHttpReq.send(getquerystring());
	}

	function getquerystring() {
		///var form     = document.forms['f1'];
		//var word = form.word.value;
		//qstr = 'w=' + escape(word);  // NOTE: no '?' before querystring
		qstr = 'w=11';
		return qstr;
	}

	function updatepage(str){
		//document.getElementById("lbl_recall_string").innerHTML = str;

		// $history_id . "/" . $customer_id . "/" . $customer_name . "/" . $estate_id . "/" . $tel_no . "/" . $tel_1 . "," . $tel_2 . "," . $tel_3 . ":";
		
		if (str != "") {
			var item_estate = str.split(":");
			var item_field = item_estate[0].split("/");
			
			//alert("item_estate[0] : " + item_estate[0]);
			
			document.getElementById("recall_history_id").value = item_field[0];
			document.getElementById("recall_customer_id").value = item_field[1];
			document.getElementById("recall_customer_name").value = item_field[2];
			document.getElementById("recall_estate_id").value = item_field[3];
			document.getElementById("recall_tel_no").value = item_field[4];
			document.getElementById("recall_tel_no_master").value = item_field[5];
			
			document.getElementById('lbl_recall_comment').innerHTML = "コールID: " + item_field[0] + "<br>" + "顧客名: " + item_field[2] + "<br>" + "TEL: " + item_field[4] + "<br>";
		}
	}
	
	
	var tel_1 = "";
	var tel_2 = "";
	var tel_3 = "";
	var calling_tel = "";
	var calling_flag = "";
	var prohibit_flag = "";
	var customer_id = "";
	var estate_id = "";
	
	function prohibit_proc() {
		if (customer_id == "") {
			alert("未選択の状況です。");
			return false;
		}
		sender.location.href = "call_prohibit.php?id=" + customer_id;
	}
	
	function recall_proc() {
		//alert("Recalling... ");
		
		// Unselect current line
		unselect_proc();
		//alert("Before Recalling... ");
		document.getElementById('auto_kind').checked = true;
		
		var history_id = document.getElementById("recall_history_id").value;
		var cid = document.getElementById("recall_customer_id").value;
		var name = document.getElementById("recall_customer_name").value;
		var id = document.getElementById("recall_estate_id").value;
		var called_no = document.getElementById("recall_tel_no").value;
		var tel_no_master = document.getElementById("recall_tel_no_master").value;
		var tel_nos = tel_no_master.split(",");
		
		view_details(id, name,cid,tel_nos[0],tel_nos[1],tel_nos[2],'0');
		for (var i = 0; i < 3; i++) {
			if (tel_nos[i] == called_no) {
				var j = i + 1;
				document.getElementById('tel_type_' + j).checked = true;
			}
		}
		
		if (history_id == "") return false;
		
		// ReCall 設定解除
		recall_reset(history_id);
		
		// Call処理
		call_proc();
	}
	
	function call_proc() {
		if (document.getElementById('tel_type_1').checked == true) {
			calling_tel = tel_1;
		}
		if (document.getElementById('tel_type_2').checked == true) {
			calling_tel = tel_2;
		}
		if (document.getElementById('tel_type_3').checked == true) {
			calling_tel = tel_3;
		}
		if (prohibit_flag == "1") {
			alert("禁止番号です。");
			return false;
		}
		if (customer_id == "" || calling_tel == "") {
			alert("未選択の状況です。");
			return false;
		}
		
		document.getElementById('comment_customer_id').value = customer_id;
		document.getElementById('comment_estate_id').value = estate_id;
		document.getElementById('comment_tel_no').value = calling_tel;
		
		var access_url = "http://<?php echo $local_server; ?>/caller.php?cmd=call&tel=" + calling_tel + "&ip=<?php echo $_SESSION['login_staff_ip']; ?>";
		
		//alert("calling.." + calling_tel);
		sender.location.href = access_url;
		
		access_url = "call_reg.php?cid=" + customer_id + "&eid=" + estate_id + "&tel=" + calling_tel;
		//alert("call reg.." + access_url);
		receiver.location.href = access_url;
	}
	function hangup_proc() {
		var hangup_url = "http://<?php echo $local_server; ?>/caller.php?cmd=hangup&tel=" + calling_tel + "&ip=<?php echo $_SESSION['login_staff_ip']; ?>";
		//alert("hangup.." + hangup_url);
		sender.location.href = hangup_url;
	}
	function recall_reset(id) {
		//alert("recall_reset.. " + "recall_reset.php?id=" + id);
		receiver.location.href = "recall_reset.php?id=" + id;
	}
	function view_details(id, name,cid,t1,t2,t3,pb) {
		//alert("view_details.." + id);
		//document.getElementById('lblSelectedOne').innerHTML = id + " " + name;
		
		estate_id = id;
		customer_id = cid;
		tel_1 = t1;
		tel_2 = t2;
		tel_3 = t3;		
		prohibit_flag = pb;
		document.getElementById('tel_type_1').checked = false;
		document.getElementById('tel_type_2').checked = false;
		document.getElementById('tel_type_3').checked = false;
		
		if (pb == "1") {
			document.getElementById('tel_type_1').disabled = true;
			document.getElementById('tel_type_2').disabled = true;
			document.getElementById('tel_type_3').disabled = true;
		}
		else {
			var available_no = "";
			
			if (t3 == "") {
				document.getElementById('tel_type_3').disabled = true;
			}
			else {
				available_no = "3";
				document.getElementById('tel_type_3').disabled = false;
			}
			if (t2 == "") {
				document.getElementById('tel_type_2').disabled = true;
			}
			else {
				available_no = "2";
				document.getElementById('tel_type_2').disabled = false;
			}
			if (t1 == "") {
				document.getElementById('tel_type_1').disabled = true;
			}
			else {
				available_no = "1";
				document.getElementById('tel_type_1').disabled = false;
			}
			
			if (available_no != "") {
				document.getElementById('tel_type_' + available_no).checked = true;
			}
		}
		
		details.location.href = "estate_details.php?id=" + id;
		
		//details.document.form_99.estate_id_listing.value = id;
		//details.document.form_99.action = "estate_details.php";
		//details.document.form_99.submit();
	}
	var seq_old = -1;
	function select_proc(seq) {
		//alert("seq.. " + seq);
		if (seq_old > -1) {
			if (document.getElementById('prohibit_flag_' + seq_old).value == "1") {
				document.getElementById('tr_estate_' + seq_old).style.backgroundColor = "#FFACAC";
			}
			else {
				if (seq_old % 2 == 0) {
					document.getElementById('tr_estate_' + seq_old).style.backgroundColor = "#FFFFFF";
				}
				else {
					document.getElementById('tr_estate_' + seq_old).style.backgroundColor = "#EEEEEE";
				}
			}
		}
		document.getElementById('tr_estate_' + seq).style.backgroundColor = "#F2910D";
		// event
		document.getElementById('tr_estate_' + seq).click();
			
		seq_old = seq;
	}
	function unselect_proc() {
		if (seq_old > -1) {
			if (document.getElementById('prohibit_flag_' + seq_old).value == "1") {
				document.getElementById('tr_estate_' + seq_old).style.backgroundColor = "#FFACAC";
			}
			else {
				if (seq_old % 2 == 0) {
					document.getElementById('tr_estate_' + seq_old).style.backgroundColor = "#FFFFFF";
				}
				else {
					document.getElementById('tr_estate_' + seq_old).style.backgroundColor = "#EEEEEE";
				}
			}
			seq_old = -1;
		}
	}
	function check_continue() {
		//alert("check_continue");
		// 自動設定のチェック
		//if (document.getElementById('auto_kind').checked == true) {
		//	select_proc(parseInt(seq_old) + 1);
		//}
		
		var view_count = parseInt(document.getElementById('view_count').value);
		for (var i = parseInt(seq_old) + 1; i < view_count; i++) {
			if (document.getElementById('prohibit_flag_' + i).value == "") {
				select_proc(i);
				break;
			}
		}
	}
	function comment_proc() {
		document.history_form.submit();
		
		if (auto_process_flag == 1) {
			check_continue();
			call_proc();
		}
		else {
			document.getElementById('tr_estate_' + seq_old).click();
		}
	}
	function next_proc() {
		var next_seq = parseInt(seq_old) + 1;
		//alert(next_seq);
		select_proc(next_seq);
		
	}
	var auto_process_flag = 0;
	function auto_process() {
		if (document.getElementById('auto_kind').checked == true) {
			auto_process_flag = 0;
		}
		else {
			auto_process_flag = 1;
		}
		//alert("auto_process_flag : " + auto_process_flag);
	}
	function review_proc() {
		location.href = "mgt_list.php?view=" + document.getElementById('view_count').value;
	}
	function search_more() {
		document.getElementById('search_more_other').value = document.getElementById('search_more').value;
		document.search_form.submit();
	}
	function auto_off() {
		document.getElementById('auto_kind').checked = true;
		auto_process_flag = 0;
	}

	function delete_proc(id) {
		if (confirm("削除しても良いですか？")) {
			location.href = "mgt_list.php?proc=delete&id=" + id;
		}
	}
	function update_proc(id) {
		location.href = "mgt_list_upd.php?id=" + id;
	}
	function export_proc() {
		window.open("db_export_1.php");
	}
	function import_proc() {
		document.import_form.submit();
	}
	function sample_view() {
		location.href = "sample.csv";
	}
	</script>
	
	</head>

	<body class="skin-1" onload="auto_off();">
		<?php include_once "navbardd.php"; ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<?php include_once "sidebar.php"; ?>

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>

					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							Home
						</li>
						<li>管理</li>
						<li class="active">リスト管理</li>						
					</ul><!-- /.breadcrumb -->
				</div>
				<div class="page-content">
							<div class="page-header">
								<h1>
								リスト管理 
									<!-- small>
										<i class="ace-icon fa fa-angle-double-right"></i>
										社内用ローカルサーバーIPを設定してください
									</small -->
								</h1>
							</div><!-- /.page-header -->					
					<!-- ?php include_once "settingbox.php"; ? -->

					<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->


							<div class="row">
								<div class="col-xs-12 no-padding">
									<!-- h3 class="header smaller lighter blue">jQuery dataTables</h3>
									<div class="table-header">
										Results for "Latest Registered Domains"
									</div -->
										
<?
	include('./search_form_list.php');
?>


									<!-- <div class="table-responsive"> -->
									<div class="space-2"></div>
									<!-- <div class="dataTables_borderWrap"> -->
									<div class="scrollable" data-height="400" style="width:100%;height:700px;">
																				
										<div style="width:100%;height:50px;">
										<form name="import_form" action="mgt_list.php" method="post" id="search" enctype="multipart/form-data">
										Display 
										<select name="view_count" id="view_count" onchange="review_proc();" style="height:26px;">
<?
		$array_view_count			= array(10 => 10, 25 => 25, 50 => 50, 100 => 100);
		echo create_option_list($array_view_count, $view_count);
?>
										</select>
										records&nbsp;&nbsp;
	
										<table id="sample-table-1_xx">
											<tr>
												<td><input type="file" name="data_file" value="" size="20"></td>
												<td><input type="button" name="btn_import" value="インポート" onclick="import_proc();" >&nbsp;&nbsp;</td>
												<td><input type="button" name="btn_export" value="エクスポート" onclick="export_proc();" >&nbsp;&nbsp;</td>
												<td>&nbsp;&nbsp;<a href="#" onclick="sample_view();">サンプル</a></td>
											</tr>
										</table>

										<input type="hidden" name="proc_type" value="upload" />
										<input type="hidden" name="proc_step" value="1" />
										<input type="hidden" name="virgin" value="*" />
										</form>	
										</div>					
																
										<br>表示： <?php echo $pageing_contents; ?> 
										&nbsp;&nbsp;&nbsp;&nbsp;


												<table id="sample-table-1_xx" class="table table-stripedxx table-bordered table-hoverxx nowrap xxx">
													<thead>
														<tr>
															<th class="center">ID</th>
															<!--<th>物件ジョブ</th>
															<th>物件ステータス</th>-->
															<th class="center">変更</th>
															<th class="center">削除</th>
															<th>物件ID</th>
															<th>物件名</th>
															<th>物件住所</th>
															<th>部屋番号</th>
															<th>専有面積</th>
															<th>築年数_年</th>
															<th>築年数_月</th>
															<th>構造</th>
															<th>沿線</th>
															<th>駅</th>
															<th>総戸数</th>
															<th>管理会社</th>
															<th>分譲会社</th>
															<th>抵当権者</th>
															<th>ローン残額</th>
															<th>情報取得日</th>
															<th>物件備考</th>
															<th>顧客ID</th>
															<th>顧客名</th>
															<th>顧客郵便番号</th>
															<th>顧客住所1</th>
															<th>顧客住所2</th>
															<th>顧客住所3</th>
															<th>顧客TEL1</th>
															<th>顧客TEL2</th>
															<th>顧客TEL3</th>
															<th>顧客備考</th>
															<th>顧客勤務先</th>
															<th>顧客属性</th>
															<th>DM反響ステータス</th>
															<th>見込み取得日</th>
															<th>PMステータス</th>
															<th>賃料</th>
															<th>管理費</th>
															<th>修繕積立金</th>
															<th>賃貸情報備考</th>
															<th>担当者</th>
														</tr>
													</thead>

													<tbody>
<?php 

	$total_count			= 0;
	$array_bg_color		= array("#FFFFFF", "#EEEEEE");


	$sql = "SELECT e.*, c.name customer_name, c.tel_1, c.tel_2, c.tel_3, ";
	$sql .= "   c.prefecture owner_prefecture, c.address_1 owner_address_1, c.prohibit_flag, c.prefecture customer_prefecture, c.address_1 customer_address_1, c.address_2 customer_address_2 ";
	$sql .= "FROM estate e ";
	$sql .= "LEFT JOIN customer c ON e.customer_id = c.id ";
	$sql .= "WHERE e.active_flag = '1' ";
	//$sql .= "AND (c.call_status IS NULL OR c.call_status NOT IN(5,6,7,8,9)) ";
	// Update 2014/11/04
	//$sql .= "AND (c.call_status IS NULL OR c.call_status NOT IN(5,6,7,8,9,10)) ";
	//$sql .= "AND c.prohibit_flag IS NULL ";
	//$sql .= "AND c.mybox_flag IS NULL ";
	$sql .= $sql_sub;
	$sql .= $sort_string;
	$sql .= "LIMIT $offset, $view_count ";	
	//echo "SQL : $sql<br>";
	$rs 			= mysql_query($sql);
	$seq			= 0;
	while ($item = @mysql_fetch_array($rs)) {
		$estate_id	   					= $item['id'];
		$id_mgt							= $item['id_mgt'];
		$job                        	= $item['job'];
		$status_code                	= $item['status_code'];
		$name                       	= $item['name'];
		$address                    	= $item['address'];
		$room_no                    	= $item['room_no'];
		$area                       	= $item['area'];
		$built_date                 	= $item['built_date'];
		$structure                  	= $item['structure'];
		$railroad_name              	= $item['railroad_name'];
		$station_name               	= $item['station_name'];
		$room_count                 	= $item['room_count'];
		$management_company_name    	= $item['management_company_name'];
		$sell_company_name          	= $item['sell_company_name'];
		$mortgagee                  	= $item['mortgagee'];
		$loan_remnant               	= $item['loan_remnant'];
		$get_date                   	= $item['get_date'];
		$remarks                    	= $item['remarks'];
		$customer_id                	= $item['customer_id'];
		$customer_name              	= $item['customer_name'];
		$customer_staff_id				= $item['customer_staff_id'];
		$customer_zip               	= $item['customer_zip'];
		$customer_prefecture        	= $item['customer_prefecture'];
		$customer_address_1         	= $item['customer_address_1'];
		$customer_address_2         	= $item['customer_address_2'];
		$customer_tel_1             	= $item['customer_tel_1'];
		$customer_tel_2             	= $item['customer_tel_2'];
		$customer_tel_3             	= $item['customer_tel_3'];
		$customer_remarks           	= $item['customer_remarks'];
		$customer_company_name      	= $item['customer_company_name'];
		$customer_evaluate_string   	= $item['customer_evaluate_string'];
		$customer_dm_status         	= $item['customer_dm_status'];
		$customer_expect_date       	= $item['customer_expect_date'];
		$pm_status                  	= $item['pm_status'];
		$fee_rent                   	= $item['fee_rent'];
		$fee_management             	= $item['fee_management'];
		$fee_correct                	= $item['fee_correct'];
		$rent_remarks               	= $item['rent_remarks'];
		
		$bg_color					= $array_bg_color[$total_count % 2];
?>
														<tr style="background-color:<?php echo $bg_color; ?>;cursor:pointer;" id="tr_history_<?php echo $total_count; ?>" onclick="select_proc('<?php echo $total_count; ?>');memo_view('<?php echo $memo_1; ?>');">
															<td class="center"><?php echo $estate_id; ?></td>
															<!--<td><?php echo $job; ?></td>
															<td><?php echo $status_code; ?></td>-->
															<td class="center">	
																<div class="hidden-sm hidden-xs action-buttons">
																	<a class="green" href="#" onclick="update_proc('<?= $estate_id ?>')">
																		<i class="ace-icon fa fa-pencil bigger-130"></i>
																	</a>
																</div>	
																</td>
															<td class="center">	
																<div class="hidden-sm hidden-xs action-buttons">
																	<a class="red" href="#" onclick="delete_proc('<?= $estate_id ?>')">
																		<i class="ace-icon fa fa-trash-o bigger-130"></i>
																	</a>
																</div>	
																</td>
															<td><?php echo $id_mgt; ?></td>
															<td><?php echo $name; ?></td>
															<td><?php echo $address; ?></td>
															<td><?php echo $room_no; ?></td>
															<td><?php echo $area; ?></td>
															<td><?php echo substr($built_date, 0, 4); ?></td>
															<td><?php echo substr($built_date, 5, 2); ?></td>
															<td><?php echo $structure; ?></td>
															<td><?php echo $railroad_name; ?></td>
															<td><?php echo $station_name; ?></td>
															<td><?php echo $room_count; ?></td>
															<td><?php echo $management_company_name; ?></td>
															<td><?php echo $sell_company_name; ?></td>
															<td><?php echo $mortgagee; ?></td>
															<td><?php echo $loan_remnant; ?></td>
															<td><?php echo $get_date; ?></td>
															<td><?php echo $remarks; ?></td>
															<td><?php echo $customer_id; ?></td>
															<td><?php echo $customer_name; ?></td>
															<td><?php echo $customer_zip; ?></td>
															<td><?php echo $customer_prefecture; ?></td>
															<td><?php echo $customer_address_1; ?></td>
															<td><?php echo $customer_address_2; ?></td>
															<td><?php echo $customer_tel_1; ?></td>
															<td><?php echo $customer_tel_2; ?></td>
															<td><?php echo $customer_tel_3; ?></td>
															<td><?php echo $customer_remarks; ?></td>
															<td><?php echo $customer_company_name; ?></td>
															<td><?php echo $customer_evaluate_string; ?></td>
															<td><?php echo $customer_dm_status; ?></td>
															<td><?php echo $customer_expect_date; ?></td>
															<td><?php echo $pm_status; ?></td>
															<td><?php echo $fee_rent; ?></td>
															<td><?php echo $fee_management; ?></td>
															<td><?php echo $fee_correct; ?></td>
															<td><?php echo $rent_remarks; ?></td>
															<td><?php echo $array_staff_id[$customer_staff_id]; ?></td>
														</tr>
<?php
  	  	$total_count++;
	}
?>
													</tbody>
												</table>	
												
									</div>

								</div>
								<!-- start rightcol -->

								</div>

				</div><!-- /.page-content -->
			</div><!-- /.main-content -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='./assets/js/jquery.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='./assets/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='./assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="./assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->
		<script src="./assets/js/jquery.dataTables.min.js"></script>
		<script src="./assets/js/jquery.dataTables.bootstrap.js"></script>
		<script src="./assets/js/chosen.jquery.min.js"></script>		
		<script src="./assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="./assets/js/date-time/bootstrap-timepicker.min.js"></script>
		<script src="./assets/js/jqGrid/jquery.jqGrid.min.js"></script>
		<script src="./assets/js/jqGrid/i18n/grid.locale-en.js"></script>		
		<script src="./assets/js/date-time/moment.min.js"></script>
		<script src="./assets/js/date-time/daterangepicker.min.js"></script>
		<script src="./assets/js/date-time/bootstrap-datetimepicker.min.js"></script>
		<script src="./assets/js/jquery.autosize.min.js"></script>


		<!-- ace scripts -->
		<script src="./assets/js/ace-elements.min.js"></script>
		<script src="./assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = 
				$('#sample-table-2')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.dataTable( {
					bAutoWidth: false,
					"aoColumns": [
					  { "bSortable": false },
					  null, null,null, null, null,
					  { "bSortable": false }
					]
			
					
					//,
					//"sScrollY": "200px",
					//"bPaginate": false,
			
					//"sScrollX": "100%",
					//"sScrollXInner": "120%",
					//"bScrollCollapse": true,
					//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
					//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
					//"iDisplayLength": 50
			    } );
				
			
			
				$(document).on('click', 'th input:checkbox' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					//var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
				$('textarea[class*=autosize]').autosize({append: "\n"});
				$('.chosen-select').chosen({allow_single_deselect:true}); 
				//resize the chosen on window resize
				$(window).on('resize.chosen', function() {
					var w = $('.chosen-select').parent().width();
					$('.chosen-select').next().css({'width':w});
				}).trigger('resize.chosen');				

				$('#chosen-multiple-style').on('click', function(e){
					var target = $(e.target).find('input[type=radio]');
					var which = parseInt(target.val());
					if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
					 else $('#form-field-select-4').removeClass('tag-input-style');
				});	
				//chosen plugin inside a modal will have a zero width because the select element is originally hidden
				//and its width cannot be determined.
				//so we set the width after modal is show
				$('#modal-form').on('shown.bs.modal', function () {
					$(this).find('.chosen-container').each(function(){
						$(this).find('a:first-child').css('width' , '210px');
						$(this).find('.chosen-drop').css('width' , '210px');
						$(this).find('.chosen-search input').css('width' , '200px');
					});
				})
				/**
				//or you can activate the chosen plugin after modal is shown
				//this way select element becomes visible with dimensions and chosen works as expected
				$('#modal-form').on('shown', function () {
					$(this).find('.modal-chosen').chosen();
				})
				*/										
				$('#date-timepicker1').datetimepicker().next().on(ace.click_event, function(){
					$(this).prev().focus();
				});


				//editables on first profile page
				$.fn.editable.defaults.mode = 'inline';
				$.fn.editableform.loading = "<div class='editableform-loading'><i class='ace-icon fa fa-spinner fa-spin fa-2x light-blue'></i></div>";
			    $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="ace-icon fa fa-check"></i></button>'+
			                                '<button type="button" class="btn editable-cancel"><i class="ace-icon fa fa-times"></i></button>';    
				
				//editables 
				
				//text editable
			    $('#username').editable({
					type: 'text',
					name: 'username'
			    });
			
			
				
				//select2 editable
				var countries = [];
			    $.each({ "CA": "Canada", "IN": "India", "NL": "Netherlands", "TR": "Turkey", "US": "United States"}, function(k, v) {
			        countries.push({id: k, text: v});
			    });
			
				var cities = [];
				cities["CA"] = [];
				$.each(["Toronto", "Ottawa", "Calgary", "Vancouver"] , function(k, v){
					cities["CA"].push({id: v, text: v});
				});
				cities["IN"] = [];
				$.each(["Delhi", "Mumbai", "Bangalore"] , function(k, v){
					cities["IN"].push({id: v, text: v});
				});
				cities["NL"] = [];
				$.each(["Amsterdam", "Rotterdam", "The Hague"] , function(k, v){
					cities["NL"].push({id: v, text: v});
				});
				cities["TR"] = [];
				$.each(["Ankara", "Istanbul", "Izmir"] , function(k, v){
					cities["TR"].push({id: v, text: v});
				});
				cities["US"] = [];
				$.each(["New York", "Miami", "Los Angeles", "Chicago", "Wysconsin"] , function(k, v){
					cities["US"].push({id: v, text: v});
				});
				
				var currentValue = "NL";
			    $('#country').editable({
					type: 'select2',
					value : 'NL',
					//onblur:'ignore',
			        source: countries,
					select2: {
						'width': 140
					},		
					success: function(response, newValue) {
						if(currentValue == newValue) return;
						currentValue = newValue;
						
						var new_source = (!newValue || newValue == "") ? [] : cities[newValue];
						
						//the destroy method is causing errors in x-editable v1.4.6+
						//it worked fine in v1.4.5
						/**			
						$('#city').editable('destroy').editable({
							type: 'select2',
							source: new_source
						}).editable('setValue', null);
						*/
						
						//so we remove it altogether and create a new element
						var city = $('#city').removeAttr('id').get(0);
						$(city).clone().attr('id', 'city').text('Select City').editable({
							type: 'select2',
							value : null,
							//onblur:'ignore',
							source: new_source,
							select2: {
								'width': 140
							}
						}).insertAfter(city);//insert it after previous instance
						$(city).remove();//remove previous instance
						
					}
			    });
			
				$('#city').editable({
					type: 'select2',
					value : 'Amsterdam',
					//onblur:'ignore',
			        source: cities[currentValue],
					select2: {
						'width': 140
					}
			    });
			
			
				



			})
		</script>


		<link rel="stylesheet" href="./assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="./docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="./assets/js/ace/ace.onpage-help.js"></script>

		<script src="./docs/assets/js/rainbow.js"></script>
		<script src="./docs/assets/js/language/generic.js"></script>
		<script src="./docs/assets/js/language/html.js"></script>
		<script src="./docs/assets/js/language/css.js"></script>
		<script src="./docs/assets/js/language/javascript.js"></script>

<IFRAME name="sender" style="BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; BORDER-BOTTOM: 0px"
marginwidth="0" marginheight="0" src="interface_get.php" frameborder="0" width="0" height="0" scrolling="NO">
</IFRAME>
<br>
<IFRAME name="receiver" style="BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; BORDER-BOTTOM: 0px"
marginwidth="0" marginheight="0" src="interface_get.php" frameborder="0" width="0" height="0" scrolling="NO">
</IFRAME>

	</body>
</html>