<?php
include 'AppLib.php';

class SampleLib extends AppLib{
    function __construct(){
        App::uses('SampleModel', 'Model');
    }
    function getSampleLib(){
        $app = new SampleModel();
        return $app->getSampleData();
    }
}
