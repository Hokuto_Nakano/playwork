<?php

class Util {
    public static function FormatMonthToStr($yyyymm){
        if(!$yyyymm || strlen($yyyymm) < 6) return '';
        $yyyy = substr($yyyymm, 0, 4);
        $mm = substr($yyyymm, 4, 2);
        return $yyyy . '年' . $mm . '月';
    }

    public static function GetThisMonth(){
        return (int)date("Ym");
    }

    public static function GetYYYY($yyyymm){
        if(!$yyyymm) $yyyymm = date("Ym");
        return substr($yyyymm, 0, 4);
    }
    public static function GetMM($yyyymm){
        if(!$yyyymm) $yyyymm = date("Ym");
        return substr($yyyymm, 4, 2);
    }

    public static function RegistChatInfo($userid, $name, $affiliation, $loginid, $password){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://chat.pwork.biz/users/add");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "X-Requested-With: XMLHttpRequest"));
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            json_encode(array(
                'userid' => $userid,
                'name' => $name,
                'affiliation' => $affiliation,
                'loginid' => $loginid,
                'password' => $password
            )
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        $decoded_output = json_decode($output);
        curl_close($ch);
        return $decoded_output ? $decoded_output->redirect_url : false;
    }
}
