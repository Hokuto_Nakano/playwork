<?php echo $this->Html->script('rankingapbreakdown', array('inline' => false)); ?>
<script>
    init.push(function () {
        //$('#slim').css('position', 'relative');
        //$('#slim').css('height', '620px');
        // $('#slim').css('overflow', 'scroll');
    })
</script>
<style>
    .alt-table-responsive {
        overflow-y: auto;
        overflow-x: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        -webkit-overflow-scrolling: touch;
    }
</style>
<div class="col-sm-2 form-group">
    <label>事業部</label>
    <select id="divisionId" name="divisionId" class="form-control">
        <option value="0">指定なし</option>
        <?php foreach($divisions as $key => $division){ ?>
            <option value="<?php echo($division['id']); ?>" <?php if ($divisionId == $division['id']) {echo 'selected';} ?>><?php echo($division['name']); ?></option>
        <?php } ?>
    </select>
</div>
<div style="float:right;margin-right:24px;text-align:right;">
    <button class="btn btn-default" type="button" onclick="location.href='/ranking_ap_breakdowns/<?php echo $year.$month; ?>/<?php echo $divisionId; ?>/1'">再計算</button>
    <div style="font-size:11px;">最終更新：<?php echo $modified; ?></div>
</div>
<br>
<br>
<div class="col-sm-12 form-group">
    <div id="slim" class="alt-table-responsive">
        <table class="table table-bordered table-striped table-condensed" style="font-size:12px;">
            <thead>
            <tr>
                <th rowspan="3" style="vertical-align:middle;">社員番号</th>
                <th rowspan="3" style="vertical-align:middle;">名前</th>
                <th rowspan="3" style="vertical-align:middle;">職級</th>
                <th rowspan="3" style="vertical-align:middle;">事業部</th>
                <th rowspan="3" style="vertical-align:middle;">合計BP</th>

                <?php foreach ($productCategories as $productCategory) { ?>
                <th colspan="<?php echo count($productCategory['acquisitionType']) ?>"><?php echo $productCategory['name']?></th>
                <?php } ?>
            </tr>
            <tr>
                <?php
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionType'] as $acquisitionType) { ?>
                            <th><?php echo $acquisitionType ?></th>
                    <?php } ?>
                <?php } ?>
            </tr>
            <tr>
                <?php
                foreach ($productCategories as $productCategory) {
                    foreach ($productCategory['point'] as $point) { ?>
                        <th><?php echo $point ?>BP</th>
                    <?php } ?>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
                <?php if (!$staffs) { ?>
                    <tr>
                        <td colspan="<?php echo $colspan ?>">データはありません</td>
                    </tr>
                <?php } ?>
                <?php foreach ($staffs as $staff) { ?>
                <tr>
                    <td><?php echo $staff['staffNo'] ?></td>
                    <td><?php echo $staff['staffName'] ?></td>
                    <td><?php echo $staff['jobType'] ?></td>
                    <td><?php echo $staff['division'] ?></td>
                    <td><?php echo $staff['totalBp'] ?></td>
                    <?php foreach ($staff['count'] as $count) { ?>
                        <?php foreach ($count as $totalCount) { ?>
                            <td><?php echo $totalCount ?></td>
                        <?php } ?>
                    <?php } ?>
                </tr>
            <? } ?>
            </tbody>
        </table>
    </div>
</div>
