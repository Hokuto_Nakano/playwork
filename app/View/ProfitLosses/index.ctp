<?php echo $this->Html->script('profitloss', array('inline' => false)); ?>

<style>
.table>thead:not(:first-child)>tr>th.lead{
  padding-top:12px;
  border-top:none;
}
</style>

<form id="division_form" class="form-group">
  <div class="col-sm-4">
    <label>事業部</label>
    <select id="division_id" name="division_id" class="form-control">
      <option value="0">指定なし</option>
      <?php foreach($divisions as $division){ ?>
        <?php if($division['Division']['id'] == $division_id) { ?>
          <option value="<?php echo $division['Division']['id']; ?>" selected><?php echo $division['Division']['name']; ?></option>
        <?php } else { ?>
          <option value="<?php echo $division['Division']['id']; ?>"><?php echo $division['Division']['name']; ?></option>
        <?php } ?>
      <?php } ?>
    </select>
  </div>
  <div class="col-sm-4">
    <label>エリア</label>
    <select id="area_id" name="area_id" class="form-control">
      <option value="0">指定なし</option>
      <?php foreach($areas as $area){ ?>
        <?php if($area['Area']['id'] == $area_id) { ?>
          <option value="<?php echo $area['Area']['id']; ?>" selected><?php echo $area['Area']['name']; ?></option>
        <?php } else if (!$updateFlg) { ?>
          <option value="<?php echo $area['Area']['id']; ?>"><?php echo $area['Area']['name']; ?></option>
        <?php } ?>
      <?php } ?>
    </select>
  </div>
  <div class="col-sm-4">
    <label>部署</label>
    <select id="section_id" name="section_id" class="form-control">
      <option value="0">指定なし</option>
      <?php foreach($sections as $section){ ?>
        <?php if($section['Section']['id'] == $section_id) { ?>
          <option value="<?php echo $section['Section']['id']; ?>" selected><?php echo $section['Section']['name']; ?></option>
        <?php } else if (!$updateFlg) { ?>
          <option value="<?php echo $section['Section']['id']; ?>"><?php echo $section['Section']['name']; ?></option>
        <?php } ?>
      <?php } ?>
    </select>
  </div>
</form>

<div class="form-group">
  <button id="new" type="button" class="btn btn-primary">新規登録</button>
  <div style="float:right;margin-right:24px;text-align:right;">
      <button class="btn btn-default" type="button" onclick="location.href='/profit_losses/<?php echo $year.$month; ?>?division_id=<?php echo $division_id; ?>&area_id=<?php echo $area_id; ?>&section_id=<?php echo $section_id; ?>&update=1'">再計算</button>
      <div style="font-size:11px;">最終更新：<?php echo $modified; ?></div>
  </div>
</div>
<table class="table">
  <thead>
    <tr>
      <th class="lead">売上</th>
      <th class="lead"><?php echo $totalSale; ?>円</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($sales as $sale){ ?>
      <tr><td><?php echo $sale[0]["product_name"]; ?></td><td><?php echo $sale[0]["sumPrice"]; ?>円</td></tr>
    <?php } ?>
    <?php foreach($plValues as $plValue){ ?>
      <?php if($plValue["PlCategory"]["id"] == 1){ ?>
      <tr id="<?php echo $plValue["PlValue"]["id"]; ?>" class="update clickable"><td><?php echo $plValue["PlItem"]["item_name"]; ?></td><td><?php echo $plValue["PlValue"]["value"]; ?>円</td></tr>
      <?php } ?>
    <?php } ?>
  </tbody>
  <thead>
    <tr>
      <th class="lead">費用</th>
      <th class="lead"><?php echo $totalCost; ?>円</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($costs as $staffType => $cost){ ?>
      <tr><td>人件費：<?php echo $staffType; ?></td><td><?php echo $cost; ?>円</td></tr>
    <?php } ?>
    <?php foreach($plValues as $plValue){ ?>
      <?php if($plValue["PlCategory"]["id"] == 2){ ?>
      <tr id="<?php echo $plValue["PlValue"]["id"]; ?>" class="update clickable"><td><?php echo $plValue["PlItem"]["item_name"]; ?></td><td><?php echo $plValue["PlValue"]["value"]; ?>円</td></tr>
      <?php } ?>
    <?php } ?>
  </tbody>
  <thead>
    <tr class="active">
      <th class="lead">利益A</th>
      <th class="lead <?php echo $profitA< 0 ? "text-warning":"text-info"; ?>"><?php echo $profitA; ?>円</th>
    </tr>
  </thead>
  <thead>
    <tr>
      <th class="lead">自由項目</th>
      <th class="lead"><?php echo $totalFree; ?>円</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($plValues as $plValue){ ?>
    <?php if($plValue["PlCategory"]["id"] == 3){ ?>
    <tr id="<?php echo $plValue["PlValue"]["id"]; ?>" class="update clickable"><td><?php echo $plValue["PlItem"]["item_name"]; ?></td><td><?php echo $plValue["PlValue"]["value"]; ?>円</td></tr>
    <?php } ?>
  <?php } ?>
  </tbody>
  <thead>
    <tr class="info">
      <th class="lead">利益B</th>
      <th class="lead <?php echo $profitA< 0 ? "text-danger":"text-primary"; ?>"><?php echo $profitB; ?>円</th>
    </tr>
  </thead>
</table>

<div class="modal fade" id="plModal" tabindex="-1"></div>
