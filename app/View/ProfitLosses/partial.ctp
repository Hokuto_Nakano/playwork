<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form id="plvalueForm">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">PL項目の値を設定</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <div class="col-sm-12">
              <div><?php echo $base_name; ?></div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-6">
              <label>PLカテゴリー：項目名</label>
              <? if(!$plValue['id']){ ?>
              <select id="item_id" name="item_id" class="form-control">
                <?php foreach($plItems as $plItem){ ?>
                  <option value="<?php echo $plItem['PlItem']['id']; ?>" <?php if ($plValue['item_id'] == $plItem['PlItem']['id']) {echo 'selected';} ?>><?php echo $plItem['PlCategory']['category_name'] ."：". $plItem['PlItem']['item_name']; ?></option>
                <?php } ?>
              </select>
              <?php }else{ ?>
              <span id="item_name" name="item_name" class="form-control" readonly><?= $plValue['category_name'] ."：". $plValue['item_name']; ?></span>
              <input type="hidden" id="item_id" name="item_id" class="form-control" value="<?= $plValue['item_id']; ?>"/>
              <?php } ?>
            </div>
            <div class="col-sm-6">
              <label>金額</label>
              <input type="text" id="value" name="value" class="form-control" value="<?= $plValue['value']; ?>" required/>
            </div>
          </div>
          <input type="hidden" id="plvalue_id" name="plvalue_id" class="form-control" value="<?= $plValue['id']; ?>"/>
          <input type="hidden" id="division_id" name="division_id" class="form-control" value="<?= $plValue['division_id']; ?>"/>
          <input type="hidden" id="area_id" name="area_id" class="form-control" value="<?= $plValue['area_id']; ?>"/>
          <input type="hidden" id="section_id" name="section_id" class="form-control" value="<?= $plValue['section_id']; ?>"/>
      </div>
      <div class="modal-footer">
        <? if(!$plValue['id']){ ?>
          <button type="submit" class="btn btn-primary">登録</button>
        <? }else{ ?>
          <button type="submit" class="btn btn-primary">更新</button>
          <button id="remove" type="button" class="btn btn-danger">削除</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>
