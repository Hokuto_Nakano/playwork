<?= $this->Html->script('incentive', array('inline' => false)); ?>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form onsubmit="incentiveCtrl.regist();return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <?php if($incentive['id']) { ?>
          <h4 class="modal-title">インセンティブ設定（更新）</h4>
        <?php } else { ?>
          <h4 class="modal-title">インセンティブ設定（登録）</h4>
        <?php } ?>
      </div>
      <div class="modal-body">
        <input type="hidden" id="incentive_id" name="incentive_id" value="<?= $incentive['id']; ?>" />
        <div class="form-group">
          <label>インセンティブ名(20文字まで)</label>
          <input type="text" id="incentive_name" name="incentive_name" class="form-control" maxlength="20" value="<?= $incentive['incentive_name']; ?>" required/>
        </div>
          <div class="form-group">
              <label>時給に加算（チェックなしで月給に加算）</label>
              <input type="checkbox" name="hourly_plus">
          </div>
        <div class="form-group">
          <label>集計期間</label>
          <select id="aggregate_period" name="aggregate_period" class="form-control">
            <?php foreach($aggregate_periods as $key => $value){ ?>
            <option value="<?= $key ?>" <?= isset($incentive['aggregate_period'])&&$incentive['aggregate_period']==$key?"selected":"" ?>><?= $value ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label>条件</label>
          <select id="incentive_term" name="incentive_term" class="form-control" onchange="incentiveCtrl.selectTerm()">
            <?php foreach($incentive_terms as $key => $value){ ?>
            <option value="<?= $key ?>" <?= isset($incentive['incentive_term'])&&$incentive['incentive_term']==$key?"selected":"" ?>><?= $value ?></option>
            <?php } ?>
          </select>
        </div>
        <div id="js-ranking_range" class="form-group" style="<?php  echo isset($incentive['ranking_range']) ? '' : 'display:none'; ?>">
          <label>ランキング範囲</label>
          <select id="ranking_range" name="ranking_range" class="form-control">
            <?php foreach($ranking_ranges as $key => $value){ ?>
            <option value="<?= $key ?>" <?= isset($incentive['ranking_range'])&&$incentive['ranking_range']==$key?"selected":"" ?>><?= $value ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label>対象区分</label>
          <select id="target_type" name="target_type" class="form-control">
            <?php foreach($target_types as $key => $value){ ?>
            <option value="<?= $key ?>" <?= isset($incentive['target_type'])&&$incentive['target_type']==$key?"selected":"" ?>><?= $value ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <?php if(!$incentive['disabled_flg']){ ?>
      <div class="modal-footer">
        <?php if($incentive['id']){ ?>
            <button type="submit" class="btn btn-default">更新</button>
        <button type="button" class="btn btn-warning" onclick="incentiveCtrl.delete();">無効</button>
        <?php }else{ ?>
        <button type="submit" class="btn btn-default">登録</button>
        <?php } ?>
        <div id="error" style="color:red;"></div>
      </div>
      <?php } ?>
    </form>
  </div>
</div>
