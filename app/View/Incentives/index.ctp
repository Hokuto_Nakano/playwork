<?php echo $this->Html->script('incentive', array('inline' => false)); ?>

<div class="col-sm-12">
    <button class="btn btn-primary" type="button" onclick="incentiveCtrl.showModal('new')">追加</button>
</div>
<div class="col-sm-12">
    <table class="table">
        <thead>
        <tr>
            <th>インセンティブ名</th>
            <th>加算対象</th>
            <th>集計期間</th>
            <th>条件</th>
            <th>ランキング範囲</th>
            <th>対象区分</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($incentives as $incentive) { ?>
            <tr>
                <td><?= $incentive['incentive_name']; ?></td>
                <td><?= $hourly_pluses[$incentive['hourly_plus']]; ?></td>
                <td><?= $aggregate_periods[$incentive['aggregate_period']]; ?></td>
                <td><?= $incentive_terms[$incentive['incentive_term']]; ?></td>
                <td><?= $incentive['ranking_range'] ? $ranking_ranges[$incentive['ranking_range']] : "-"; ?></td>
                <td><?= $target_types[$incentive['target_type']]; ?></td>
                <td>
                    <button type="button" class="btn btn-xs btn-info"
                            onclick="location.href='/incentive_rules/<?= $incentive['id']; ?>'"
                            style="margin-right:4px;">詳細ルール
                    </button>
                    <button type="button" class="btn btn-xs btn-danger"
                            onclick="incentiveCtrl.delete(<?= $incentive['id']; ?>);">削除
                    </button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<div class="modal fade" id="incentiveModal" tabindex="-1">
</div>
