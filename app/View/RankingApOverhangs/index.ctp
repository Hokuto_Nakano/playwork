<?php echo $this->Html->script('rankingapoverhang', array('inline' => false)); ?>

<input type="hidden" id="isMonitor" value="<?php echo $isMonitor; ?>" />

<div class="col-sm-2 form-group">
    <label style="<?php echo $isMonitor ? 'color:#ddd;' : '' ?>">事業部</label>
    <select id="divisionId" name="divisionId" class="form-control">
        <option value="0">指定なし</option>
        <?php foreach($divisions as $key => $division){ ?>
            <option value="<?php echo($division['id']); ?>" <?php if ($divisionId == $division['id']) {echo 'selected';} ?>><?php echo($division['name']); ?></option>
        <?php } ?>
    </select>
</div>
<div style="float:right;margin-right:24px;margin-bottom:10px;text-align:right;">
    <button style="margin-right: 10px" class="btn btn-default" type="button" onclick="location.href='/ranking_ap_overhangs/<?php echo $year.$month; ?>/<?php echo $divisionId; ?>/0/1'">再計算</button>
    <div style="font-size:11px;">最終更新：<?php echo $modified; ?></div>
    <strong class="lead"><?php echo($count); ?>件</strong>
    <strong class="lead"><?php echo($page); ?> / <?php echo($totalPage); ?></strong>
    <button id="prev" style="margin-left: 10px" class="btn btn-primary" type="button">前へ</button>
    <button id="next" style="margin-left: 10px" class="btn btn-primary" type="button">次へ</button>
</div>
<br>
<br>
<div class="col-sm-12 form-group">
    <table class="table table-bordered table-condensed" style="font-size:12px;">
        <thead style="<?php echo $isMonitor ? 'background:#eee;color:#333;' : '' ?>">
        <tr>
            <th>順位</th>
            <th>前日比</th>
            <th>名前</th>
            <th>RANK</th>
            <th>目標BP</th>
            <th>獲得BP</th>
            <th>進捗</th>
            <th>BP平均値</th>
            <th>稼働時間</th>
            <th>着地想定BP</th>
            <th>当月内取消</th>
            <th>暫定インセン</th>
            <th>時間インセン</th>
            <th>職級</th>
            <th>事業部</th>
        </tr>
        </thead>
        <tbody>
        <?php if (!$staffs) { ?>
            <tr>
                <td colspan="15">データはありません</td>
            </tr>
        <?php } ?>
        <?php
        foreach ($staffs as $rank => $staff) {
            $nameColor = "color:#666;";
            $fontSet = "";
            if($staff['currentRanking'] < 4){
                $nameColor = "color:blue;";
                $fontSet = "font-size:".(string)((4-$staff['currentRanking'])*1.5+12)."px;";
            }
        ?>
            <tr style="<?php echo $isMonitor ? 'background:#fcfcfc;' : '' ?>">
                <td><?php echo $staff['currentRanking'] ?></td>
                <td><?php echo $staff['previousDay'] ?></td>
                <td style="<?php echo $nameColor.$fontSet; ?>"><?php echo $staff['staffName'] ?></td>
                <td><?php echo $staff['rank'] ?></td>
                <td><?php echo $staff['goalPoint'] ?></td>
                <td><?php echo $staff['totalBp'] ?></td>
                <td><?php echo $staff['progress'] ?>%</td>
                <td><?php echo $staff['averageBp'] ?></td>
                <td><?php echo $staff['totalWorkHours'] ?>時間</td>
                <td><?php echo $staff['landingBp'] ?></td>
                <td style="color:red;"><?php echo $staff['kaiyakuCount'] ?></td>
                <td><?php echo $staff['provisionalIncentive'] ?>円</td>
                <td style="color:blue;"><?php echo $staff['hourlyIncentive'] ?>円UP</td>
                <td><?php echo $staff['jobType'] ?></td>
                <td><?php echo $staff['division'] ?></td>
            </tr>
        <? } ?>
        </tbody>
    </table>
</div>
<div id="pageInfo">
    <input type="hidden" name="count" value="<?php echo($count); ?>">
    <input type="hidden" name="page" value="<?php echo($page); ?>">
    <input type="hidden" name="offset" value="<?php echo($offset); ?>">
    <input type="hidden" name="limit" value="<?php echo($limit); ?>">
</div>
