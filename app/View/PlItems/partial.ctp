<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form id="plitemForm">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">PL項目</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <label>PLカテゴリー</label>
              <select id="category_id" name="category_id" class="form-control">
                <?php foreach($categories as $category){ ?>
                  <option value="<?php echo $category['PlCategory']['id']; ?>" <?php if ($plItem['category_id'] == $category['PlCategory']['id']) {echo 'selected';} ?>><?php echo $category['PlCategory']['category_name']; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>PL項目名</label>
              <input type="text" id="item_name" name="item_name" class="form-control" value="<?= $plItem['item_name']; ?>" required/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>計算方法</label>
              <select id="calc" name="calc" class="form-control">
                <?php foreach($pl_calc as $key => $calc){ ?>
                  <option value="<?php echo $key; ?>" <?php if ($plItem['calc'] == $key) {echo 'selected';} ?>><?php echo $calc; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>適用開始月</label>
              <input type="text" id="start_month" name="start_month" class="form-control" maxlength="6" minlength="6" value="<?= $plItem['start_month']; ?>" required/>
            </div>
          </div>
          <input type="hidden" id="plitem_id" name="plitem_id" class="form-control" value="<?= $plItem['id']; ?>"/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">更新</button>
        <? if($plItem['id']){ ?>
        <button id="remove" type="button" class="btn btn-danger">削除</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>
