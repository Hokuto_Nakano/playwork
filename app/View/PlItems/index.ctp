<?php echo $this->Html->script('plitem', array('inline' => true)); ?>

<div class="col-md-4">
  <table class="table table-bordered">
    <thead>
      <tr class="info"><th>分類</th></tr>
    </thead>
    <tbody>
      <tr><td>売上</td></tr>
      <tr><td>費用</td></tr>
      <tr class="active"><td>利益A（売上 + 費用）</td></tr>
      <tr><td>自由項目</td></tr>
      <tr class="active"><td>利益B（利益A + 自由項目）</td></tr>
    </tbody>
  </table>
</div>
<div class="col-md-8">
  <div class="form-group">
    <button id="new" type="button" class="btn btn-primary">新規登録</button>
  </div>
  <table class="table">
    <thead>
      <tr>
        <th>分類</th>
        <th>項目名</th>
        <th>計算方法</th>
        <th>適用開始月</th>
      </tr>
    </thead>
    <tbody>
      <? foreach($plitems as $plitem){ ?>
      <tr id="<?= $plitem['PlItem']['id']; ?>" class="clickable update">
        <td><?= $plitem['PlCategory']['category_name'] ?></td>
        <td><?= $plitem['PlItem']['item_name'] ?></td>
        <td><?= $pl_calc[$plitem['PlItem']['calc']] ?></td>
        <td><?= $plitem['PlItem']['start_month'] ?></td>
      </tr>
      <? } ?>
    </tbody>
  </table>
</div>

<div class="modal fade" id="plItemModal" tabindex="-1"></div>
