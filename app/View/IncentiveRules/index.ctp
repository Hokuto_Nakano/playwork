<?php echo $this->Html->script('incentiverule', array('inline' => false)); ?>

<input type=hidden id="incentive_id" name="incentive_id" value="<?= $incentive_id;?>" />

<form class="row">
    <?php $rankingRange = $incentive['ranking_range'] ?>
    <?php if ($rankingRange == 'division' || $rankingRange == 'area' || $rankingRange == 'section') { ?>

        <div class="col-md-12">
            <label>ランキング範囲（<?php echo($rankingRangeName); ?>）</label>
        </div>
        <div class="col-md-12 form-group">
            <?php if ($rankingRange == 'division' || $rankingRange == 'area' || $rankingRange == 'section') { ?>
            <div class="col-md-2">
                <select id="divisionId" name="divisionId" class="form-control" onchange="incentiveRuleCtrl.changeDivision();">
                    <?php foreach($divisions as $key => $division){ ?>
                        <option value="<?php echo($division['id']); ?>" <?php if ($divisionId == $division['id']) {echo 'selected';} ?>><?php echo($division['name']); ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php } ?>
            <?php if ($rankingRange == 'area' || $rankingRange == 'section') { ?>
            <div class="col-md-2">
                <select id="areaId" name="areaId" class="form-control" onchange="incentiveRuleCtrl.changeArea();">
                    <?php foreach($areas as $key => $area){ ?>
                        <option value="<?php echo($area['id']); ?>" <?php if ($areaId == $area['id']) {echo 'selected';} ?>><?php echo($area['name']); ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php } ?>
            <?php if ($rankingRange == 'section') { ?>
            <div class="col-md-2">
                <select id="sectionId" name="sectionId" class="form-control" onchange="incentiveRuleCtrl.changeSection();">
                    <?php foreach($sections as $key => $section){ ?>
                        <option value="<?php echo($section['id']); ?>" <?php if ($sectionId == $section['id']) {echo 'selected';} ?>><?php echo($section['name']); ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if ($incentiveTerm == 'rank') { ?>
    <div class="col-md-12 form-group">
        <div class="col-md-3">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">ランキング定義</span>
                </div>
                <div>
                    <?php if ($ranking) { ?>
                        <input type="text" name="ranking" class="xs-input" value="<?php echo $ranking ?>" style="font-weight: bold;">位まで
                    <?php } else { ?>
                        <input type="text" name="ranking" class="xs-input" value="5" style="font-weight: bold;">位まで
                    <?php } ?>
                    <div id="hiddenInput">
                        <?php if ($ranking) {
                            $i = 0;
                            while ($i < $ranking) {
                            ?>
                            <input id="rank[]" name="rank[]" type="hidden" value="<?php echo $i+1 ?>位">
                        <?php $i++;}} else { ?>
                            <input id="rank[]" name="rank[]" type="hidden" value="1位">
                            <input id="rank[]" name="rank[]" type="hidden" value="2位">
                            <input id="rank[]" name="rank[]" type="hidden" value="3位">
                            <input id="rank[]" name="rank[]" type="hidden" value="4位">
                            <input id="rank[]" name="rank[]" type="hidden" value="5位">
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <div class="col-md-12 form-group">
        <div class="col-md-3">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">件数定義</span>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>ランク</th>
                        <?php if (INCENTIVE_COUNT_TYPE == 0) { ?>
                        <th>獲得BP</th>
                        <?php } else { ?>
                        <th>獲得件数</th>
                        <?php } ?>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="count_rules">
                    <?php
                    $index = 0;
                    foreach($incentiveCounts as $rankId => $incentiveCount){ ?>
                        <tr id="count_<?= $index++; ?>">
                            <td class="td-input"><input id="rank[]" name="rank[]" class="xs-input" type="text" value="<?= $incentiveCount["rank"]; ?>"/></td>
                            <td class="td-input"><input id="count[]" name="count[]" class="xs-input" type="number" step="0.1" min="0" value="<?= $incentiveCount["count"]; ?>"/>以上</td>
                            <td><button class="btn btn-danger btn-xs" type="button" onclick="incentiveRuleCtrl.deleteRow('count', this.parentNode.parentNode.id)">×</button></td>
                        </tr>
                    <?php } ?>
                    <?php if(count($incentiveCounts) == 0){ ?>
                        <tr id="count_0">
                            <td class="td-input"><input id="rank[]" name="rank[]" class="xs-input" type="text" value=""/></td>
                            <td class="td-input"><input id="count[]" name="count[]" class="xs-input" type="number" step="0.1" min="0" value=""/>以上</td>
                            <td><button class="btn btn-danger btn-xs" type="button" onclick="incentiveRuleCtrl.deleteRow('count', this.parentNode.parentNode.id)">×</button></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td><button class="btn btn-info btn-xs" type="button" onclick="incentiveRuleCtrl.addRow('count')">＋</button></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="col-md-12 text-right">
        <button class="btn btn-primary" type="button" onclick="incentiveRuleCtrl.reflect()">インセンティブ定義に反映</button>
    </div>
    <div class="col-md-12 form-group">
        <div class="col-md-9">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">インセンティブ定義</span>
                </div>
                <table class="table">
                    <thead>
                    <tr id="count_ranks">
                        <?php if ($incentive['target_type'] != 'notype') { ?>
                            <th></th>
                        <?php } ?>
                        <?php foreach($incentiveCounts as $rankId => $incentiveCount){ ?>
                            <th><?= $incentiveCount["rank"]; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody id="pay_rules">
                    <?php
                    $index = 0;
                    foreach($incentivePays as $incentivePay){ ?>
                        <tr id="pay_<?= $index++; ?>">
                            <?php if ($incentive['target_type'] != 'notype') { ?>
                            <td class="td-input">
                                <select id="target[]" name="target[]" class="form-control">
                                    <?php foreach($incentiveTarget as $key => $target){ ?>
                                        <option value="<?php echo($key); ?>" <?php echo $incentivePay["target"] == $key ? "selected" : ""; ?>><?php echo($target); ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <?php } ?>
                            <?php foreach($incentivePay["pays"] as $key => $pay){ ?>
                                <td class="td-input"><input id="pays[]" name="pays[]" class="xs-input" type="number" step="1" min="0" value="<?= $pay; ?>"/>円</td>
                            <?php } ?>
                            <td><button class="btn btn-danger btn-xs" type="button" onclick="incentiveRuleCtrl.deleteRow('pay', this.parentNode.parentNode.id)">×</button></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <?php if ($incentive['target_type'] != 'notype') { ?>
                        <tr>
                            <td><button class="btn btn-info btn-xs" type="button" onclick="incentiveRuleCtrl.addRow('pay')">＋</button></td>
                        </tr>
                    <?php } ?>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-12 text-right">
        <button class="btn btn-primary" type="button" onclick="incentiveRuleCtrl.update()">インセンティブルールを登録（更新）</button>
    </div>
</div>
