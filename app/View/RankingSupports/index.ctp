<?php echo $this->Html->script('rankingsupport', array('inline' => false)); ?>

<div style="float:right;margin-bottom:10px;margin-right:24px;text-align:right;">
    <button class="btn btn-default" type="button" onclick="location.href='/ranking_supports/<?php echo $year.$month; ?>/1'">再計算</button>
    <div style="font-size:11px;">最終更新：<?php echo $modified; ?></div>
</div>
<div class="col-sm-12 form-group">
    <table class="table table-bordered table-striped table-condensed" style="font-size:12px;">
        <thead>
        <tr>
            <th>順位</th>
            <th>前日比</th>
            <th>名前</th>
            <th>役職</th>
            <?php foreach ($productCategories as $productCategory) { ?>
                <th><?php echo $productCategory['name']?></th>
            <?php } ?>
            <th>合計</th>
            <th>稼働時間</th>
        </tr>
        </thead>
        <tbody>
        <?php if(!$staffs) { ?>
            <tr>
                <td colspan="<?php echo $colspan ?>">データはありません</td>
            </tr>
        <?php } ?>
        <?php foreach ($staffs as $rank => $staff) { ?>
            <tr>
                <td><?php echo $staff['currentRanking'] ?></td>
                <td><?php echo $staff['previousDay'] ?></td>
                <td><?php echo $staff['staffName'] ?></td>
                <td><?php echo $staff['jobType'] ?></td>
                <?php foreach ($staff['count'] as $count) { ?>
                    <td><?php echo $count ?></td>
                <?php } ?>
                <td><?php echo $staff['totalCount'] ?></td>
                <td><?php echo $staff['totalWorkHours'] ?>時間</td>
            </tr>
        <? } ?>
        </tbody>
    </table>
</div>
