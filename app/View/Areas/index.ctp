<?= $this->Html->script('base', array('inline' => false)); ?>

<?//= json_encode($bases); ?>

<h3>エリア登録</h3>

<button class="btn btn-primary" onclick="getArea('new');">エリア追加</button>
<button class="btn btn-primary" onclick="getSection('new');">部署追加</button>

<table class="table">
  <thead>
    <tr>
      <!-- <th>id</th> -->
      <th>事業部</th>
      <!-- <th>事業部略名</th> -->
      <th>エリア</th>
      <!-- <th>エリア略名</th> -->
      <th>部署</th>
      <!-- <th>部署略名</th> -->
      <!-- <th></th> -->
    </tr>
  </thead>
  <tbody>
    <? foreach($bases as $base){ ?>
    <tr>
      <td class="clickable" onclick="getDivision('<?= $base['division_id']; ?>');"><?= $base['division_name']; ?></td>
      <?php if($base['area_id']){ ?>
      <td class="clickable" onclick="getArea('<?= $base['area_id']; ?>');"><?= $base['area_name']; ?></td>
      <? }else{ ?>
      <td><?= $base['area_name']; ?></td>
      <? } ?>
      <?php if($base['section_id']){ ?>
      <td class="clickable" onclick="getSection('<?= $base['section_id']; ?>');"><?= $base['section_name']; ?></td>
      <? }else{ ?>
      <td><?= $base['section_name']; ?></td>
      <? } ?>
    </tr>
    <? } ?>
  </tbody>
</table>

<div class="modal fade" id="baseModal" tabindex="-1">
</div>
