<?= $this->Html->script('area', array('inline' => true)); ?>

<script type="text/javascript">
  var areaCtrl = new AreaCtrl();
</script>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form onsubmit="areaCtrl.regist();return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">エリア編集</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <div class="col-md-8">
              <label>事業部</label>
              <div class="dropdown">
                <input type="text" id="division_name" name="division_name" class="form-control" value="<?= $division['name']; ?>" disabled/>
                <input type="hidden" id="division_id" name="division_id" value="<?= $division['id']; ?>"/>
              </div>
            </div>
            <div class="col-md-4">
              <label>事業部省略名</label>
              <input type="text" id="division_short_name" name="division_short_name" class="form-control" value="<?= $division['short_name']; ?>" disabled/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>エリア名</label>
              <input type="text" id="area_name" name="area_name" class="form-control" value="<?= $area['name']; ?>" required/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>エリア省略名</label>
              <input type="text" id="area_short_name" name="area_short_name" class="form-control" value="<?= $area['short_name']; ?>"/>
            </div>
          </div>
          <input type="hidden" id="area_id" name="area_id" class="form-control" value="<?= $area['id']; ?>"/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">更新</button>
        <? if($area['id']){ ?>
        <button type="button" class="btn btn-danger" onclick="areaCtrl.delete('<?= $area['id']; ?>');">削除</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>