<?php echo $this->Html->script('reportmonthlies', array('inline' => false)); ?>

<style>
    .alt-table-responsive {
        overflow-y: auto;
        overflow-x: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        -webkit-overflow-scrolling: touch;
    }
</style>

<div class="col-sm-2 form-group">
    <label>事業部</label>
    <select id="divisionId" name="divisionId" class="form-control">
        <option value="0">指定なし</option>
        <?php foreach($divisions as $key => $division){ ?>
            <option value="<?php echo($division['id']); ?>" <?php if ($divisionId == $division['id']) {echo 'selected';} ?>><?php echo($division['name']); ?></option>
        <?php } ?>
    </select>
</div>
<div style="float:right;margin-top:25px;margin-right:24px;text-align:right;">
    <button class="btn btn-default" type="button" onclick="location.href='/report_monthlies/<?php echo $year.$month; ?>/<?php echo $divisionId; ?>/1'">再計算</button>
    <div style="font-size:11px;">最終更新：<?php echo $modified; ?></div>
</div>
<br>
<br>
<div class="col-sm-12 form-group">
    <div id="slim" class="alt-table-responsive">
        <table class="table table-bordered table-condensed alt-table-responsive" style="font-size:12px;">
            <thead>
            <tr>
                <th rowspan="3" style="vertical-align:middle;">事業部</th>
                <th rowspan="3" style="vertical-align:middle;">エリア</th>
                <th rowspan="3" style="vertical-align:middle;">部署</th>
                <th rowspan="2" colspan="3" style="vertical-align:middle;">BP</th>
                <th rowspan="3" style="vertical-align:middle;">人数</th>
                <th rowspan="3" style="vertical-align:middle;">BP平均値</th>
                <th rowspan="3" style="vertical-align:middle;">獲得件数</th>
                <th rowspan="3" style="vertical-align:middle;">件数平均値</th>
                <th id="colSize" colspan="<?php echo $colspan ?>">BP内訳</th>
            </tr>
            <tr>
                <?php foreach ($productCategories as $productCategory) { ?>
                    <th colspan="<?php echo count($productCategory['acquisitionType']) ?>"><?php echo $productCategory['name']?></th>
                <?php } ?>
            </tr>
            <tr>
                <th>目標</th>
                <th>実績</th>
                <th>達成率</th>
                <?php
                foreach ($productCategories as $productCategory) {
                    foreach ($productCategory['acquisitionType'] as $acquisitionType) { ?>
                        <th><?php echo $acquisitionType ?></th>
                    <?php } ?>
                <?php } ?>
            </tr>

            </thead>
            <tbody>
                <?php if (!$goals) { ?>
                    <tr>
                        <td colspan="<?php echo $colspan + 10 ?>">データはありません</td>
                    </tr>
                <?php } ?>
                <?php $areaIdx = 0; $divisionIdx = 0; ?>
                <?php foreach ($goals as $idx => $goal) { ?>
                <tr>
                    <td><?php echo $goal['divisionName'] ?></td>
                    <td><?php echo $goal['areaName'] ?></td>
                    <td><?php echo $goal['sectionName'] ?></td>
                    <td><?php echo $goal['goalBp'] ?>BP</td>
                    <td><?php echo $goal['totalBp'] ?>BP</td>
                    <td><?php echo $goal['achievementRate'] ?>%</td>
                    <td><?php echo $goal['staffCount'] ?></td>
                    <td><?php echo $goal['averageBp'] ?></td>
                    <td><?php echo $goal['allCount'] ?>件</td>
                    <td><?php echo $goal['averageCount'] ?></td>
                    <?php foreach ($goal['totalCount'] as $count) { ?>
                        <?php foreach ($count as $totalCount) { ?>
                            <td><?php echo $totalCount ?></td>
                        <?php }?>
                    <?php }?>
                </tr>

                    <?php if ( $idx == count($goals)-1 || ($goals[$idx+1]['areaId'] != $goal['areaId']) ) { ?>
                        <tr class="active" style="font-weight: bold;">
                            <td><?php echo $totalAreaData[$areaIdx]['divisionName'] ?></td>
                            <td colspan="2"><?php echo $totalAreaData[$areaIdx]['areaName'] ?></td>
                            <td><?php echo $totalAreaData[$areaIdx]['goalBp'] ?>BP</td>
                            <td><?php echo $totalAreaData[$areaIdx]['totalBp'] ?>BP</td>
                            <td><?php echo $totalAreaData[$areaIdx]['achievementRate'] ?>%</td>
                            <td><?php echo $totalAreaData[$areaIdx]['staffCount'] ?></td>
                            <td><?php echo $totalAreaData[$areaIdx]['averageBp'] ?></td>
                            <td><?php echo $totalAreaData[$areaIdx]['allCount'] ?>件</td>
                            <td><?php echo $totalAreaData[$areaIdx]['averageCount'] ?></td>
                            <?php foreach ($totalAreaData[$areaIdx]['totalCount'] as $count) { ?>
                                <?php foreach ($count as $totalCount) { ?>
                                    <td><?php echo $totalCount ?></td>
                                <?php }?>
                            <?php }?>
                        </tr>
                        <?php $areaIdx++; ?>
                    <?php } ?>

                    <?php if ( $idx == count($goals)-1 || ($goals[$idx+1]['divisionId'] != $goal['divisionId']) ) { ?>
                        <tr class="info" style="font-weight: bold;">
                            <td colspan="3"><?php echo $totalDivisionData[$divisionIdx]['divisionName'] ?></td>
                            <td><?php echo $totalDivisionData[$divisionIdx]['goalBp'] ?>BP</td>
                            <td><?php echo $totalDivisionData[$divisionIdx]['totalBp'] ?>BP</td>
                            <td><?php echo $totalDivisionData[$divisionIdx]['achievementRate'] ?>%</td>
                            <td><?php echo $totalDivisionData[$divisionIdx]['staffCount'] ?></td>
                            <td><?php echo $totalDivisionData[$divisionIdx]['averageBp'] ?></td>
                            <td><?php echo $totalDivisionData[$divisionIdx]['allCount'] ?>件</td>
                            <td><?php echo $totalDivisionData[$divisionIdx]['averageCount'] ?></td>
                            <?php foreach ($totalDivisionData[$divisionIdx]['totalCount'] as $count) { ?>
                                <?php foreach ($count as $totalCount) { ?>
                                    <td><?php echo $totalCount ?></td>
                                <?php }?>
                            <?php }?>
                        </tr>
                        <?php $divisionIdx++; ?>
                    <?php } ?>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
