<?php echo $this->Html->script('login', array('inline' => false)); ?>

<div>
  <div class="container">
    <p style="margin-top:164px;text-align:center;"><img src="img/logo_main.jpg" ></p>
  </div>
  <div class="login_box col-sm-offset-4">
    <form onsubmit="login();return false;">
      <div class="col-sm-3">
        <ul>
          <li><span>LOGIN ID</span></li><li><span>PASS</span></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <ul>
          <li>
            <input type="text" id="loginid" name="loginid" class="form-control" required/>
          </li>
          <li>
            <input type="password" id="password" name="password" class="form-control"/>
          </li>
        </ul>
        <button type="submit" class="btn btn-default">ログイン</button>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>
