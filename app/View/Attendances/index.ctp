<?php echo $this->Html->script('attendance', array('inline' => false)); ?>
<?php echo $this->Html->script('../assets/js/moment.min', array('inline' => false)); ?>
<?php echo $this->Html->script('../assets/datepicker/js/bootstrap-datepicker.min', array('inline' => false)); ?>
<?php echo $this->Html->script('../assets/js/date-time/locales/bootstrap-datepicker.ja.js', array('inline' => false)); ?>
<?php echo $this->Html->css( '../assets/datepicker/css/bootstrap-datepicker.min.css'); ?>
<?php echo $this->Html->script('../assets/js/jquery.blockUI', array('format' => false)); ?>

<div class="col-sm-12">
    <div id="message"></div>
</div>
<div class="col-sm-6">
    <input type="file" name="file" />
</div>
<div class="col-sm-6" style="text-align: right;">
    <button id="updateBtn" class="btn btn-primary" type="button">更新</button>
</div>
<div id='attendance-data' class="col-sm-12">
    <table class="table table-striped">
        <thead>
            <tr>
                <td>社員ID</td>
                <td>社員氏名</td>
                <td>出勤日</td>
                <td>出勤日時（計算前）</td>
                <td>退勤日時（計算前）</td>
                <td>出勤日時（計算後）</td>
                <td>退勤日時（計算後）</td>
                <td>実働時間</td>
                <td>休憩時間</td>
                <td>残業時間</td>
            </tr>
        </thead>
        <tbody>
            <?php if(isset($attendanceData)) { ?>
                <?php foreach($attendanceData as $data) { ?>
                    <tr>
                        <td><?php echo($data['Attendance']['staff_id']); ?></td>
                        <td><?php echo($data['StaffHistory']['name']); ?></td>
                        <td><?php echo($data['Attendance']['attendance_date']); ?></td>
                        <td>
                            <? if(date('H:i:s', strtotime($data['Attendance']['input_attendance_datetime'])) == '00:00:00') { ?>
                                <input type="text" name="inputAttendanceDatetime" size="8" value="00:00:00">
                            <?php } else { ?>
                                <?php echo(date('H:i:s', strtotime($data['Attendance']['input_attendance_datetime']))); ?>
                            <?php } ?>
                        </td>
                        <td>
                            <? if(date('H:i:s', strtotime($data['Attendance']['input_leave_datetime'])) == '00:00:00') { ?>
                                <input type="text" name="inputLeaveDatetime" size="8" value="00:00:00">
                            <?php } else { ?>
                                <?php echo(date('H:i:s', strtotime($data['Attendance']['input_leave_datetime']))); ?>
                            <?php } ?>
                        </td>
                        <td>
                            <? if(date('H:i:s', strtotime($data['Attendance']['output_attendance_datetime'])) == '00:00:00') { ?>
                                <input type="text" name="outputAttendanceDatetime" size="8" value="00:00:00">
                            <?php } else { ?>
                                <?php echo(date('H:i:s', strtotime($data['Attendance']['output_attendance_datetime']))); ?>
                            <?php } ?>
                        </td>
                        <td>
                            <? if(date('H:i:s', strtotime($data['Attendance']['output_leave_datetime'])) == '00:00:00') { ?>
                                <input type="text" name="outputLeaveDatetime" size="8" value="00:00:00">
                            <?php } else { ?>
                                <?php echo(date('H:i:s', strtotime($data['Attendance']['output_leave_datetime']))); ?>
                            <?php } ?>
                        </td>
                        <td><?php echo($data['Attendance']['work_hours']); ?></td>
                        <td><?php echo($data['Attendance']['break_hours']); ?></td>
                        <td>
                            <input type="text" name="over_time" size="8" value="<?php echo($data['Attendance']['over_time']); ?>">
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="10">データはありません</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div class="modal fade" id="attendanceModal" tabindex="-1">
</div>
