<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
  <?php echo $this->Html->charset(); ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>
    <?php //echo $cakeDescription ?>:
    <?php echo $this->fetch('title'); ?>
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  <?php
    echo $this->Html->meta('icon');

    echo $this->Html->css('//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin');
    //echo $this->Html->css('cake.generic');
    echo $this->Html->css('../assets/css/font-awesome.min.css');
    echo $this->Html->css('../assets/stylesheets/bootstrap.min.css');
    echo $this->Html->css('../assets/stylesheets/pixel-admin.min.css');
    echo $this->Html->css('../assets/stylesheets/widgets.min.css');
    echo $this->Html->css('../assets/stylesheets/rtl.min.css');
    echo $this->Html->css('../assets/stylesheets/themes.css');
    //echo $this->Html->css('../assets/stylesheets/themes_wid.css');
    echo $this->Html->css('style');
    echo $this->Html->script('../assets/js/jquery.min.js');
    echo $this->Html->script('../assets/javascripts/bootstrap.min.js');
    echo $this->Html->script('../assets/js/underscore-min.js');
    echo $this->Html->script('common');

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
  ?>

<style>
body{
  background-image: url('/img/monitor.png') !important;
  background-repeat: repeat-x !important;
}
</style>
</head>

<body class="theme-default main-menu-animated second">

<script>var init = [];</script>

<div>

  <?php echo $this->Flash->render(); ?>

  <div id="content-wrapper">

    <h1 style="color:#f0f0f0"><?php echo $header; ?></h1>

    <input type="hidden" id="year" name="year" value="<?= $year; ?>"/>
    <input type="hidden" id="month" name="month" value="<?= $month; ?>"/>

    <?php echo $this->fetch('content'); ?>

  </div>

  <!-- Get jQuery from Google CDN -->
  <!--[if !IE]> -->
    <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">'+"<"+"/script>"); </script>
  <!-- <![endif]-->
  <!--[if lte IE 9]>
    <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
  <![endif]-->


  <!-- Pixel Admin's javascripts -->
  <?php
    // echo $this->Html->script('../assets/javascripts/bootstrap.min.js');
    echo $this->Html->script('../assets/javascripts/pixel-admin.min.js');
  ?>

  <script type="text/javascript">
    init.push(function () {
      // Javascript code here
    })
    window.PixelAdmin.start(init);
  </script>

</body>
</html>
