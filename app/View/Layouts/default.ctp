<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html lang="ja" class="ie8"> <![endif]-->
<!--[if IE 9]>         <html lang="ja" class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="ja" class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
  <?php echo $this->Html->charset(); ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>PlayWork Office</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  <?php
    echo $this->Html->meta('icon');

    echo $this->Html->css('//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin');
    //echo $this->Html->css('cake.generic');
    echo $this->Html->css('../assets/css/font-awesome.min.css');
    echo $this->Html->css('../assets/stylesheets/bootstrap.min.css');
    echo $this->Html->css('../assets/stylesheets/pixel-admin.min.css');
    echo $this->Html->css('../assets/stylesheets/widgets.min.css');
    echo $this->Html->css('../assets/stylesheets/rtl.min.css');
    echo $this->Html->css('../assets/stylesheets/themes.css');
    //echo $this->Html->css('../assets/stylesheets/themes_wid.css');
    echo $this->Html->css('style');
    echo $this->Html->script('../assets/js/jquery.min.js');
    echo $this->Html->script('../assets/javascripts/bootstrap.min.js');
    echo $this->Html->script('../assets/js/underscore-min.js');
    echo $this->Html->script('common');

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
  ?>
<style>
#loading{
  position:fixed;
  left:0;
  right:0;
  top:0;
  bottom:0;
  background-color:rgba(240,240,255,0.7);
  z-index:1000;
  display:none;
}
#loading h2{
  position:absolute;
  top:0;
  bottom:0;
  left:0;
  right:0;
  margin:auto;
  width:100px;
  height:100px;
  color:#000;
}
</style>
<script>
window.onbeforeunload = function(e) {
  if(window.location.href.indexOf('ranking') > -1 || window.location.href.indexOf('report') > -1 || window.location.href.indexOf('profit_losses') > -1){
    Common.startLoading();
  }
};
</script>
</head>
<?php if(Router::url() === '/login') { ?>
<body class="theme-default main-menu-animated top">
<?php }else{ ?>
<body class="theme-default main-menu-animated second">
<?php } ?>

<div id="loading"><h2>Loading...</h2></div>

<script>var init = [];</script>

<?php if(Router::url() === '/login') { ?>
<div>
<?php }else{ ?>
<div id="main-wrapper">
<?php } ?>

  <?php echo $this->element('menu'); ?>

  <?php echo $this->Flash->render(); ?>

  <div id="content-wrapper">

    <?php if(isset($header)){ echo "<h3>".$header."</h3>"; } ?>

    <?php if(isset($year) && isset($month)){ ?>
      <div class="history">
        <input type="number" id="year" name="year" class="year" value="<?= $year; ?>" onchange="Common.onChangeYear()"/>年
        <?php for($i=1; $i<=12; $i++){ ?>
          <label class="month <?= $i==$month ? 'lead' : ''; ?>"><a href="javascript:void(0);" onclick="Common.onChangeMonth('<?= $i; ?>')"><?= $i; ?>月</a></label>
        <?php } ?>
      </div>
      <input type="hidden" id="month" name="month" value="<?= $month; ?>"/>
    <?php } ?>

    <?php echo $this->fetch('content'); ?>

  </div>

  <?php //echo $this->element('sql_dump'); ?>

  <!-- Get jQuery from Google CDN -->
  <!--[if !IE]> -->
    <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">'+"<"+"/script>"); </script>
  <!-- <![endif]-->
  <!--[if lte IE 9]>
    <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
  <![endif]-->


  <!-- Pixel Admin's javascripts -->
  <?php
    // echo $this->Html->script('../assets/javascripts/bootstrap.min.js');
    echo $this->Html->script('../assets/javascripts/pixel-admin.min.js');
  ?>

  <script type="text/javascript">
    init.push(function () {
      // Javascript code here
    })
    window.PixelAdmin.start(init);
  </script>

</body>
</html>
