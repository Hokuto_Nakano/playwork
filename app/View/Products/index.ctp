<?php echo $this->Html->script('product', array('inline' => true)); ?>

<?php //echo json_encode($products); ?>

<div class="form-group">
  <button type="button" class="btn btn-primary" onclick="productCtrl.showDetailModal('new');">新規登録</button>
  <a style="position:relative;left:25px;top:4px;" href="javascript:void(0)" onclick="javascript:location.href='/acquisition_types'">獲得区分・BP設定</a>
</div>
<table class="table">
  <thead>
    <tr>
      <th>商材分類</th>
      <th>商品ファミリー</th>
      <th>ID</th>
      <th>商品名</th>
      <th>価格</th>
    </tr>
  </thead>
  <tbody>
    <? foreach($products as $product){ ?>
    <tr>
      <td class="clickable" onclick="productCtrl.showCategoryModal('<?= $product['product_category_id']; ?>');"><?= $product['product_category_name'] ?></td>
      <td class="clickable" onclick="productCtrl.showFamilyModal('<?= $product['product_family_id']; ?>');"><?= $product['product_family_name'] ?></td>
      <td class="clickable"><?= $product['product_detail_id'] ?></td>
      <td class="clickable" onclick="productCtrl.showDetailModal('<?= $product['product_id']; ?>');"><?= $product['product_detail_name'] ?></td>
      <td class="clickable" onclick="productCtrl.showDetailModal('<?= $product['product_id']; ?>');"><?= $product['price'] ?></td>
    </tr>
    <? } ?>
  </tbody>
</table>

<div class="modal fade" id="productModal" tabindex="-1"></div>
