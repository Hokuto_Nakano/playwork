<?= $this->Html->script('division', array('inline' => true)); ?>

<script type="text/javascript">
  var divisionCtrl = new DivisionCtrl();
</script>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form onsubmit="divisionCtrl.regist();return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">事業部編集</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>事業部名</label>
          <input type="text" id="division_name" name="division_name" class="form-control" value="<?= $division['name']; ?>" required/>
        </div>
        <div class="form-group">
          <label>事業部省略名</label>
          <input type="text" id="division_short_name" name="division_short_name" class="form-control" value="<?= $division['short_name']; ?>"/>
        </div>
        <input type="hidden" id="division_id" name="division_id" value="<?= $division['id']; ?>"/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">更新</button>
        <? if($division['id']){ ?>
        <button type="button" class="btn btn-danger" onclick="divisionCtrl.delete('<?= $division['id']; ?>');">削除</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>