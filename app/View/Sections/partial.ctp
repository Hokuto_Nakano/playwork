<?= $this->Html->script('section', array('inline' => true)); ?>

<script type="text/javascript">
  var sectionCtrl = new SectionCtrl(JSON.parse('<?= json_encode($divisions); ?>'));
</script>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form onsubmit="sectionCtrl.regist();return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title"><?= $section['id'] ? '部署編集' : '拠点登録'; ?></h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <div class="col-md-8">
              <label>事業部</label>
              <? if($section['id']){ ?>
                <input type="text" id="division_name" name="division_name" class="form-control" value="<?= $selectDivision['name']; ?>" disabled/>
              <? }else{ ?>
                <div class="dropdown">
                  <input type="text" id="division_name" name="division_name" class="form-control" data-toggle="dropdown" onkeydown="sectionCtrl.setOldDivisionName(this.value)" onkeyup="sectionCtrl.onChangeDivisionName(this.value)" value="<?= $selectDivision['name']; ?>"/>
                  <ul class="dropdown-menu">
                    <? foreach($divisions as $division){ ?>
                      <li>
                        <a href="javascript:void(0)" onclick="sectionCtrl.selectDivision('<?= $division['Division']['id']; ?>');" ><?= $division['Division']['name']; ?></a>
                      </li>
                    <? } ?>
                  </ul>
                </div>
              <? } ?>
              <input type="hidden" id="division_id" name="division_id" value="<?= $selectDivision['id']; ?>"/>
            </div>
            <div class="col-md-4">
              <label>事業部省略名</label>
              <input type="text" id="division_short_name" name="division_short_name" class="form-control" value="<?= $selectDivision['short_name']; ?>" <?= $section['id'] ? 'disabled' : ''; ?>/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
              <label>エリア</label>
              <? if($section['id']){ ?>
                <input type="text" id="area_name" name="area_name" class="form-control" value="<?= $selectArea['name']; ?>" disabled/>
              <? }else{ ?>
                <div class="dropdown">
                  <input type="text" id="area_name" name="area_name" class="form-control" data-toggle="dropdown" onkeydown="sectionCtrl.setOldAreaName(this.value)" onkeyup="sectionCtrl.onChangeAreaName(this.value)"  value="<?= $selectArea['name']; ?>"/>
                  <ul id="selectAreas" class="dropdown-menu">
                  </ul>
                </div>
              <? } ?>
              <input type="hidden" id="area_id" name="area_id" value="<?= $selectArea['id']; ?>"/>
            </div>
            <div class="col-md-4">
              <label>エリア省略名</label>
              <input type="text" id="area_short_name" name="area_short_name" class="form-control" value="<?= $selectArea['short_name']; ?>" <?= $section['id'] ? 'disabled' : ''; ?>/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>部署名</label>
              <input type="text" id="section_name" name="section_name" class="form-control" value="<?= $section['name']; ?>" required/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>部署省略名</label>
              <input type="text" id="section_short_name" name="section_short_name" class="form-control" value="<?= $section['short_name']; ?>"/>
            </div>
          </div>
          <input type="hidden" id="section_id" name="section_id" class="form-control" value="<?= $section['id']; ?>"/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><?= $section['id'] ? '更新' : '登録' ?></button>
        <? if($section['id']){ ?>
        <button type="button" class="btn btn-danger" onclick="sectionCtrl.delete('<?= $section['id']; ?>');">削除</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>