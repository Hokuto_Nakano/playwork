<?php echo $this->Html->script('pay', array('inline' => false)); ?>
<style>
    .alt-table-responsive {
        overflow-y: auto;
        overflow-x: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        -webkit-overflow-scrolling: touch;
    }
</style>
<div class="form-group">
    <div class="form-inline">
        <a href="/pays/download"><button id="download" class="btn btn-primary" type="button">CSVヘッダー出力</button></a>
        <input type="file" name="csv" style="display: initial;">
        <div style="float:right;margin-right:24px;">
            <strong class="lead"><?php echo($count); ?>件</strong>
            <strong class="lead"><?php echo($page); ?> / <?php echo($totalPage); ?></strong>
            <button id="prev" style="margin-left: 10px" class="btn btn-primary" type="button">前へ</button>
            <button id="next" style="margin-left: 10px" class="btn btn-primary" type="button">次へ</button>
        </div>
    </div>
</div>
<div id="message">
</div>
<div id="contents" class="table-responsive alt-table-responsive" style="overflow: auto">
    <table class="table">
        <thead>
        <tr>
            <td>ID</td>
            <td>氏名</td>
            <td>時給</td>
            <td>時給✖️稼働時間</td>
            <?php foreach ($incentiveNames as $incentiveName) { ?>
                <td><?php echo $incentiveName ?></td>
            <?php } ?>
            <td>突発的インセンティブ</td>
        </tr>
        </thead>
        <tbody>
        <?php if(isset($staff)) { ?>
            <?php foreach($staff as $data) { ?>
                <tr class="clickable">
                    <td>
                        <span><?php echo($data['StaffHistory']['id']); ?></span>
                        <input type="hidden" name="id" value="<?php echo($data['StaffHistory']['id']); ?>">
                    </td>
                    <td><?php echo($data['StaffHistory']['name']); ?></td>
                    <td><?php if (isset($data['StaffPay']['pay'])) { echo $data['StaffPay']['pay']; } else { echo 0; } ?></td>
                    <td>
                        <?php
                            if (isset($data['StaffPay']['pay']) && isset($data['Attendance']['total_work_hours'])) {

                                $times = explode(':', $data['Attendance']['total_work_hours']);
                                $times[1] = $times[1] / 60;

                                $result = $data['StaffPay']['pay'] * $times[0] + ($data['StaffPay']['pay'] * $times[1]);

                                echo(floor($result));
                            } else {
                                echo 0;
                            }
                        ?>
                    </td>
                    <?php foreach ($data['StaffIncentive'] as $staffIncentive) { ?>
                        <td><?php echo $staffIncentive; ?></td>
                    <?php } ?>
                    <td>
                        <?php
                        if (isset($data['StaffPay']['incentive'])) {
                            echo($data['StaffPay']['incentive']);
                        } else {
                            echo 0;
                        }
                        ?>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="10">データはありません</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<div id="pageInfo">
    <input type="hidden" name="count" value="<?php echo($count); ?>">
    <input type="hidden" name="page" value="<?php echo($page); ?>">
    <input type="hidden" name="offset" value="<?php echo($offset); ?>">
    <input type="hidden" name="limit" value="<?php echo($limit); ?>">
</div>
