<?php echo $this->Html->script('staff', array('inline' => false)); ?>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">スタッフ登録</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
              <span class="lead"></span>
          </div>
          <div id="form-id" class="form-group" style="display: none;">
              <label>ID</label>
              <input type="hidden" id="id" name="id" class="form-control" value="<?php echo $id; ?>" />
              <input type="hidden" id="history_id" name="history_id" class="form-control" value="<?php echo $history_id; ?>" />
              <input type="hidden" id="staff_id" name="staff_id" class="form-control" value="<?php echo $staff_id; ?>" />
              <input type="hidden" id="start_month" name="start_month" class="form-control" value="<?php echo $start_month; ?>" />
              <input type="hidden" id="update_flg" name="update_flg" class="form-control" value="<?php echo $updateFlg; ?>" />
              <input type="hidden" id="admin_flg" name="admin_flg" class="form-control" value="<?php echo $adminFlg; ?>" />
          </div>
          <div id="form-staff-no" class="form-group">
              <div class="col-sm-6">
                  <label>社員番号</label>
                  <input type="text" id="staff_no" name="staff_no" class="form-control" value="<?php echo $staff_no; ?>" required/>
              </div>
              <div class="col-sm-6">
                  <label>氏名</label>
                  <input type="text" id="name" name="name" class="form-control" value="<?php echo $name; ?>" required/>
              </div>
          </div>
          <?php if (!$updateFlg) { ?>
              <div id="form-login-id" class="form-group">
                  <div class="col-sm-6">
                      <label>ログインID</label>
                      <input type="text" id="login_id" name="login_id" class="form-control" value="<?php echo $login_id; ?>" required/>
                  </div>
                  <div class="col-sm-6">
                      <label>パスワード</label>
                      <input type="text" id="password" name="password" class="form-control" value="<?php echo $password; ?>" required/>
                  </div>
              </div>
          <?php } ?>
          <?php if ($adminFlg && $updateFlg) { ?>
              <div id="form-login-id" class="form-group">
                  <div class="col-sm-6">
                      <label>パスワード</label>
                      <input type="password" id="password" name="password" class="form-control" value="<?php echo $password; ?>" required/>
                  </div>
                  <div class="col-sm-6">
                      <label>パスワード確認</label>
                      <input type="password" id="password_confirm" name="password_confirm" class="form-control" value="<?php echo $password; ?>" required/>
                  </div>
              </div>
          <?php } ?>
          <div id="form-attendance-id" class="form-group">
              <div class="col-sm-12">
                  <label>外部勤怠システムID</label>
                  <input type="text" id="outside_attendance_id" name="outside_attendance_id" class="form-control" value="<?php echo $outside_attendance_id; ?>" required/>
              </div>
          </div>
          <div id="form-division" class="form-group">
              <div class="col-sm-4">
                  <label>事業部</label>
                  <select id="division_id" name="division_id" class="form-control">
                      <?php foreach($divisions as $division){ ?>
                          <?php if($division['Division']['id'] == $division_id) { ?>
                              <option value="<?php echo $division['Division']['id']; ?>" selected><?php echo $division['Division']['name']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $division['Division']['id']; ?>"><?php echo $division['Division']['name']; ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </div>
              <div class="col-sm-4">
                  <label>エリア</label>
                  <select id="area_id" name="area_id" class="form-control">
                      <?php foreach($areas as $area){ ?>
                          <?php if($area['Area']['id'] == $area_id) { ?>
                              <option value="<?php echo $area['Area']['id']; ?>" selected><?php echo $area['Area']['name']; ?></option>
                          <?php } else if (!$updateFlg) { ?>
                              <option value="<?php echo $area['Area']['id']; ?>"><?php echo $area['Area']['name']; ?></option>
                          <?php } ?>
                      <?php } ?>

                  </select>
              </div>
              <div class="col-sm-4">
                  <label>部署</label>
                  <select id="section_id" name="section_id" class="form-control">
                      <?php foreach($sections as $section){ ?>
                          <?php if($section['Section']['id'] == $section_id) { ?>
                              <option value="<?php echo $section['Section']['id']; ?>" selected><?php echo $section['Section']['name']; ?></option>
                          <?php } else if (!$updateFlg) { ?>
                              <option value="<?php echo $section['Section']['id']; ?>"><?php echo $section['Section']['name']; ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div id="form-jobtype" class="form-group">
              <div class="col-sm-6">
                  <label>職種区分</label>
                  <select id="job_type_id" name="job_type_id" class="form-control">
                      <?php foreach($jobtypes as $jobtype){ ?>
                          <?php if($jobtype['JobType']['id'] == $jobtype_id) { ?>
                              <option value="<?php echo $jobtype['JobType']['id']; ?>" selected><?php echo $jobtype['JobType']['type']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $jobtype['JobType']['id']; ?>"><?php echo $jobtype['JobType']['type'] ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </div>
              <div class="col-sm-6">
                  <label>社員区分</label>
                  <select id="staff_type_id" name="staff_type_id" class="form-control">
                      <?php foreach($stafftypes as $stafftype){ ?>
                          <?php if($stafftype['StaffType']['id'] == $stafftype_id) { ?>
                              <option value="<?php echo $stafftype['StaffType']['id']; ?>" selected><?php echo $stafftype['StaffType']['type']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $stafftype['StaffType']['id']; ?>"><?php echo $stafftype['StaffType']['type'] ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div id="form-callcenter" class="form-group">
              <div class="col-sm-6">
                  <label>コールセンター区分</label>
                  <select id="call_center_id" name="call_center_id" class="form-control">
                      <?php foreach($callcenters as $callcenter){ ?>
                          <?php if($callcenter['CallCenter']['id'] == $callcenter_id) { ?>
                              <option value="<?php echo $callcenter['CallCenter']['id']; ?>" selected><?php echo $callcenter['CallCenter']['name']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $callcenter['CallCenter']['id']; ?>"><?php echo $callcenter['CallCenter']['name'] ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </div>
              <div class="col-sm-6">
                  <label>シフト区分</label>
                  <select id="shift_type_id" name="shift_type_id" class="form-control">
                      <?php foreach($shifttypes as $shifttype){ ?>
                          <?php if($shifttype['ShiftType']['id'] == $shifttype_id) { ?>
                              <option value="<?php echo $shifttype['ShiftType']['id']; ?>" selected><?php echo $shifttype['ShiftType']['type']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $shifttype['ShiftType']['id']; ?>"><?php echo $shifttype['ShiftType']['type'] ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div id="form-hire-date" class="form-group">
              <div class="col-sm-6">
                  <label>会社名</label>
                  <select id="company_id" name="company_id" class="form-control">
                      <?php foreach($companies as $company){ ?>
                          <?php if($company['Company']['id'] == $company_id) { ?>
                              <option value="<?php echo $company['Company']['id']; ?>" selected><?php echo $company['Company']['name']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $company['Company']['id']; ?>"><?php echo $company['Company']['name'] ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </div>
              <div class="col-sm-6">
                  <label>入社日</label>
                  <input type="text" id="hire_int_date" name="hire_int_date" class="form-control" value="<?php echo $hire_int_date; ?>" required/>
              </div>
          </div>
      </div>
      <div class="modal-footer">
          <button id="registBtn" type="submit" class="btn btn-primary">登録</button>
          <button id="deleteBtn" type="submit" class="btn btn-danger">削除</button>
          <?php if ($updateFlg) { ?>
              <button id="disabledBtn" type="submit" class="btn btn-warning">無効</button>
          <?php } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>
