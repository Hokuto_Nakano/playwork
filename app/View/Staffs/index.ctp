<?php echo $this->Html->script('staff', array('inline' => false)); ?>

<div class="form-group">
    <div class="form-inline">
        <button id="insert" class="btn btn-primary" type="button">スタッフ登録</button>
        <a href="/staffs/download"><button id="download" class="btn btn-primary" type="button">CSVヘッダー出力</button></a>
        <input type="file" name="file" style="display: initial;">
        <div style="float:right;margin-right:24px;">
            <strong class="lead"><?php echo($count); ?>件</strong>
            <strong class="lead"><?php echo($page); ?> / <?php echo($totalPage); ?></strong>
            <button id="prev" style="margin-left: 10px" class="btn btn-primary" type="button">前へ</button>
            <button id="next" style="margin-left: 10px" class="btn btn-primary" type="button">次へ</button>
        </div>
    </div>
</div>
<div id="message">
</div>
<table class="table">
    <thead>
        <tr>
            <td>ID</td>
            <td>社員番号</td>
            <td>氏名</td>
            <td>外部勤怠システムID</td>
            <td>事業部</td>
            <td>エリア</td>
            <td>部署</td>
            <td>職種区分</td>
            <td>社員区分</td>
            <td>コールセンター区分</td>
            <td>シフト区分</td>
            <td>会社名</td>
            <td>入社日</td>
            <td></td>
        </tr>
    </thead>
    <tbody>
        <?php if(isset($staff)) { ?>
            <?php foreach($staff as $data) { ?>
                <tr class="clickable">
                    <td><span class="id"><?php echo($data['hist']['id']); ?></span></td>
                    <td><span class="staff_no"><?php echo($data['stf']['staff_no']); ?></span></td>
                    <td><span class="name"><?php echo($data['hist']['name']); ?></span></td>
                    <td><span class="outside_attendance_id"><?php echo($data['stf']['outside_attendance_id']); ?></span></td>
                    <td><span class="division_name"><?php echo($data['divis']['name']); ?></span></td>
                    <td><span class="area_name"><?php echo($data['area']['name']); ?></span></td>
                    <td><span class="section_name"><?php echo($data['sec']['name']); ?></span></td>
                    <td><span class="job_type"><?php echo($data['job']['type']); ?></span></td>
                    <td><span class="staff_type"><?php echo($data['sfttype']['type']); ?></span></td>
                    <td><span class="call_center"><?php echo($data['cc']['name']); ?></span></td>
                    <td><span class="shift_type"><?php echo($data['shift']['type']); ?></span></td>
                    <td><span class="shift_type"><?php echo($data['company']['name']); ?></span></td>
                    <td><span class="hire_int_date"><?php echo($data['stf']['hire_int_date']); ?></span></td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="12">データはありません</td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<div id="pageInfo">
    <input type="hidden" name="count" value="<?php echo($count); ?>">
    <input type="hidden" name="page" value="<?php echo($page); ?>">
    <input type="hidden" name="offset" value="<?php echo($offset); ?>">
    <input type="hidden" name="limit" value="<?php echo($limit); ?>">
</div>
<div class="modal fade" id="staffModal" tabindex="-1">
</div>
