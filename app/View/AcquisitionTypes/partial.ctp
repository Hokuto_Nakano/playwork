<?= $this->Html->script('acquisitionType', array('inline' => true)); ?>

<?php //echo json_encode($acquisitionType); ?>
<script type="text/javascript">
  var acquisitionTypeCtrl = new AcquisitionTypeCtrl(JSON.parse('<?= json_encode($categories); ?>'));
</script>


<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form onsubmit="acquisitionTypeCtrl.regist();return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">獲得区分・ポイント登録</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="category_id" name="category_id" value="<?= $category['id']; ?>"/>
        <input type="hidden" id="acquisition_type_id" name="acquisition_type_id" value="<?= $acquisitionType['id']; ?>"/>
        <input type="hidden" id="start_month" name="start_month" value="<?= $year.$month; ?>"/>

        <div class="form-group">
          <div class="col-md-12">
            <label>商品カテゴリ名</label>
            <? if($acquisitionType['id']){ ?>
              <input type="text" id="category_name" name="category_name" class="form-control" value="<?= $category['name']; ?>" <?= $acquisitionType['id'] ? 'disabled' : ''; ?>/>
            <? }else{ ?>
              <div class="dropdown">
                <button type="button" class="btn btn-default form-control" id="category_name" name="category_name" data-toggle="dropdown"><?= $category['name']; ?></button>
                <ul class="dropdown-menu">
                  <? foreach($categories as $category){ ?>
                    <li>
                      <a href="javascript:void(0)" onclick="acquisitionTypeCtrl.selectCategory('<?= $category['ProductCategory']['id']; ?>');" ><?= $category['ProductCategory']['name']; ?></a>
                    </li>
                  <? } ?>
                </ul>
              </div>
            <? } ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-6">
            <label>獲得区分</label>
            <input type="text" id="acquisition_type" name="acquisition_type" class="form-control" value="<?= $acquisitionType['acquisition_type']; ?>" required/>
            <? if($acquisitionType['id']){ ?><div class="text-danger">※すべての月に反映されます</div><? } ?>
          </div>
          <div class="col-md-6">
            <label><?= $year."年".$month."月の獲得ポイント" ?></label>
            <input type="text" id="point" name="point" class="form-control" value="<?= $acquisitionType['point']; ?>" required/>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">更新</button>
        <? if($acquisitionType['id']){ ?>
        <button type="button" class="btn btn-danger" onclick="acquisitionTypeCtrl.delete('<?= $acquisitionType['id']; ?>');">削除</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>