<?php echo $this->Html->script('acquisitiontype', array('inline' => true)); ?>

<?php //echo json_encode($acquisitionTypes); ?>
<script type="text/javascript">
  var acquisitionTypeCtrl = new AcquisitionTypeCtrl();
</script>

<div class="form-group">
  <button type="button" class="btn btn-primary" onclick="acquisitionTypeCtrl.showModal('new')">追加</button>
  <a style="position:relative;left:25px;top:4px;" href="javascript:void(0)" onclick="javascript:location.href='/products'">商材一覧へ戻る</a>
</div>
<table class="table">
  <thead>
    <tr>
      <th>商材分類</th>
      <th>ID</th>
      <th>獲得区分</th>
      <th>獲得ポイント</th>
    </tr>
  </thead>
  <tbody>
    <? foreach($acquisitionTypes as $acquisitionType){ ?>
    <tr>
      <td><?= $acquisitionType['product_category_name'] ?></td>
      <td><?= $acquisitionType['acquisition_type_id'] ?></td>
      <td class="clickable" onclick="acquisitionTypeCtrl.showModal('<?= $acquisitionType['acquisition_type_id']; ?>');"><?= $acquisitionType['acquisition_type'] ?></td>
      <td class="clickable" onclick="acquisitionTypeCtrl.showModal('<?= $acquisitionType['acquisition_type_id']; ?>');"><?= $acquisitionType['point'] ?></td>
    </tr>
    <? } ?>
  </tbody>
</table>

<div class="modal fade" id="acquisitionTypeModal" tabindex="-1"></div>
