<?php echo $this->Html->script('productdetail', array('inline' => false)); ?>

<?//= json_encode($productPlans); ?>

<h3>プラン・価格情報</h3>
<div>
  <button class="btn btn-primary" type="button" onclick="getProductPlan('new')">新規登録</button>
  <a class="btn btn-default" type="button" href="./product_details" onclick="javascript:void(0)">詳細登録</a>
</div>
<div>
  <table class="table">
    <thead>
      <tr>
        <td>商材名</td>
        <td>サービス名</td>
        <td>プラン名</td>
        <td>省略名</td>
        <td>価格</td>
        <td></td>
      </tr>
    </thead>
    <tbody>
    <?php
      $beforeProductName = "";
      $productName = "";
      $beforeDetailName = "";
      $detailName = "";
      for($i=0; $i<count($productPlans); $i++){
        $productName = ($beforeProductName != "" && $beforeProductName == $productPlans[$i]['Product']['name']) ? "" : $productPlans[$i]['Product']['name'];
        $detailName = ($beforeDetailName != "" && $beforeDetailName == $productPlans[$i]['ProductDetail']['name']) ? "" : $productPlans[$i]['ProductDetail']['name'];
    ?>
      <tr>
        <td><span id="productName<?= $i ?>"><?= $productName ?></span></td>
          <td><span id="detailName<?= $i ?>"><?= $detailName ?></span></td>
        <td><span id="name<?= $i ?>"><?= $productPlans[$i]['ProductPlan']['name'] ?></span></td>
        <td><span id="short_name<?= $i ?>"><?= $productPlans[$i]['ProductPlan']['short_name'] ?></span></td>
        <td><span id="price<?= $i ?>"><?= $productPlans[$i]['ProductPrice'][0]['price'] ?></span></td>
        <td>
          <button type="button" class="btn btn-primary" data-toggle="modal" onclick="getProductPlan('<?= $productPlans[$i]['ProductPlan']['id']; ?>')">更新</button>
          <span>&nbsp;</span>
          <button type="button" class="btn btn-danger" onclick="deleteProductPlan('<?= $productPlans[$i]['ProductPlan']['id']; ?>')">削除</button>
        </td>
      </tr>
      <?php $beforeProductName = $productPlans[$i]['Product']['name']; ?>
      <?php } ?>
    </tbody>
  </table>
</div>

<div class="modal fade" id="productPlanModal" tabindex="-1">
</div>