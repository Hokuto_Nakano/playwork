<div>
    <a href="/staff_types">社員区分管理</a>
</div>
<div>
    <a href="/shift_types">シフト区分管理</a>
</div>
<div>
    <a href="/job_types">職種管理</a>
</div>
<div>
    <a href="/bases">拠点管理</a>
</div>
<div>
    <a href="/call_centers">コールセンター管理</a>
</div>
<div>
    <a href="/staffs">スタッフ管理</a>
</div>
<div>
    <a href="/products">商材管理</a>
</div>
<div>
    <a href="/attendances_settings">勤怠設定</a>
</div>
<div>
    <a href="/attendances">勤怠管理</a>
</div>
<div>
    <a href="/input_formats">フォーマット入出力</a>
</div>
