<?php echo $this->Html->script('paydetail', array('inline' => false)); ?>
<div id="message"></div>
<div class="col-md-12">
    <h4><?php echo($name) ?> <?php echo($startYear.'年') ?><?php echo($startMonth.'月') ?></h4>
</div>
<br>
<input type="hidden" name="staffHistoryId" value="<?php echo($staffHistoryId) ?>">
<div class="col-md-12 form-group">
    <button type="button" class="btn btn-primary update">更新</button>
</div>
<div class="col-md-6 table-responsive">
    <table class="table table-bordered">
        <tbody>
            <tr>
                <td>時給</td>
                <td><?php echo($pay) ?></td>
            </tr>
            <tr>
                <td>稼働時間</td>
                <td><?php echo($totalWorkHours) ?></td>
            </tr>
            <tr>
                <td>時給✖️稼働時間</td>
                <td><?php echo($payCalc) ?></td>
            </tr>

            <?php foreach ($staffIncentives as $staffIncentive) { ?>
                <tr>
                    <td><?php
                            if ($staffIncentive['Incentive']['hourly_plus']) {
                                echo '時給インセンティブ合計';
                            } else {
                                echo '月給インセンティブ合計';
                            }
                        ?></td>
                    <td><?php echo($staffIncentive[0]['incentive']) ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td>突発的インセンティブ</td>
                <td>
                    <input type="text" name="incentive" value="<?php echo($incentive) ?>" style="width: 80px;">
                </td>
            </tr>
            <tr>
                <td>目標BP</td>
                <td>
                    <input type="text" name="bp" value="<?php echo($bp) ?>" style="width: 80px;">
                </td>
            </tr>
        </tbody>
     </table>

    <table class="table table-bordered">
        <thead>
        <tr>
            <td>時給/月給</td>
            <td>インセンティブ名</td>
            <td>対象年月</td>
            <td>インセンティブ額</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($staffIncentivesBreakdowns as $staffIncentiveBreakdown) { ?>
            <tr>
                <td>
                    <?php
                        if($staffIncentiveBreakdown['Incentive']['hourly_plus']) {
                            echo '時給';
                        } else {
                            echo '月給';
                        }
                    ?>
                </td>
                <td><?php echo($staffIncentiveBreakdown['Incentive']['incentive_name']); ?></td>
                <td><?php
                    $month = $staffIncentiveBreakdown['StaffIncentive']['month'];
                    if ($month) {echo $month; } else { echo '累計';}
                    ?>
                </td>
                <td><?php echo($staffIncentiveBreakdown['StaffIncentive']['incentive']) ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

