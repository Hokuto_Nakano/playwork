<?= $this->Html->script('productcategory', array('inline' => true)); ?>

<script type="text/javascript">
  var categoryCtrl = new ProductCategoryCtrl();
</script>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form onsubmit="categoryCtrl.regist();return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">商品カテゴリ編集</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <div class="col-md-12">
            <label>商品カテゴリ名</label>
            <input type="text" id="product_category_name" name="product_category_name" class="form-control" value="<?= $category['name']; ?>" required/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label>商品カテゴリ省略名</label>
            <input type="text" id="product_category_short_name" name="product_category_short_name" class="form-control" value="<?= $category['short_name']; ?>"/>
          </div>
        </div>
        <input type="hidden" id="product_category_id" name="product_category_id" value="<?= $category['id']; ?>"/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">更新</button>
        <? if($category['id']){ ?>
        <button type="button" class="btn btn-danger" onclick="categoryCtrl.delete('<?= $category['id']; ?>');">削除</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>