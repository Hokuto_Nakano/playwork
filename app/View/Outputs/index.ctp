<?php echo $this->Html->script('output', array('inline' => false)); ?>

<div class="row">
    <div class="col-sm-2 form-group">
        <label>事業部</label>
        <select id="divisionId" name="divisionId" class="form-control">
            <option value="0">指定なし</option>
            <?php foreach ($divisions as $key => $division) { ?>
                <option value="<?php echo($division['id']); ?>" <?php if ($divisionId == $division['id']) {
                    echo 'selected';
                } ?>><?php echo($division['name']); ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-sm-2 form-group">
        <label>エリア</label>
        <select id="areaId" name="areaId" class="form-control">
            <option value="0">指定なし</option>
            <?php foreach($areas as $area){ ?>
                <?php if($area['Area']['id'] == $areaId) { ?>
                    <option value="<?php echo $area['Area']['id']; ?>" selected><?php echo $area['Area']['name']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $area['Area']['id']; ?>"><?php echo $area['Area']['name']; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </div>
    <div class="col-sm-2 form-group">
        <label>部署</label>
        <select id="sectionId" name="sectionId" class="form-control">
            <option value="0">指定なし</option>
            <?php foreach($sections as $section){ ?>
                <?php if($section['Section']['id'] == $sectionId) { ?>
                    <option value="<?php echo $section['Section']['id']; ?>" selected><?php echo $section['Section']['name']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $section['Section']['id']; ?>"><?php echo $section['Section']['name']; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <button id="ranking_ap_overhangs" class="download btn btn-lg" type="button">アポラン（貼出用）</button>
    </div>
    <div class="form-group">
        <button id="ranking_ap_breakdowns" class="download btn btn-lg" type="button">アポラン（内訳）</button>
    </div>
    <div class="form-group">
        <button id="ranking_ap_countdowns" class="download btn btn-lg" type="button">目標件数カウントダウン</button>
    </div>
    <div class="form-group">
        <button id="ranking_supports" class="download btn btn-lg" type="button">サポート処理件数</button>
    </div>
    <div class="form-group">
        <button id="report_monthlies" class="download btn btn-lg" type="button">月報</button>
    </div>
    <div class="form-group">
        <button id="profit_losses" class="download btn btn-lg" type="button">PL</button>
    </div>
</div>
