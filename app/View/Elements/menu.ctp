<?php if(Router::url() === '/login') { ?>

<!-- <div id="main-menu-bg"></div>

<div id="main-menu" role="navigation">
  <div id="main-menu-inner">
    <ul class="navigation">
      <li>
        <a href="index.html"><img src="/img/logo.jpg"></a>
      </li>
      <li>
        <a href="index.html"><img src="/img/btn_01.jpg"></a>
      </li>
      <li>
        <a href="index.html"><img src="/img/btn_02.jpg"></a>
      </li>
      <li>
        <a href="index.html"><img src="/img/btn_03.jpg"></a>
      </li>
      <li>
        <a href="index.html"><img src="/img/btn_04.jpg"></a>
      </li>
    </ul>
  </div>
</div> -->

<?php }else{

  App::uses('Incentive', 'Model');
  $incentiveModel = new Incentive();
  $incentives = $incentiveModel->getList();

?>

<div id="main-menu-bg" class="second_left bg-gray"></div>

<div id="main-menu" role="navigation">
  <div id="main-menu-inner">
    <div id="main-menu-logo" class="bg-lightgray" style="min-height:150px;position: relative;">
        <a style="text-align:center;" href="/"><img src="/img/logo_03.png" style="position: absolute;right: 0;bottom: 0;left: 0;margin: auto;max-width: 200px;"></a>
    </div>
    <ul class="navigation">
      <li style="background-color: #00a0e9 !important;text-align: center;color: #fff;margin-bottom: 3px;padding: 7px;>
          <span class="mm-text">Menu</span></a>
      </li>

        <?php if ($this->Session->read('authority') != 0) { ?>
      <li>
          <a href="/input_formats"><i class="menu-icon fa fa-file-text"></i><span class="mm-text">獲得データ入出力</span></a>
      </li>
      <li>
          <a href="/outputs"><i class="menu-icon fa fa-file-text"></i><span class="mm-text">ファイル出力</span></a>
      </li>
      <li>
          <a href="/attendances"><i class="menu-icon fa fa-file-text"></i><span class="mm-text">勤怠データ入出力</span></a>
      </li>
      <li class="mm-dropdown">
        <a href="#"><i class="menu-icon fa fa-list-alt"></i><span class="mm-text mmc-dropdown-delay animated fadeIn">ランキング</span></a>
        <ul class="mmc-dropdown-delay animated fadeInLeft" style="">
          <li>
            <a tabindex="-1" href="/ranking_ap_overhangs"><span class="mm-text">アポラン（貼出用）</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/ranking_ap_breakdowns"><span class="mm-text">アポラン（内訳）</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/ranking_ap_countdowns"><span class="mm-text">目標件数カウントダウン</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/ranking_supports"><span class="mm-text">サポート処理件数</span></a>
          </li>
        </ul>
      </li>
      <li class="mm-dropdown">
        <a href="#"><i class="menu-icon fa fa-calculator"></i><span class="mm-text mmc-dropdown-delay animated fadeIn">集計</span></a>
        <ul class="mmc-dropdown-delay animated fadeInLeft" style="">
          <li>
            <a tabindex="-1" href="/report_monthlies"><span class="mm-text">月報</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/profit_losses"><span class="mm-text">PL</span></a>
          </li>
        </ul>
      </li>
      <li class="mm-dropdown">
        <a href="#"><i class="menu-icon fa fa-users"></i><span class="mm-text mmc-dropdown-delay animated fadeIn">スタッフ管理</span></a>
        <ul class="mmc-dropdown-delay animated fadeInLeft" style="">
          <li>
            <a tabindex="-1" href="/staffs"><span class="mm-text">スタッフ一覧</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/pays"><span class="mm-text">給与・インセンティブ</span></a>
          </li>
        </ul>
      </li>
      <li class="mm-dropdown">
        <a href="#"><i class="menu-icon fa fa-briefcase"></i><span class="mm-text mmc-dropdown-delay animated fadeIn">商材管理</span></a>
        <ul class="mmc-dropdown-delay animated fadeInLeft" style="">
          <li>
            <a tabindex="-1" href="/products"><span class="mm-text">商材一覧</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/acquisition_types"><span class="mm-text">獲得区分・ポイント</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/formats"><span class="mm-text">フォーマット</span></a>
          </li>
        </ul>
      </li>
      <li class="mm-dropdown">
        <a tabindex="-1" href="#"><i class="menu-icon fa fa-gg-circle"></i><span class="mm-text mmc-dropdown-delay animated fadeIn">インセンティブ管理</span></a>
        <ul>
          <?php foreach($incentives as $incentive){ ?>
          <li>
            <a tabindex="-1" href="/incentive_rules/<?= $incentive["id"]; ?>"><span class="mm-text"><?= $incentive["incentive_name"]; ?></span></a>
          </li>
          <?php } ?>
        </ul>
      </li>
        <?php if ($this->Session->read('authority') == 10) { ?>
      <li class="mm-dropdown">
        <a href="#"><i class="menu-icon fa fa-gears"></i><span class="mm-text mmc-dropdown-delay animated fadeIn">マスタ設定</span></a>
        <ul class="mmc-dropdown-delay animated fadeInLeft" style="">
          <li>
            <a tabindex="-1" href="/bases"><span class="mm-text">拠点設定</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/staff_types"><span class="mm-text">社員区分設定</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/shift_types"><span class="mm-text">シフト区分設定</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/job_types"><span class="mm-text">職種設定</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/call_centers"><span class="mm-text">コールセンター設定</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/attendances_settings"><span class="mm-text">勤怠設定</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/companies"><span class="mm-text">会社設定</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/incentives"><span class="mm-text">インセンティブ設定</span></a>
          </li>
          <li>
              <a tabindex="-1" href="/goals"><span class="mm-text">拠点目標設定</span></a>
          </li>
          <li>
              <a tabindex="-1" href="/pl_items"><span class="mm-text">PL項目設定</span></a>
          </li>
          <li>
            <a tabindex="-1" href="/count_items"><span class="mm-text">カウント項目設定</span></a>
          </li>
        </ul>
      </li>
        <?php } ?>
      <li>
        <a href="/monitor" target="_blank"><i class="menu-icon fa fa-tv"></i><span class="mm-text">モニター表示<i class="menu-icon fa fa-external-link" style="font-size:8px;"></i></span></a>
      </li>
        <?php } ?>
      <li>
        <a href="/chat" target="_blank"><i class="menu-icon fa fa-comment"></i><span class="mm-text">チャット<i class="menu-icon fa fa-external-link" style="font-size:8px;"></i></span></a>
      </li>
        <li>
            <a href="/account"><i class="menu-icon fa fa-users"></i><span class="mm-text">アカウント管理</span></a>
        </li>
    </ul>
    <div class="menu-content btn btn-primary">
      <a href="/logout" style="color:#f0f0f0;">ログアウト</a>
    </div>
  </div>
</div>

<?php } ?>
