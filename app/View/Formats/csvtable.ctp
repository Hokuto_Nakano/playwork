<?php echo $this->Html->script('format', array('inline' => false)); ?>

<table class="table table-striped">
    <thead>
        <td>必要</td>
        <td>ユニークキー</td>
        <td>名寄せキー</td>
        <td>CSVカラム</td>
        <td>データ型</td>
    </thead>
    <tbody>
        <%
            var nameCollectingKey = nameCollecting.replace('column','');
        %>
        <% _.each(headerNames, function(data, i) { %>
            <tr>
                <td><input type="checkbox" name="row<%= i %>" checked></td>
                <td>
                    <% if (_.isEmpty(commonKeyColumns)) { %>
                        <input type="checkbox" name="uniq<%= i %>">
                    <% } else { %>
                        <% if (commonKeyColumns.indexOf(data) != -1) { %>
                            <input type="checkbox" name="uniq<%= i %>" checked>
                        <% } else { %>
                            <input type="checkbox" name="uniq<%= i %>">
                        <% } %>
                    <% } %>
                </td>
                <td>
                    <% if (nameCollectingKey && nameCollectingKey == i) { %>
                        <input type="checkbox" name="nameCollecting<%= i %>" checked>
                    <% } else { %>
                        <input type="checkbox" name="nameCollecting<%= i %>">
                    <% } %>
                </td>
                <td><%= data %></td>
                <td>
                    <select id="dataType" name="dataType" class="form-control">
                        <% if (_.isEmpty(dataTypes)) { %>
                            <option value="varchar">文字列</option>
                            <option value="date">日付</option>
                            <option value="int">整数</option>
                            <option value="float">小数</option>
                        <% } else { %>
                            <% if (dataTypes[i] === 'VARCHAR') { %>
                                <option value="varchar" selected>文字列</option>
                                <option value="date">日付</option>
                                <option value="int">整数</option>
                                <option value="float">小数</option>
                            <% } else if (dataTypes[i] === 'DATE') { %>
                                <option value="varchar">文字列</option>
                                <option value="date" selected>日付</option>
                                <option value="int">整数</option>
                                <option value="float">小数</option>
                            <% } else if (dataTypes[i] === 'INT') { %>
                                <option value="varchar">文字列</option>
                                <option value="date">日付</option>
                                <option value="int" selected>整数</option>
                                <option value="float">小数</option>
                            <% } else if (dataTypes[i] === 'FLOAT') { %>
                                <option value="varchar">文字列</option>
                                <option value="date" >日付</option>
                                <option value="int">整数</option>
                                <option value="float" selected>小数</option>
                            <% } %>
                        <% } %>
                    </select>
                </td>
            </tr>
        <% }); %>
    </tbody>
</table>
