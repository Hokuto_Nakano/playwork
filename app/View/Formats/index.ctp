<?php echo $this->Html->script('format', array('format' => false)); ?>

<div id="message" class="alert alert-danger" style="padding: 20px; display: none;">
</div>
<br>
<div class="col-sm-12 mt10">
    <button id="registBtn" type="button" class="btn btn-primary">登録</button>
</div>

<br>
<div id="data" style="margin-top: 30px;">
    <table class="table">
        <thead>
            <tr>
                <td>ID</td>
                <td>商材名</td>
                <td>商材インプットテーブル名</td>
                <td>共通キーカラム名</td>
                <td>名寄せキーカラム</td>
                <td>更新日時</td>
            </tr>
        </thead>
        <tbody>
            <?php if(isset($productCsvTableMap)) { ?>
                <?php foreach($productCsvTableMap as $data ) { ?>
                    <tr class="clickable">
                        <td><?php echo($data['ProductCsvTableMap']['id']); ?></td>
                        <td><?php echo($data['ProductCategory']['name']); ?></td>
                        <td><?php echo($data['ProductCsvTableMap']['product_input_table_name']); ?></td>
                        <td><?php echo($data['ProductCsvTableMap']['common_key_columns']); ?></td>
                        <td><?php echo($data['ProductCsvTableMap']['name_collecting']); ?></td>
                        <td><?php echo($data['ProductCsvTableMap']['modified']); ?></td>
                    </tr>
            <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="6">データはありません</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<div class="modal fade bs-example-modal-lg" id="formatModal" tabindex="-1" data-backdrop="static">
</div>
