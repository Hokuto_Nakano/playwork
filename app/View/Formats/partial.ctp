<?php echo $this->Html->script('format', array('inline' => false)); ?>

<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <form>
      <div class="modal-header">
      </div>
      <div class="modal-body">

          <div class="col-sm-12">
              <div class="col-sm-3">
                  <?php if(!$updateFlg) { ?>
                      <select id="modal_product_categories" name="modal_product_categories" class="form-control">
                          <?php foreach($prooductCategories as $key => $value){ ?>
                              <option value="<?php echo $value['ProductCategory']['id']; ?>"><?php echo $value['ProductCategory']['name'] ?></option>
                          <?php } ?>
                      </select>
                  <?php } ?>
              </div>
              <div class="col-sm-6 text-center">
                  <span id="page">1/6 page</span>
              </div>
              <div class="col-sm-3">
                  <?php if(!$updateFlg) { ?>
                      <div>
                          <input type="file" name="file" />
                      </div>
                  <?php } ?>
              </div>
          </div>
          <div class="col-sm-12 mt20">
              <span id="captionText"></span>
          </div>
          <div class="col-sm-12 mt10">
              <div id="csv_table">
              </div>
              <div id="acquisition_table" style="display: none;">
              </div>
              <div id="status_table" style="display: none;">
              </div>
              <div id="plan_table" style="display: none;">
              </div>
              <div id="count_table" style="display: none;">
              </div>
              <div id="staff_table" style="display: none;">
              </div>
          </div>
      </div>
      <div class="modal-footer">
          <div id="modal-message" style="color: rgb(255,0,0);"></div>
          <button id="close" type="button" class="btn btn-default">キャンセル</button>
          <?php if($updateFlg) { ?>
              <button id="disabled" type="submit" class="btn btn-warning">無効</button>
          <?php } ?>
          <button id="prev" type="button" class="btn btn-default" style="display: none;">戻る</button>
          <button id="next" type="button" class="btn btn-default" disabled="disabled">次へ</button>
      </div>
    </form>
  </div>
</div>
