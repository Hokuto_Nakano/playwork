<?php echo $this->Html->script('format', array('inline' => false)); ?>

<table class="table table-striped">
    <thead>
        <td>有無</td>
        <td>ステータス</td>
        <td>CSVカラム</td>
        <td>条件</td>
        <td>比較文字列</td>
    </thead>
    <tbody>
        <% _.each(labels, function(data, i) { %>
            <tr>
                <td>
                    <% if ((productCsvColumnNames[i] && comparisonConditions[i]) || updateFlg === false) { %>
                        <input type="checkbox" name="yesOrNo" value="yes" checked>
                    <% } else { %>
                        <input type="checkbox" name="yesOrNo" value="yes">
                    <% } %>
                </td>
                <td>
                    <div><%= data.label %></div>
                    <div style="font-size: 8px"><%= data.message %></div>
                    <input type="hidden" name="<%= data.value %>" value="<%= data.value %>">
                </td>
                <td>
                    <% if ((productCsvColumnNames[i] && comparisonConditions[i]) || updateFlg === false) { %>
                        <select id="csv_column" name="csv_column" class="form-control">
                    <% } else { %>
                        <select id="csv_column" name="csv_column" class="form-control" disabled>
                    <% }%>
                        <% _.each(columnNames, function(columnName) { %>
                            <% if(_.isEmpty(productCsvColumnNames)) { %>
                                <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                            <% } else { %>
                                <% if (productCsvColumnNames[i] === columnName[Object.keys(columnName)]) { %>
                                    <option value="<%= Object.keys(columnName) %>" selected><%= columnName[Object.keys(columnName)] %></option>
                                <% } else { %>
                                    <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                <% } %>
                            <% } %>
                        <% }); %>
                    </select>
                </td>
                <td>
                    <% if ((productCsvColumnNames[i] && comparisonConditions[i]) || updateFlg === false) { %>
                        <select id="conditions" name="conditions" class="form-control">
                    <% } else { %>
                        <select id="conditions" name="conditions" class="form-control" disabled>
                    <% }%>
                        <% if(_.isEmpty(comparisonConditions)) { %>
                            <option value="indexOf">が次の文字を含むとき</option>
                            <option value="equal">が次の文字と一致するとき</option>
                            <option value="notIndexOf">が次の文字を含めないとき</option>
                            <option value="notEqual">が次の文字と異なるとき</option>
                        <% } else { %>
                            <% if (comparisonConditions[i] === 'indexOf') { %>
                                <option value="indexOf" selected>が次の文字を含むとき</option>
                                <option value="equal">が次の文字と一致するとき</option>
                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                <option value="notEqual">が次の文字と異なるとき</option>
                            <% } else if (comparisonConditions[i] === 'equal') { %>
                                <option value="indexOf">が次の文字を含むとき</option>
                                <option value="equal" selected>が次の文字と一致するとき</option>
                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                <option value="notEqual">が次の文字と異なるとき</option>
                            <% } else if (comparisonConditions[i] === 'notIndexOf') { %>
                                <option value="indexOf">が次の文字を含むとき</option>
                                <option value="equal">が次の文字と一致するとき</option>
                                <option value="notIndexOf" selected>が次の文字を含めないとき</option>
                                <option value="notEqual">が次の文字と異なるとき</option>
                            <% } else if (comparisonConditions[i] === 'notEqual') { %>
                                <option value="indexOf">が次の文字を含むとき</option>
                                <option value="equal">が次の文字と一致するとき</option>
                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                <option value="notEqual" selected>が次の文字と異なるとき</option>
                            <% } else { %>
                                <option value="indexOf">が次の文字を含むとき</option>
                                <option value="equal">が次の文字と一致するとき</option>
                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                <option value="notEqual">が次の文字と異なるとき</option>
                            <% } %>
                        <% } %>
                    </select>
                </td>
                <td>
                    <% if(_.isEmpty(comparisonStrings)) { %>
                        <% if ((productCsvColumnNames[i] && comparisonConditions[i]) || updateFlg === false) { %>
                            <input id="compare" name="compare" type="text">
                        <% } else { %>
                            <input id="compare" name="compare" type="text" style="background-color: #f5f5f5;" disabled>
                        <% } %>
                    <% } else { %>
                        <% if (productCsvColumnNames[i] && comparisonConditions[i]) { %>
                            <input id="compare" name="compare" type="text" value="<%= comparisonStrings[i] %>">
                        <% } else { %>
                            <input id="compare" name="compare" type="text" style="background-color: #f5f5f5;" disabled>
                        <% } %>
                    <% } %>
                </td>
            </tr>
        <% }); %>
    </tbody>
</table>
