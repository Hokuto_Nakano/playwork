<?php echo $this->Html->script('format', array('inline' => false)); ?>

<table class="table table-striped">
    <thead>
        <td>カウント項目</td>
        <td>CSVカラム</td>
        <td></td>
        <td>条件</td>
        <td>ユーザー属性</td>
    </thead>

    <%
        var rows = [];
        var self = this;
        _.each(staffCountConditionIds, function(staffCountConditionId, row) {

            if (self.staffCountConditionId != staffCountConditionId) {
                rows.push(row);
            }
            self.staffCountConditionId = staffCountConditionId;
        });
        var lastPos = 0;
        self.staffCountConditionId = undefined;
    %>
    <% _.each(countItemLabels, function(data, i) { %>
    <tbody>
        <%
            var j = i;
            if (i != 0) {
                j = lastPos;
            }
            if (_.isEmpty(rows) || rows.length < (i+1)) {
                index = 1;
                j = 0;
            }
            if (rows.length > (i+1)) {
                index = rows[i+1];

            }
            if (rows.length === (i+1)) {
                index = staffCountConditionIds.length;
            }

            var k = 0;
        %>
        <% while( j < index) { %>
            <% if (k == 0) { %>
                <tr>
                    <td>
                        <span><%= data %></span>
                        <input type="hidden" name="count_item_label" value="<%= choiceItemKeys[i] %>">
                    </td>
                    <td>
                        <select id="identify-user" name="identify-user" class="form-control">
                            <% _.each(columnNames, function(columnName) { %>
                                <% if(_.isEmpty(userIdColumnNames) && _.isEmpty(userNameColumnNames)) { %>
                                    <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                <% } else { %>
                                    <% if (userIdColumnNames[j].replace('column','') == Object.keys(columnName) || userNameColumnNames[j].replace('column','') == Object.keys(columnName)) { %>
                                        <option value="<%= Object.keys(columnName) %>" selected><%= columnName[Object.keys(columnName)] %></option>
                                    <% } else { %>
                                        <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                    <% } %>
                                <% } %>
                            <% }); %>
                        </select>
                    </td>
                    <td>
                        <button type="button" class="add-identify-user-row-btn">＋</button>
                    </td>
                    <td>
                        <select id="conditions" name="conditions" class="form-control">
                            <% if(_.isEmpty(staffConditions)) { %>
                                <option value="indexOf">が次の文字を含むとき</option>
                                <option value="equal">が次の文字と一致するとき</option>
                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                <option value="notEqual">が次の文字と異なるとき</option>
                            <% } else { %>
                                <% if (staffConditions[j] === 'indexOf') { %>
                                    <option value="indexOf" selected>が次の文字を含むとき</option>
                                    <option value="equal">が次の文字と一致するとき</option>
                                    <option value="notIndexOf">が次の文字を含めないとき</option>
                                    <option value="notEqual">が次の文字と異なるとき</option>
                                <% } else if (staffConditions[j] === 'equal') { %>
                                    <option value="indexOf">が次の文字を含むとき</option>
                                    <option value="equal" selected>が次の文字と一致するとき</option>
                                    <option value="notIndexOf">が次の文字を含めないとき</option>
                                    <option value="notEqual">が次の文字と異なるとき</option>
                                <% } else if (staffConditions[j] === 'notIndexOf') { %>
                                    <option value="indexOf">が次の文字を含むとき</option>
                                    <option value="equal">が次の文字と一致するとき</option>
                                    <option value="notIndexOf" selected>が次の文字を含めないとき</option>
                                    <option value="notEqual">が次の文字と異なるとき</option>
                                <% } else if (staffConditions[j] === 'notEqual') { %>
                                    <option value="indexOf">が次の文字を含むとき</option>
                                    <option value="equal">が次の文字と一致するとき</option>
                                    <option value="notIndexOf">が次の文字を含めないとき</option>
                                    <option value="notEqual" selected>が次の文字と異なるとき</option>
                                <% } %>
                            <% } %>
                        </select>
                    </td>
                    <td>
                        <select id="staff" name="staff" class="form-control">
                            <% if(_.isEmpty(userIdColumnNames) && _.isEmpty(userNameColumnNames)) { %>
                                <option value="staffNo">社員番号</option>
                                <option value="name">氏名</option>
                            <% } else { %>
                                <% if(!_.isEmpty(userIdColumnNames[j])) { %>
                                    <option value="staffNo" selected>社員番号</option>
                                    <option value="name">氏名</option>
                                <% } else if (!_.isEmpty(userNameColumnNames[j])) { %>
                                    <option value="staffNo">社員番号</option>
                                    <option value="name" selected>氏名</option>
                                <% } else { %>
                                    <option value="staffNo">社員番号</option>
                                    <option value="name">氏名</option>
                                <% } %>
                            <% } %>
                        </select>
                    </td>
                </tr>
            <% } else { %>
                <% if (staffConditions[j]) { %>
                    <tr><td></td><td></td><td></td><td>かつ</td><td></td></tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <select id="conditions" name="conditions" class="form-control">
                                <% if(_.isEmpty(staffConditions)) { %>
                                    <option value="indexOf">が次の文字を含むとき</option>
                                    <option value="equal">が次の文字と一致するとき</option>
                                    <option value="notIndexOf">が次の文字を含めないとき</option>
                                    <option value="notEqual">が次の文字と異なるとき</option>
                                <% } else { %>
                                    <% if (staffConditions[j] === 'indexOf') { %>
                                        <option value="indexOf" selected>が次の文字を含むとき</option>
                                        <option value="equal">が次の文字と一致するとき</option>
                                        <option value="notIndexOf">が次の文字を含めないとき</option>
                                        <option value="notEqual">が次の文字と異なるとき</option>
                                    <% } else if (staffConditions[j] === 'equal') { %>
                                        <option value="indexOf">が次の文字を含むとき</option>
                                        <option value="equal" selected>が次の文字と一致するとき</option>
                                        <option value="notIndexOf">が次の文字を含めないとき</option>
                                        <option value="notEqual">が次の文字と異なるとき</option>
                                    <% } else if (staffConditions[j] === 'notIndexOf') { %>
                                        <option value="indexOf">が次の文字を含むとき</option>
                                        <option value="equal">が次の文字と一致するとき</option>
                                        <option value="notIndexOf" selected>が次の文字を含めないとき</option>
                                        <option value="notEqual">が次の文字と異なるとき</option>
                                    <% } else if (staffConditions[j] === 'notEqual') { %>
                                        <option value="indexOf">が次の文字を含むとき</option>
                                        <option value="equal">が次の文字と一致するとき</option>
                                        <option value="notIndexOf">が次の文字を含めないとき</option>
                                        <option value="notEqual" selected>が次の文字と異なるとき</option>
                                    <% } %>
                                <% } %>
                            </select>
                        </td>
                        <td>
                            <select id="staff" name="staff" class="form-control">
                                <% if(_.isEmpty(userIdColumnNames) && _.isEmpty(userNameColumnNames)) { %>
                                    <option value="staffNo">社員番号</option>
                                    <option value="name">氏名</option>
                                <% } else { %>
                                    <% if(!_.isEmpty(userNameColumnNames[j])) { %>
                                        <option value="staffNo">社員番号</option>
                                        <option value="name" selected>氏名</option>
                                    <% } else if (!_.isEmpty(userIdColumnNames[j])) { %>
                                        <option value="staffNo" selected>社員番号</option>
                                        <option value="name">氏名</option>
                                    <% } else { %>
                                        <option value="staffNo">社員番号</option>
                                        <option value="name">氏名</option>
                                    <% } %>
                                <% } %>
                            </select>
                        </td>
                        <td>
                            <button type="button" class="del-row-btn">×</button>
                        </td>
                    </tr>
                <% } %>
            <% } %>
            <% j++; k++; lastPos = index; %>
        <% } %>
    </tbody>
    <% }); %>
</table>
