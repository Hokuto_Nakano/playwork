<?php echo $this->Html->script('format', array('inline' => false)); ?>

<table class="table table-striped">
    <thead>
        <td></td>
        <td>カウント項目</td>
        <td>計上日</td>
        <td></td>
        <td>CSVカラム</td>
        <td>条件</td>
        <td>比較文字列</td>
    </thead>
    <%
        var rows = [];
        var self = this;
        _.each(_.clone(itemNames), function(itemName, row) {
            if (self.itemName != itemName) {
                rows.push(row);
            }
            self.itemName = itemName;
        });

        var row = 0;
        var number = 0;
        self.itemName = undefined;
    %>
    <% _.each(itemNames, function(itemName, i) { %>
        <% if (self.itemName !== itemName) { %>
            <%
                var columnName = '';
                var flg = false;
                var index = i;
                if (dateColumnNames.length == 0 ) {
                    // 新規の場合は1行のみ出力
                    index = 1;
                } else {
                    if (rows.length > (number+1)) {
                        index = rows[number+1];
                    }
                    if (rows.length === (number+1)) {
                        index = itemNames.length;
                    }
                }

            %>
            <tbody>
                <%
                    var j = i;
                    var k = 0;
                %>
                <% while( j < index) { %>
                    <% if (k == 0 ) { %>
                        <tr>
                            <td>
                                <button type="button" class="add-countItem-btn">＋</button>
                            </td>
                            <td>
                                <select id="count_items" name="count_items" class="form-control">
                                    <% _.each(countItems, function(countItem) { %>
                                        <% if (countItem[Object.keys(countItem)] === itemName) { %>
                                            <option value="<%= Object.keys(countItem) %>" selected><%= countItem[Object.keys(countItem)] %></option>
                                        <% } else { %>
                                            <option value="<%= Object.keys(countItem) %>"><%= countItem[Object.keys(countItem)] %></option>
                                        <% } %>
                                    <% }); %>
                                </select>
                            </td>
                            <td>
                                <select id="recorded_date" name="recorded_date" class="form-control">
                                    <% _.each(columnNames, function(columnName) { %>
                                        <% if(_.isEmpty(dateColumnNames)) { %>
                                            <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                        <% } else { %>
                                            <% if (dateColumnNames[i].replace('column', '') == Object.keys(columnName)) { %>
                                                <option value="<%= Object.keys(columnName) %>" selected><%= columnName[Object.keys(columnName)] %></option>
                                            <% } else { %>
                                                <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                            <% } %>
                                        <% } %>
                                    <% }); %>
                                </select>
                            </td>
                            <td>
                                <button type="button" class="add-row-btn">＋</button>
                            </td>
                            <td>
                                <select id="csv_column" name="csv_column" class="form-control">
                                    <% _.each(columnNames, function(columnName) { %>
                                        <% if(_.isEmpty(dataColumnNames)) { %>
                                            <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                        <% } else { %>
                                            <% if (dataColumnNames[i].replace('column', '') == Object.keys(columnName)) { %>
                                                <option value="<%= Object.keys(columnName) %>" selected><%= columnName[Object.keys(columnName)] %></option>
                                            <% } else { %>
                                                <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                            <% } %>
                                        <% } %>
                                    <% }); %>
                                </select>
                            </td>
                            <td>
                                <select id="conditions" name="conditions" class="form-control">
                                    <% if(_.isEmpty(comparisonConditions)) { %>
                                        <option value="indexOf">が次の文字を含むとき</option>
                                        <option value="equal">が次の文字と一致するとき</option>
                                        <option value="notIndexOf">が次の文字を含めないとき</option>
                                        <option value="notEqual">が次の文字と異なるとき</option>
                                    <% } else { %>
                                        <% if (comparisonConditions[j] === 'indexOf') { %>
                                            <option value="indexOf" selected>が次の文字を含むとき</option>
                                            <option value="equal">が次の文字と一致するとき</option>
                                            <option value="notIndexOf">が次の文字を含めないとき</option>
                                            <option value="notEqual">が次の文字と異なるとき</option>
                                        <% } else if (comparisonConditions[j] === 'equal') { %>
                                            <option value="indexOf">が次の文字を含むとき</option>
                                            <option value="equal" selected>が次の文字と一致するとき</option>
                                            <option value="notIndexOf">が次の文字を含めないとき</option>
                                            <option value="notEqual">が次の文字と異なるとき</option>
                                        <% } else if (comparisonConditions[j] === 'notIndexOf') { %>
                                            <option value="indexOf">が次の文字を含むとき</option>
                                            <option value="equal">が次の文字と一致するとき</option>
                                            <option value="notIndexOf" selected>が次の文字を含めないとき</option>
                                            <option value="notEqual">が次の文字と異なるとき</option>
                                        <% } else if (comparisonConditions[j] === 'notEqual') { %>
                                            <option value="indexOf">が次の文字を含むとき</option>
                                            <option value="equal">が次の文字と一致するとき</option>
                                            <option value="notIndexOf">が次の文字を含めないとき</option>
                                            <option value="notEqual" selected>が次の文字と異なるとき</option>
                                        <% } %>
                                    <% } %>
                                </select>
                            </td>
                            <td>
                                <% if(_.isEmpty(comparisonStrings)) { %>
                                    <input id="compare" name="compare" type="text">
                                <% } else { %>
                                    <input id="compare" name="compare" type="text" value="<%= comparisonStrings[j] %>">
                                <% } %>
                            </td>
                            <% if (row != 0) { %>
                                <td>
                                    <button type="button" class="del-row-btn">×</button>
                                </td>
                            <% } %>
                        </tr>
                    <% } else { %>
                        <tr><td></td><td></td><td></td><td></td><td>かつ</td><td></td><td></td><td></td></tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <select id="csv_column" name="csv_column" class="form-control">
                                    <% _.each(columnNames, function(columnName) { %>
                                        <% if(_.isEmpty(dataColumnNames)) { %>
                                            <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                        <% } else { %>
                                            <% if (dataColumnNames[j].replace('column', '') == Object.keys(columnName)) { %>
                                                <option value="<%= Object.keys(columnName) %>" selected><%= columnName[Object.keys(columnName)] %></option>
                                            <% } else { %>
                                                <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                            <% } %>
                                        <% } %>
                                    <% }); %>
                                </select>
                            </td>
                            <td>
                                <select id="conditions" name="conditions" class="form-control">
                                    <% if(_.isEmpty(comparisonConditions)) { %>
                                        <option value="indexOf">が次の文字を含むとき</option>
                                        <option value="equal">が次の文字と一致するとき</option>
                                        <option value="notIndexOf">が次の文字を含めないとき</option>
                                        <option value="notEqual">が次の文字と異なるとき</option>
                                    <% } else { %>
                                        <% if (comparisonConditions[j] === 'indexOf') { %>
                                            <option value="indexOf" selected>が次の文字を含むとき</option>
                                            <option value="equal">が次の文字と一致するとき</option>
                                            <option value="notIndexOf">が次の文字を含めないとき</option>
                                            <option value="notEqual">が次の文字と異なるとき</option>
                                        <% } else if (comparisonConditions[j] === 'equal') { %>
                                            <option value="indexOf">が次の文字を含むとき</option>
                                            <option value="equal" selected>が次の文字と一致するとき</option>
                                            <option value="notIndexOf">が次の文字を含めないとき</option>
                                            <option value="notEqual">が次の文字と異なるとき</option>
                                        <% } else if (comparisonConditions[j] === 'notIndexOf') { %>
                                            <option value="indexOf">が次の文字を含むとき</option>
                                            <option value="equal">が次の文字と一致するとき</option>
                                            <option value="notIndexOf" selected>が次の文字を含めないとき</option>
                                            <option value="notEqual">が次の文字と異なるとき</option>
                                        <% } else if (comparisonConditions[j] === 'notEqual') { %>
                                            <option value="indexOf">が次の文字を含むとき</option>
                                            <option value="equal">が次の文字と一致するとき</option>
                                            <option value="notIndexOf">が次の文字を含めないとき</option>
                                            <option value="notEqual" selected>が次の文字と異なるとき</option>
                                        <% } %>
                                    <% } %>
                                </select>
                            </td>
                            <td>
                                <% if(_.isEmpty(comparisonStrings)) { %>
                                    <input id="compare" name="compare" type="text">
                                <% } else { %>
                                    <input id="compare" name="compare" type="text" value="<%= comparisonStrings[j] %>">
                                <% } %>
                            </td>
                            <td>
                                <button type="button" class="del-row-btn">×</button>
                            </td>
                        </tr>
                    <% } %>
                    <% j++; k++; %>
                <% } %>
            </tbody>
            <% number++; %>
        <% } %>
        <%
            self.itemName = itemName;
            row++;
        %>
    <% }); %>
    <% self.itemName = undefined; %>
</table>
