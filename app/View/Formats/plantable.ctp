<?php echo $this->Html->script('format', array('inline' => false)); ?>

<table class="table table-striped">
    <thead>
        <td>商材ファミリー</td>
        <td>商材プラン</td>
        <td></td>
        <td>CSVカラム</td>
        <td>条件</td>
        <td>比較文字列</td>
    </thead>
    <% if (_.isEmpty(planNames)) { %>
        <tbody>
            <tr>
                <td colspan="4">データはありません</td>
            </tr>
        </tbody>
        <% $('#next').prop('disabled', true); %>
    <% } else { %>
        <%
            var rows = [];
            var self = this;
            _.each(_.clone(planNames), function(data, row) {
                if (self.planFamilyName != _.values(data.productFamily)[0] || self.planDetailName != _.values(data.productDetail)[0]) {
                    rows.push(row);
                }
                self.planFamilyName = _.values(data.productFamily)[0];
                self.planDetailName = _.values(data.productDetail)[0];
            });

            var number = 0;
            self.planFamilyName = undefined;
            self.planDetailName = undefined;
        %>
        <% _.each(planNames, function(data, i) { %>
            <% if (self.planFamilyName != _.values(data.productFamily)[0] || self.planDetailName != _.values(data.productDetail)[0]) { %>
                <%
                    var columnName = '';
                    var flg = false;
                    var index = i;

                    if (rows.length > (number+1)) {
                        index = rows[number+1];
                    }
                    if (rows.length === (number+1)) {
                        index = planNames.length;
                    }

                %>
                <tbody>
                    <%
                        var j = i;
                        var k = 0;

                    %>
                    <% while( j < index) { %>
                        <% if (k == 0 ) { %>
                            <tr>
                                <td>
                                    <span><%= _.values(data.productFamily) %></span>
                                    <input type="hidden" name="productFamilyId" value="<%= _.keys(data.productFamily) %>">
                                </td>
                                <td>
                                    <span><%= _.values(data.productDetail) %></span>
                                    <input type="hidden" name="productId" value="<%= _.keys(data.productDetail) %>">
                                </td>
                                <td>
                                    <button type="button" class="addPlanConditionBtn">＋</button>
                                </td>
                                <td>
                                    <select id="csv_column" name="csv_column" class="form-control">
                                        <% _.each(columnNames, function(columnName) { %>
                                            <% if(_.isEmpty(productCsvColumnNames)) { %>
                                                <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                            <% } else { %>
                                                <% if (productCsvColumnNames[i] === columnName[Object.keys(columnName)]) { %>
                                                    <option value="<%= Object.keys(columnName) %>" selected><%= columnName[Object.keys(columnName)] %></option>
                                                <% } else { %>
                                                    <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                                <% } %>
                                            <% } %>
                                        <% }); %>
                                    </select>
                                </td>
                                <td>
                                    <select id="conditions" name="conditions" class="form-control">
                                        <% if(_.isEmpty(comparisonConditions)) { %>
                                            <option value="indexOf">が次の文字を含むとき</option>
                                            <option value="equal">が次の文字と一致するとき</option>
                                            <option value="notIndexOf">が次の文字を含めないとき</option>
                                            <option value="notEqual">が次の文字と異なるとき</option>
                                        <% } else { %>
                                            <% if (comparisonConditions[i] === 'indexOf') { %>
                                                <option value="indexOf" selected>が次の文字を含むとき</option>
                                                <option value="equal">が次の文字と一致するとき</option>
                                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                                <option value="notEqual">が次の文字と異なるとき</option>
                                            <% } else if (comparisonConditions[i] === 'equal') { %>
                                                <option value="indexOf">が次の文字を含むとき</option>
                                                <option value="equal" selected>が次の文字と一致するとき</option>
                                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                                <option value="notEqual">が次の文字と異なるとき</option>
                                            <% } else if (comparisonConditions[i] === 'notIndexOf') { %>
                                                <option value="indexOf">が次の文字を含むとき</option>
                                                <option value="equal">が次の文字と一致するとき</option>
                                                <option value="notIndexOf" selected>が次の文字を含めないとき</option>
                                                <option value="notEqual">が次の文字と異なるとき</option>
                                            <% } else if (comparisonConditions[i] === 'notEqual') { %>
                                                <option value="indexOf">が次の文字を含むとき</option>
                                                <option value="equal">が次の文字と一致するとき</option>
                                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                                <option value="notEqual" selected>が次の文字と異なるとき</option>
                                            <% } %>
                                        <% } %>
                                    </select>
                                </td>
                                <td>
                                    <% if(_.isEmpty(comparisonStrings)) { %>
                                        <input id="compare" name="compare" type="text">
                                    <% } else { %>
                                        <input id="compare" name="compare" type="text" value="<%= comparisonStrings[i] %>">
                                    <% } %>
                                </td>
                            </tr>
                        <% } else { %>
                            <tr><td></td><td></td><td></td><td>かつ</td><td></td><td></td><td></td></tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <select id="csv_column" name="csv_column" class="form-control">
                                        <% _.each(columnNames, function(columnName) { %>
                                            <% if(_.isEmpty(productCsvColumnNames)) { %>
                                                <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                            <% } else { %>
                                                <% if (productCsvColumnNames[j] === columnName[Object.keys(columnName)]) { %>
                                                    <option value="<%= Object.keys(columnName) %>" selected><%= columnName[Object.keys(columnName)] %></option>
                                                <% } else { %>
                                                    <option value="<%= Object.keys(columnName) %>"><%= columnName[Object.keys(columnName)] %></option>
                                                <% } %>
                                            <% } %>
                                        <% }); %>
                                    </select>
                                </td>
                                <td>
                                    <select id="conditions" name="conditions" class="form-control">
                                        <% if(_.isEmpty(comparisonConditions)) { %>
                                            <option value="indexOf">が次の文字を含むとき</option>
                                            <option value="equal">が次の文字と一致するとき</option>
                                            <option value="notIndexOf">が次の文字を含めないとき</option>
                                            <option value="notEqual">が次の文字と異なるとき</option>
                                        <% } else { %>
                                            <% if (comparisonConditions[j] === 'indexOf') { %>
                                                <option value="indexOf" selected>が次の文字を含むとき</option>
                                                <option value="equal">が次の文字と一致するとき</option>
                                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                                <option value="notEqual">が次の文字と異なるとき</option>
                                            <% } else if (comparisonConditions[j] === 'equal') { %>
                                                <option value="indexOf">が次の文字を含むとき</option>
                                                <option value="equal" selected>が次の文字と一致するとき</option>
                                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                                <option value="notEqual">が次の文字と異なるとき</option>
                                            <% } else if (comparisonConditions[j] === 'notIndexOf') { %>
                                                <option value="indexOf">が次の文字を含むとき</option>
                                                <option value="equal">が次の文字と一致するとき</option>
                                                <option value="notIndexOf" selected>が次の文字を含めないとき</option>
                                                <option value="notEqual">が次の文字と異なるとき</option>
                                            <% } else if (comparisonConditions[j] === 'notEqual') { %>
                                                <option value="indexOf">が次の文字を含むとき</option>
                                                <option value="equal">が次の文字と一致するとき</option>
                                                <option value="notIndexOf">が次の文字を含めないとき</option>
                                                <option value="notEqual" selected>が次の文字と異なるとき</option>
                                            <% } %>
                                        <% } %>
                                    </select>
                                </td>
                                <td>
                                    <% if(_.isEmpty(comparisonStrings)) { %>
                                        <input id="compare" name="compare" type="text">
                                    <% } else { %>
                                        <input id="compare" name="compare" type="text" value="<%= comparisonStrings[j] %>">
                                    <% } %>
                                </td>
                                <td>
                                    <button type="button" class="del-row-btn">×</button>
                                </td>
                            </tr>
                        <% } %>
                        <% j++; k++; %>
                    <% } %>
                </tbody>
                <% number++; %>
            <% } %>
            <%
                self.planFamilyName = _.values(data.productFamily)[0];
                self.planDetailName = _.values(data.productDetail)[0];
            %>
        <% }); %>
        <%
            self.planFamilyName = undefined;
            self.planDetailName = undefined;
        %>
    <% } %>
</table>
