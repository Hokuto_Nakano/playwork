<?= $this->Html->script('productdetail', array('inline' => true)); ?>

<?php //echo json_encode($categories); ?>

<script type="text/javascript">
  var detailCtrl = new ProductDetailCtrl(JSON.parse('<?= json_encode($categories); ?>'));
</script>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form onsubmit="detailCtrl.regist();return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">商材登録</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="product_id" name="product_id" class="form-control" value="<?= $product['product_id']; ?>"/>
        <input type="hidden" id="product_category_id" name="product_category_id" value="<?= $product['product_category_id']; ?>"/>
        <input type="hidden" id="product_family_id" name="product_family_id" value="<?= $product['product_family_id']; ?>"/>
        <input type="hidden" id="product_detail_id" name="product_detail_id" class="form-control" value="<?= $product['product_detail_id']; ?>"/>
        <input type="hidden" id="start_month" name="start_month" value="<?= $year.$month; ?>"/>

        <div class="form-group">
          <span class="lead"><?= $year."年".$month."月"; ?></span>
        </div>
        <div class="form-group">
          <div class="col-md-8">
            <label>商品カテゴリ名</label>
            <? if($product['product_id']){ ?>
              <input type="text" id="product_category_name" name="product_category_name" class="form-control" value="<?= $product['product_category_name']; ?>" disabled/>
            <? }else{ ?>
              <div class="dropdown">
                <input type="text" id="product_category_name" name="product_category_name" class="form-control" data-toggle="dropdown" onkeydown="detailCtrl.setOldCategoryName(this.value)" onkeyup="detailCtrl.onChangeCategoryName(this.value)" value="<?= $product['product_category_name']; ?>" required/>
                <ul class="dropdown-menu">
                  <? foreach($categories as $category){ ?>
                    <li>
                      <a href="javascript:void(0)" onclick="detailCtrl.selectCategory('<?= $category['ProductCategory']['id']; ?>');" ><?= $category['ProductCategory']['name']; ?></a>
                    </li>
                  <? } ?>
                </ul>
              </div>
            <? } ?>
          </div>
          <div class="col-md-4">
            <label>商品カテゴリ省略名</label>
            <input type="text" id="product_category_short_name" name="product_category_short_name" class="form-control" value="<?= $product['product_category_short_name']; ?>" <?= $product['product_id'] ? 'disabled' : ''; ?>/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-8">
            <label>商品ファミリ名</label>
            <? if($product['product_id']){ ?>
              <input type="text" id="product_family_name" name="product_family_name" class="form-control" value="<?= $product['product_family_name']; ?>" disabled/>
            <? }else{ ?>
              <div class="dropdown">
                <input type="text" id="product_family_name" name="product_family_name" class="form-control" data-toggle="dropdown" onkeydown="detailCtrl.setOldFamilyName(this.value)" onkeyup="detailCtrl.onChangeFamilyName(this.value)" value="<?= $product['product_family_name']; ?>" required/>
                <ul id="selectFamilies" class="dropdown-menu">
                </ul>
              </div>
            <? } ?>
          </div>
          <div class="col-md-4">
            <label>商品ファミリ省略名</label>
            <input type="text" id="product_family_short_name" name="product_family_short_name" class="form-control" value="<?= $product['product_family_short_name']; ?>" <?= $product['product_id'] ? 'disabled' : ''; ?>/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label>商品プラン名</label>
            <input type="text" id="product_detail_name" name="product_detail_name" class="form-control" value="<?= $product['product_detail_name']; ?>" required/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label>商品プラン省略名</label>
            <input type="text" id="product_detail_short_name" name="product_detail_short_name" class="form-control" value="<?= $product['product_detail_short_name']; ?>"/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label>売上単価</label>
            <input type="text" id="price" name="price" class="form-control" value="<?= $product['price']; ?>" required/>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <? if($product['product_detail_id']){ ?>
        <button type="submit" class="btn btn-primary">更新</button>
        <button type="button" class="btn btn-danger" onclick="detailCtrl.delete('<?= $product['product_id']; ?>');">削除</button>
        <? }else{ ?>
        <button type="submit" class="btn btn-primary">登録</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>
