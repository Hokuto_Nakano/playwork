<?php echo $this->Html->script('attendancesetting', array('inline' => false)); ?>

<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php if(isset($id)) { ?>
                    <h4 class="modal-title">勤怠設定（更新）</h4>
                    <input type="hidden" name="id" value="<?php echo($id) ?>" />
                <?php } else { ?>
                    <h4 class="modal-title">勤怠設定（登録）</h4>
                <?php } ?>
            </div>
            <div class="modal-body">
                 <div class="form-group">
                    <label>出勤時間単位</label>
                     <select id="attendance_time_unit" name="attendance_time_unit" class="form-control">
                         <?php foreach($attendanceTimeUnit as $key => $value){ ?>
                             <?php if(isset($attendanceTimeUnitSelected) && $key == $attendanceTimeUnitSelected) { ?>
                                 <option value="<?php echo($key); ?>" selected><?php echo($value); ?></option>
                             <?php } else { ?>
                                 <option value="<?php echo($key); ?>"><?php echo($value); ?></option>
                             <?php } ?>
                         <?php } ?>
                     </select>
                </div>
                <div class="form-group">
                    <label>出勤時間端数処理</label>
                    <select id="attendance_time_rounding" name="attendance_time_rounding" class="form-control">
                        <?php foreach($attendanceTimeRounding as $key => $value){ ?>
                            <?php if(isset($attendanceTimeUnitSelected) && $key == $attendanceTimeRoundingSelected) { ?>
                                <option value="<?php echo($key); ?>" selected><?php echo($value); ?></option>
                            <?php } else { ?>
                                <option value="<?php echo($key); ?>"><?php echo($value); ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>退勤時間単位</label>
                    <select id="leave_time_unit" name="leave_time_unit" class="form-control">
                        <?php foreach($leaveTimeUnit as $key => $value){ ?>
                            <?php if(isset($attendanceTimeUnitSelected) && $key == $leaveTimeUnitSelected) { ?>
                                <option value="<?php echo($key); ?>" selected><?php echo($value); ?></option>
                            <?php } else { ?>
                                <option value="<?php echo($key); ?>"><?php echo($value); ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>退勤時間端数処理</label>
                    <select id="leave_time_rounding" name="leave_time_rounding" class="form-control">
                        <?php foreach($leaveTimeRounding as $key => $value){ ?>
                            <?php if(isset($attendanceTimeUnitSelected) && $key == $leaveTimeRoundingSelected) { ?>
                                <option value="<?php echo($key); ?>" selected><?php echo($value); ?></option>
                            <?php } else { ?>
                                <option value="<?php echo($key); ?>"><?php echo($value); ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>休憩時間単位</label>
                    <select id="break_time_unit" name="break_time_unit" class="form-control">
                        <?php foreach($breakTimeUnit as $key => $value){ ?>
                            <?php if(isset($attendanceTimeUnitSelected) && $key == $breakTimeUnitSelected) { ?>
                                <option value="<?php echo($key); ?>" selected><?php echo($value); ?></option>
                            <?php } else { ?>
                                <option value="<?php echo($key); ?>"><?php echo($value); ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>休憩時間端数処理</label>
                    <select id="break_time_rounding" name="break_time_rounding" class="form-control">
                        <?php foreach($breakTimeRounding as $key => $value){ ?>
                            <?php if(isset($attendanceTimeUnitSelected) && $key == $breakTimeRoundingSelected) { ?>
                                <option value="<?php echo($key); ?>" selected><?php echo($value); ?></option>
                            <?php } else { ?>
                                <option value="<?php echo($key); ?>"><?php echo($value); ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>最大退勤時間</label>
                    <input type="text" id="max_leave_time" name="max_leave_time" class="form-control" value="<?php echo($maxLeaveTime); ?>"/>
                </div>
            </div>
            <div class="modal-footer">
                <button id="registBtn" type="submit" class="btn btn-default">登録</button>
             <div id="error" style="color:red;"></div>
            </div>
        </form>
    </div>
</div>
