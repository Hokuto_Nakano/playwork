<?php echo $this->Html->script('attendancesetting', array('inline' => false)); ?>

<div>
    <h3>勤怠設定</h3>
</div>
<div class="col-sm-6">
    <button id="settingBtn" class="btn btn-primary" type="button">勤怠設定</button>
</div>
<div class="col-sm-6">
    <input type="file" name="file" />
</div>
<div class="col-sm-12 mt10">
    <div id="message">
    </div>
</div>
<div class="col-sm-12 mt30">
    <div id="contents">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>外部勤怠システムIDカラム</td>
                    <td>氏名カラム</td>
                    <td>日付カラム（出退勤共通）</td>
                    <td>出勤日カラム</td>
                    <td>退勤日カラム</td>
                    <td>出勤時刻カラム</td>
                    <td>退勤時刻カラム</td>
                    <td>出勤日時カラム</td>
                    <td>退勤日時カラム</td>
                    <td>実働時間カラム</td>
                    <td>休憩時間カラム</td>
                    <td>休憩入り時刻カラム</td>
                    <td>休憩戻り時刻カラム</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <?php if(isset($attendanceCsvMaps)) { ?>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['other_id_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['name_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['date_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['attendance_date_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['leave_date_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['attendance_time_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['leave_time_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['attendance_datetime_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['leave_datetime_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['work_hours_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['break_hours_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['break_in_time_column']); ?></td>
                        <td><?php echo($attendanceCsvMaps['AttendanceCsvMap']['break_out_time_column']); ?></td>
                    <?php } else { ?>
                        <td colspan="13">データはありません</td>
                    <?php } ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="attendanceModal" tabindex="-1">
</div>
