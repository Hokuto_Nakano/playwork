<?php echo $this->Html->script('attendancesetting', array('inline' => false)); ?>

<div id="mapping_contents">
    <div class="col-sm-6">
        <table class="table">
            <thead>
                <tr>
                    <td>CSVカラム</td>
                    <td>紐付けるデータ</td>
                </tr>
            </thead>
            <tbody>
               <% _.each(csvColumns, function(data) { %>
                    <tr>
                        <td>
                            <%= data.name %>
                        </td>
                        <td>
                            <select id="<%= data.key %>" name="<%= data.key %>" class="form-control">
                                <% _.each(csvMaps, function(csvMap){ %>
                                    <option value="<%= _.keys(csvMap) %>"><%= _.values(csvMap) %></option>
                                <% }); %>
                            </select>
                        </td>
                    </tr>
                <% }); %>
            </tbody>
        </table>
    </div>
    <button id="regBtn" class="btn btn-primary" type="button">登録</button>
</div>
