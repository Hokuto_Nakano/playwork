<?php echo $this->Html->script('account', array('inline' => false)); ?>

<h3>アカウント管理</h3>
<div id="message"></div>
<div class="col-sm-12 form-group">
    <div>ログインID</div>
    <div>
        <input type="text" name="loginId" value="<?php echo($loginId) ?>">
    </div>
</div>
<div class="col-sm-12 form-group">
    <div>パスワード</div>
    <div>
        <input type="password" name="password" value="<?php echo($password) ?>">
    </div>
</div>
<div class="col-sm-12 form-group">
    <button type="submit" class="btn btn-primary">更新</button>
</div>
