<?php echo $this->Html->script('rankingapcountdown', array('inline' => false)); ?>

<script>
    init.push(function () {
        var countData = <?php echo $graph ?>;
//             {"date": "2016-11-01", "licensed": 3407, "sorned": 660},
//             {"date": "2016-11-06", "licensed": 3351, "sorned": 629},
//             {"date": "2016-11-11", "licensed": 3269, "sorned": 618},
//             {"date": "2016-11-16", "licensed": 3246, "sorned": 661},
//             {"date": "2016-11-21", "licensed": 3171, "sorned": 676},
//             {"date": "2016-11-26", "licensed": 3155, "sorned": 681},
//             {"date": "2016-11-30", "licensed": 3226, "sorned": 620}
        ;
        //var mokuhyo = 4000;
//        for (var i = 1; i <= 30; i++) {
//            tax_data.push({
//                "date": "2016-11-" + i,
//                "mokuhyo": ((4000 / 30) * i > 4000) ? 4000 : (4000 / 30) * i,
//                "bp": (i < 20) ? i * 100 : null
//            });
//        }
        Morris.Line({
            element: 'hero-graph',
            data: countData,
            xkey: 'date',
            xLabels: 'day',
            ykeys: ['averageCount', 'goalCount'],
            //labels: ['bp', 'mokuhyo'],
            lineColors: PixelAdmin.settings.consts.COLORS,
            lineWidth: 2,
            pointSize: 3,
            gridLineColor: '#cfcfcf',
            //resize: true
        });
    });
</script>

<div class="row">
    <div class="col-sm-12">
        <form action="" class="panel form-horizontal">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="col-sm-4 panel-title text-center">目標獲得件数</div>
                        <div class="col-sm-4 panel-title text-center text-bold text-lg"><?= $goalTotal ?></div>
                        <div class="col-sm-4 panel-title text-center">Countdown!</div>
                    </div>
                    <div style="float:right;margin-right:24px;text-align:right;">
                        <button class="btn btn-default" type="button" onclick="location.href='/ranking_ap_countdowns/<?php echo $year.$month; ?>/1'">再計算</button>
                        <div style="font-size:11px;">最終更新：<?php echo $modified; ?></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="graph-container">
                        <div id="hero-graph" class="graph"></div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>事業部（エリア名）</th>
                            <th>目標</th>
                            <th>必要件数</th>
                            <th>実績</th>
                            <th>達成率</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!$goals) { ?>
                            <tr>
                                <td colspan="5">目標が設定されていません</td>
                            </tr>
                        <?php } ?>
                        <?php foreach ($goals as $goal) { ?>
                            <tr>
                                <td><?= $goal['divisionName']; ?>(<?= $goal['areaName']; ?>)</td>
                                <td><?= $goal["goalCount"]; ?></td>
                                <td><?= $goal["necessary"]; ?></td>
                                <td><?= $goal["achievement"]; ?></td>
                                <td><?= $goal['rate']; ?>%</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>
