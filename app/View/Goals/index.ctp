<?= $this->Html->script('goal', array('inline' => false)); ?>

<div id="message"></div>

<table class="table">
    <thead>
    <tr>
        <th>事業部</th>
        <th>エリア</th>
        <th>部署</th>
        <th>目標件数</th>
        <th>目標BP</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($bases as $base) { ?>
        <tr>
            <td>
                <input type="hidden" name="divisionId[]" value="<?= $base['division_id'] ?>">
                <?= $base['division_name']; ?></td>
            <td>
                <input type="hidden" name="areaId[]" value="<?= $base['area_id'] ?>">
                <?= $base['area_name']; ?></td>
            <td>
                <input type="hidden" name="sectionId[]" value="<?= $base['section_id'] ?>">
                <?= $base['section_name']; ?>
            </td>
            <td><input type="text" name="goalCount[]" value="<?= $base['goal_count'] ?>"></td>
            <td><input type="text" name="goalBp[]" value="<?= $base['goal_bp'] ?>"></td>
        </tr>
    <? } ?>
    </tbody>
</table>
<div>
    <button class="btn btn-primary">登録</button>
</div>
<div class="modal fade" id="baseModal" tabindex="-1"></div>
