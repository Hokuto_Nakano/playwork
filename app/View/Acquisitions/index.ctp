<?php echo $this->Html->script('acquisition', array('inline' => false)); ?>

<h3>獲得区分・BP情報</h3>
<div>
  <button class="btn btn-primary" type="button" onclick="getAcquisition('new')">新規登録</button>
  <a class="btn btn-default" type="button" href="./products" onclick="javascript:void(0)">商材登録</a>
</div>
<div>
  <table class="table">
    <thead>
      <tr>
        <td>商材名</td>
        <td>獲得区分</td>
        <td>獲得BP</td>
        <td></td>
      </tr>
    </thead>
    <tbody>
    <?php
      $beforeName = "";
      $productName = "";
      for($i=0; $i<count($acquisitions); $i++){
        $productName = ($beforeName != "" && $beforeName == $acquisitions[$i]['Product']['name']) ? "" : $acquisitions[$i]['Product']['name'];
    ?>
      <tr>
        <td><span id="name<?= $i ?>"><?= $productName ?></span></td>
        <td><span id="type<?= $i ?>"><?= $acquisitions[$i]['Acquisition']['type'] ?></span></td>
        <td><span id="bp<?= $i ?>"><?= $acquisitions[$i]['Acquisition']['bp'] ?></span></td>
        <td>
          <button type="button" class="btn btn-primary" data-toggle="modal" onclick="getAcquisition('<?= $acquisitions[$i]['Acquisition']['id']; ?>')">更新</button>
          <span>&nbsp;</span>
          <button type="button" class="btn btn-danger" onclick="deleteAcquisition('<?= $acquisitions[$i]['Acquisition']['id']; ?>')">削除</button>
        </td>
      </tr>
      <?php $beforeName = $acquisitions[$i]['Product']['name']; ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="modal fade" id="acquisitionModal" tabindex="-1">
</div>