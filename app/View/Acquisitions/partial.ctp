<?= $this->Html->script('acquisition', array('inline' => false)); ?>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form onsubmit="registAcquisition();return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">獲得区分・BP登録</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label>商材</label>
            <?php if($id > 0){ ?>
              <input type="text" id="product_name" name="product_name" class="form-control" value="<?= $selectProduct['name']; ?>" readonly/>
            <?php }else{ ?>
              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="width:100%;">
                  <span id="product_name"><?= $selectProduct['name']; ?></span>
                  <input type="hidden" id="product_id" name="product_id" value="<?= $selectProduct['id']; ?>"/>
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <?php foreach($products as $product){ ?>
                    <li>
                      <a href="#" onclick="setProduct('<?= $product['Product']['id']; ?>', '<?= $product['Product']['name']; ?>');" ><?= $product['Product']['name']; ?></a>
                    </li>
                  <?php } ?>
                </ul>
              </div>
            <?php } ?>
          </div>
          <div class="form-group">
            <label>獲得区分</label>
            <input type="text" id="type" name="type" class="form-control" value="<?= $type; ?>" <?= $id > 0 ? 'readonly':'required'; ?>/>
          </div>
          <div class="form-group">
            <label>獲得BP</label>
            <input type="text" id="bp" name="bp" class="form-control" value="<?= $bp; ?>"/>
          </div>
          <input type="hidden" id="id" name="id" class="form-control" value="<?= $id; ?>"/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">登録</button>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>