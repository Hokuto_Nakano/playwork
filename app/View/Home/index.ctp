
<div class="row" style="margin-bottom:16px;">
  <div class="col-sm-4">
    <a href="/input_formats">
      <div class="menu-panel cat_01">
        <i class="menu-icon fa fa-file-text"></i><div>獲得データ入出力</div>
      </div>
    </a>
  </div>
  <div class="col-sm-4">
    <a href="/attendances">
      <div class="menu-panel cat_02">
        <i class="menu-icon fa fa-file-text"></i><div>勤怠データ入出力</div>
      </div>
    </a>
  </div>
  <div class="col-sm-4">
    <a href="/">
      <div class="menu-panel cat_03">
        <i class="menu-icon fa fa-list-alt"></i><div>ランキング</div>
      </div>
    </a>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <a href="/staffs">
      <div class="menu-panel cat_04">
        <i class="menu-icon fa fa-users"></i><div>スタッフ管理</div>
      </div>
    </a>
  </div>
  <div class="col-sm-4">
    <a href="/products">
      <div class="menu-panel cat_05">
        <i class="menu-icon fa fa-briefcase"></i><div>商材管理</div>
      </div>
    </a>
  </div>
  <div class="col-sm-4">
    <a href="#">
      <div class="menu-panel cat_06">
        <i class="menu-icon fa fa-thumbs-up"></i><div>インセンティブ管理</div>
      </div>
    </a>
  </div>
</div>
