<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form id="countitemForm">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">カウント項目</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <label>項目名</label>
              <input type="text" id="item_name" name="item_name" class="form-control" value="<?= $countItem['item_name']; ?>" required/>
            </div>
          </div>
          <input type="hidden" id="item_id" name="item_id" class="form-control" value="<?= $countItem['id']; ?>"/>
      </div>
      <div class="modal-footer">
        <? if(!$countItem['id']){ ?>
          <button type="submit" class="btn btn-primary">登録</button>
        <? }else{ ?>
          <button type="submit" class="btn btn-primary">更新</button>
          <button id="remove" type="button" class="btn btn-danger">削除</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>
