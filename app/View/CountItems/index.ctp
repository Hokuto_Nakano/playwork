<?php echo $this->Html->script('countitem', array('inline' => true)); ?>

<div class="form-group">
  <button id="new" type="button" class="btn btn-primary">新規登録</button>
</div>
<table class="table">
  <thead>
    <tr>
      <th>id</th>
      <th>項目名</th>
    </tr>
  </thead>
  <tbody>
    <? foreach($countItems as $countItem){ ?>
    <tr id="<?= $countItem['CountItem']['id']; ?>" class="clickable update">
      <th><?= $countItem['CountItem']['id']; ?></th>
      <td><?= $countItem['CountItem']['item_name'] ?></td>
    </tr>
    <? } ?>
  </tbody>
</table>

<div class="modal fade" id="countItemModal" tabindex="-1"></div>
