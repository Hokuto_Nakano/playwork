<?php echo $this->Html->script('inputformat', array('format' => false)); ?>
<?php echo $this->Html->script('../assets/js/jquery.blockUI', array('format' => false)); ?>
<style>
    .alt-table-responsive {
        overflow-y: auto;
        overflow-x: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        -webkit-overflow-scrolling: touch;
    }
</style>

<?php if(count($productCategories) > 0){ ?>
<form type="post" name="csvForm" enctype="multipart/form-data">
    <div class="col-sm-4">
        <select id="product_category" name="product_category" class="form-control">
            <?php foreach($productCategories as $key => $value){ ?>
                <?php if ($value['ProductCategory']['id'] == $selected) { ?>
                    <option value="<?php echo($value['ProductCategory']['id']); ?>" selected><?php echo($value['ProductCategory']['name']); ?></option>
                <?php } else { ?>
                    <option value="<?php echo($value['ProductCategory']['id']); ?>"><?php echo($value['ProductCategory']['name']); ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </div>
    <div class="col-sm-8">
        <input type="file" name="csv" />
    </div>
    <div class="col-sm-12 mt10">
        <div id="message">
        </div>
    </div>
    <?php if(isset($columnMapData) && isset($inputTableData)) { ?>
        <div class="col-sm-12">
            <div style="float:right;margin-right:24px;">
                <strong class="lead">ページ指定</strong>
                <input type="number" name="pageNo" value="<?php echo($page); ?>" style="width:40px">
            </div>
        </div>
        <div class="col-sm-12">
            <div style="float:left;">
                <button id="allSelect" style="margin-left: 10px" class="btn btn-primary" type="button">全選択</button>
                <button id="allCancel" style="margin-left: 10px" class="btn btn-primary" type="button">全解除</button>
                <button id="delete" style="margin-left: 10px" class="btn btn-primary" type="button">削除</button>
            </div>
            <div style="float:right;margin-right:24px;">
                <strong class="lead"><?php echo($count); ?>件</strong>
                <strong class="lead"><?php echo($page); ?> / <span id="totalPage"><?php echo($totalPage); ?></span></strong>
                <button id="prev" style="margin-left: 10px" class="btn btn-primary" type="button">前へ</button>
                <button id="next" style="margin-left: 10px" class="btn btn-primary" type="button">次へ</button>
            </div>
        </div>
    <?php } else { ?>
        <div class="col-sm-12">
            <span>データはありません</span>
        </div>
    <?php } ?>
</form>
<div class="col-sm-12 mt30">
    <div id="contents" class="table-responsive alt-table-responsive" style="overflow: auto">
        <?php if(isset($columnMapData) && isset($inputTableData)) { ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>削除</td>
                        <td>ID</td>
                        <?php foreach($columnMapData as $key => $value) { ?>
                            <?php foreach ($value as $k => $v) { ?>
                                <td><?php echo($v['product_csv_column_name']); ?></td>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($inputTableData as $key => $value) { ?>
                        <tr>
                            <td>
                                <input type="checkbox" name="check[]">
                            </td>
                            <?php foreach ($value as $k => $v) { ?>
                                <?php if ($k == 'id') { ?>
                                    <input type="hidden" name="id" value="<?php echo $v; ?>">
                                <?php } ?>
                                <?php if ($k != 'regist_key') { ?>
                                    <td><?php echo($v); ?></td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</div>
<div id="pageInfo">
    <input type="hidden" name="count" value="<?php echo($count); ?>">
    <input type="hidden" name="page" value="<?php echo($page); ?>">
    <input type="hidden" name="offset" value="<?php echo($offset); ?>">
    <input type="hidden" name="limit" value="<?php echo($limit); ?>">
</div>
<?php }else{ ?>
<div class="col-md-12 lead">
  <p>商材が登録されていません。</p>
  <p>データをインポートする前に<a href="/products">商材を登録</a>してください</p>
</div>
<?php } ?>
