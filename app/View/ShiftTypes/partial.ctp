<?php echo $this->Html->script('shifttype', array('inline' => false)); ?>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">シフト区分登録</h4>
      </div>
      <div class="modal-body">
          <div id="form-first" class="form-group" style="display: none;">
            <label>シフト区分ID</label>
            <input type="text" id="id" name="id" class="form-control" value="<?php echo $id; ?>" readOnly/>
          </div>
          <div id="form-second" class="form-group">
              <label>シフト区分</label>
              <input type="text" id="type" name="type" class="form-control" value="<?php echo $type; ?>" required/>
          </div>
          <div id="form-third" class="form-group">
              <label>休憩有無フラグ</label>
              <div class="dropdown">
                  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="width:100%;">
                  <?php if(isset($is_break)) { ?>
                      <span id="is_break_label"><?php echo $is_break_label; ?></span>
                      <input type="hidden" id="is_break" name="is_break" value="<?php echo $is_break; ?>"/>
                  <?php } else { ?>
                          <span id="is_break_label">休憩なし</span>
                          <input type="hidden" id="is_break" name="is_break" value="0"/>
                  <?php } ?>
                 </button>
            <ul class="dropdown-menu">
                <li>
                    <a id="is_break_0" href="#">休憩なし</a>
                </li>
                <li>
                    <a id="is_break_1" href="#">休憩あり</a>
                </li>
            </ul>
          </div>
      </div>
      <div class="modal-footer">
        <button id="registBtn" type="submit" class="btn btn-default">登録</button>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>
