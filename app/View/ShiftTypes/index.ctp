<?php echo $this->Html->script('shifttype', array('inline' => false)); ?>

<h3>シフト区分情報</h3>
<button id="insert" class="btn btn-primary" type="button">新規登録</button>
<table class="table">
    <thead>
        <tr>
            <td>
                id
            </td>
            <td>
                type
            </td>
            <td>
                is_break
            </td>
            <td>
            </td>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<div class="modal fade" id="shifttypeModal" tabindex="-1">
</div>
