<?= $this->Html->script('productfamily', array('inline' => true)); ?>

<?php echo json_encode($family); ?>
<script type="text/javascript">
  var familyCtrl = new ProductFamilyCtrl();
</script>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form onsubmit="familyCtrl.regist();return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">商品ファミリ編集</h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
          <div class="col-md-8">
            <label>商品カテゴリ名</label>
            <input type="text" id="category_name" name="category_name" class="form-control" value="<?= $category['name']; ?>" disabled/>
            <input type="hidden" id="category_id" name="category_id" value="<?= $category['id']; ?>"/>
          </div>
          <div class="col-md-4">
            <label>商品カテゴリ省略名</label>
            <input type="text" id="category_short_name" name="category_short_name" class="form-control" value="<?= $category['short_name']; ?>" disabled/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label>商品ファミリ名</label>
            <input type="text" id="product_family_name" name="product_family_name" class="form-control" value="<?= $family['name']; ?>" required/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <label>商品ファミリ省略名</label>
            <input type="text" id="product_family_short_name" name="product_family_short_name" class="form-control" value="<?= $family['short_name']; ?>"/>
          </div>
        </div>
        <input type="hidden" id="product_family_id" name="product_family_id" value="<?= $family['id']; ?>"/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">更新</button>
        <? if($family['id']){ ?>
        <button type="button" class="btn btn-danger" onclick="familyCtrl.delete('<?= $family['id']; ?>');">削除</button>
        <? } ?>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>