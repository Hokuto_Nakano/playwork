<?php echo $this->Html->script('callcenter', array('inline' => false)); ?>

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <form>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">コールセンター登録</h4>
      </div>
      <div class="modal-body">
          <div id="form-first" class="form-group" style="display: none;">
            <label>コールセンターID</label>
            <input type="text" id="id" name="id" class="form-control" value="<?php echo $id; ?>" readOnly/>
          </div>
          <div id="form-second" class="form-group">
            <label>コールセンター名</label>
            <input type="text" id="name" name="name" class="form-control" value="<?php echo $name; ?>" required/>
          </div>
      </div>
      <div class="modal-footer">
        <button id="registBtn" type="submit" class="btn btn-default">登録</button>
        <div id="error" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>
