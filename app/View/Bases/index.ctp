<?= $this->Html->script('base', array('inline' => false)); ?>

<script type="text/javascript">
  var baseCtrl = new BaseCtrl();
</script>

<h3>拠点登録</h3>
<button class="btn btn-primary" onclick="baseCtrl.showSectionModal('new');">新規登録</button>
<table class="table">
  <thead>
    <tr>
      <th>事業部</th>
      <th>エリア</th>
      <th>部署</th>
    </tr>
  </thead>
  <tbody>
    <? foreach($bases as $base){ ?>
    <tr>
      <td class="clickable" onclick="baseCtrl.showDivisionModal('<?= $base['division_id']; ?>');"><?= $base['division_name']; ?></td>
      <?php if($base['area_id']){ ?>
      <td class="clickable" onclick="baseCtrl.showAreaModal('<?= $base['area_id']; ?>');"><?= $base['area_name']; ?></td>
      <? }else{ ?>
      <td><?= $base['area_name']; ?></td>
      <? } ?>
      <?php if($base['section_id']){ ?>
      <td class="clickable" onclick="baseCtrl.showSectionModal('<?= $base['section_id']; ?>');"><?= $base['section_name']; ?></td>
      <? }else{ ?>
      <td><?= $base['section_name']; ?></td>
      <? } ?>
    </tr>
    <? } ?>
  </tbody>
</table>
<div class="modal fade" id="baseModal" tabindex="-1"></div>
