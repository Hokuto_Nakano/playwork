<?php
App::uses('AppModel', 'Model');
App::uses('Incentive', 'Model');
App::uses('IncentiveRule', 'Model');
App::uses('StaffCountUp', 'Model');
App::uses('StaffHistory', 'Model');
App::uses('ProductCategory', 'Model');

class StaffIncentive extends AppModel {

    function getIncentive($staffId) {
        $staffIncentives = $this->find('all', array(
            'conditions' => array(
                'StaffIncentive.staff_id' => $staffId
            ),
            'fields' => array(
                'StaffIncentive.staff_id',
                'sum(StaffIncentive.incentive) incentive',
                'Incentive.hourly_plus'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'incentives',
                    'alias' => 'Incentive',
                    'conditions' => array(
                        'Incentive.id = StaffIncentive.incentive_id'
                    )
                )
            ),
            'group' => array(
                'Incentive.hourly_plus'
            )
        ));

        $data = array();
        foreach ($staffIncentives as $staffIncentive) {
            $data[] = array(
                'incentive' => $staffIncentive[0]['incentive'],
                'hourlyPlus' => $staffIncentive['Incentive']['hourly_plus']
            );
        }

        return $data;
    }

    function calcIncentive($startMonth) {

        $this->StaffCountUp = new StaffCountUp();
        $this->StaffHistory = new StaffHistory();
        $this->ProductCategory = new ProductCategory();
        $this->Incentive = new Incentive();

        // incentiveを参照
        $incentives = $this->Incentive->getAll();

        foreach ($incentives as $j => $incentive) {
            $incentiveId = $incentive['Incentive']['id'];
            $aggregatePeriod = $incentive['Incentive']['aggregate_period'];
            $incentiveTerm = $incentive['Incentive']['incentive_term'];
            $rankingRange = $incentive['Incentive']['ranking_range'];
            $targetType = $incentive['Incentive']['target_type'];

            // delete-insert
            $this->deleteAll(array('incentive_id' => $incentiveId));

            if ($aggregatePeriod == 'monthly') {
                // 月間ランキング
                if ($incentiveTerm == 'rank') {

                    if ($rankingRange == 'all') {
                        // 月間ーランキングー全体
                        if (INCENTIVE_COUNT_TYPE == 0) {
                            $this->doMonthlyAllBpRanking($incentive, $targetType, $startMonth);
                        } else {
                            $this->doMonthlyAllRanking($incentive, $targetType);
                        }
                    } else if ($rankingRange == 'division') {
                        // 月間ーランキングー事業部
                        if (INCENTIVE_COUNT_TYPE == 0) {
                            $this->doMonthlyDivisionBpRanking($incentive, $targetType, $startMonth);
                        } else {
                            $this->doMonthlyDivisionRanking($incentive, $targetType);
                        }
                    } else if ($rankingRange == 'area') {
                        // 月間ーランキングーエリア
                        if (INCENTIVE_COUNT_TYPE == 0) {
                            $this->doMonthlyAreaBpRanking($incentive, $targetType, $startMonth);
                        } else {
                            $this->doMonthlyAreaRanking($incentive, $targetType);
                        }
                    } else {
                        // 月間ーランキングー部署
                        if (INCENTIVE_COUNT_TYPE == 0) {
                            $this->doMonthlySectionBpRanking($incentive, $targetType, $startMonth);
                        } else {
                            $this->doMonthlySectionRanking($incentive, $targetType);
                        }
                    }
                } else {
                    // 月間件数
                    if (INCENTIVE_COUNT_TYPE == 0) {
                        $this->doMonthlyBp($incentive, $targetType, $startMonth);
                    } else {
                        $this->doMonthlyCount($incentive, $targetType);
                    }
                }

            } else {
                // 累計
                if ($incentiveTerm == 'rank') {

                    if ($rankingRange == 'all') {
                        // 累計ーランキングー全体
                        if (INCENTIVE_COUNT_TYPE == 0) {
                            $this->doTotalAllBpRanking($incentive, $targetType, $startMonth);
                        } else {
                            $this->doTotalAllRanking($incentive, $targetType);
                        }
                    } else if ($rankingRange == 'division') {
                        // 累計ーランキングー事業部
                        if (INCENTIVE_COUNT_TYPE == 0) {
                            $this->doTotalDivisionBpRanking($incentive, $targetType, $startMonth);
                        } else {
                            $this->doTotalDivisionRanking($incentive, $targetType);
                        }
                    } else if ($rankingRange == 'area') {
                        // 累計ーランキングーエリア
                        if (INCENTIVE_COUNT_TYPE == 0) {
                            $this->doTotalAreaBpRanking($incentive, $targetType, $startMonth);
                        } else {
                            $this->doTotalAreaRanking($incentive, $targetType);
                        }
                    } else {
                        // 累計ーランキングー部署
                        if (INCENTIVE_COUNT_TYPE == 0) {
                            $this->doTotalSectionBpRanking($incentive, $targetType, $startMonth);
                        } else {
                            $this->doTotalSectionRanking($incentive, $targetType);
                        }
                    }

                } else {
                    // 累計件数
                    if (INCENTIVE_COUNT_TYPE == 0) {
                        $this->doTotalBp($incentive, $targetType, $startMonth);
                    } else {
                        $this->doTotalCount($incentive, $targetType);
                    }
                }
            }
        }
    }

    function doMonthlyAllRanking($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];

        // 集計年月を取得
        $targetMonths = $this->StaffCountUp->getTargetMonths();

        if ($targetType == 'staff') {
            // 月間ーランキングー全体ー社員区分
            foreach ($targetMonths as $targetMonth) {
                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getMonthlyAllRanking($targetMonth);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                $idx = 0;
                $lastStaffTypeId = 0;
                foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                    $staffTypeId = $incentiveRule['staff_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];

                    // 社員区分が変わった場合
                    if ($staffTypeId != $lastStaffTypeId) {
                        $idx = 0;
                    }

                    // 社員区分で除外
                    $callback = function ($staffCountUp) use ($staffTypeId) {
                        return ($staffCountUp['staffTypeId'] == $staffTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    if ($idx > count($staffCountUps)-1) break;

                    $staffCountUp = $staffCountUps[$idx];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastStaffTypeId = $staffTypeId;
                    $idx++;
                }
            }
        } else if ($targetType == 'job') {
            // 月間ーランキングー全体ー職種区分
            foreach ($targetMonths as $targetMonth) {
                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getMonthlyAllRanking($targetMonth);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                $idx = 0;
                $lastJobTypeId = 0;
                foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                    $jobTypeId = $incentiveRule['job_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];

                    // 職種区分が変わった場合
                    if ($jobTypeId != $lastJobTypeId) {
                        $idx = 0;
                    }

                    // 職種区分で除外
                    $callback = function ($staffCountUp) use ($jobTypeId) {
                        return ($staffCountUp['jobTypeId'] == $jobTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    if ($idx > count($staffCountUps)-1) break;

                    $staffCountUp = $staffCountUps[$idx];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastJobTypeId = $jobTypeId;
                    $idx++;
                }
            }
        } else {
            // 月間ーランキングー全体ー区分なし
            foreach ($targetMonths as $targetMonth) {
                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getMonthlyAllRanking($targetMonth);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                    $incentiveAmount = $incentiveRule['incentive'];

                    if ($rank > count($staffCountUps)-1) break;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }
                }
            }
        }
    }

    function doMonthlyAllBpRanking($incentive, $targetType, $startMonth) {
        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        // 集計年月を取得
        $targetMonths = $this->StaffCountUp->getTargetMonths();

        if ($targetType == 'staff') {
            // 月間ーランキングー全体ー社員区分
            foreach ($targetMonths as $targetMonth) {
                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaff($targetMonth);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'staffTypeId' => $staff['staffTypeId'],
                        'jobTypeId' => $staff['jobTypeId'],
                        'count' => $totalBp,
                        'month' => $targetMonth
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                $idx = 0;
                $lastStaffTypeId = 0;
                foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                    $staffTypeId = $incentiveRule['staff_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];

                    // 社員区分が変わった場合
                    if ($staffTypeId != $lastStaffTypeId) {
                        $idx = 0;
                    }

                    // 社員区分で対象外は除外
                    $callback = function ($staffCountUp) use ($staffTypeId) {
                        return ($staffCountUp['staffTypeId'] == $staffTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    if ($idx > count($staffCountUps)-1) break;

                    $staffCountUp = $staffCountUps[$idx];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    // ランキングに合致したインセンティブを登録（1、1位、第1位を想定）
                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastStaffTypeId = $staffTypeId;
                    $idx++;
                }
            }
        } else if ($targetType == 'job') {
            // 月間ーランキングー全体ー職種区分
            foreach ($targetMonths as $targetMonth) {
                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaff($targetMonth);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'staffTypeId' => $staff['staffTypeId'],
                        'jobTypeId' => $staff['jobTypeId'],
                        'count' => $totalBp,
                        'month' => $targetMonth
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                $idx = 0;
                $lastJobTypeId = 0;
                foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                    $jobTypeId = $incentiveRule['job_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];

                    // 職種区分が変わった場合
                    if ($jobTypeId != $lastJobTypeId) {
                        $idx = 0;
                    }

                    // 職種区分で対象外は除外
                    $callback = function ($staffCountUp) use ($jobTypeId) {
                        return ($staffCountUp['jobTypeId'] == $jobTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    if ($idx > count($staffCountUps)-1) break;

                    $staffCountUp = $staffCountUps[$idx];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    // ランキングに合致したインセンティブを登録（1、1位、第1位を想定）
                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastJobTypeId = $jobTypeId;
                    $idx++;
                }
            }
        } else {
            // 月間ーランキングー全体ー区分なし
            foreach ($targetMonths as $targetMonth) {
                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaff($targetMonth);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp,
                        'month' => $targetMonth
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                    $incentiveAmount = $incentiveRule['incentive'];

                    if ($rank > count($staffCountUps)-1) break;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }
                }
            }
        }
    }

    function doMonthlyDivisionRanking($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];

        // 集計年月を取得
        $targetMonths = $this->StaffCountUp->getTargetMonths();

        if ($targetType == 'staff') {

            // 月間ーランキングー事業部ー社員区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastStaffTypeId = 0;
                $lastDivisionId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $staffTypeId = $incentiveRule['staff_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $divisionId = $incentiveRule['division_id'];

                    // ランキングを取得（獲得件数順にソート）
                    $staffCountUps = $this->StaffCountUp->getMonthlyDivisionRanking($targetMonth, $divisionId);

                    // 獲得件数=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 社員区分が変わった場合 (複数の社員区分でインセンティブ定義)
                    if ($staffTypeId != $lastStaffTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 社員区分で対象外は除外
                    $callback = function ($staffCountUp) use ($staffTypeId) {
                        return ($staffCountUp['staffTypeId'] == $staffTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastStaffTypeId = $staffTypeId;
                    $lastDivisionId = $divisionId;
                    $rank++;
                }
            }
        } else if ($targetType == 'job') {
            // 月間ーランキングー事業部ー職種区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastJobTypeId = 0;
                $lastDivisionId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $jobTypeId = $incentiveRule['job_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $divisionId = $incentiveRule['division_id'];

                    // ランキングを取得（獲得件数順にソート）
                    $staffCountUps = $this->StaffCountUp->getMonthlyDivisionRanking($targetMonth, $divisionId);

                    // 獲得件数=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($jobTypeId != $lastJobTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 社員区分で対象外は除外
                    $callback = function ($staffCountUp) use ($jobTypeId) {
                        return ($staffCountUp['jobTypeId'] == $jobTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastJobTypeId = $jobTypeId;
                    $lastDivisionId = $divisionId;
                    $rank++;
                }
            }
        } else {
            // 月間ーランキングー事業部ー区分なし
            foreach ($targetMonths as $targetMonth) {
                $lastDivisionId = 0;
                $rank = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $incentiveAmount = $incentiveRule['incentive'];
                    $divisionId = $incentiveRule['division_id'];

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // ランキングを取得（獲得件数順にソート）
                    $staffCountUps = $this->StaffCountUp->getMonthlyDivisionRanking($targetMonth, $divisionId);

                    // 獲得件数=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }
                    $lastDivisionId = $divisionId;
                    $rank++;
                }
            }
        }
    }

    function doMonthlyDivisionBpRanking($incentive, $targetType, $startMonth) {
        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        // 集計年月を取得
        $targetMonths = $this->StaffCountUp->getTargetMonths();

        if ($targetType == 'staff') {

            // 月間ーランキングー事業部ー社員区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastStaffTypeId = 0;
                $lastDivisionId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $StaffTypeId = $incentiveRule['staff_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $divisionId = $incentiveRule['division_id'];

                    // 社員IDを取得
                    $staffs = $this->StaffHistory->getLatestStaffByDivisionId($divisionId, $targetMonth);
                    $staffCountUps = array();
                    $sort = array();

                    foreach ($staffs as $idx => $staff) {
                        $totalBp = 0;
                        foreach ($productCategories as $productCategory) {
                            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                                $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                                $totalBp += $data['bp'];
                            }
                        }
                        // 社員IDと獲得BPを設定
                        $staffCountUps[] = array(
                            'staffId' => $staff['staffId'],
                            'staffTypeId' => $staff['staffTypeId'],
                            'jobTypeId' => $staff['jobTypeId'],
                            'count' => $totalBp,
                            'month' => $targetMonth
                        );

                        // ソートのキーは獲得BP
                        $sort[$idx] = $totalBp;
                    }

                    // ソート
                    if ($staffCountUps) {
                        array_multisort($sort, SORT_DESC, $staffCountUps);
                    }

                    // 獲得BP=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 社員区分が変わった場合 (複数の社員区分でインセンティブ定義)
                    if ($StaffTypeId != $lastStaffTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 社員区分で対象外は除外
                    $callback = function ($staffCountUp) use ($StaffTypeId) {
                        return ($staffCountUp['staffTypeId'] == $StaffTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }
                    $lastStaffTypeId = $StaffTypeId;
                    $lastDivisionId = $divisionId;
                    $rank++;
                }
            }
        } else if ($targetType == 'job') {
            // 月間ーランキングー事業部ー職種区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastJobTypeId = 0;
                $lastDivisionId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $jobTypeId = $incentiveRule['job_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $divisionId = $incentiveRule['division_id'];

                    // 社員IDを取得
                    $staffs = $this->StaffHistory->getLatestStaffByDivisionId($divisionId, $targetMonth);
                    $staffCountUps = array();
                    $sort = array();

                    foreach ($staffs as $idx => $staff) {
                        $totalBp = 0;
                        foreach ($productCategories as $productCategory) {
                            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                                $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                                $totalBp += $data['bp'];
                            }
                        }
                        // 社員IDと獲得BPを設定
                        $staffCountUps[] = array(
                            'staffId' => $staff['staffId'],
                            'staffTypeId' => $staff['staffTypeId'],
                            'jobTypeId' => $staff['jobTypeId'],
                            'count' => $totalBp,
                            'month' => $targetMonth
                        );

                        // ソートのキーは獲得BP
                        $sort[$idx] = $totalBp;
                    }

                    // ソート
                    if ($staffCountUps) {
                        array_multisort($sort, SORT_DESC, $staffCountUps);
                    }

                    // 獲得BP=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($jobTypeId != $lastJobTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 職種区分で対象外は除外
                    $callback = function ($staffCountUp) use ($jobTypeId) {
                        return ($staffCountUp['jobTypeId'] == $jobTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastJobTypeId = $jobTypeId;
                    $lastDivisionId = $divisionId;
                    $rank++;
                }
            }
        } else {
            // 月間ーランキングー事業部ー区分なし
            foreach ($targetMonths as $targetMonth) {
                $lastDivisionId = 0;
                $rank = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $incentiveAmount = $incentiveRule['incentive'];
                    $divisionId = $incentiveRule['division_id'];

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 社員IDを取得
                    $staffs = $this->StaffHistory->getLatestStaffByDivisionId($divisionId, $targetMonth);
                    $staffCountUps = array();
                    $sort = array();

                    foreach ($staffs as $idx => $staff) {
                        $totalBp = 0;
                        foreach ($productCategories as $productCategory) {
                            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                                $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                                $totalBp += $data['bp'];
                            }
                        }
                        // 社員IDと獲得BPを設定
                        $staffCountUps[] = array(
                            'staffId' => $staff['staffId'],
                            'count' => $totalBp,
                            'month' => $targetMonth
                        );

                        // ソートのキーは獲得BP
                        $sort[$idx] = $totalBp;
                    }

                    // ソート
                    if ($staffCountUps) {
                        array_multisort($sort, SORT_DESC, $staffCountUps);
                    }

                    // 獲得BP=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }
                    $lastDivisionId = $divisionId;
                    $rank++;
                }
            }
        }
    }

    function doMonthlyAreaRanking($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];

        // 集計年月を取得
        $targetMonths = $this->StaffCountUp->getTargetMonths();
        if ($targetType == 'staff') {

            // 月間ーランキングーエリアー社員区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastStaffTypeId = 0;
                $lastAreaId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $staffTypeId = $incentiveRule['staff_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $areaId = $incentiveRule['area_id'];

                    // ランキングを取得（獲得件数順にソート）
                    $staffCountUps = $this->StaffCountUp->getMonthlyAreaRanking($targetMonth, $areaId);

                    // 獲得件数=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($staffTypeId != $lastStaffTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 社員区分で除外
                    $callback = function ($staffCountUp) use ($staffTypeId) {
                        return ($staffCountUp['staffTypeId'] == $staffTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastStaffTypeId = $staffTypeId;
                    $lastAreaId = $areaId;
                    $rank++;
                }
            }
        } else if ($targetType == 'job') {
            // 月間ーランキングーエリアー職種区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastJobTypeId = 0;
                $lastAreaId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $jobTypeId = $incentiveRule['job_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $areaId = $incentiveRule['area_id'];

                    // ランキングを取得（獲得件数順にソート）
                    $staffCountUps = $this->StaffCountUp->getMonthlyAreaRanking($targetMonth, $areaId);

                    // 獲得件数=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($jobTypeId != $lastJobTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 職種区分で除外
                    $callback = function ($staffCountUp) use ($jobTypeId) {
                        return ($staffCountUp['jobTypeId'] == $jobTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastJobTypeId = $jobTypeId;
                    $lastAreaId = $areaId;
                    $rank++;
                }
            }
        } else {
            // 月間ーランキングーエリアー区分なし
            foreach ($targetMonths as $targetMonth) {
                $lastAreaId = 0;
                $rank = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $incentiveAmount = $incentiveRule['incentive'];
                    $areaId = $incentiveRule['area_id'];

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // ランキングを取得（獲得件数順にソート）
                    $staffCountUps = $this->StaffCountUp->getMonthlyAreaRanking($targetMonth, $areaId);

                    // 獲得件数=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }
                    $lastAreaId = $areaId;
                    $rank++;
                }
            }
        }
    }

    function doMonthlyAreaBpRanking($incentive, $targetType, $startMonth) {
        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        // 集計年月を取得
        $targetMonths = $this->StaffCountUp->getTargetMonths();
        if ($targetType == 'staff') {

            // 月間ーランキングーエリアー社員区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastStaffTypeId = 0;
                $lastAreaId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $staffTypeId = $incentiveRule['staff_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $areaId = $incentiveRule['area_id'];

                    // 社員IDを取得
                    $staffs = $this->StaffHistory->getLatestStaffByAreaId($areaId, $targetMonth);
                    $staffCountUps = array();
                    $sort = array();

                    foreach ($staffs as $idx => $staff) {
                        $totalBp = 0;
                        foreach ($productCategories as $productCategory) {
                            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                                $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                                $totalBp += $data['bp'];
                            }
                        }
                        // 社員IDと獲得BPを設定
                        $staffCountUps[] = array(
                            'staffId' => $staff['staffId'],
                            'staffTypeId' => $staff['staffTypeId'],
                            'jobTypeId' => $staff['jobTypeId'],
                            'count' => $totalBp,
                            'month' => $targetMonth
                        );

                        // ソートのキーは獲得BP
                        $sort[$idx] = $totalBp;
                    }

                    // ソート
                    if ($staffCountUps) {
                        array_multisort($sort, SORT_DESC, $staffCountUps);
                    }

                    // 獲得BP=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($staffTypeId != $lastStaffTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 職種区分で対象外は除外
                    $callback = function ($staffCountUp) use ($staffTypeId) {
                        return ($staffCountUp['staffTypeId'] == $staffTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastStaffTypeId = $staffTypeId;
                    $lastAreaId = $areaId;
                    $rank++;
                }
            }
        } else if ($targetType == 'job') {
            // 月間ーランキングーエリアー職種区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastJobTypeId = 0;
                $lastAreaId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $jobTypeId = $incentiveRule['job_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $areaId = $incentiveRule['area_id'];

                    $staffs = $this->StaffHistory->getLatestStaffByAreaId($areaId, $targetMonth);
                    $staffCountUps = array();
                    $sort = array();

                    foreach ($staffs as $idx => $staff) {
                        $totalBp = 0;
                        foreach ($productCategories as $productCategory) {
                            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                                $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                                $totalBp += $data['bp'];
                            }
                        }
                        // 社員IDと獲得BPを設定
                        $staffCountUps[] = array(
                            'staffId' => $staff['staffId'],
                            'staffTypeId' => $staff['staffTypeId'],
                            'jobTypeId' => $staff['jobTypeId'],
                            'count' => $totalBp,
                            'month' => $targetMonth
                        );

                        // ソートのキーは獲得BP
                        $sort[$idx] = $totalBp;
                    }

                    // ソート
                    if ($staffCountUps) {
                        array_multisort($sort, SORT_DESC, $staffCountUps);
                    }

                    // 獲得BP=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($jobTypeId != $lastJobTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 職種区分で対象外は除外
                    $callback = function ($staffCountUp) use ($jobTypeId) {
                        return ($staffCountUp['jobTypeId'] == $jobTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastJobTypeId = $jobTypeId;
                    $lastAreaId = $areaId;
                    $rank++;
                }
            }
        } else {
            // 月間ーランキングーエリアー区分なし
            foreach ($targetMonths as $targetMonth) {
                $lastAreaId = 0;
                $rank = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $incentiveAmount = $incentiveRule['incentive'];
                    $areaId = $incentiveRule['area_id'];

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    $staffs = $this->StaffHistory->getLatestStaffByAreaId($areaId, $targetMonth);
                    $staffCountUps = array();
                    $sort = array();

                    foreach ($staffs as $idx => $staff) {
                        $totalBp = 0;
                        foreach ($productCategories as $productCategory) {
                            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                                $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                                $totalBp += $data['bp'];
                            }
                        }
                        // 社員IDと獲得BPを設定
                        $staffCountUps[] = array(
                            'staffId' => $staff['staffId'],
                            'count' => $totalBp,
                            'month' => $targetMonth
                        );

                        // ソートのキーは獲得BP
                        $sort[$idx] = $totalBp;
                    }

                    // ソート
                    if ($staffCountUps) {
                        array_multisort($sort, SORT_DESC, $staffCountUps);
                    }

                    // 獲得BP=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }
                    $lastAreaId = $areaId;
                    $rank++;
                }
            }
        }
    }

    function doMonthlySectionRanking($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];

        // 集計年月を取得
        $targetMonths = $this->StaffCountUp->getTargetMonths();
        if ($targetType == 'staff') {

            // 月間ーランキングー部署ー社員区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastStaffTypeId = 0;
                $lastSectionId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $staffTypeId = $incentiveRule['staff_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $sectionId = $incentiveRule['section_id'];

                    // ランキングを取得（獲得件数順にソート）
                    $staffCountUps = $this->StaffCountUp->getMonthlySectionRanking($targetMonth, $sectionId);

                    // 獲得件数=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($staffTypeId != $lastStaffTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 社員区分で除外
                    $callback = function ($staffCountUp) use ($staffTypeId) {
                        return ($staffCountUp['staffTypeId'] == $staffTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastStaffTypeId = $staffTypeId;
                    $lastSectionId = $sectionId;
                    $rank++;
                }
            }
        } else if ($targetType == 'job') {
            // 月間ーランキングー部署ー職種区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastJobTypeId = 0;
                $lastSectionId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $jobTypeId = $incentiveRule['job_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $sectionId = $incentiveRule['section_id'];

                    // ランキングを取得（獲得件数順にソート）
                    $staffCountUps = $this->StaffCountUp->getMonthlySectionRanking($targetMonth, $sectionId);

                    // 獲得件数=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($jobTypeId != $lastJobTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // 職種区分で除外
                    $callback = function ($staffCountUp) use ($jobTypeId) {
                        return ($staffCountUp['jobTypeId'] == $jobTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];
                    $staffHistoryJobTypeId = $this->StaffHistory->getJobTypeId($staffId, $month);

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastJobTypeId = $jobTypeId;
                    $lastSectionId = $sectionId;
                    $rank++;
                }
            }
        } else {
            // 月間ーランキングー部署ー区分なし
            foreach ($targetMonths as $targetMonth) {
                $lastSectionId = 0;
                $rank = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $incentiveAmount = $incentiveRule['incentive'];
                    $sectionId = $incentiveRule['section_id'];

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    // ランキングを取得（獲得件数順にソート）
                    $staffCountUps = $this->StaffCountUp->getMonthlySectionRanking($targetMonth, $sectionId);

                    // 獲得件数=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }
                    $lastSectionId = $sectionId;
                    $rank++;
                }
            }
        }
    }

    function doMonthlySectionBpRanking($incentive, $targetType, $startMonth) {
        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        // 集計年月を取得
        $targetMonths = $this->StaffCountUp->getTargetMonths();
        if ($targetType == 'staff') {

            // 月間ーランキングー部署ー社員区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastStaffTypeId = 0;
                $lastSectionId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $staffTypeId = $incentiveRule['staff_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $sectionId = $incentiveRule['section_id'];

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($staffTypeId != $lastStaffTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    $staffs = $this->StaffHistory->getLatestStaffBySectionId($sectionId, $targetMonth);
                    $staffCountUps = array();
                    $sort = array();

                    foreach ($staffs as $idx => $staff) {
                        $totalBp = 0;
                        foreach ($productCategories as $productCategory) {
                            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                                $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                                $totalBp += $data['bp'];
                            }
                        }
                        // 社員IDと獲得BPを設定
                        $staffCountUps[] = array(
                            'staffId' => $staff['staffId'],
                            'staffTypeId' => $staff['staffTypeId'],
                            'jobTypeId' => $staff['jobTypeId'],
                            'count' => $totalBp,
                            'month' => $targetMonth
                        );

                        // ソートのキーは獲得BP
                        $sort[$idx] = $totalBp;
                    }

                    // ソート
                    if ($staffCountUps) {
                        array_multisort($sort, SORT_DESC, $staffCountUps);
                    }

                    // 獲得BP=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分で対象外は除外
                    $callback = function ($staffCountUp) use ($staffTypeId) {
                        return ($staffCountUp['staffTypeId'] == $staffTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastStaffTypeId = $staffTypeId;
                    $lastSectionId = $sectionId;
                    $rank++;
                }
            }
        } else if ($targetType == 'job') {
            // 月間ーランキングー部署ー職種区分
            foreach ($targetMonths as $targetMonth) {
                $rank = 0;
                $lastJobTypeId = 0;
                $lastSectionId = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $jobTypeId = $incentiveRule['job_type_id'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    $sectionId = $incentiveRule['section_id'];

                    // 職種区分が変わった場合 (複数の職種区分でインセンティブ定義)
                    if ($jobTypeId != $lastJobTypeId) {
                        $rank = 0;
                    }

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    $staffs = $this->StaffHistory->getLatestStaffBySectionId($sectionId, $targetMonth);
                    $staffCountUps = array();
                    $sort = array();

                    foreach ($staffs as $idx => $staff) {
                        $totalBp = 0;
                        foreach ($productCategories as $productCategory) {
                            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                                $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                                $totalBp += $data['bp'];
                            }
                        }
                        // 社員IDと獲得BPを設定
                        $staffCountUps[] = array(
                            'staffId' => $staff['staffId'],
                            'staffTypeId' => $staff['staffTypeId'],
                            'jobTypeId' => $staff['jobTypeId'],
                            'count' => $totalBp,
                            'month' => $targetMonth
                        );

                        // ソートのキーは獲得BP
                        $sort[$idx] = $totalBp;
                    }

                    // ソート
                    if ($staffCountUps) {
                        array_multisort($sort, SORT_DESC, $staffCountUps);
                    }

                    // 獲得BP=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // 職種区分で対象外は除外
                    $callback = function ($staffCountUp) use ($jobTypeId) {
                        return ($staffCountUp['jobTypeId'] == $jobTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }

                    $lastJobTypeId = $jobTypeId;
                    $lastSectionId = $sectionId;
                    $rank++;
                }
            }
        } else {
            // 月間ーランキングー部署ー区分なし
            foreach ($targetMonths as $targetMonth) {
                $lastSectionId = 0;
                $rank = 0;
                foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {

                    $incentiveAmount = $incentiveRule['incentive'];
                    $sectionId = $incentiveRule['section_id'];

                    // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                    if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                        $rank = 0;
                    }

                    $staffs = $this->StaffHistory->getLatestStaffBySectionId($sectionId, $targetMonth);
                    $staffCountUps = array();
                    $sort = array();

                    foreach ($staffs as $idx => $staff) {
                        $totalBp = 0;
                        foreach ($productCategories as $productCategory) {
                            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                                $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                                $totalBp += $data['bp'];
                            }
                        }
                        // 社員IDと獲得BPを設定
                        $staffCountUps[] = array(
                            'staffId' => $staff['staffId'],
                            'count' => $totalBp,
                            'month' => $targetMonth
                        );

                        // ソートのキーは獲得BP
                        $sort[$idx] = $totalBp;
                    }

                    // ソート
                    if ($staffCountUps) {
                        array_multisort($sort, SORT_DESC, $staffCountUps);
                    }

                    // 獲得BP=0は対象外
                    $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                        return $staffCountUp['count'] > 0;
                    });

                    // ランキングがない場合は以降の処理はスルー
                    if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                    $staffCountUp = $staffCountUps[$rank];
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];

                    $data = array(
                        'staff_id' => $staffId,
                        'incentive_id' => $incentiveId,
                        'incentive' => $incentiveAmount,
                        'month' => $month
                    );
                    $this->create();
                    if (!$this->save($data)) {
                        throw new InternalErrorException();
                    }
                    $lastSectionId = $sectionId;
                    $rank++;
                }
            }
        }
    }

    function doTotalAllRanking($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];

        // ランキングを取得（獲得件数順にソート）
        $staffCountUps = $this->StaffCountUp->getTotalCount();

        // 獲得件数=0は対象外
        $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
            return $staffCountUp['count'] > 0;
        });

        if ($targetType == 'staff') {
            // 累計ーランキングー全体ー社員区分
            $idx = 0;
            $lastStaffTypeId = 0;
            foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];

                // 社員区分が変わった場合
                if ($staffTypeId != $lastStaffTypeId) {
                    $idx = 0;
                }

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                if ($idx > count($staffCountUps)-1) break;

                $staffId = $staffCountUps[$idx]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastStaffTypeId = $staffTypeId;
                $idx++;
            }
        } else if ($targetType == 'job') {
            // 累計ーランキングー全体ー職種区分
            $idx = 0;
            $lastJobTypeId = 0;
            foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];

                // 職種区分が変わった場合
                if ($jobTypeId != $lastJobTypeId) {
                    $idx = 0;
                }

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                if ($idx > count($staffCountUps)-1) break;

                $staffId = $staffCountUps[$idx]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastJobTypeId = $jobTypeId;
                $idx++;
            }
        } else {
            // 累計ーランキングー全体ー区分なし
            foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                $incentiveAmount = $incentiveRule['incentive'];

                if ($rank > count($staffCountUps)-1) break;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount,
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
            }
        }
    }

    function doTotalAllBpRanking($incentive, $targetType, $startMonth) {
        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        // 社員IDを取得
        $staffs = $this->StaffHistory->getLatestStaff();
        $staffCountUps = array();
        $sort = array();

        foreach ($staffs as $idx => $staff) {
            $totalBp = 0;
            foreach ($productCategories as $productCategory) {
                foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                    $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                    $totalBp += $data['bp'];
                }
            }
            // 社員IDと獲得BPを設定
            $staffCountUps[] = array(
                'staffId' => $staff['staffId'],
                'staffTypeId' => $staff['staffTypeId'],
                'jobTypeId' => $staff['jobTypeId'],
                'count' => $totalBp
            );

            // ソートのキーは獲得BP
            $sort[$idx] = $totalBp;
        }

        // ソート
        if ($staffCountUps) {
            array_multisort($sort, SORT_DESC, $staffCountUps);
        }

        // 獲得BP=0は対象外
        $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
            return $staffCountUp['count'] > 0;
        });

        if ($targetType == 'staff') {
            // 累計ーランキングー全体ー社員区分
            $idx = 0;
            $lastStaffTypeId = 0;
            foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];

                // 社員区分が変わった場合
                if ($staffTypeId != $lastStaffTypeId) {
                    $idx = 0;
                }

                // 社員区分で対象外は除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                if ($idx > count($staffCountUps)-1) break;

                $staffId = $staffCountUps[$idx]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastStaffTypeId = $staffTypeId;
                $idx++;
            }
        } else if ($targetType == 'job') {
            // 累計ーランキングー全体ー職種区分
            $idx = 0;
            $lastJobTypeId = 0;
            foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];

                // 職種区分が変わった場合
                if ($jobTypeId != $lastJobTypeId) {
                    $idx = 0;
                }

                // 職種区分で対象外は除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                if ($idx > count($staffCountUps)-1) break;

                $staffId = $staffCountUps[$idx]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastJobTypeId = $jobTypeId;
                $idx++;
            }
        } else {
            // 累計ーランキングー全体ー区分なし
            foreach ($incentive['IncentiveRule'] as $rank => $incentiveRule) {
                $incentiveAmount = $incentiveRule['incentive'];

                if ($rank > count($staffCountUps)-1) break;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount,
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
            }
        }
    }

    function doTotalDivisionRanking($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];

        if ($targetType == 'staff') {
            // 累計ーランキングー事業部ー社員区分
            $rank = 0;
            $lastStaffTypeId = 0;
            $lastDivisionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $divisionId = $incentiveRule['division_id'];

                // 社員区分が変わった場合
                if ($staffTypeId != $lastStaffTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理した事業所IDとは異なっている
                if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getTotalDivisionRanking($divisionId);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastStaffTypeId = $staffTypeId;
                $lastDivisionId = $divisionId;
                $rank++;
            }
        } else if ($targetType == 'job') {
            // 累計ーランキングー事業部ー職種区分
            $rank = 0;
            $lastJobTypeId = 0;
            $lastDivisionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $divisionId = $incentiveRule['division_id'];

                // 社員区分が変わった場合
                if ($jobTypeId != $lastJobTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理した事業所IDとは異なっている
                if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getTotalDivisionRanking($divisionId);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }

                $lastJobTypeId = $jobTypeId;
                $lastDivisionId = $divisionId;
                $rank++;
            }
        } else {
            // 累計ーランキングー事業部ー区分なし
            $rank = 0;
            $lastDivisionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $incentiveAmount = $incentiveRule['incentive'];
                $divisionId = $incentiveRule['division_id'];

                // 次のインセンティブルールの場合、前回処理した事業所IDとは異なっている
                if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getTotalDivisionRanking($divisionId);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];
                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastDivisionId = $divisionId;
                $rank++;
            }
        }
    }

    function doTotalDivisionBpRanking($incentive, $targetType, $startMonth) {
        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        if ($targetType == 'staff') {
            // 累計ーランキングー事業部ー社員区分
            $rank = 0;
            $lastStaffTypeId = 0;
            $lastDivisionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $divisionId = $incentiveRule['division_id'];

                // 社員区分が変わった場合
                if ($staffTypeId != $lastStaffTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理した事業所IDとは異なっている
                if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaffByDivisionId($divisionId);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp,
                        'staffTypeId' => $staff['staffTypeId'],
                        'jobTypeId' => $staff['jobTypeId'],
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 社員区分で対象外は除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastStaffTypeId = $staffTypeId;
                $lastDivisionId = $divisionId;
                $rank++;
            }
        } else if ($targetType == 'job') {
            // 累計ーランキングー事業部ー職種区分
            $rank = 0;
            $lastJobTypeId = 0;
            $lastDivisionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $divisionId = $incentiveRule['division_id'];

                // 社員区分が変わった場合
                if ($jobTypeId != $lastJobTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理した事業所IDとは異なっている
                if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaffByDivisionId($divisionId);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp,
                        'staffTypeId' => $staff['staffTypeId'],
                        'jobTypeId' => $staff['jobTypeId']
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 職種区分で対象外は除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastJobTypeId = $jobTypeId;
                $lastDivisionId = $divisionId;
                $rank++;
            }
        } else {
            // 累計ーランキングー事業部ー区分なし
            $rank = 0;
            $lastDivisionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $incentiveAmount = $incentiveRule['incentive'];
                $divisionId = $incentiveRule['division_id'];

                // 次のインセンティブルールの場合、前回処理した事業所IDとは異なっている
                if ($lastDivisionId && ($divisionId != $lastDivisionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaffByDivisionId($divisionId);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];
                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastDivisionId = $divisionId;
                $rank++;
            }
        }
    }

    function doTotalAreaRanking($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];

        if ($targetType == 'staff') {
            // 累計ーランキングーエリアー社員区分
            $rank = 0;
            $lastStaffTypeId = 0;
            $lastAreaId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $areaId = $incentiveRule['area_id'];

                // 職種区分が変わった場合
                if ($staffTypeId != $lastStaffTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理したエリアIDとは異なっている
                if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getTotalAreaRanking($areaId);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastStaffTypeId = $staffTypeId;
                $lastAreaId = $areaId;
                $rank++;
            }
        } else if ($targetType == 'job') {
            // 累計ーランキングーエリアー職種区分
            $rank = 0;
            $lastJobTypeId = 0;
            $lastAreaId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $areaId = $incentiveRule['area_id'];

                // 職種区分が変わった場合
                if ($jobTypeId != $lastJobTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理したエリアIDとは異なっている
                if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getTotalAreaRanking($areaId);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }

                $lastJobTypeId = $jobTypeId;
                $lastAreaId = $areaId;
                $rank++;
            }
        } else {
            // 累計ーランキングーエリアー区分なし
            $rank = 0;
            $lastAreaId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $incentiveAmount = $incentiveRule['incentive'];
                $areaId = $incentiveRule['area_id'];

                // 次のインセンティブルールの場合、前回処理したエリアIDとは異なっている
                if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getTotalAreaRanking($areaId);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];
                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastAreaId = $areaId;
                $rank++;
            }
        }
    }

    function doTotalAreaBpRanking($incentive, $targetType, $startMonth) {
        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        if ($targetType == 'staff') {
            // 累計ーランキングーエリアー社員区分
            $rank = 0;
            $lastStaffTypeId = 0;
            $lastAreaId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $areaId = $incentiveRule['area_id'];

                // 職種区分が変わった場合
                if ($staffTypeId != $lastStaffTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理したエリアIDとは異なっている
                if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaffByAreaId($areaId);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp,
                        'staffTypeId' => $staff['staffTypeId'],
                        'jobTypeId' => $staff['jobTypeId']
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 社員区分で除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastStaffTypeId = $staffTypeId;
                $lastAreaId = $areaId;
                $rank++;
            }
        } else if ($targetType == 'job') {
            // 累計ーランキングーエリアー職種区分
            $rank = 0;
            $lastJobTypeId = 0;
            $lastAreaId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $areaId = $incentiveRule['area_id'];

                // 職種区分が変わった場合
                if ($jobTypeId != $lastJobTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理したエリアIDとは異なっている
                if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaffByAreaId($areaId);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp,
                        'staffTypeId' => $staff['staffTypeId'],
                        'jobTypeId' => $staff['jobTypeId']
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastJobTypeId = $jobTypeId;
                $lastAreaId = $areaId;
                $rank++;
            }
        } else {
            // 累計ーランキングーエリアー区分なし
            $rank = 0;
            $lastAreaId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $incentiveAmount = $incentiveRule['incentive'];
                $areaId = $incentiveRule['area_id'];

                // 次のインセンティブルールの場合、前回処理したエリアIDとは異なっている
                if ($lastAreaId && ($areaId != $lastAreaId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaffByAreaId($areaId);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];
                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastAreaId = $areaId;
                $rank++;
            }
        }
    }

    function doTotalSectionRanking($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];

        if ($targetType == 'staff') {
            // 累計ーランキングー部署ー社員区分
            $rank = 0;
            $lastStaffTypeId = 0;
            $lastSectionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $sectionId = $incentiveRule['section_id'];

                // 社員区分が変わった場合
                if ($staffTypeId != $lastStaffTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getTotalSectionRanking($sectionId);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 社員区分で除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }

                $lastStaffTypeId = $staffTypeId;
                $lastSectionId = $sectionId;
                $rank++;
            }
        } else if ($targetType == 'job') {
            // 累計ーランキングーエリアー職種区分
            $rank = 0;
            $lastJobTypeId = 0;
            $lastSectionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $sectionId = $incentiveRule['section_id'];

                // 職種区分が変わった場合
                if ($jobTypeId != $lastJobTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getTotalSectionRanking($sectionId);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }

                $lastJobTypeId = $jobTypeId;
                $lastSectionId = $sectionId;
                $rank++;
            }
        } else {
            // 累計ーランキングー部署ー区分なし
            $rank = 0;
            $lastSectionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $incentiveAmount = $incentiveRule['incentive'];
                $sectionId = $incentiveRule['section_id'];

                // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // ランキングを取得（獲得件数順にソート）
                $staffCountUps = $this->StaffCountUp->getTotalSectionRanking($sectionId);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];
                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastSectionId = $sectionId;
                $rank++;
            }
        }
    }

    function doTotalSectionBpRanking($incentive, $targetType, $startMonth) {
        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        if ($targetType == 'staff') {
            // 累計ーランキングー部署ー社員区分
            $rank = 0;
            $lastStaffTypeId = 0;
            $lastSectionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $sectionId = $incentiveRule['section_id'];

                // 社員区分が変わった場合
                if ($staffTypeId != $lastStaffTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaffBySectionId($sectionId);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp,
                        'staffTypeId' => $staff['staffTypeId'],
                        'jobTypeId' => $staff['jobTypeId']
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 社員区分で除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastStaffTypeId = $staffTypeId;
                $lastSectionId = $sectionId;
                $rank++;
            }
        } else if ($targetType == 'job') {
            // 累計ーランキングーエリアー職種区分
            $rank = 0;
            $lastJobTypeId = 0;
            $lastSectionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveAmount = $incentiveRule['incentive'];
                $sectionId = $incentiveRule['section_id'];

                // 職種区分が変わった場合
                if ($jobTypeId != $lastJobTypeId) {
                    $idx = 0;
                }

                // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaffBySectionId($sectionId);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp,
                        'staffTypeId' => $staff['staffTypeId'],
                        'jobTypeId' => $staff['jobTypeId'],
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // 社員区分で除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];

                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastJobTypeId = $jobTypeId;
                $lastSectionId = $sectionId;
                $rank++;
            }
        } else {
            // 累計ーランキングー部署ー区分なし
            $rank = 0;
            $lastSectionId = 0;
            foreach ($incentive['IncentiveRule'] as $idx => $incentiveRule) {
                $incentiveAmount = $incentiveRule['incentive'];
                $sectionId = $incentiveRule['section_id'];

                // 次のインセンティブルールの場合、前回処理した部署IDとは異なっている
                if ($lastSectionId && ($sectionId != $lastSectionId) && ($idx != count($incentive['IncentiveRule']) - 1)) {
                    $rank = 0;
                }

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaffBySectionId($sectionId);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                if ($staffCountUps) {
                    array_multisort($sort, SORT_DESC, $staffCountUps);
                }

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                // ランキングがない場合は以降の処理はスルー
                if (!$staffCountUps || !isset($staffCountUps[$rank])) continue;

                $staffId = $staffCountUps[$rank]['staffId'];
                $data = array(
                    'staff_id' => $staffId,
                    'incentive_id' => $incentiveId,
                    'incentive' => $incentiveAmount
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
                $lastSectionId = $sectionId;
                $rank++;
            }
        }
    }

    function doTotalCount($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];
        $staffCountUps = $this->StaffCountUp->getTotalCount();

        // 獲得件数=0は対象外
        $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
            return $staffCountUp['count'] > 0;
        });

        if ($targetType == 'staff') {
            // 累計ー獲得件数ー社員区分
            foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveCount = $incentiveRule['count'];
                $incentiveAmount = $incentiveRule['incentive'];

                // 社員区分で除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                foreach ($staffCountUps as $staffCountUp){
                    $staffId = $staffCountUp['staffId'];
                    $count = $staffCountUp['count'];

                    // 獲得件数に合致したインセンティブを登録
                    if ($count >= $incentiveCount) {
                        $data = array(
                            'staff_id' => $staffId,
                            'incentive_id' => $incentiveId,
                            'incentive' => $incentiveAmount
                        );
                        $this->create();
                        if (!$this->save($data)) {
                            throw new InternalErrorException();
                        }
                        break;
                    }
                }
            }
        } else if ($targetType == 'job') {
            // 累計ー獲得件数ー職種区分
            foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveCount = $incentiveRule['count'];
                $incentiveAmount = $incentiveRule['incentive'];

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                foreach ($staffCountUps as $staffCountUp){
                    $staffId = $staffCountUp['staffId'];
                    $count = $staffCountUp['count'];

                    // 獲得件数に合致したインセンティブを登録
                    if ($count >= $incentiveCount) {
                        $data = array(
                            'staff_id' => $staffId,
                            'incentive_id' => $incentiveId,
                            'incentive' => $incentiveAmount
                        );
                        $this->create();
                        if (!$this->save($data)) {
                            throw new InternalErrorException();
                        }
                        break;
                    }
                }
            }
        } else {
            // 累計ー獲得件数ー区分なし
            foreach ($staffCountUps as $staffCountUp){
                $staffId = $staffCountUp['staffId'];
                $count = $staffCountUp['count'];
                foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                    $incentiveCount = $incentiveRule['count'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    // 獲得件数に合致したインセンティブを登録
                    if ($count >= $incentiveCount) {
                        $data = array(
                            'staff_id' => $staffId,
                            'incentive_id' => $incentiveId,
                            'incentive' => $incentiveAmount
                        );
                        $this->create();
                        if (!$this->save($data)) {
                            throw new InternalErrorException();
                        }
                        break;
                    }
                }
            }
        }
    }

    function doTotalBp($incentive, $targetType, $startMonth) {

        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        // 社員IDを取得
        $staffs = $this->StaffHistory->getLatestStaff();
        $staffCountUps = array();
        $sort = array();

        foreach ($staffs as $idx => $staff) {
            $totalBp = 0;
            foreach ($productCategories as $productCategory) {
                foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                    $data = $this->StaffCountUp->getTotalBpCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth);
                    $totalBp += $data['bp'];
                }
            }
            // 社員IDと獲得BPを設定
            $staffCountUps[] = array(
                'staffId' => $staff['staffId'],
                'staffTypeId' => $staff['staffTypeId'],
                'jobTypeId' => $staff['jobTypeId'],
                'count' => $totalBp
            );

            // ソートのキーは獲得BP
            $sort[$idx] = $totalBp;
        }

        // ソート
        array_multisort($sort, SORT_DESC, $staffCountUps);

        // 獲得BP=0は対象外
        $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
            return $staffCountUp['count'] > 0;
        });

        if ($targetType == 'staff') {
            // 累計ー獲得BPー社員区分
            foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                $staffTypeId = $incentiveRule['staff_type_id'];
                $incentiveCount = $incentiveRule['count'];
                $incentiveAmount = $incentiveRule['incentive'];

                // 社員区分で除外
                $callback = function ($staffCountUp) use ($staffTypeId) {
                    return ($staffCountUp['staffTypeId'] == $staffTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                foreach ($staffCountUps as $staffCountUp){
                    $staffId = $staffCountUp['staffId'];
                    $count = $staffCountUp['count'];

                    // 獲得件数に合致したインセンティブを登録
                    if ($count >= $incentiveCount) {
                        $data = array(
                            'staff_id' => $staffId,
                            'incentive_id' => $incentiveId,
                            'incentive' => $incentiveAmount
                        );
                        $this->create();
                        if (!$this->save($data)) {
                            throw new InternalErrorException();
                        }
                        break;
                    }
                }
            }
        } else if ($targetType == 'job') {
            // 累計ー獲得BPー職種区分
            foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                $jobTypeId = $incentiveRule['job_type_id'];
                $incentiveCount = $incentiveRule['count'];
                $incentiveAmount = $incentiveRule['incentive'];

                // 職種区分で除外
                $callback = function ($staffCountUp) use ($jobTypeId) {
                    return ($staffCountUp['jobTypeId'] == $jobTypeId);
                };
                $staffCountUps = array_filter($staffCountUps, $callback);
                $staffCountUps = array_values($staffCountUps);

                foreach ($staffCountUps as $staffCountUp){
                    $staffId = $staffCountUp['staffId'];
                    $count = $staffCountUp['count'];

                    // 獲得件数に合致したインセンティブを登録
                    if ($count >= $incentiveCount) {
                        $data = array(
                            'staff_id' => $staffId,
                            'incentive_id' => $incentiveId,
                            'incentive' => $incentiveAmount
                        );
                        $this->create();
                        if (!$this->save($data)) {
                            throw new InternalErrorException();
                        }
                        break;
                    }
                }
            }
        } else {
            // 累計ー獲得BPー区分なし
            foreach ($staffCountUps as $staffCountUp){
                $staffId = $staffCountUp['staffId'];
                $count = $staffCountUp['count'];
                foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                    $incentiveCount = $incentiveRule['count'];
                    $incentiveAmount = $incentiveRule['incentive'];
                    // 獲得件数に合致したインセンティブを登録
                    if ($count >= $incentiveCount) {
                        $data = array(
                            'staff_id' => $staffId,
                            'incentive_id' => $incentiveId,
                            'incentive' => $incentiveAmount
                        );
                        $this->create();
                        if (!$this->save($data)) {
                            throw new InternalErrorException();
                        }
                        break;
                    }
                }
            }
        }
    }

    function doMonthlyCount($incentive, $targetType) {
        $incentiveId = $incentive['Incentive']['id'];
        $targetMonths = $this->StaffCountUp->getTargetMonths();

        if ($targetType == 'staff') {
            // 月間ー獲得件数ー社員区分
            foreach ($targetMonths as $targetMonth) {

                $staffCountUps = $this->StaffCountUp->getMonthlyCount($targetMonth);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                    $staffTypeId = $incentiveRule['staff_type_id'];
                    $incentiveCount = $incentiveRule['count'];
                    $incentiveAmount = $incentiveRule['incentive'];

                    // 社員区分で除外
                    $callback = function ($staffCountUp) use ($staffTypeId) {
                        return ($staffCountUp['staffTypeId'] == $staffTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    foreach ($staffCountUps as $staffCountUp) {
                        $staffId = $staffCountUp['staffId'];
                        $month = $staffCountUp['month'];
                        $count = $staffCountUp['count'];

                        // 獲得件数に合致したインセンティブを登録
                        if ($count >= $incentiveCount) {
                            $data = array(
                                'staff_id' => $staffId,
                                'incentive_id' => $incentiveId,
                                'incentive' => $incentiveAmount,
                                'month' => $month
                            );
                            $this->create();
                            if (!$this->save($data)) {
                                throw new InternalErrorException();
                            }
                            break;
                        }
                    }
                }
            }
        } else if ($targetType == 'job') {
            // 月間ー獲得件数ー職種区分
            foreach ($targetMonths as $targetMonth) {

                $staffCountUps = $this->StaffCountUp->getMonthlyCount($targetMonth);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                    $jobTypeId = $incentiveRule['job_type_id'];
                    $incentiveCount = $incentiveRule['count'];
                    $incentiveAmount = $incentiveRule['incentive'];

                    // 職種区分で除外
                    $callback = function ($staffCountUp) use ($jobTypeId) {
                        return ($staffCountUp['jobTypeId'] == $jobTypeId);
                    };
                    $staffCountUps = array_filter($staffCountUps, $callback);
                    $staffCountUps = array_values($staffCountUps);

                    foreach ($staffCountUps as $staffCountUp) {
                        $staffId = $staffCountUp['staffId'];
                        $month = $staffCountUp['month'];
                        $count = $staffCountUp['count'];

                        // 獲得件数に合致したインセンティブを登録
                        if ($count >= $incentiveCount) {
                            $data = array(
                                'staff_id' => $staffId,
                                'incentive_id' => $incentiveId,
                                'incentive' => $incentiveAmount,
                                'month' => $month
                            );
                            $this->create();
                            if (!$this->save($data)) {
                                throw new InternalErrorException();
                            }
                            break;
                        }
                    }
                }
            }
        } else {
            // 月間ー獲得件数ー区分なし
            foreach ($targetMonths as $targetMonth) {

                $staffCountUps = $this->StaffCountUp->getMonthlyCount($targetMonth);

                // 獲得件数=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                foreach ($staffCountUps as $staffCountUp) {
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];
                    $count = $staffCountUp['count'];
                    foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                        $incentiveCount = $incentiveRule['count'];
                        $incentiveAmount = $incentiveRule['incentive'];
                        // 獲得件数に合致したインセンティブを登録
                        if ($count >= $incentiveCount) {
                            $data = array(
                                'staff_id' => $staffId,
                                'incentive_id' => $incentiveId,
                                'incentive' => $incentiveAmount,
                                'month' => $month
                            );
                            $this->create();
                            if (!$this->save($data)) {
                                throw new InternalErrorException();
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    function doMonthlyBp($incentive, $targetType, $startMonth) {
        $incentiveId = $incentive['Incentive']['id'];
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        $targetMonths = $this->StaffCountUp->getTargetMonths();

        if ($targetType == 'staff') {
            // 月間ー獲得件数ー社員区分
            foreach ($targetMonths as $targetMonth) {

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaff($targetMonth);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp,
                        'month' => $targetMonth
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                array_multisort($sort, SORT_DESC, $staffCountUps);

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                foreach ($staffCountUps as $staffCountUp) {
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];
                    $count = $staffCountUp['count'];
                    $staffHistoryStaffTypeId = $this->StaffHistory->getStaffTypeId($staffId, $month);
                    foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                        $staffTypeId = $incentiveRule['staff_type_id'];
                        $incentiveCount = $incentiveRule['count'];
                        $incentiveAmount = $incentiveRule['incentive'];
                        // 獲得件数に合致したインセンティブを登録
                        if ($count >= $incentiveCount && $staffTypeId == $staffHistoryStaffTypeId) {
                            $data = array(
                                'staff_id' => $staffId,
                                'incentive_id' => $incentiveId,
                                'incentive' => $incentiveAmount,
                                'month' => $month
                            );
                            $this->create();
                            if (!$this->save($data)) {
                                throw new InternalErrorException();
                            }
                            break;
                        }
                    }
                }
            }
        } else if ($targetType == 'job') {
            // 月間ー獲得件数ー職種区分
            foreach ($targetMonths as $targetMonth) {

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaff($targetMonth);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'staffTypeId' => $staff['staffTypeId'],
                        'jobTypeId' => $staff['jobTypeId'],
                        'count' => $totalBp,
                        'month' => $targetMonth
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                array_multisort($sort, SORT_DESC, $staffCountUps);

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                foreach ($staffCountUps as $staffCountUp) {
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];
                    foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                        $jobTypeId = $incentiveRule['job_type_id'];
                        $incentiveCount = $incentiveRule['count'];
                        $incentiveAmount = $incentiveRule['incentive'];

                        // 職種区分で対象外は除外
                        $callback = function ($staffCountUp) use ($jobTypeId) {
                            return ($staffCountUp['jobTypeId'] == $jobTypeId);
                        };
                        $staffCountUps = array_filter($staffCountUps, $callback);
                        $staffCountUps = array_values($staffCountUps);
                        $count = $staffCountUp['count'];

                        // 獲得件数に合致したインセンティブを登録
                        if ($count >= $incentiveCount) {
                            $data = array(
                                'staff_id' => $staffId,
                                'incentive_id' => $incentiveId,
                                'incentive' => $incentiveAmount,
                                'month' => $month
                            );
                            $this->create();
                            if (!$this->save($data)) {
                                throw new InternalErrorException();
                            }
                            break;
                        }
                    }
                }
            }
        } else {
            // 月間ー獲得件数ー区分なし
            foreach ($targetMonths as $targetMonth) {

                // 社員IDを取得
                $staffs = $this->StaffHistory->getLatestStaff($targetMonth);
                $staffCountUps = array();
                $sort = array();

                foreach ($staffs as $idx => $staff) {
                    $totalBp = 0;
                    foreach ($productCategories as $productCategory) {
                        foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                            $data = $this->StaffCountUp->getCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $targetMonth);
                            $totalBp += $data['bp'];
                        }
                    }
                    // 社員IDと獲得BPを設定
                    $staffCountUps[] = array(
                        'staffId' => $staff['staffId'],
                        'count' => $totalBp,
                        'month' => $targetMonth
                    );

                    // ソートのキーは獲得BP
                    $sort[$idx] = $totalBp;
                }

                // ソート
                array_multisort($sort, SORT_DESC, $staffCountUps);

                // 獲得BP=0は対象外
                $staffCountUps = array_filter($staffCountUps, function($staffCountUp) {
                    return $staffCountUp['count'] > 0;
                });

                foreach ($staffCountUps as $staffCountUp) {
                    $staffId = $staffCountUp['staffId'];
                    $month = $staffCountUp['month'];
                    $count = $staffCountUp['count'];
                    foreach ($incentive['IncentiveRule'] as $k => $incentiveRule) {
                        $incentiveCount = $incentiveRule['count'];
                        $incentiveAmount = $incentiveRule['incentive'];
                        // 獲得件数に合致したインセンティブを登録
                        if ($count >= $incentiveCount) {
                            $data = array(
                                'staff_id' => $staffId,
                                'incentive_id' => $incentiveId,
                                'incentive' => $incentiveAmount,
                                'month' => $month
                            );
                            $this->create();
                            if (!$this->save($data)) {
                                throw new InternalErrorException();
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

}
