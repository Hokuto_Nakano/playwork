<?php
App::uses('AppModel', 'Model');

class StaffType extends AppModel {

    function getPayments($yyyymm, $divisionId = 0, $areaId = 0, $sectionId = 0) {

        $condition = '';
        $param = array(
            $yyyymm, $yyyymm, $yyyymm, $yyyymm, $yyyymm
        );

        // 事業部指定、エリア指定、部署指定
        if ($divisionId > 0) {
            $condition = 'and Division.id = ? ';
            $param = array(
                $yyyymm, $yyyymm, $divisionId, $yyyymm, $yyyymm, $yyyymm, $divisionId
            );
        } else if ($areaId > 0) {
            $condition = 'and Area.id = ? ';
            $param = array(
                $yyyymm, $yyyymm, $areaId, $yyyymm, $yyyymm, $yyyymm, $areaId
            );
        } else if ($sectionId > 0) {
            $condition = 'and Section.id = ? ';
            $param = array(
                $yyyymm, $yyyymm,$sectionId, $yyyymm, $yyyymm, $yyyymm, $sectionId
            );
        }

        // 時給x稼働時間 + 時給インセンx稼働時間 + 月給インセン
        $sql = "select
                    StaffType.id staff_type_id,
                    StaffType.type staff_type,
                    ifnull(main_tbl.incentive, 0) incentive,
                    ifnull(main_tbl.hourly_plus, -1) hourly_plus,
                    ifnull(sub_tbl.staff_pay, 0) staff_pay,
                    ifnull(sub_tbl.total_work_hours, '00:00:00') total_work_hours
                from staff_types StaffType
                left join (
                select
                    StaffType.id staff_type_id,
                    StaffType.type,
                    StaffIncentive.month,
                    sum(StaffIncentive.incentive) incentive,
                    Incentive.hourly_plus
                from staff_histories StaffHistory
                inner join (select staff_id, MAX(staff_histories.start_month) max_start_month from staff_histories where start_month <= ? and disabled_flg = 0 group by staff_id) sub_StaffHistory on sub_StaffHistory.staff_id = StaffHistory.staff_id and sub_StaffHistory.max_start_month = StaffHistory.start_month
                inner join staff_incentives StaffIncentive on StaffIncentive.staff_id = StaffHistory.staff_id
                inner join sections Section on Section.id = StaffHistory.section_id
                inner join areas Area on Area.id = Section.area_id
                inner join divisions Division on Division.id = Area.division_id
                inner join staff_types StaffType on StaffType.id = StaffHistory.staff_type_id
                left join incentives Incentive on Incentive.id = StaffIncentive.incentive_id
                where (StaffIncentive.month = 0 or StaffIncentive.month = ?)  ". $condition ." 
                group by StaffType.id, Incentive.hourly_plus
                ) main_tbl on main_tbl.staff_type_id = StaffType.id
                left join(     
                select 
                    StaffType.id staff_type_id,
                    StaffType.type staff_type,
                    ifnull(HourlyStaffIncentive.staff_pay, 0) staff_pay,
                    ifnull(HourlyStaffIncentive.total_work_hours, '00:00:00') total_work_hours
                from staff_types StaffType
                left join (
                select
                    StaffType.id staff_type_id,
                    StaffType.type,
                    sum(ifnull(StaffPay.pay, 0)) staff_pay,
                    sec_to_time(sum(time_to_sec(ifnull(Attendance.total_work_hours, '00:00:00')))) total_work_hours
                from staff_histories StaffHistory
                inner join (select staff_id, MAX(staff_histories.start_month) max_start_month from staff_histories where start_month <= ? and disabled_flg = 0 group by staff_id) sub_StaffHistory on sub_StaffHistory.staff_id = StaffHistory.staff_id and sub_StaffHistory.max_start_month = StaffHistory.start_month
                inner join sections Section on Section.id = StaffHistory.section_id
                inner join areas Area on Area.id = Section.area_id
                inner join divisions Division on Division.id = Area.division_id
                inner join staff_types StaffType on StaffType.id = StaffHistory.staff_type_id
                left join staff_pays StaffPay on StaffPay.staff_id = StaffHistory.id
                left join (select staff_id, sec_to_time(sum(time_to_sec(work_hours))) total_work_hours from attendances where attendance_month = ? group by staff_id) Attendance on Attendance.staff_id = StaffHistory.staff_id
                where StaffPay.month = ? ". $condition ." 
                group by StaffType.id
                ) HourlyStaffIncentive on HourlyStaffIncentive.staff_type_id = StaffType.id
                ) sub_tbl on sub_tbl.staff_type_id = StaffType.id
                order by StaffType.id;";

        // 1 責任者	13612	0	12416	151:00:00 のような形式
        $payments = $this->query($sql, $param);

        $data = array();
        $pay = 0;
        $flg = true;
        $staffType = null;
        foreach ($payments as $idx => $payment) {

            // 最初と最後以外はここで対応
            if (($idx != 0 && $payments[$idx-1]['StaffType']['staff_type_id'] != $payment['StaffType']['staff_type_id'])) {
                $data[$staffType] = $pay;
                $pay = 0;
                $flg = true;
            }

            // 社員区分
            $staffType = $payment['StaffType']['staff_type'];

            // インセンティブ額
            $incentive = $payment[0]['incentive'];

            // 時給or月給
            $hourlyPlus = $payment[0]['hourly_plus'];

            // 時給
            $staffPay = $payment[0]['staff_pay'];

            // 稼働時間
            $totalWorkHours = $payment[0]['total_work_hours'];
            $times = explode(':', $totalWorkHours);

            // 時給×稼働時間
            if ($flg) {
                $pay += floor(($staffPay * $times[0]) + ($staffPay * $times[1] / 60));
                $flg = false;
            }

            // 時給インセンティブor月給インセンティブ
            if ($hourlyPlus) {
                // 時給インセンティブ×稼働時間
                $pay += floor(($incentive * $times[0]) + ($incentive * $times[1] / 60));
            } else {
                // 月給インセンティブ
                $pay += $incentive;
            }

            // 最初と最後はここで対応
            if (($idx == 0 && $payments[$idx+1]['StaffType']['staff_type_id'] != $payment['StaffType']['staff_type_id']) || $idx == count($payments)-1) {
                $data[$staffType] = $pay;
                $pay = 0;
                $flg = true;
            }

        }
        return $data;
    }

}
