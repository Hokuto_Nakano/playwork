<?php
App::uses('AppModel', 'Model');

class StaffPay extends AppModel {

    function findFirst($condition) {
        $staffPay = $this->find('first', $condition);
        return $staffPay && isset ($staffPay['StaffPay']) ? $staffPay['StaffPay'] : null;
    }
}
