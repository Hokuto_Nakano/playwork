<?php
App::uses('AppModel', 'Model');

class StaffPointGoal extends AppModel {

    function findFirst($condition) {
        $staffPointGoal = $this->find('first', $condition);
        return $staffPointGoal && isset ($staffPointGoal['StaffPointGoal']) ? $staffPointGoal['StaffPointGoal'] : null;
    }
}
