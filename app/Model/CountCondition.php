<?php
App::uses('AppModel', 'Model');

class CountCondition extends AppModel {

    var $belongsTo = array('CountItem');

    function getListByTableMapId($tableMapId){
        $countConditions = $this->find('all', array(
            'conditions' => array('table_map_id' => $tableMapId)
        ));
        if(!$countConditions) return null;
        $result = array();
        foreach($countConditions as $countCondition){
            $result[] = $countCondition["CountCondition"];
        }
        return $result;
    }
}
