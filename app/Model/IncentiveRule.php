<?php
App::uses('AppModel', 'Model');

class IncentiveRule extends AppModel {

    function getAllByIncentiveId($incentiveId, $divisionId, $areaId, $sectionId, $yyyymm){
        $conditions = array(
            'conditions' => array(
                'IncentiveRule.incentive_id' => $incentiveId,
                'IncentiveRule.division_id' => $divisionId,
                'IncentiveRule.area_id' => $areaId,
                'IncentiveRule.section_id' => $sectionId,
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select incentive_id, MAX(start_month) max_start_month from incentive_rules where start_month <=' .$yyyymm .' group by incentive_id)',
                    'alias' => 'sub_tbl',
                    'conditions' => array(
                        'IncentiveRule.start_month = sub_tbl.max_start_month',
                        'IncentiveRule.incentive_id = sub_tbl.incentive_id'
                    )
                ),
            )
        );
        $incentiveRules = $this->find('all', $conditions);

        $result = array();
        foreach($incentiveRules as $incentiveRule){
            $result[] = array(
                "id" => $incentiveRule["IncentiveRule"]["id"],
                "incentive_id" => $incentiveRule["IncentiveRule"]["incentive_id"],
                "division_id" => $incentiveRule["IncentiveRule"]["division_id"],
                "area_id" => $incentiveRule["IncentiveRule"]["area_id"],
                "section_id" => $incentiveRule["IncentiveRule"]["section_id"],
                "staff_type_id" => $incentiveRule["IncentiveRule"]["staff_type_id"],
                "job_type_id" => $incentiveRule["IncentiveRule"]["job_type_id"],
                "rank_name" => $incentiveRule["IncentiveRule"]["rank_name"],
                "count" => $incentiveRule["IncentiveRule"]["count"],
                "incentive" => $incentiveRule["IncentiveRule"]["incentive"],
                "start_month" => $incentiveRule["IncentiveRule"]["start_month"],
            );
        }

        return $result;
    }
    function getByEachId ($divisionId, $areaId, $sectionId) {
        $incentiveRule = $this->findByDivisionIdAndAreaIdAndSectionId($divisionId, $areaId, $sectionId);

        if ($incentiveRule) {
            return array('id' => $incentiveRule['IncentiveRule']['id']);
        } else {
            return array();
        }
    }
}
