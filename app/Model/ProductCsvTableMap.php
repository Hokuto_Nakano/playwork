<?php
App::uses('AppModel', 'Model');

class ProductCsvTableMap extends AppModel {

    function getByProductInputTableName($tableName){
        $productCsvTableMap = $this->findByProductInputTableName($tableName);
        if(!$productCsvTableMap) return null;
        return $productCsvTableMap["ProductCsvTableMap"];
    }
}
