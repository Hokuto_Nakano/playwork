<?php
App::uses('AppModel', 'Model');

class ProductCsvPlanMap extends AppModel {

    //var $belongsTo = array('Product');

    function getListByTableMapId($tableMapId){
        $planMaps = $this->find('all', array(
            'conditions' => array('table_map_id' => $tableMapId)
        ));
        if(!$planMaps) return null;
        $result = array();
        foreach($planMaps as $planMap){
            $result[] = $planMap["ProductCsvPlanMap"];
        }
        return $result;
    }
}
