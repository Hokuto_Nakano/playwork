<?php
App::uses('AppModel', 'Model');
App::uses('AcquisitionPointHistory', 'Model');

class StaffCountUp extends AppModel {

    function getMonthlyCount($targetMonth) {
        // 月間カウント
        $sql = "select StaffHistory.staff_id, StaffHistory.staff_type_id, StaffHistory.job_type_id, keiyaku.month, (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join (
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                order by count desc;";
        $staffCountUps = $this->query($sql, array(
            $targetMonth, $targetMonth
        ));

        $data = array();
        foreach ($staffCountUps as $idx => $staffCountUp) {
            $data[] = array(
                'staffId' => $staffCountUp['StaffHistory']['staff_id'],
                'staffTypeId' => $staffCountUp['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staffCountUp['StaffHistory']['job_type_id'],
                'month' => $staffCountUp['keiyaku']['month'],
                'count' => $staffCountUp[0]['count']
            );
        }
        return $data;
    }

    function getMonthlyAllRanking($targetMonth) {

        // 月間カウント
        $sql = "select StaffHistory.staff_id, StaffHistory.staff_type_id, StaffHistory.job_type_id, keiyaku.month, (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join (
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                order by count desc;";

        $staffCountUps = $this->query($sql, array(
            $targetMonth, $targetMonth
        ));

        $data = array();
        foreach ($staffCountUps as $idx => $staffCountUp) {
            $data[] = array(
                'staffId' => $staffCountUp['StaffHistory']['staff_id'],
                'staffTypeId' => $staffCountUp['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staffCountUp['StaffHistory']['job_type_id'],
                'month' => $staffCountUp['keiyaku']['month'],
                'count' => $staffCountUp[0]['count']
            );
        }
        return $data;
    }

    function getMonthlyDivisionRanking($targetMonth, $divisionId) {

        // 月間事業部ランキング
        $sql = "select StaffHistory.staff_id, StaffHistory.staff_type_id, StaffHistory.job_type_id, keiyaku.month, Division.id division_id, (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                inner join divisions as Division on (Division.id = Area.division_id)  
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join (
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                inner join divisions as Division on (Division.id = Area.division_id)  
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where Division.id = ? and (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                order by count desc;";

        $staffCountUps = $this->query($sql, array(
            $targetMonth, $targetMonth, $divisionId
        ));
        $data = array();
        foreach ($staffCountUps as $idx => $staffCountUp) {
            $data[] = array(
                'staffId' => $staffCountUp['StaffHistory']['staff_id'],
                'staffTypeId' => $staffCountUp['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staffCountUp['StaffHistory']['job_type_id'],
                'month' => $staffCountUp['keiyaku']['month'],
                'count' => $staffCountUp[0]['count'],
                'divisionId' => $staffCountUp['Division']['division_id']
            );
        }
        return $data;
    }

    function getMonthlyAreaRanking($targetMonth, $areaId) {

        // 月間事業部ランキング
        $sql = "select StaffHistory.staff_id, StaffHistory.staff_type_id, StaffHistory.job_type_id, keiyaku.month, Area.id area_id, (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join (
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where Area.id = ? and (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                order by count desc;";
        $staffCountUps = $this->query($sql, array(
            $targetMonth, $targetMonth, $areaId
        ));
        $data = array();
        foreach ($staffCountUps as $idx => $staffCountUp) {
            $data[] = array(
                'staffId' => $staffCountUp['StaffHistory']['staff_id'],
                'staffTypeId' => $staffCountUp['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staffCountUp['StaffHistory']['job_type_id'],
                'month' => $staffCountUp['keiyaku']['month'],
                'count' => $staffCountUp[0]['count'],
                'areaId' => $staffCountUp['Area']['area_id']
            );
        }
        return $data;
    }

    function getMonthlySectionRanking($targetMonth, $sectionId) {

        // 月間事業部ランキング
        $sql = "select StaffHistory.staff_id, StaffHistory.staff_type_id, StaffHistory.job_type_id, keiyaku.month, Section.id section_id, (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join (
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                left join (
                select staff_id, date_format(recorded_date, '%Y%m') month, contract_status, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ? group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where Section.id = ? and (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                order by count desc;";

        $staffCountUps = $this->query($sql, array(
            $targetMonth, $targetMonth, $sectionId
        ));

        $data = array();
        foreach ($staffCountUps as $idx => $staffCountUp) {
            $data[] = array(
                'staffId' => $staffCountUp['StaffHistory']['staff_id'],
                'staffTypeId' => $staffCountUp['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staffCountUp['StaffHistory']['job_type_id'],
                'month' => $staffCountUp['keiyaku']['month'],
                'count' => $staffCountUp[0]['count'],
                'sectionId' => $staffCountUp['Section']['section_id']
            );
        }
        return $data;
    }

    function getTotalCount() {
        // 累計カウント
        $sql = "select StaffHistory.staff_id, StaffHistory.staff_type_id, StaffHistory.job_type_id, (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                left join (
                select staff_id, contract_status, count(*) cnt from staff_count_ups group by staff_id, contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join (
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                left join (
                select staff_id, contract_status, count(*) cnt from staff_count_ups group by staff_id, contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                order by count desc;";

        $staffCountUps = $this->query($sql);

        $data = array();
        foreach ($staffCountUps as $idx => $staffCountUp) {
            $data[] = array(
                'staffId' => $staffCountUp['StaffHistory']['staff_id'],
                'staffTypeId' => $staffCountUp['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staffCountUp['StaffHistory']['job_type_id'],
                'count' => $staffCountUp[0]['count']
            );
        }
        return $data;
    }

    function getTotalDivisionRanking($divisionId) {

        $sql = "select StaffHistory.staff_id, StaffHistory.staff_type_id, StaffHistory.job_type_id, Division.id division_id, (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                inner join divisions as Division on (Division.id = Area.division_id) 
                left join (
                select staff_id, contract_status, count(*) cnt from staff_count_ups group by staff_id, contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join(
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                inner join divisions as Division on (Division.id = Area.division_id)  
                left join (
                select staff_id, contract_status, count(*) cnt from staff_count_ups group by staff_id, contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where Division.id = ? and (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                order by count desc;";


        $staffCountUps = $this->query($sql, array(
            $divisionId
        ));

        $data = array();
        foreach ($staffCountUps as $idx => $staffCountUp) {
            $data[] = array(
                'staffId' => $staffCountUp['StaffHistory']['staff_id'],
                'staffTypeId' => $staffCountUp['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staffCountUp['StaffHistory']['job_type_id'],
                'count' => $staffCountUp[0]['count'],
                'divisionId' => $staffCountUp['Division']['division_id']
            );
        }
        return $data;
    }

    function getTotalAreaRanking($areaId) {

        $sql = "select StaffHistory.staff_id, StaffHistory.staff_type_id, StaffHistory.job_type_id, Area.id area_id, (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                left join (
                select staff_id, contract_status, count(*) cnt from staff_count_ups group by staff_id, contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join(
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                left join (
                select staff_id, contract_status, count(*) cnt from staff_count_ups group by staff_id, contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where Area.id = ? and (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                order by count desc;";

        $staffCountUps = $this->query($sql, array(
            $areaId
        ));

        $data = array();
        foreach ($staffCountUps as $idx => $staffCountUp) {
            $data[] = array(
                'staffId' => $staffCountUp['StaffHistory']['staff_id'],
                'staffTypeId' => $staffCountUp['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staffCountUp['StaffHistory']['job_type_id'],
                'count' => $staffCountUp[0]['count'],
                'areaId' => $staffCountUp['Area']['area_id']
            );
        }
        return $data;
    }

    function getTotalSectionRanking($sectionId) {

        $sql = "select StaffHistory.staff_id, StaffHistory.staff_type_id, StaffHistory.job_type_id, Section.id section_id, (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                left join (
                select staff_id, contract_status, count(*) cnt from staff_count_ups group by staff_id, contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join(
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                left join (
                select staff_id, contract_status, count(*) cnt from staff_count_ups group by staff_id, contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where Section.id = ? and (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                order by count desc;";

        $staffCountUps = $this->query($sql, array(
            $sectionId
        ));

        $data = array();
        foreach ($staffCountUps as $idx => $staffCountUp) {
            $data[] = array(
                'staffId' => $staffCountUp['StaffHistory']['staff_id'],
                'staffTypeId' => $staffCountUp['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staffCountUp['StaffHistory']['job_type_id'],
                'count' => $staffCountUp[0]['count'],
                'sectionId' => $staffCountUp['Section']['section_id']
            );
        }
        return $data;
    }

    function getTargetMonths() {
        // 月間事業部ランキング
        $staffCountUps = $this->find('all', array(
            'fields' => array(
                "date_format(recorded_date, '%Y%m') as month"
            ),
            'group' => array(
                "date_format(recorded_date, '%Y%m')"
            )
        ));
        $data = array();
        foreach ($staffCountUps as $staffCountUp) {
            $data[] = $staffCountUp[0]['month'];
        }
        return $data;
    }

    function getCount($staffId, $productCategoryId, $acquisitionTypeId, $startMonth, $countItemId = 0) {

        $connection = $this->getDataSource()->getConnection();
        $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        if ($countItemId) {

            $sql = "select count(*) keiyaku, kaiyaku.cnt, count(*) - kaiyaku.cnt total_count 
                from staff_count_ups, (select count(*) cnt from staff_count_ups where product_category_id = ? and staff_id = ? and acquisition_type_id = ? 
                and count_item_id = ? and contract_status = '解約' and date_format(recorded_date, '%Y%m') = ?) kaiyaku 
                where product_category_id = ? and staff_id = ? and acquisition_type_id = ? and count_item_id = ? and contract_status = '契約' and date_format(recorded_date, '%Y%m') = ?;";

            $param = array(
                $productCategoryId,
                $staffId,
                $acquisitionTypeId,
                $countItemId,
                $startMonth,
                $productCategoryId,
                $staffId,
                $acquisitionTypeId,
                $countItemId,
                $startMonth
            );

        } else {
            $sql = "select count(*) keiyaku, kaiyaku.cnt, count(*) - kaiyaku.cnt total_count 
                from staff_count_ups, (select count(*) cnt from staff_count_ups where product_category_id = ? and staff_id = ? and acquisition_type_id = ? 
                and contract_status = '解約' and date_format(recorded_date, '%Y%m') = ?) kaiyaku 
                where product_category_id = ? and staff_id = ? and acquisition_type_id = ? and contract_status = '契約' and date_format(recorded_date, '%Y%m') = ?;";

            $param = array(
                $productCategoryId,
                $staffId,
                $acquisitionTypeId,
                $startMonth,
                $productCategoryId,
                $staffId,
                $acquisitionTypeId,
                $startMonth
            );
        }

        $staffCountUps = $this->query($sql, $param);

        $this->AcquisitionPointHistory = new AcquisitionPointHistory();
        $acquisitionPointHistory = $this->AcquisitionPointHistory->find('all', array(
            'conditions' => array(
                'AcquisitionPointHistory.acquisition_type_id' => $acquisitionTypeId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select acquisition_type_id, max(start_month) max_start_month from acquisition_point_histories where start_month <= '. $startMonth .' group by acquisition_type_id)',
                    'alias' => 'max_tbl',
                    'conditions' => array(
                        'max_tbl.acquisition_type_id = AcquisitionPointHistory.acquisition_type_id',
                        'max_tbl.max_start_month = AcquisitionPointHistory.start_month'
                    )
                )
            )
        ));

        $point = 0;
        if (isset($acquisitionPointHistory[0])) {
            $point = $acquisitionPointHistory[0]['AcquisitionPointHistory']['point'];
        }

        foreach ($staffCountUps as $idx => $staffCountUp) {
            return array(
                'totalCount' => $staffCountUp[0]['total_count'],
                'bp' =>  $staffCountUp[0]['total_count'] * $point
            );
        }
    }

    function getTotalBpCount($staffId, $productCategoryId, $acquisitionTypeId, $startMonth, $countItemId = 0) {

        $connection = $this->getDataSource()->getConnection();
        $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        if ($countItemId) {

            $sql = "select count(*) keiyaku, kaiyaku.cnt, count(*) - kaiyaku.cnt total_count 
                from staff_count_ups, (select count(*) cnt from staff_count_ups where product_category_id = ? and staff_id = ? and acquisition_type_id = ? 
                and count_item_id = ? and contract_status = '解約') kaiyaku 
                where product_category_id = ? and staff_id = ? and acquisition_type_id = ? and count_item_id = ? and contract_status = '契約';";

            $param = array(
                $productCategoryId,
                $staffId,
                $acquisitionTypeId,
                $countItemId,
                $productCategoryId,
                $staffId,
                $acquisitionTypeId,
                $countItemId,
            );

        } else {
            $sql = "select count(*) keiyaku, kaiyaku.cnt, count(*) - kaiyaku.cnt total_count 
                from staff_count_ups, (select count(*) cnt from staff_count_ups where product_category_id = ? and staff_id = ? and acquisition_type_id = ? 
                and contract_status = '解約') kaiyaku 
                where product_category_id = ? and staff_id = ? and acquisition_type_id = ? and contract_status = '契約';";

            $param = array(
                $productCategoryId,
                $staffId,
                $acquisitionTypeId,
                $productCategoryId,
                $staffId,
                $acquisitionTypeId,
            );
        }

        $staffCountUps = $this->query($sql, $param);

        $this->AcquisitionPointHistory = new AcquisitionPointHistory();
        $acquisitionPointHistory = $this->AcquisitionPointHistory->find('all', array(
            'conditions' => array(
                'AcquisitionPointHistory.acquisition_type_id' => $acquisitionTypeId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select acquisition_type_id, max(start_month) max_start_month from acquisition_point_histories where start_month <= '. $startMonth .' group by acquisition_type_id)',
                    'alias' => 'max_tbl',
                    'conditions' => array(
                        'max_tbl.acquisition_type_id = AcquisitionPointHistory.acquisition_type_id',
                        'max_tbl.max_start_month = AcquisitionPointHistory.start_month'
                    )
                )
            )
        ));

        $point = 0;
        if (isset($acquisitionPointHistory[0])) {
            $point = $acquisitionPointHistory[0]['AcquisitionPointHistory']['point'];
        }

        foreach ($staffCountUps as $idx => $staffCountUp) {
            return array(
                'totalCount' => $staffCountUp[0]['total_count'],
                'bp' =>  $staffCountUp[0]['total_count'] * $point
            );
        }
    }

    function getTotalAreaCount($month, $areaId, $countItemId) {

        $sql = "select Area.id area_id, sum((ifnull(keiyaku.cnt,0) - kaiyaku_cnt)) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                left join (
                select staff_id, date_format(recorded_date, '%Y%m%d') date, contract_status, count(*) cnt from staff_count_ups where count_item_id = ? and date_format(recorded_date, '%Y%m') = ? and date_format(recorded_date, '%Y%m%d') < date_format(now(), '%Y%m%d') group by staff_id, date_format(recorded_date, '%Y%m%d'), contract_status having contract_status = '契約'
                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                inner join (
                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                inner join sections as Section on (StaffHistory.section_id = Section.id) 
                inner join areas as Area on (Area.id = Section.area_id) 
                left join (
                select staff_id, date_format(recorded_date, '%Y%m%d') date, contract_status, count(*) cnt from staff_count_ups where count_item_id = ? and date_format(recorded_date, '%Y%m') = ? and date_format(recorded_date, '%Y%m%d') < date_format(now(), '%Y%m%d') group by staff_id, date_format(recorded_date, '%Y%m%d'), contract_status having contract_status = '解約'
                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
                where Area.id = ? and (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
                group by Area.id
                order by count desc;";

        $staffCountUps = $this->query($sql, array(
            $countItemId, $month, $countItemId, $month, $areaId
        ));

        if (!$staffCountUps) {
            return array(
                'areaId' => 0,
                'count' => 0
            );
        }

        return array(
            'areaId' => $staffCountUps[0]['Area']['area_id'],
            'count' => $staffCountUps[0][0]['count']
        );
    }

//    function getCountDownGraph($month, $countItemId) {
//
//        $sql = "select keiyaku.date, sum((ifnull(keiyaku.cnt,0) - kaiyaku_cnt)) count from staff_histories StaffHistory
//                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
//                inner join sections as Section on (StaffHistory.section_id = Section.id)
//                inner join areas as Area on (Area.id = Section.area_id)
//                left join (
//                select staff_id, date_format(recorded_date, '%Y%m%d') date, contract_status, count(*) cnt from staff_count_ups where count_item_id = ? and date_format(recorded_date, '%Y%m') = ? and date_format(recorded_date, '%Y%m%d') < date_format(now(), '%Y%m%d') group by staff_id, date_format(recorded_date, '%Y%m%d'), contract_status having contract_status = '契約'
//                ) keiyaku on keiyaku.staff_id = StaffHistory.staff_id
//                inner join (
//                select StaffHistory.staff_id, ifnull(kaiyaku.cnt,0) kaiyaku_cnt from staff_histories StaffHistory
//                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
//                inner join sections as Section on (StaffHistory.section_id = Section.id)
//                inner join areas as Area on (Area.id = Section.area_id)
//                left join (
//                select staff_id, date_format(recorded_date, '%Y%m%d') date, contract_status, count(*) cnt from staff_count_ups where count_item_id = ? and date_format(recorded_date, '%Y%m') = ? and date_format(recorded_date, '%Y%m%d') < date_format(now(), '%Y%m%d') group by staff_id, date_format(recorded_date, '%Y%m%d'), contract_status having contract_status = '解約'
//                ) kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
//                ) sub_tbl on sub_tbl.staff_id = StaffHistory.staff_id
//                where (ifnull(keiyaku.cnt,0) - kaiyaku_cnt) > 0
//                group by Area.id, keiyaku.date
//                order by keiyaku.date asc;";
//
//        $staffCountUps = $this->query($sql, array(
//            $countItemId, $month, $countItemId, $month
//        ));
//        $data = array();
//        foreach ($staffCountUps as $staffCountUp) {
//
//            $data[] = array(
//                'date' => $staffCountUp['keiyaku']['date'],
//                'count' => $staffCountUp[0]['count']
//            );
//        }
//
//        return $data;
//    }

    function getSupportCount($month, $countItemId, $staffId, $productCategoryId) {

        $sql = "select StaffHistory.staff_id, (ifnull(keiyaku.cnt, 0) - ifnull(kaiyaku.cnt, 0)) count from staff_histories StaffHistory
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id) tbl_max on tbl_max.staff_id = StaffHistory.staff_id and tbl_max.max_start_month = StaffHistory.start_month
                left join (select staff_id, count(*) cnt from staff_count_ups where count_item_id = ? and product_category_id = ? and date_format(recorded_date, '%Y%m') = ? group by staff_id, product_category_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '契約') keiyaku on keiyaku.staff_id = StaffHistory.staff_id
                left join (select staff_id, count(*) cnt from staff_count_ups where count_item_id = ? and product_category_id = ? and date_format(recorded_date, '%Y%m') = ? group by staff_id, product_category_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '解約') kaiyaku on kaiyaku.staff_id = StaffHistory.staff_id
                where StaffHistory.staff_id = ?;
                ";

        $staffCountUps = $this->query($sql, array(
            $countItemId, $productCategoryId, $month, $countItemId, $month, $productCategoryId, $staffId
        ));

        $data = array(
            'count' => $staffCountUps[0][0]['count']
        );

        return $data;
    }

    function getTotalBpCountBySection($sectionId, $productCategoryId, $acquisitionTypeId, $startMonth, $countItemId = 0) {

        $connection = $this->getDataSource()->getConnection();
        $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        if ($countItemId) {

            $sql = "select count(*) keiyaku, kaiyaku.cnt, count(*) - kaiyaku.cnt total_count 
                from staff_count_ups, (select count(*) cnt from staff_count_ups where product_category_id = ? and staff_id = ? and acquisition_type_id = ? 
                and count_item_id = ? and contract_status = '解約') kaiyaku 
                where product_category_id = ? and staff_id = ? and acquisition_type_id = ? and count_item_id = ? and contract_status = '契約';";

            $param = array(
                $productCategoryId,
                $sectionId,
                $acquisitionTypeId,
                $countItemId,
                $productCategoryId,
                $sectionId,
                $acquisitionTypeId,
                $countItemId,
            );

        } else {
            $sql = "select count(*) keiyaku, kaiyaku.cnt, count(*) - kaiyaku.cnt total_count 
                from staff_count_ups, (select count(*) cnt from staff_count_ups where product_category_id = ? and staff_id = ? and acquisition_type_id = ? 
                and contract_status = '解約') kaiyaku 
                where product_category_id = ? and staff_id = ? and acquisition_type_id = ? and contract_status = '契約';";

            $param = array(
                $productCategoryId,
                $sectionId,
                $acquisitionTypeId,
                $productCategoryId,
                $sectionId,
                $acquisitionTypeId,
            );
        }

        $staffCountUps = $this->query($sql, $param);

        $this->AcquisitionPointHistory = new AcquisitionPointHistory();
        $acquisitionPointHistory = $this->AcquisitionPointHistory->find('all', array(
            'conditions' => array(
                'AcquisitionPointHistory.acquisition_type_id' => $acquisitionTypeId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select acquisition_type_id, max(start_month) max_start_month from acquisition_point_histories where start_month <= '. $startMonth .' group by acquisition_type_id)',
                    'alias' => 'max_tbl',
                    'conditions' => array(
                        'max_tbl.acquisition_type_id = AcquisitionPointHistory.acquisition_type_id',
                        'max_tbl.max_start_month = AcquisitionPointHistory.start_month'
                    )
                )
            )
        ));

        $point = 0;
        if (isset($acquisitionPointHistory[0])) {
            $point = $acquisitionPointHistory[0]['AcquisitionPointHistory']['point'];
        }

        foreach ($staffCountUps as $idx => $staffCountUp) {
            return array(
                'totalCount' => $staffCountUp[0]['total_count'],
                'bp' =>  $staffCountUp[0]['total_count'] * $point
            );
        }
    }

    function getTotalSectionCount($month, $sectionId, $productCategoryId, $acquisitionTypeId, $countItemId) {

        $sql = "select distinct Section.id, Section.name, ifnull(keiyaku.cnt,0) keiyaku_cnt, ifnull(kaiyaku.cnt,0) kaiyaku_cnt, (ifnull(keiyaku.cnt,0) - ifnull(kaiyaku.cnt,0)) count
                from staff_count_ups StaffCountUp
                inner join staff_histories StaffHistory on StaffHistory.staff_id = StaffCountUp.staff_id
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where start_month <= ? and disabled_flg = 0 group by staff_id) sub_StaffHistory on sub_StaffHistory.staff_id = StaffHistory.staff_id and sub_StaffHistory.max_start_month = StaffHistory.start_month
                inner join sections Section on Section.id = StaffHistory.section_id
                inner join areas Area on Area.id = Section.area_id
                inner join divisions Division on Division.id = Area.division_id
                left join (
                select Section.id section_id, count(*) cnt from staff_count_ups 
                inner join staff_histories StaffHistory on StaffHistory.staff_id = staff_count_ups.staff_id
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where start_month <= ? and disabled_flg = 0 group by staff_id) sub_StaffHistory on sub_StaffHistory.staff_id = StaffHistory.staff_id and sub_StaffHistory.max_start_month = StaffHistory.start_month
                inner join sections Section on Section.id = StaffHistory.section_id
                inner join areas Area on Area.id = Section.area_id
                inner join divisions Division on Division.id = Area.division_id
                where product_category_id = ?  and acquisition_type_id = ? and count_item_id = ? and contract_status = '契約' and date_format(recorded_date, '%Y%m') = ?
                group by Section.id) keiyaku on keiyaku.section_id = Section.id
                left join (
                select Section.id section_id, count(*) cnt from staff_count_ups 
                inner join staff_histories StaffHistory on StaffHistory.staff_id = staff_count_ups.staff_id
                inner join (select staff_id, max(start_month) max_start_month from staff_histories where start_month <= ? and disabled_flg = 0 group by staff_id) sub_StaffHistory on sub_StaffHistory.staff_id = StaffHistory.staff_id and sub_StaffHistory.max_start_month = StaffHistory.start_month
                inner join sections Section on Section.id = StaffHistory.section_id
                inner join areas Area on Area.id = Section.area_id
                inner join divisions Division on Division.id = Area.division_id
                where product_category_id = ?  and acquisition_type_id = ? and count_item_id = ? and contract_status = '解約' and date_format(recorded_date, '%Y%m') = ?
                group by Section.id) kaiyaku on kaiyaku.section_id = Section.id
                where Section.id = ?
                ;";

        $staffCountUps = $this->query($sql, array(
            $month,
            $month, $productCategoryId, $acquisitionTypeId, $countItemId, $month,
            $month, $productCategoryId, $acquisitionTypeId, $countItemId, $month,
            $sectionId
        ));

        $this->AcquisitionPointHistory = new AcquisitionPointHistory();
        $acquisitionPointHistory = $this->AcquisitionPointHistory->find('all', array(
            'conditions' => array(
                'AcquisitionPointHistory.acquisition_type_id' => $acquisitionTypeId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select acquisition_type_id, max(start_month) max_start_month from acquisition_point_histories where start_month <= '. $month .' group by acquisition_type_id)',
                    'alias' => 'max_tbl',
                    'conditions' => array(
                        'max_tbl.acquisition_type_id = AcquisitionPointHistory.acquisition_type_id',
                        'max_tbl.max_start_month = AcquisitionPointHistory.start_month'
                    )
                )
            )
        ));

        if (!$staffCountUps) {
            return array(
                'sectionId' => $sectionId,
                'count' => 0,
                'bp' => 0
            );
        }

        $point = 0;
        if (isset($acquisitionPointHistory[0])) {
            $point = $acquisitionPointHistory[0]['AcquisitionPointHistory']['point'];
        }

        return array(
            'sectionId' => $staffCountUps[0]['Section']['id'],
            'count' => $staffCountUps[0][0]['count'],
            'bp' => $staffCountUps[0][0]['count'] * $point
        );
    }

    function getAcquisitionCount($staffId, $productCategoryId, $acquisitionTypeId, $month, $maekakuId, $atokakuId) {

        // 今日
        $today = date('Ymd');
        // 先月
        $lastMonth = date('Ym', strtotime(date('Y-m-1') . '-1 month'));
        // 翌月
        $nextMonth = date('Ym', strtotime(date('Y-m-1') . '+1 month'));

        // アポランと月報で使用
        $staffCountUps = $this->find('all', array(
            'fields' => array(
                'acquisition_type_id',
                'product_category_id',
                'staff_id',
                'name_collecting',
                'group_concat(count_item_id) count_item_ids',
                'group_concat(recorded_date) recorded_dates',
                "date_add(last_day('".$today."'),interval 1 day) next_month_1st",
                "date_format('".$today."', '%Y-%m-01') current_month_1st"
            ),
            'conditions' => array(
                'acquisition_type_id' => $acquisitionTypeId,
                'product_category_id' => $productCategoryId,
                'staff_id' => $staffId,
                'count_item_id' => array($maekakuId, $atokakuId),
                "date_format(recorded_date, '%Y%m') between ? and ?" => array($lastMonth, $nextMonth)
            ),
            'group' => array(
                'name_collecting'
            )
        ));

        $totalCount = 0;

        // 獲得件数の条件
        // ①当月1日〜末日で後確OKのもの
        // ②前確日が当月1日〜末日でかつ後確日が翌月1日のもの
        // ③前確日が前月1日〜末日でかつ後確日が当月1日のもの
        foreach ($staffCountUps as $idx => $staffCountUp) {

            $countStatus = false;

            // 翌月1日と当月1日を取得
            $nextMonth1st = date('Ymd', strtotime($staffCountUp[0]['next_month_1st']));
            $currentMonth1st = date('Ymd', strtotime($staffCountUp[0]['current_month_1st']));

            $countItemIds = $staffCountUp[0]['count_item_ids'];
            $recordedDates = $staffCountUp[0]['recorded_dates'];

            $countItemIds = explode(',', $countItemIds);
            $recordedDates = explode(',', $recordedDates);

            // 複数行あった場合
            if (count($countItemIds) > 1 && count($recordedDates) > 1) {

                // 前確と後確をサーチ
                $maekakuIdx = array_search($maekakuId, $countItemIds);
                $atokakuIdx = array_search($atokakuId, $countItemIds);

                // ①と②について処理
                $maekakuRecordedDate = $recordedDates[$maekakuIdx];
                $maekakuDatetime = new DateTime($maekakuRecordedDate);
                $atokakuRecordedDate = $recordedDates[$atokakuIdx];
                $atokakuDatetime = new DateTime($atokakuRecordedDate);

                // ①の条件
                if (!$countStatus && $atokakuDatetime->format('Ym') == $month) {
                    $totalCount++;
                    $countStatus = true;
                }

                // ②の条件
                if (!$countStatus && ($maekakuDatetime->format('Ym') == $month && $atokakuDatetime->format('Ymd') == $nextMonth1st)) {
                    $totalCount++;
                }

                // ③の条件
                if ($maekakuDatetime->format('Ym') == $lastMonth && $atokakuDatetime->format('Ymd') == $currentMonth1st) {
                    $totalCount--;
                }

            }

            // 一行の場合は後確についてのみ処理する
            if (count($countItemIds) == 1 && count($recordedDates) == 1) {
                $countItemId = $countItemIds[0];
                if ($countItemId == $atokakuId) {

                    $atokakuRecordedDate = $recordedDates[0];
                    $atokakuDatetime = new DateTime($atokakuRecordedDate);

                    // ①のみ処理する
                    if ($atokakuDatetime->format('Ym') == $month) {
                        $totalCount++;
                    }
                }
            }
        }

        $this->AcquisitionPointHistory = new AcquisitionPointHistory();
        $acquisitionPointHistory = $this->AcquisitionPointHistory->find('all', array(
            'conditions' => array(
                'AcquisitionPointHistory.acquisition_type_id' => $acquisitionTypeId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select acquisition_type_id, max(start_month) max_start_month from acquisition_point_histories where start_month <= '. $month .' group by acquisition_type_id)',
                    'alias' => 'max_tbl',
                    'conditions' => array(
                        'max_tbl.acquisition_type_id = AcquisitionPointHistory.acquisition_type_id',
                        'max_tbl.max_start_month = AcquisitionPointHistory.start_month'
                    )
                )
            )
        ));

        $point = 0;
        if (isset($acquisitionPointHistory[0])) {
            $point = $acquisitionPointHistory[0]['AcquisitionPointHistory']['point'];
        }

        return array(
            'totalCount' => $totalCount,
            'bp' =>  $totalCount * $point
        );

    }

    function getAcquisitionSectionCount($month, $sectionId, $productCategoryId, $acquisitionTypeId, $maekakuId, $atokakuId) {

        // 今日
        $today = date('Ymd');
        // 先月
        $lastMonth = date('Ym', strtotime(date('Y-m-1') . '-1 month'));
        // 翌月
        $nextMonth = date('Ym', strtotime(date('Y-m-1') . '+1 month'));

        // アポランと月報で使用
        $staffCountUps = $this->find('all', array(
            'fields' => array(
                'acquisition_type_id',
                'product_category_id',
                'Section.id',
                'name_collecting',
                'group_concat(count_item_id) count_item_ids',
                'group_concat(recorded_date) recorded_dates',
                "date_add(last_day('".$today."'),interval 1 day) next_month_1st",
                "date_format('".$today."', '%Y-%m-01') current_month_1st"
            ),
            'conditions' => array(
                'acquisition_type_id' => $acquisitionTypeId,
                'product_category_id' => $productCategoryId,
                'Section.id' => $sectionId,
                'count_item_id' => array($maekakuId, $atokakuId),
                "date_format(recorded_date, '%Y%m') between ? and ?" => array($lastMonth, $nextMonth)
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'staff_histories',
                    'alias' => 'StaffHistory',
                    'conditions' => array(
                        'StaffHistory.staff_id = StaffCountUp.staff_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id)',
                    'alias' => 'max_StaffHistory',
                    'conditions' => array(
                        'max_StaffHistory.staff_id = StaffHistory.staff_id',
                        'max_StaffHistory.max_start_month = StaffHistory.start_month'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'sections',
                    'alias' => 'Section',
                    'conditions' => array(
                        'Section.id = StaffHistory.section_id'
                    )
                )
            ),
            'group' => array(
                'name_collecting',
                'Section.id'
            )
        ));

        if (!$staffCountUps) {
            return array(
                'sectionId' => $sectionId,
                'count' => 0,
                'bp' => 0
            );
        }

        $totalCount = 0;

        // 獲得件数の条件
        // ①当月1日〜末日で後確OKのもの
        // ②前確日が当月1日〜末日でかつ後確日が翌月1日のもの
        // ③前確日が前月1日〜末日でかつ後確日が当月1日のもの
        foreach ($staffCountUps as $idx => $staffCountUp) {

            $countStatus = false;

            // 翌月1日と当月1日を取得
            $nextMonth1st = date('Ymd', strtotime($staffCountUp[0]['next_month_1st']));
            $currentMonth1st = date('Ymd', strtotime($staffCountUp[0]['current_month_1st']));

            $countItemIds = $staffCountUp[0]['count_item_ids'];
            $recordedDates = $staffCountUp[0]['recorded_dates'];

            $countItemIds = explode(',', $countItemIds);
            $recordedDates = explode(',', $recordedDates);

            // 複数行あった場合
            if (count($countItemIds) > 1 && count($recordedDates) > 1) {

                // 前確と後確をサーチ
                $maekakuIdx = array_search($maekakuId, $countItemIds);
                $atokakuIdx = array_search($atokakuId, $countItemIds);

                // ①と②について処理
                $maekakuRecordedDate = $recordedDates[$maekakuIdx];
                $maekakuDatetime = new DateTime($maekakuRecordedDate);
                $atokakuRecordedDate = $recordedDates[$atokakuIdx];
                $atokakuDatetime = new DateTime($atokakuRecordedDate);

                // ①の条件
                if (!$countStatus && $atokakuDatetime->format('Ym') == $month) {
                    $totalCount++;
                    $countStatus = true;
                }

                // ②の条件
                if (!$countStatus && ($maekakuDatetime->format('Ym') == $month && $atokakuDatetime->format('Ymd') == $nextMonth1st)) {
                    $totalCount++;
                }

                // ③の条件
                if ($maekakuDatetime->format('Ym') == $lastMonth && $atokakuDatetime->format('Ymd') == $currentMonth1st) {
                    $totalCount--;
                }

            }

            // 一行の場合は後確についてのみ処理する
            if (count($countItemIds) == 1 && count($recordedDates) == 1) {
                $countItemId = $countItemIds[0];
                if ($countItemId == $atokakuId) {

                    $atokakuRecordedDate = $recordedDates[0];
                    $atokakuDatetime = new DateTime($atokakuRecordedDate);

                    // ①のみ処理する
                    if ($atokakuDatetime->format('Ym') == $month) {
                        $totalCount++;
                    }
                }
            }
        }

        $this->AcquisitionPointHistory = new AcquisitionPointHistory();
        $acquisitionPointHistory = $this->AcquisitionPointHistory->find('all', array(
            'conditions' => array(
                'AcquisitionPointHistory.acquisition_type_id' => $acquisitionTypeId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select acquisition_type_id, max(start_month) max_start_month from acquisition_point_histories where start_month <= '. $month .' group by acquisition_type_id)',
                    'alias' => 'max_tbl',
                    'conditions' => array(
                        'max_tbl.acquisition_type_id = AcquisitionPointHistory.acquisition_type_id',
                        'max_tbl.max_start_month = AcquisitionPointHistory.start_month'
                    )
                )
            )
        ));

        $point = 0;
        if (isset($acquisitionPointHistory[0])) {
            $point = $acquisitionPointHistory[0]['AcquisitionPointHistory']['point'];
        }

        return array(
            'sectionId' => $sectionId,
            'count' => $totalCount,
            'bp' =>  $totalCount * $point
        );

    }

    function getAcquisitionAreaCount($month, $areaId, $atokakuId) {

        // 当月であれば当日まで
        $lastDay = date('Ymd');

        if ($month != date('Ym')) {
            // 月末
            $lastDay = new DateTime($month . '01');
            $lastDay = $lastDay->format('Ymt');
        }

        // 月初
        $firstDay = new DateTime($month.'01');
        $firstDay = $firstDay->format('Ymd');

        $staffCountUps = $this->find('all', array(
            'fields' => array(
                'acquisition_type_id',
                'product_category_id',
                'Area.id',
                'name_collecting',
                'recorded_date'
            ),
            'conditions' => array(
                'Area.id' => $areaId,
                'count_item_id' =>$atokakuId,
                "date_format(recorded_date, '%Y%m%d') between ? and ?" => array($firstDay, $lastDay)
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'staff_histories',
                    'alias' => 'StaffHistory',
                    'conditions' => array(
                        'StaffHistory.staff_id = StaffCountUp.staff_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id)',
                    'alias' => 'max_StaffHistory',
                    'conditions' => array(
                        'max_StaffHistory.staff_id = StaffHistory.staff_id',
                        'max_StaffHistory.max_start_month = StaffHistory.start_month'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'sections',
                    'alias' => 'Section',
                    'conditions' => array(
                        'Section.id = StaffHistory.section_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array(
                        'Area.id = Section.area_id'
                    )
                )
            ),
            'group' => array(
                'name_collecting',
                'Area.id'
            )
        ));

        if (!$staffCountUps) {
            return array(
                'areaId' => $areaId,
                'count' => 0
            );
        }

        $totalCount = 0;

        // 獲得件数の条件 アポランと月報以外
        // ①当月1日〜末日で後確OKのもの
        foreach ($staffCountUps as $idx => $staffCountUp) {

            $recordedDate = $staffCountUp['StaffCountUp']['recorded_date'];
            $atokakuDatetime = new DateTime($recordedDate);

            // ①のみ処理する
            if ($atokakuDatetime->format('Ym') == $month) {
                $totalCount++;
            }
        }

        return array(
            'areaId' => $areaId,
            'count' => $totalCount,
        );

    }

    function getCountDownGraph($month, $atokakuId) {


        // 当月であれば当日まで
        $lastDay = date('Ymd');

        if ($month != date('Ym')) {
            // 月末
            $lastDay = new DateTime($month . '01');
            $lastDay = $lastDay->format('Ymt');
        }

        // 月初
        $firstDay = new DateTime($month.'01');
        $firstDay = $firstDay->format('Ymd');

        $staffCountUps = $this->find('all', array(
            'fields' => array(
                'count(*) count',
                'recorded_date'
            ),
            'conditions' => array(
                'count_item_id' =>$atokakuId,
                "date_format(recorded_date, '%Y%m%d') between ? and ?" => array($firstDay, $lastDay)
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'staff_histories',
                    'alias' => 'StaffHistory',
                    'conditions' => array(
                        'StaffHistory.staff_id = StaffCountUp.staff_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id)',
                    'alias' => 'max_StaffHistory',
                    'conditions' => array(
                        'max_StaffHistory.staff_id = StaffHistory.staff_id',
                        'max_StaffHistory.max_start_month = StaffHistory.start_month'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'sections',
                    'alias' => 'Section',
                    'conditions' => array(
                        'Section.id = StaffHistory.section_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array(
                        'Area.id = Section.area_id'
                    )
                )
            ),
            'group' => array(
                'recorded_date'
            ),
            'order' => array(
                'recorded_date'
            )
        ));


        $data = array();
        foreach ($staffCountUps as $staffCountUp) {
            $datetime = new DateTime($staffCountUp['StaffCountUp']['recorded_date']);
            $data[] = array(
                'date' => $datetime->format('Ymd'),
                'count' => $staffCountUp[0]['count']
            );
        }

        return $data;
    }

}

