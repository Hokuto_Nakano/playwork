<?php
App::uses('AppModel', 'Model');
App::uses('Goal', 'Model');

class Division extends AppModel {
    var $hasMany = array('Area');

    function getBaseList(){
        $params = array(
            'fields' => array('Division.id', 'Division.name', 'Division.short_name', 'Area.id', 'Area.name', 'Area.short_name', 'Section.id', 'Section.name', 'Section.short_name'),
            'order' => array('Division.name ASC', 'Area.name ASC', 'Section.name ASC'),
            'joins' => array(
                array(
                    'type' => 'LEFT OUTER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array('Area.division_id = Division.id'),
                ),
                array(
                    'type' => 'LEFT OUTER',
                    'table' => 'sections',
                    'alias' => 'Section',
                    'conditions' => array('Section.area_id = Area.id'),
                ),
            ),
        );
        $rows = $this->find('all', $params);

        $bases = array();
        $before_division_name = '';
        $before_area_name = '';
        foreach($rows as $row){
            $division_name = ($before_division_name != '' && $before_division_name == $row['Division']['name']) ? '' : $row['Division']['name'];
            $division_shortname = !$division_name || !$row['Division']['short_name'] ? '' : ' ('.$row['Division']['short_name'].')';
            $area_name = (!$division_name && $before_area_name != '' && $before_area_name == $row['Area']['name']) ? '' : $row['Area']['name'];
            $area_shortname = !$area_name || !$row['Area']['short_name'] ? '' : ' ('.$row['Area']['short_name'].')';
            $section_name = $row['Section']['name'];
            $section_shortname = !$row['Section']['short_name'] ? '' : ' ('.$row['Section']['short_name'].')';
            $bases[] = array(
                'division_id' => $row['Division']['id'],
                'division_name' => $division_name.$division_shortname,
                'area_id' => $row['Area']['id'],
                'area_name' => $area_name.$area_shortname,
                'section_id' => $row['Section']['id'],
                'section_name' => $section_name.$section_shortname,
            );

            $before_division_name = $row['Division']['name'];
            $before_area_name = $row['Area']['name'];
        }

        return $bases;
    }

    function getBaseGoalList($month){
        $params = array(
            'fields' => array('Division.id', 'Division.name', 'Division.short_name', 'Area.id', 'Area.name', 'Area.short_name', 'Section.id', 'Section.name', 'Section.short_name'),
            'order' => array('Division.name ASC', 'Area.name ASC', 'Section.name ASC'),
            'joins' => array(
                array(
                    'type' => 'LEFT OUTER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array('Area.division_id = Division.id'),
                ),
                array(
                    'type' => 'LEFT OUTER',
                    'table' => 'sections',
                    'alias' => 'Section',
                    'conditions' => array('Section.area_id = Area.id'),
                )
            )
        );
        $rows = $this->find('all', $params);

        $bases = array();
        $before_division_name = '';
        $before_area_name = '';

        $this->Goal = new Goal();

        foreach($rows as $row){
            $division_name = ($before_division_name != '' && $before_division_name == $row['Division']['name']) ? '' : $row['Division']['name'];
            $division_shortname = !$division_name || !$row['Division']['short_name'] ? '' : ' ('.$row['Division']['short_name'].')';
            $area_name = (!$division_name && $before_area_name != '' && $before_area_name == $row['Area']['name']) ? '' : $row['Area']['name'];
            $area_shortname = !$area_name || !$row['Area']['short_name'] ? '' : ' ('.$row['Area']['short_name'].')';
            $section_name = $row['Section']['name'];
            $section_shortname = !$row['Section']['short_name'] ? '' : ' ('.$row['Section']['short_name'].')';

            $divisionId = $row['Division']['id'];
            $areaId = $row['Area']['id'] ? $row['Area']['id'] : 0;
            $sectionId = $row['Section']['id'] ? $row['Section']['id'] : 0;

            $goal = $this->Goal->getGoalCount($divisionId, $areaId, $sectionId, $month);

            $bases[] = array(
                'division_id' => $divisionId,
                'division_name' => $division_name.$division_shortname,
                'area_id' => $areaId,
                'area_name' => $area_name.$area_shortname,
                'section_id' => $sectionId,
                'section_name' => $section_name.$section_shortname,
                'goal_count' => $goal['goalCount'],
                'goal_bp' => $goal['goalBp']
            );

            $before_division_name = $row['Division']['name'];
            $before_area_name = $row['Area']['name'];
        }

        return $bases;
    }

    function getAllSimpleDivisions() {
        $divisions = $this->find('all', array(
            'fields' => array(
                'Division.id', 'Division.name'
            ),
            'order' => 'id'
        ));
        $ret = array();
        foreach ($divisions as $index => $division) {
            $ret[] = array(
                'id' => $division['Division']['id'],
                'name' => $division['Division']['name']
            );
        }
        return $ret;
    }

    function getFirstDivisionId() {
        $division = $this->find('first', array(
            'fields' => array(
                'Division.id'
            ),
            'order' => 'id'
        ));
        return $division['Division']['id'];
    }
}
