<?php
App::uses('AppModel', 'Model');

class ProductCsvAcquisitionMap extends AppModel {

    function getListByTableMapId($tableMapId){
        $acquisitionMaps = $this->find('all', array(
            'conditions' => array('table_map_id' => $tableMapId)
        ));
        if(!$acquisitionMaps) return null;
        $result = array();
        foreach($acquisitionMaps as $acquisitionMap){
            $result[] = $acquisitionMap["ProductCsvAcquisitionMap"];
        }
        return $result;
    }
}
