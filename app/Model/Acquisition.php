<?php
App::uses('AppModel', 'Model');
App::uses('ProductCsvTableMap', 'Model');
App::uses('ProductCsvAcquisitionMap', 'Model');
App::uses('ProductCsvStatusMap', 'Model');
App::uses('CountCondition', 'Model');
App::uses('StaffCondition', 'Model');
App::uses('ProductCsvPlanMap', 'Model');
App::uses('AcquisitionProduct', 'Model');
App::uses('StaffCondition', 'Model');
App::uses('StaffCountUp', 'Model');
App::uses('Staff', 'Model');
App::uses('StaffHistory', 'Model');
App::uses('CountItem', 'Model');


class Acquisition extends AppModel {

    var $hasOne = array('AcquisitionProduct');

    function aggregate($tableName, $month){

        $this->ProductCsvTableMap = new ProductCsvTableMap;
        $tableMap = $this->ProductCsvTableMap->getByProductInputTableName($tableName);
        if(!$tableMap) return false;

        $productCategoryId = $tableMap['product_category_id'];
        $nameCollectingColumnName = $tableMap['name_collecting'];

        $this->CountCondition = new CountCondition;
        $countConditions = $this->CountCondition->getListByTableMapId($tableMap["id"]);

        $this->ProductCsvAcquisitionMap = new ProductCsvAcquisitionMap;
        $acquisitionMaps = $this->ProductCsvAcquisitionMap->getListByTableMapId($tableMap["id"]);

        $this->ProductCsvStatusMap = new ProductCsvStatusMap;
        $statusMaps = $this->ProductCsvStatusMap->getListByTableMapId($tableMap["id"]);

        $this->ProductCsvPlanMap = new ProductCsvPlanMap;
        $planMaps = $this->ProductCsvPlanMap->getListByTableMapId($tableMap["id"]);

        $this->StaffCondition = new StaffCondition;
        $this->Staff = new Staff;
        $this->StaffHistory = new StaffHistory;
        $this->StaffCountUp = new StaffCountUp;
        $this->AcquisitionProduct = new AcquisitionProduct;

        // delete-insertのためdelete
        $acquisition = $this->find('all', array(
                'conditions' => array(
                    'product_category_id' => $productCategoryId
                ))
        );
        if ($acquisition) {
            foreach ($acquisition as $idx => $item) {
                $acquisitionId = $item['Acquisition']['id'];
                $acquisitionProductId = $item['AcquisitionProduct']['id'];
                $this->delete($acquisitionId);
                $this->AcquisitionProduct->delete($acquisitionProductId);
            }
        }
        $this->StaffCountUp->deleteAll(array('product_category_id' => $productCategoryId));

        $dbConfig = new DATABASE_CONFIG();

        $pdo = new PDO('mysql:host='.$dbConfig->default['host'].';dbname='.$dbConfig->default['database'].';charset=utf8',$dbConfig->default['login'],$dbConfig->default['password'],
            array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $query = 'select * from '.$tableName.';';

        $dynamicTableRows = $pdo->query($query);

        $idx = 0;
        $rowCount = $dynamicTableRows->rowCount() - 1;
        $acquisitionProductData = array();
        $specificData = null;
        $specificFlg = false;

        // 名寄せキーをまとめる
        $query = 'select ' . $nameCollectingColumnName . ' from '.$tableName.';';
        $nameCollectingRows = $pdo->query($query);
        $nameCollectings = array();
        foreach($nameCollectingRows as $row){
            $nameCollectings[] = $row[$nameCollectingColumnName];
        }
        // count_item_idをまとめる
        $countItemIds = array();
        foreach ($countConditions as $countCondition) {
            $countItemIds[] = $countCondition["count_item_id"];
        }

        // 獲得のcount_item_idを取得しておく
        $this->CountItem = new CountItem();
        $cntItemIds = $this->CountItem->getCountItemId();
        $sectionStaffId = 0;
        $sectionStaff = null;

        foreach($dynamicTableRows as $row){

            $staffIdList = array();
            $countItemIdx = 0;

            $nameCollecting = $row[$nameCollectingColumnName];

            $acquisitionTypeId = 0;
            foreach($acquisitionMaps as $acquisitionMap){
                if($this->compare($acquisitionMap["comparison_condition"], $row[$acquisitionMap["product_input_column_name"]], $acquisitionMap["comparison_string"])){
                    $acquisitionTypeId = $acquisitionMap["acquisition_type_id"];
                    break;
                }
            }

            $contractStatus = "";
            foreach($statusMaps as $statusMap){
                if($this->compare($statusMap["comparison_condition"], $row[$statusMap["product_input_column_name"]], $statusMap["comparison_string"])){
                    $contractStatus = $statusMap["contract_status"];
                    break;
                }
            }

            foreach($countConditions as $countCondition){
                $countItemId = $countCondition["count_item_id"];
                $acquisitionDate = $row[$countCondition["date_column_name"]];

                if($this->compare($countCondition["comparison_condition"], $row[$countCondition["data_column_name"]], $countCondition["comparison_string"])){

                    $staffConditions = $this->StaffCondition->find('all', array(
                        'conditions' => array(
                            'count_condition_id' => $countCondition["id"]
                        ),
                        'fields' => array('StaffCondition.*')
                    ));

                    if ($staffConditions) {

                        $staffIds = array();
                        foreach ($staffConditions as $index => $list) {
                            $staffIds[$index] = array();
                            $staffCondition = $list['StaffCondition'];

                            if ($staffCondition["user_id_column_name"]) {
                                $userIdColumnName = $row[$staffCondition["user_id_column_name"]];
                            } else {
                                $userIdColumnName = null;
                            }
                            if ($staffCondition["user_name_column_name"]) {
                                $userNameColumnName = $row[$staffCondition["user_name_column_name"]];
                            } else {
                                $userNameColumnName = null;
                            }

                            $staffs = $this->getCountUpStaff($staffCondition["comparison_condition"], $userIdColumnName, $userNameColumnName, $month);
                            if (!$staffs) {
                                break;
                            } else {
                                foreach ($staffs as $item => $staff) {
                                    $countUpStaff = $staff['Staff'];
                                    $staffIds[$index][] = $countUpStaff['id'];
                                }
                            }
                        }
                        // 配列が返る
                        $staffIdList = $this->getStaffId($staffIds);
                    }

                    // count_item_idが異なる場合は登録
                    if (($countItemIdx == count($countItemIds) -1 || $countItemIds[$countItemIdx+1] != $countItemId) &&
                        ($idx == count($nameCollectings) -1 || $nameCollecting != $nameCollectings[$idx+1])) {

                        if ($acquisitionTypeId != 0 && !empty($contractStatus)) {
                            foreach ($staffIdList as $staffId) {
                                $data = array(
                                    "product_category_id" => $productCategoryId,
                                    "count_item_id" => $countItemId,
                                    'acquisition_type_id' => $acquisitionTypeId,
                                    "staff_id" => $staffId,
                                    'contract_status' => $contractStatus,
                                    "recorded_date" => $acquisitionDate,
                                    'name_collecting' => $nameCollecting
                                );
                                $this->StaffCountUp->create();
                                if (!$this->StaffCountUp->save($data)) {
                                    $this->deleteInputTable($pdo, $tableName);
                                    throw new InternalErrorException('登録に失敗しました。[テーブル：staff_count_ups]');
                                }

                                // 獲得の場合はスタッフを保存しておく
                                if (is_numeric(array_search($countItemId, $cntItemIds))) {
                                    $sectionStaffId = $staffId;
                                }
                            }
                        }
                    }

                }
                $countItemIdx++;
            }

            $flags = array();
            $productId = 0;
            foreach($planMaps as $planMapIdx => $planMap){
                if($this->compare($planMap["comparison_condition"], $row[$planMap["product_input_column_name"]], $planMap["comparison_string"])){
                    $productId = $planMap["product_id"];
                    $flags[] = true;
                    if (count($planMaps)-1 == $planMapIdx || $planMaps[$planMapIdx+1]['product_id'] != $productId) {
                        // falseが含まれているか調べる。全てtrueの場合はfalseが返る
                        $result = in_array(false, $flags);
                        if (!$result) {
                            break;
                        } else {
                            $flags = array();
                        }
                    }

                } else {
                    $productId = 0;
                    $flags[] = false;
                    if (count($planMaps)-1 == $planMapIdx || $planMaps[$planMapIdx]['product_id'] != $planMaps[$planMapIdx+1]['product_id']) {
                        $flags = array();
                    }
                }
            }

            if ($acquisitionTypeId != 0 && $acquisitionDate != 0 && !empty($contractStatus)) {

                if (!$specificFlg) {
                    $specificData = array(
                        "product_category_id" => $productCategoryId,
                        "acquisition_type_id" => $acquisitionTypeId,
                        "acquisition_date" => $acquisitionDate,
                        "contract_status" => $contractStatus
                    );
                    $specificFlg = true;
                }
            }

            if ($productId != 0 && $specificFlg) {

                // カウントされたスタッフから部署を特定する
                if ($sectionStaffId) {
                    $sectionStaff = $this->StaffHistory->getStaff($sectionStaffId, $month);
                }

                // acquisition_productsに登録するデータ
                $acquisition_month = date('Ym', strtotime($acquisitionDate));
                $acquisitionProductData[] = array(
                    'product_id' => $productId,
                    'division_id' => $sectionStaff ? $sectionStaff['division_id'] : 0,
                    'area_id' => $sectionStaff ? $sectionStaff['area_id'] : 0,
                    'section_id' => $sectionStaff ? $sectionStaff['section_id'] : 0,
                    'acquisition_date' => $acquisitionDate,
                    'acquisition_month' => $acquisition_month
                );
                $sectionStaffId = 0;
                $sectionStaff = null;
            }

            if ($idx == $rowCount || $nameCollecting != $nameCollectings[$idx+1]) {

                // 名寄せキーが前行と等しくない場合、acquisitionの登録
                if ($specificFlg) {
                    $this->create();
                    if (!$this->save($specificData)) {
                        $this->deleteInputTable($pdo, $tableName);
                        throw new InternalErrorException('登録に失敗しました。[テーブル：acquisitions]');
                    }
                    // aquisition_productsの登録
                    $acquisitionId = $this->getLastInsertID();
                    if ($acquisitionId != 0) {
                        foreach ($acquisitionProductData as $value) {
                            $value['acquisition_id'] = $acquisitionId;
                            $this->AcquisitionProduct->create();
                            if (!$this->AcquisitionProduct->save($value)) {
                                $this->deleteInputTable($pdo, $tableName);
                                throw new InternalErrorException('登録に失敗しました。[テーブル：acquisition_products]');
                            }
                        }
                    }
                    $specificFlg = false;
                    $specificData = null;
                    $acquisitionProductData = array();
                }

            }
            $idx++;
        }
    }

    function getCountUpStaff($type, $staffNo, $staffName, $startMonth){

        $staff = null;

        if ($staffNo) {

            switch($type) {
                case COMPARE_EQUAL:
                    $staff = $this->Staff->findAll(array(
                            'conditions' => array('Staff.staff_no' => $staffNo),
                            'joins' => array(
                                array(
                                    'type' => 'INNER',
                                    'table' => 'staff_histories',
                                    'alias' => 'StaffHistory',
                                    'conditions' => array(
                                        'StaffHistory.staff_id = Staff.id',
                                    )
                                ),
                                array(
                                    'type' => 'INNER',
                                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                                    'alias' => 'sub_StaffHistory',
                                    'conditions' => array(
                                        'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                                        'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                                    )
                                )
                            )
                        )
                    );
                    break;
                case COMPARE_NOTEQUAL:
                    $staff = $this->Staff->findAll(array(
                        'conditions' => array(
                            'NOT' => array(
                                'Staff.staff_no' => $staffNo
                            )
                        ),
                        'joins' => array(
                            array(
                                'type' => 'INNER',
                                'table' => 'staff_histories',
                                'alias' => 'StaffHistory',
                                'conditions' => array(
                                    'StaffHistory.staff_id = Staff.id',
                                )
                            ),
                            array(
                                'type' => 'INNER',
                                'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                                'alias' => 'sub_StaffHistory',
                                'conditions' => array(
                                    'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                                    'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                                )
                            )
                        )
                    ));
                    break;
                case COMPARE_INDEXOF:
                    $staff = $this->Staff->findAll(array(
                        'conditions' => array(
                            'Staff.staff_no LIKE' => '%'.$staffNo.'%'
                        ),
                        'joins' => array(
                            array(
                                'type' => 'INNER',
                                'table' => 'staff_histories',
                                'alias' => 'StaffHistory',
                                'conditions' => array(
                                    'StaffHistory.staff_id = Staff.id',
                                )
                            ),
                            array(
                                'type' => 'INNER',
                                'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                                'alias' => 'sub_StaffHistory',
                                'conditions' => array(
                                    'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                                    'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                                )
                            ),
                        )
                    ));
                    break;
                case COMPARE_NOTINDEXOF:
                    $staff = $this->Staff->findAll(array(
                        'conditions' => array(
                            'Staff.staff_no not LIKE' => '%'.$staffNo.'%'
                        ),
                        'joins' => array(
                            array(
                                'type' => 'INNER',
                                'table' => 'staff_histories',
                                'alias' => 'StaffHistory',
                                'conditions' => array(
                                    'StaffHistory.staff_id = Staff.id',
                                )
                            ),
                            array(
                                'type' => 'INNER',
                                'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                                'alias' => 'sub_StaffHistory',
                                'conditions' => array(
                                    'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                                    'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                                )
                            )
                        )
                    ));
                    break;
            }

        } else if ($staffName) {

            switch($type) {
                case COMPARE_EQUAL:
                    $staff = $this->StaffHistory->findAll(array(
                        'conditions' => array(
                            'StaffHistory.name' => $staffName
                        ),
                        'joins' => array(
                            array(
                                'type' => 'INNER',
                                'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                                'alias' => 'sub_StaffHistory',
                                'conditions' => array(
                                    'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                                    'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                                )
                            )
                        )
                    ));
                    break;
                case COMPARE_NOTEQUAL:
                    $staff = $this->StaffHistory->findAll(array(
                        'conditions' => array(
                            'NOT' => array(
                                'StaffHistory.name' => $staffName
                            )
                        ),
                        'joins' => array(
                            array(
                                'type' => 'INNER',
                                'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                                'alias' => 'sub_StaffHistory',
                                'conditions' => array(
                                    'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                                    'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                                )
                            )
                        )
                    ));
                    break;
                case COMPARE_INDEXOF:
                    $staff = $this->StaffHistory->findAll(array(
                        'conditions' => array(
                            'StaffHistory.name LIKE' => '%'.$staffName.'%'
                        ),
                        'joins' => array(
                            array(
                                'type' => 'INNER',
                                'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                                'alias' => 'sub_StaffHistory',
                                'conditions' => array(
                                    'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                                    'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                                )
                            )
                        )
                    ));
                    break;
                case COMPARE_NOTINDEXOF:
                    $staff = $this->StaffHistory->findAll(array(
                        'conditions' => array(
                            'StaffHistory.name not LIKE' => '%'.$staffName.'%'
                        ),
                        'joins' => array(
                            array(
                                'type' => 'INNER',
                                'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                                'alias' => 'sub_StaffHistory',
                                'conditions' => array(
                                    'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                                    'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                                )
                            )
                        )
                    ));
                    break;
            }
        }
        // else {
        //     $this->deleteInputTable($pdo, $tableName);
        //     throw new InternalErrorException('6/6pageで指定したカラムが空です。');
        // }

        return $staff;
    }

    function compare($type, $colName, $text){
        $result = false;
        if($type == COMPARE_EQUAL){
            $result = $colName == $text;
        }
        else if($type == COMPARE_NOTEQUAL){
            $result = $colName != $text;
        }
        else if($type == COMPARE_INDEXOF){
            if (empty($text)) return false;
            $result = strpos($colName, $text) !== FALSE;
        }
        else if($type == COMPARE_NOTINDEXOF){
            $result = strpos($colName, $text) === FALSE;
        }

        return $result;
    }

    function getStaffId($staffIds) {

        if (count($staffIds) == 0) {
            return array();
        } else if (count($staffIds) == 1) {
            return $staffIds[0];
        } else if (count($staffIds) == 2) {

            $containsId = array();
            foreach ($staffIds[0] as $staffId0) {
                $result = in_array($staffId0, $staffIds[1]);
                if ($result) {
                    $containsId[] = $staffId0;
                }
            }
            return count($containsId) == 0 ? array() : $containsId;
        } else {
            $this->log('getStaffId 不正なstaff_conditions', LOG_WARNING);
            return array();
        }
    }

    function deleteInputTable ($pdo, $tableName){

        try {
            $query = 'delete from '.$tableName.';';
            $pdo->query($query);

        } catch (PDOException $e) {
            $this->log($e->getMessage());
        }
        $this->log($query, LOG_DEBUG);
    }

}
