<?php
App::uses('AppModel', 'Model');
App::import('Model', 'Division');


class Area extends AppModel {
    var $belongsTo = array('Division');
    // var $hasMany = array('Section');

    function getFirstAreas() {
        $this->Division = new Division();
        $division = $this->Division->find('first', array(
            'fields' => array(
                'id'
            ),
            'order' => 'id'
        ));

        $areas = $this->find('all', array(
            'conditions' => array(
                'division_id' => $division['Division']['id']
            ),
            'fields' => array(
                'id', 'name'
            ),
            'order' => 'id'
        ));

        $ret = array();
        foreach ($areas as $index => $area) {
            $ret[] = array(
                'id' => $area['Area']['id'],
                'name' => $area['Area']['name']
            );
        }
        return $ret;
    }

    function getAllSimpleAreas() {
        $areas = $this->find('all', array(
            'fields' => array(
                'Area.id', 'Area.name'
            )
        ));
        $ret = array();
        foreach ($areas as $index => $area) {
            $ret[] = array(
                'id' => $area['Area']['id'],
                'name' => $area['Area']['name']
            );
        }
        return $ret;
    }

    function getAllSimpleAreasByDivisionId($divisionId) {
        $areas = $this->find('all', array(
            'conditions' => array(
                'division_id' => $divisionId
            ),
            'fields' => array(
                'Area.id', 'Area.name'
            )
        ));
        $ret = array();
        foreach ($areas as $index => $area) {
            $ret[] = array(
                'id' => $area['Area']['id'],
                'name' => $area['Area']['name']
            );
        }
        return $ret;
    }

    function getAllSimpleAreasById($areaId) {
        $areas = $this->find('all', array(
            'conditions' => array(
                'Area.id' => $areaId
            ),
            'fields' => array(
                'Area.id', 'Area.name'
            )
        ));
        $ret = array();
        foreach ($areas as $index => $area) {
            $ret[] = array(
                'id' => $area['Area']['id'],
                'name' => $area['Area']['name']
            );
        }
        return $ret;
    }
}
