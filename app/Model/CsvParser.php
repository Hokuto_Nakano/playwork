<?php
App::uses('AppModel', 'Model');

class CsvParser extends AppModel {

    function getCsvHeader($fileName) {
        $csvData = file($fileName, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
        $csvHeader = mb_convert_encoding($csvData[0], 'UTF-8', 'sjis-win');
        // ダブルクォーテーションが付加されている場合は削除
        $csvHeader = str_replace('"', '', $csvHeader);
        return explode(',', $csvHeader);
    }

    function getCsvData($fileName) {

        $csvData = file($fileName, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);

        // ヘッダーは飛ばして2行目から処理する
        $csvRows = array();
        $count = 0;
        foreach($csvData as $line) {
            $line = mb_convert_encoding($line, 'UTF-8', 'sjis-win');
            // ダブルクォーテーションが付加されている場合は削除
            $line = str_replace('"', '', $line);
            if ($count == 0) {
                $count++;
                continue;
            }
            $record = explode(',', $line);
            $csvRows[] = $record;
        }
        return $csvRows;
    }
}
