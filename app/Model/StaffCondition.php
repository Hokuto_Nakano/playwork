<?php
App::uses('AppModel', 'Model');

class StaffCondition extends AppModel {

    var $belongsTo = array('CountCondition');

    function getListByCountConditionId($countConditionId) {
        $staffConditions = $this->findByCountConditionId($countConditionId);
        return $staffConditions['StaffCondition'];
    }

}
