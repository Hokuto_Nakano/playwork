<?php
App::uses('AppModel', 'Model');

class Product extends AppModel {
    //var $belongsTo = array('ProductDetail');

    function createNewProduct(){
        $thisMonth = Util::GetThisMonth();
        $product = array(
            'product_id' => 0,
            'product_category_id' => 0,
            'product_category_name' => '',
            'product_category_short_name' => '',
            'product_family_id' => 0,
            'product_family_name' => '',
            'product_family_short_name' => '',
            'product_detail_id' => 0,
            'product_detail_name' => '',
            'product_detail_short_name' => '',
            'price' => 0,
            'start_month' => $thisMonth
        );

        return $product;
    }
    function getProductById($productId){
        $params = array(
            'conditions' => array('Product.id' => $productId),
            'fields' => array(
                'ProductCategory.id', 'ProductCategory.name', 'ProductCategory.short_name',
                'ProductFamily.id', 'ProductFamily.name', 'ProductFamily.short_name',
                'ProductDetail.id',
                'Product.id', 'Product.product_detail_name', 'Product.product_detail_short_name', 'Product.price', 'Product.start_month'
            ),
            'joins' => array(
                array(
                    'type' => 'LEFT',
                    'table' => 'product_details',
                    'alias' => 'ProductDetail',
                    'conditions' => array('ProductDetail.id = Product.product_detail_id'),
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'product_families',
                    'alias' => 'ProductFamily',
                    'conditions' => array('ProductFamily.id = ProductDetail.product_family_id'),
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'product_categories',
                    'alias' => 'ProductCategory',
                    'conditions' => array('ProductCategory.id = ProductFamily.product_category_id'),
                ),
            ),
        );
        $data = $this->find('first', $params);
        if(!$data) return null;

        $product = array(
            'product_id' => $data['Product']['id'],
            'product_category_id' => $data['ProductCategory']['id'],
            'product_category_name' => $data['ProductCategory']['name'],
            'product_category_short_name' => $data['ProductCategory']['short_name'],
            'product_family_id' => $data['ProductFamily']['id'],
            'product_family_name' => $data['ProductFamily']['name'],
            'product_family_short_name' => $data['ProductFamily']['short_name'],
            'product_detail_id' => $data['ProductDetail']['id'],
            'product_detail_name' => $data['Product']['product_detail_name'],
            'product_detail_short_name' => $data['Product']['product_detail_short_name'],
            'price' => $data['Product']['price'],
            'start_month' => $data['Product']['start_month']
        );

        return $product;
    }

    function getList($yyyymm){
        $conditions = array(
            'conditions' => array("Product.start_month = (SELECT MAX(start_month) FROM products AS p WHERE p.product_detail_id = Product.product_detail_id AND start_month <= $yyyymm)"),
            'fields' => array(
                'ProductCategory.id', 'ProductCategory.name', 'ProductCategory.short_name',
                'ProductFamily.id', 'ProductFamily.name', 'ProductFamily.short_name',
                'ProductDetail.id',
                'Product.id', 'Product.product_detail_name', 'Product.product_detail_short_name', 'Product.price', 'Product.start_month'
            ),
            'order' => array('ProductCategory.name ASC', 'ProductFamily.name ASC', 'Product.product_detail_name ASC'),
            'joins' => array(
                array(
                    'type' => 'LEFT',
                    'table' => 'product_details',
                    'alias' => 'ProductDetail',
                    'conditions' => array('ProductDetail.id = Product.product_detail_id'),
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'product_families',
                    'alias' => 'ProductFamily',
                    'conditions' => array('ProductFamily.id = ProductDetail.product_family_id'),
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'product_categories',
                    'alias' => 'ProductCategory',
                    'conditions' => array('ProductCategory.id = ProductFamily.product_category_id'),
                ),
            ),
        );
        $rows = $this->find('all', $conditions);

        $products = array();
        $category_name = '';
        $family_name = '';
        $detail_name = '';
        $before_category_name = '';
        $before_family_name = '';
        foreach($rows as $row){
            $category = $row['ProductCategory'];
            $family = $row['ProductFamily'];
            $detail = $row['ProductDetail'];
            $product = $row['Product'];
            $category_name = ($before_category_name != '' && $before_category_name == $category['name']) ? '' : $category['name'];
            $category_shortname = !$category_name || !$category['short_name'] ? '' : ' ('.$category['short_name'].')';
            $family_name = (!$category_name && $before_family_name != '' && $before_family_name == $family['name']) ? '' : $family['name'];
            $family_shortname = !$family_name || !$family['short_name'] ? '' : ' ('.$family['short_name'].')';
            $detail_name = $product['product_detail_name'];
            $detail_shortname = !$product['product_detail_short_name'] ? '' : ' ('.$product['product_detail_short_name'].')';
            $products[] = array(
                'product_id' => $product['id'],
                'product_category_id' => $category['id'],
                'product_category_name' => $category_name.$category_shortname,
                'product_family_id' => $family['id'],
                'product_family_name' => $family_name.$family_shortname,
                'product_detail_id' => $detail['id'],
                'product_detail_name' => $detail_name.$detail_shortname,
                'price' => $product['price'],
                'start_month' => $product['start_month']
            );

            $before_category_name = $category['name'];
            $before_family_name = $family['name'];
        }

        return $products;
    }

    function getProductByProductCategoryId($productCategoryId, $startMonth){
        $params = array(
            'conditions' => array('Product.start_month <=' => $startMonth),
            'group' => array('ProductDetail.id'),
            'order' => 'Product.start_month',
            'fields' => array(
                'ProductCategory.id', 'ProductCategory.name', 'ProductCategory.short_name',
                'ProductFamily.id', 'ProductFamily.name', 'ProductFamily.short_name',
                'ProductDetail.id',
                'Product.id', 'Product.product_detail_name', 'Product.product_detail_short_name', 'Product.price', 'Product.start_month'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'product_details',
                    'alias' => 'ProductDetail',
                    'conditions' => array('ProductDetail.id = Product.product_detail_id'),
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'product_families',
                    'alias' => 'ProductFamily',
                    'conditions' => array('ProductFamily.id = ProductDetail.product_family_id'),
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'product_categories',
                    'alias' => 'ProductCategory',
                    'conditions' => array('ProductCategory.id = ProductFamily.product_category_id and ProductCategory.id ='. $productCategoryId),
                ),
            ),
        );
        $data = $this->find('all', $params);
        if(!$data) return null;

        return $data;
    }
}
