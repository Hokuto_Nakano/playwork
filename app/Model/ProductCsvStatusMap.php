<?php
App::uses('AppModel', 'Model');

class ProductCsvStatusMap extends AppModel {

    function getListByTableMapId($tableMapId){
        $statusMaps = $this->find('all', array(
            'conditions' => array('table_map_id' => $tableMapId)
        ));
        if(!$statusMaps) return null;
        $result = array();
        foreach($statusMaps as $statusMap){
            $result[] = $statusMap["ProductCsvStatusMap"];
        }
        return $result;
    }
}
