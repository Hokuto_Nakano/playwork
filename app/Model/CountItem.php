<?php
App::uses('AppModel', 'Model');

class CountItem extends AppModel {

    function getCountItemId() {
        // 獲得のJDを取得
        $countItems = $this->find('all', array(
            'fields' => array(
                'CountItem.id'
            ),
            'conditions' => array(
                'item_name' => array(
                    '獲得件数'
                )
            )
        ));

        $data = array();
        foreach ($countItems as $countItem) {
            $data[] = $countItem['CountItem']['id'];
        }
        return $data;
    }

    function getMaekakuId() {
        // 前確IDを取得
        $countItem = $this->findByItemName('前確数');
        return $countItem['CountItem']['id'];
    }

    function getAtokakuId() {
        // 後確IDを取得
        $countItem = $this->findByItemName('後確OK数');
        return $countItem['CountItem']['id'];
    }
}
