<?php
App::uses('AppModel', 'Model');

class SampleModel extends AppModel {
    function getSampleData(){
        return "sample model data";
    }
}
