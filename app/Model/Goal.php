<?php
App::uses('AppModel', 'Model');
App::uses('Division', 'Model');

class Goal extends AppModel {

    function getList($month) {

        $goals = $this->find('all', array(
            'conditions' => array(
                'Goal.month' => $month
            ),
            'order' => 'Division.id',
            'fields' => array(
                'Goal.area_id',
                'Division.name',
                'Area.name',
                'sum(goal_count) goal_count'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array(
                        'Area.id = Goal.area_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'divisions',
                    'alias' => 'Division',
                    'conditions' => array(
                        'Division.id = Area.division_id'
                    )
                )
            ),
            'group' => array(
                'Goal.area_id',
                'Goal.month'
            )
        ));

        $data = array();
        foreach ($goals as $goal) {
            $data[] = array(
                'areaId' => $goal['Goal']['area_id'],
                'divisionName' => $goal['Division']['name'],
                'areaName' => $goal['Area']['name'],
                'goalCount' => $goal[0]['goal_count']
            );
        }
        return $data;
    }

    function getGoalCount($divisionId, $areaId, $sectionId, $month) {
        $goal = $this->findByDivisionIdAndAreaIdAndSectionIdAndMonth($divisionId, $areaId, $sectionId, $month);

        return array(
            'goalCount' => isset($goal['Goal']) ? $goal['Goal']['goal_count'] : '',
            'goalBp' => isset($goal['Goal']) ? $goal['Goal']['goal_bp'] : '',
        );
    }

    function getReportMonthly($month, $divisionId = 0) {

        $conditions = array(
            'fields' => array(
                'Division.id, Division.name, Area.id, Area.name, Section.id, Section.name, Goal.goal_bp'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'sections',
                    'alias' => 'Section',
                    'conditions' => array(
                        'Section.id = Goal.section_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array(
                        'Area.id = Goal.area_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'divisions',
                    'alias' => 'Division',
                    'conditions' => array(
                        'Division.id = Area.division_id'
                    )
                )
            ),
            'order' => array(
                'Division.id', 'Area.id', 'Section.id',
            )
        );

        if ($divisionId) {
            $conditions['conditions'] = array(
                'Division.id' => $divisionId,
                'month' => $month
            );
        } else {
            $conditions['conditions'] = array(
                'month' => $month
            );
        }

        // 対象年月の拠点目標が設定されていない場合は目標を0にする
        $this->Division = new Division();
        $divisions = $this->Division->getBaseGoalList($month);

        foreach ($divisions as $division) {
            $goalCount = $division['goal_count'];
            if ($goalCount == '') {
                $data = array(
                    'month' => $month,
                    'goal_count' => 0,
                    'goal_bp' => 0,
                    'division_id' => $division['division_id'],
                    'area_id' => $division['area_id'],
                    'section_id' => $division['section_id']
                );
                $this->create();
                if (!$this->save($data)) {
                    throw new InternalErrorException();
                }
            }
        }

        $goals = $this->find('all', $conditions);

        $data = array();
        foreach ($goals as $goal) {
            $data[] = array(
                'divisionId' => $goal['Division']['id'],
                'divisionName' => $goal['Division']['name'],
                'areaId' => $goal['Area']['id'],
                'areaName' => $goal['Area']['name'],
                'sectionId' => $goal['Section']['id'],
                'sectionName' => $goal['Section']['name'],
                'goalBp' => $goal['Goal']['goal_bp']
            );
        }
        return $data;
    }

}
