<?php
App::uses('AppModel', 'Model');

class PlItem extends AppModel {

    function getList($yyyymm){
        $conditions = array(
            'conditions' => array("PlItem.start_month = (SELECT MAX(pli.start_month) FROM pl_items AS pli WHERE pli.id = PlItem.id AND pli.start_month <= $yyyymm)"),
            'order' => array('PlCategory.calc_order ASC', 'PlCategory.show_order ASC', 'PlItem.calc ASC', 'PlItem.id ASC'),
            'fields' => array(
                'PlCategory.category_name', 'PlItem.id', 'PlItem.item_name', 'PlItem.calc', 'PlItem.start_month'
            ),
            'joins' => array(
                array(
                    'type' => 'LEFT',
                    'table' => 'pl_categories',
                    'alias' => 'PlCategory',
                    'conditions' => array('PlCategory.id = PlItem.category_id'),
                ),
            ),
        );
        $plitems = $this->find('all', $conditions);
        return $plitems;
    }

    function getById($id){
        $conditions = array(
            'conditions' => array("PlItem.id = ". $id),
            'fields' => array(
                'PlCategory.id', 'PlCategory.category_name', 'PlItem.id', 'PlItem.item_name', 'PlItem.calc', 'PlItem.start_month'
            ),
            'joins' => array(
                array(
                    'type' => 'LEFT',
                    'table' => 'pl_categories',
                    'alias' => 'PlCategory',
                    'conditions' => array('PlCategory.id = PlItem.category_id'),
                ),
            ),
        );
        $plitem = $this->find('first', $conditions);
        return $plitem;
    }
}
