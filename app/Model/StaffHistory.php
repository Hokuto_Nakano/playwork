<?php
App::uses('AppModel', 'Model');

class StaffHistory extends AppModel {
    var $belongsTo = array('Section', 'CallCenter','ShiftType', 'JobType', 'StaffType', 'Staff', 'Company');

    /**
     * 一覧表示のデータを取得する
     * @param $startMonth
     * @return mixed
     */
    function getList($startMonth, $offset, $limit) {

        // limit句のプレースホルダーを文字列にしない
        $connection = $this->getDataSource()->getConnection();
        $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        $sql = 'select hist.id, hist.start_month, stf.staff_no, stf.login_id, stf.password, stf.outside_attendance_id, stf.hire_int_date, divis.name, area.name, sec.name, job.type, sfttype.type, cc.name, shift.type, company.name, hist.name from staff_histories hist ';
	    $sql.= 'inner join staffs stf on stf.id = hist.staff_id ';
        $sql.= 'left join sections sec on sec.id = hist.section_id ';
        $sql.= 'left join areas area on area.id = sec.area_id ';
        $sql.= 'left join divisions divis on divis.id = area.division_id ';
        $sql.= 'left join job_types job on job.id = hist.job_type_id ';
        $sql.= 'left join call_centers cc on cc.id = hist.call_center_id ';
        $sql.= 'left join shift_types shift on shift.id = hist.shift_type_id ';
        $sql.= 'left join staff_types sfttype on sfttype.id = hist.staff_type_id ';
        $sql.= 'left join companies company on company.id = hist.company_id ';
        $sql.= 'inner join (select staff_id, max(start_month) max_month from staff_histories where start_month <= ? group by staff_id) max_tbl on max_tbl.staff_id = hist.staff_id and max_tbl.max_month = hist.start_month ';
        $sql.= 'where hist.disabled_flg = 0 ';
        $sql.= 'order by hist.staff_id ';
        $sql.= 'limit ?, ?;';

        $params = array($startMonth, $offset, $limit);
        return $this->query($sql, $params);
    }

    function getCount($startMonth) {

        $sql = 'select count(*) count from staff_histories hist ';
        $sql.= 'inner join staffs stf on stf.id = hist.staff_id ';
        $sql.= 'left join sections sec on sec.id = hist.section_id ';
        $sql.= 'left join areas area on area.id = sec.area_id ';
        $sql.= 'left join divisions divis on divis.id = area.division_id ';
        $sql.= 'left join job_types job on job.id = hist.job_type_id ';
        $sql.= 'left join call_centers cc on cc.id = hist.call_center_id ';
        $sql.= 'left join shift_types shift on shift.id = hist.shift_type_id ';
        $sql.= 'left join staff_types sfttype on sfttype.id = hist.staff_type_id ';
        $sql.= 'left join companies company on company.id = hist.company_id ';
        $sql.= 'inner join (select staff_id, max(start_month) max_month from staff_histories where start_month <= ? group by staff_id) max_tbl on max_tbl.staff_id = hist.staff_id and max_tbl.max_month = hist.start_month ';
        $sql.= 'where hist.disabled_flg = 0 ';
        $sql.= 'order by hist.staff_id;';

        $params = array($startMonth);
        return $this->query($sql, $params);
    }

    function findAll($param) {
        $staff = $this->find('all', $param);
        return $staff;
    }

    function getJobTypeId($staffId, $month = null) {

        $table = '(select staff_id, max(start_month) max_start_month from staff_histories where staff_id = '. $staffId .' and disabled_flg = 0)';

        if ($month) {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories where staff_id = '. $staffId .' and disabled_flg = 0 and start_month <= ' . $month .')';
        }

        $staffHistory = $this->find('first', array(
            'fields' => array(
                'StaffHistory.id', 'StaffHistory.staff_id', 'StaffHistory.job_type_id'
            ),
            'conditions' => array(
                'StaffHistory.staff_id' => $staffId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => $table,
                    'alias' => 'sub_tbl',
                    'conditions' => array(
                        'StaffHistory.start_month = sub_tbl.max_start_month'
                    )
                )
            )
        ));

        return $staffHistory['StaffHistory']['job_type_id'];
    }

    function getStaffTypeId($staffId, $month = null) {

        $table = '(select staff_id, max(start_month) max_start_month from staff_histories where staff_id = '. $staffId .' and disabled_flg = 0)';

        if ($month) {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories where staff_id = '. $staffId .' and disabled_flg = 0 and start_month <= ' . $month .')';
        }

        $staffHistory = $this->find('first', array(
            'fields' => array(
                'StaffHistory.id', 'StaffHistory.staff_id', 'StaffHistory.staff_type_id'
            ),
            'conditions' => array(
                'StaffHistory.staff_id' => $staffId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => $table,
                    'alias' => 'sub_tbl',
                    'conditions' => array(
                        'StaffHistory.start_month = sub_tbl.max_start_month'
                    )
                )
            )
        ));

        if (!$staffHistory) {
            return null;
        }

        return $staffHistory['StaffHistory']['staff_type_id'];
    }

    function getLatestStaff($startMonth = 0) {

        if ($startMonth) {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <=' . $startMonth. ' and disabled_flg = 0 group by staff_id)';
        } else {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories where disabled_flg = 0 group by staff_id)';
        }

        $staffs = $this->find('all', array(
            'fields' => array(
                'StaffHistory.*'
            ),
            'joins' => array(
                    array(
                    'type' => 'INNER',
                    'table' => $table,
                    'alias' => 'sub_tbl',
                    'conditions' => array(
                        'StaffHistory.staff_id = sub_tbl.staff_id',
                        'StaffHistory.start_month = sub_tbl.max_start_month'
                    )
                )
            )
        ));

        $data = array();
        foreach ($staffs as $idx => $staff) {

            $data[] = array(
                'staffId' => $staff['StaffHistory']['id'],
                'staffTypeId' => $staff['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staff['StaffHistory']['job_type_id'],
            );
        }
        return $data;
    }

    function getLatestStaffByDivisionId($divisionId = 0, $startMonth = 0) {

        if ($startMonth) {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories StaffHistory
                        inner join sections Section on Section.id = StaffHistory.section_id
                        inner join areas Area on Area.id = Section.area_id
                        inner join divisions Division on Division.id = Area.division_id
                        where Division.id = '. $divisionId.' and start_month <=' . $startMonth. ' and StaffHistory.disabled_flg = 0 group by staff_id)';
        } else {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories StaffHistory
                        inner join sections Section on Section.id = StaffHistory.section_id
                        inner join areas Area on Area.id = Section.area_id
                        inner join divisions Division on Division.id = Area.division_id
                        where Division.id = '. $divisionId.' and StaffHistory.disabled_flg = 0 group by staff_id)';
        }

        $staffs = $this->find('all', array(
            'fields' => array(
                'StaffHistory.*'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => $table,
                    'alias' => 'sub_tbl',
                    'conditions' => array(
                        'StaffHistory.staff_id = sub_tbl.staff_id',
                        'StaffHistory.start_month = sub_tbl.max_start_month'
                    )
                )
            )
        ));

        $data = array();
        foreach ($staffs as $idx => $staff) {

            $data[] = array(
                'staffId' => $staff['StaffHistory']['id'],
                'staffTypeId' => $staff['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staff['StaffHistory']['job_type_id'],
            );
        }
        return $data;
    }

    function getLatestStaffByAreaId($areaId = 0, $startMonth = 0) {

        if ($startMonth) {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories StaffHistory
                        inner join sections Section on Section.id = StaffHistory.section_id
                        inner join areas Area on Area.id = Section.area_id
                        where Area.id = '. $areaId.' and start_month <=' . $startMonth. ' and StaffHistory.disabled_flg = 0  group by staff_id)';
        } else {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories StaffHistory
                        inner join sections Section on Section.id = StaffHistory.section_id
                        inner join areas Area on Area.id = Section.area_id
                        where Area.id = '. $areaId.' and StaffHistory.disabled_flg = 0 group by staff_id)';
        }

        $staffs = $this->find('all', array(
            'fields' => array(
                'StaffHistory.*'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => $table,
                    'alias' => 'sub_tbl',
                    'conditions' => array(
                        'StaffHistory.staff_id = sub_tbl.staff_id',
                        'StaffHistory.start_month = sub_tbl.max_start_month'
                    )
                )
            )
        ));

        $data = array();
        foreach ($staffs as $idx => $staff) {

            $data[] = array(
                'staffId' => $staff['StaffHistory']['id'],
                'staffTypeId' => $staff['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staff['StaffHistory']['job_type_id'],
            );
        }
        return $data;
    }

    function getLatestStaffBySectionId($sectionId = 0, $startMonth = 0) {

        if ($startMonth) {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories
                        where section_id = '. $sectionId.' and start_month <=' . $startMonth. '  and disabled_flg = 0 group by staff_id)';
        } else {
            $table = '(select staff_id, max(start_month) max_start_month from staff_histories
                        where section_id = '. $sectionId.'  and disabled_flg = 0 group by staff_id)';
        }

        $staffs = $this->find('all', array(
            'fields' => array(
                'StaffHistory.*'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => $table,
                    'alias' => 'sub_tbl',
                    'conditions' => array(
                        'StaffHistory.staff_id = sub_tbl.staff_id',
                        'StaffHistory.start_month = sub_tbl.max_start_month'
                    )
                )
            )
        ));

        $data = array();
        foreach ($staffs as $idx => $staff) {

            $data[] = array(
                'staffId' => $staff['StaffHistory']['id'],
                'staffTypeId' => $staff['StaffHistory']['staff_type_id'],
                'jobTypeId' => $staff['StaffHistory']['job_type_id'],
            );
        }
        return $data;
    }

    function getStaffCount($startMonth, $sectionId) {
        $staff= $this->find('first', array(
            'fields' => array(
                'count(*) count'
            ),
            'conditions' => array(
                'section_id' => $sectionId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                    'alias' => 'sub_StaffHistory',
                    'conditions' => array(
                        'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                        'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                    )
                )
            ),
            'group' => array(
                'section_id'
            )
        ));

        if (!$staff) return 0;

        return $staff[0]['count'];
    }

    function getStaff($staffId, $startMonth) {

        $staff = $this->find('first', array(
            'fields' => array(
                'Staff.id', 'Division.id', 'Area.id', 'Section.id'
            ),
            'conditions' => array(
                'StaffHistory.staff_id' => $staffId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and staff_id = '. $staffId .' and disabled_flg = 0 group by staff_id)',
                    'alias' => 'sub_StaffHistory',
                    'conditions' => array(
                        'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                        'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array(
                        'Section.area_id = Area.id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'divisions',
                    'alias' => 'Division',
                    'conditions' => array(
                        'Area.division_id = Division.id'
                    )
                )
            ),
        ));

        if(!$staff) return false;

        $data = array(
            'staff_id' => $staff['Staff']['id'],
            'division_id' => $staff['Division']['id'],
            'area_id' => $staff['Area']['id'],
            'section_id' => $staff['Section']['id']
        );
        return $data;
    }

    function getProfile($staffId, $startMonth) {

        $staffHistory = $this->find('first', array(
            'fields' => array(
                'StaffHistory.name',
                'Section.name',
                'Area.name',
                'Division.name'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and staff_id = '. $staffId .' and disabled_flg = 0 group by staff_id)',
                    'alias' => 'sub_StaffHistory',
                    'conditions' => array(
                        'sub_StaffHistory.staff_id = StaffHistory.staff_id',
                        'sub_StaffHistory.max_start_month = StaffHistory.start_month'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array(
                        'Section.area_id = Area.id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'divisions',
                    'alias' => 'Division',
                    'conditions' => array(
                        'Area.division_id = Division.id'
                    )
                )
            )
        ));

        if (!$staffHistory) {
            return array(
                'staffName' => '',
                'affiliation' => '',
                'divisionName' => '',
                'areaName' => '',
                'sectionName' => ''
            );
        }

        $data = array(
            'staffName' => $staffHistory['StaffHistory']['name'],
            'affiliation' => $staffHistory['Division']['name'].":".$staffHistory['Area']['name'].":".$staffHistory['Section']['name'],
            'divisionName' => $staffHistory['Division']['name'],
            'areaName' => $staffHistory['Area']['name'],
            'sectionName' => $staffHistory['Section']['name']
        );

        return $data;
    }

}
