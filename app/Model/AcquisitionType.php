<?php
App::uses('AppModel', 'Model');
App::import('Model', 'ProductCategory');

class AcquisitionType extends AppModel {

    function getList($yyyymm){
        $this->ProductCategory = new ProductCategory;
        $conditions = array(
            'conditions' => array("AcquisitionPointHistory.start_month = (SELECT MAX(aph.start_month) FROM acquisition_point_histories AS aph WHERE aph.acquisition_type_id = AcquisitionPointHistory.acquisition_type_id AND aph.start_month <= $yyyymm) OR AcquisitionPointHistory.start_month IS NULL"),
            'fields' => array(
                'ProductCategory.id', 'ProductCategory.name', 'ProductCategory.short_name',
                'AcquisitionType.id', 'AcquisitionType.acquisition_type',
                'AcquisitionPointHistory.point', 'AcquisitionPointHistory.start_month'
            ),
            'order' => array('ProductCategory.name ASC', 'AcquisitionType.acquisition_type ASC'),
            'joins' => array(
            array(
                'type' => 'LEFT OUTER',
                'table' => 'acquisition_types',
                'alias' => 'AcquisitionType',
                'conditions' => array('AcquisitionType.product_category_id = ProductCategory.id'),
            ),
                array(
                    'type' => 'LEFT',
                    'table' => 'acquisition_point_histories',
                    'alias' => 'AcquisitionPointHistory',
                    'conditions' => array('AcquisitionType.id = AcquisitionPointHistory.acquisition_type_id'),
                ),
            ),
        );
        $rows = $this->ProductCategory->find('all', $conditions);

        $acquisitionTypes = array();
        $category_name = '';
        $before_category_name = '';
        foreach($rows as $row){
            $category = $row['ProductCategory'];
            $acquisitionType = $row['AcquisitionType'];
            $acquisitionPointHistory = $row['AcquisitionPointHistory'];
            $category_name = ($before_category_name != '' && $before_category_name == $category['name']) ? '' : $category['name'];
            $acquisitionTypes[] = array(
                'product_category_id' => $category['id'],
                'product_category_name' => $category_name,
                'acquisition_type_id' => $acquisitionType['id'] ? $acquisitionType['id'] : "----",
                'acquisition_type' => $acquisitionType['acquisition_type'] ? $acquisitionType['acquisition_type'] : "登録されていません",
                'point' => $acquisitionPointHistory['point'],
                'start_month' => $acquisitionPointHistory['start_month']
            );

            $before_category_name = $category['name'];
        }

        return $acquisitionTypes;
    }
}
