<?php
App::uses('AppModel', 'Model');
App::uses('IncentiveRule', 'Model');

class Incentive extends AppModel {

//    var $hasMany = array('IncentiveRule');

    function getList($yyyymm = null){
        if(!$yyyymm) $yyyymm = Util::GetThisMonth();
        $conditions = array(
            'conditions' => array("Incentive.start_month = (SELECT MAX(start_month) FROM incentives AS i WHERE i.id = Incentive.id AND start_month <= $yyyymm)")
        );
        $incentives = $this->find('all', $conditions);
        $result = array();
        foreach($incentives as $incentive){
            if($incentive["Incentive"]["disabled_flg"]) continue;
            $result[] = array(
                "id" => $incentive["Incentive"]["id"],
                "incentive_name" => $incentive["Incentive"]["incentive_name"],
                "aggregate_period" => $incentive["Incentive"]["aggregate_period"],
                "incentive_term" => $incentive["Incentive"]["incentive_term"],
                "ranking_range" => $incentive["Incentive"]["ranking_range"],
                "target_type" => $incentive["Incentive"]["target_type"],
                "hourly_plus" => $incentive["Incentive"]["hourly_plus"]
            );
        }
        return $result;
    }

    function getById($id){
        $incentive = $this->findById($id);
        $result = array(
            "id" => $incentive["Incentive"]["id"],
            "incentive_name" => $incentive["Incentive"]["incentive_name"],
            "aggregate_period" => $incentive["Incentive"]["aggregate_period"],
            "incentive_term" => $incentive["Incentive"]["incentive_term"],
            "ranking_range" => $incentive["Incentive"]["ranking_range"],
            "target_type" => $incentive["Incentive"]["target_type"],
            "disabled_flg" => $incentive["Incentive"]["disabled_flg"],
            "hourly_plus" => $incentive["Incentive"]["hourly_plus"]
        );
        return $result;
    }

    function getAll() {

        $incentives = $this->find('all', array(
            'conditions' => array(
                'disabled_flg' => 0
            )
        ));
        $this->IncentiveRule = new IncentiveRule();
        $data = array();
        foreach ($incentives as $incentive) {

            $incentiveId = $incentive['Incentive']['id'];

            $incentiveRules = $this->IncentiveRule->find('all', array(
                'conditions' => array(
                    'IncentiveRule.incentive_id' => $incentiveId
                ),
                'joins' => array(
                    array(
                        'type' => 'INNER',
                        'table' => '(select incentive_id, max(start_month) max_start_month from incentive_rules group by incentive_id)',
                        'alias' => 'sub_tbl',
                        'conditions' => array(
                            'IncentiveRule.incentive_id = sub_tbl.incentive_id',
                            'IncentiveRule.start_month = sub_tbl.max_start_month'
                        )
                    )
                )
            ));
            $rules = array();
            foreach ($incentiveRules as $incentiveRule) {
                $rules[] = $incentiveRule['IncentiveRule'];

            }
            $data[] = array(
                'Incentive' => $incentive['Incentive'],
                'IncentiveRule' => $rules
            );
        }

        return $data;

//        $incentives = $this->find('all', array(
//            'fields' => array(
//                'Incentive.*',
//                'IncentiveRule.*'
//            ),
//            'conditions' => array(
//                'disabled_flg' => 0
//            ),
//            'joins' => array(
//                array(
//                    'type' => 'INNER',
//                    'table' => 'incentive_rules',
//                    'alias' => 'IncentiveRule',
//                    'conditions' => array(
//                        'Incentive.id = IncentiveRule.incentive_id'
//                    )
//                ),
//                array(
//                    'type' => 'INNER',
//                    'table' => '(select incentive_id, max(start_month) max_start_month from incentive_rules group by incentive_id)',
//                    'alias' => 'sub_tbl',
//                    'conditions' => array(
//                        'IncentiveRule.incentive_id = sub_tbl.incentive_id',
//                        'IncentiveRule.start_month = sub_tbl.max_start_month'
//                    )
//                )
//            )
//        ));
//
//        if (!$incentives) return array();
//
//        $data = array();
//        $index = array();
//        $incentiveRules = array();
//        foreach ($incentives as $idx => $incentive) {
//            $incentiveRules[] = $incentive['IncentiveRule'];
//        }
//        $index['Incentive'] = $incentives[0]['Incentive'];
//        $index['IncentiveRule'] = $incentiveRules;
//
//        return $data;
    }

    function getIncentiveNames($startMonth) {
        $incentives = $this->find('all', array(
            'conditions' => array(
                'start_month <=' => $startMonth,
                'disabled_flg' => 0
            ),
            'order' => array(
                'id'
            )
        ));
        $data = array();
        foreach ($incentives as $incentive) {
            $data[] = $incentive['Incentive']['incentive_name'];
        }
        return $data;
    }
}
