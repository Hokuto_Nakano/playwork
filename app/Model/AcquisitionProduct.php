<?php
App::uses('AppModel', 'Model');

class AcquisitionProduct extends AppModel {

    function getGroupBySalesAmount($yyyymm, $division_id=0, $area_id=0, $section_id=0){
        $condition = "AcquisitionProduct.acquisition_month = ". $yyyymm;
        if($section_id > 0){
            $condition .= " AND AcquisitionProduct.division_id = " .$division_id. " AND AcquisitionProduct.area_id = " .$area_id. " AND AcquisitionProduct.section_id = " .$section_id;
        }
        else if($area_id > 0){
            $condition .= " AND AcquisitionProduct.division_id = " .$division_id. " AND AcquisitionProduct.area_id = " .$area_id;
        }
        else if($division_id > 0){
            $condition .= " AND AcquisitionProduct.division_id = " .$division_id;
        }
        $condition .= " AND ";
        $conditions = array(
            'conditions' => array($condition."(Product.start_month = (SELECT MAX(start_month) FROM products AS p WHERE p.id = Product.id AND start_month <= $yyyymm))"),
            'fields' => array(
                'sum(Product.price) as sumPrice', "concat(ProductCategory.name, '：', ProductFamily.name, '：', Product.product_detail_name) as product_name"
            ),
            'joins' => array(
                array(
                    'type' => 'LEFT',
                    'table' => 'products',
                    'alias' => 'Product',
                    'conditions' => array('Product.id = AcquisitionProduct.product_id'),
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'product_details',
                    'alias' => 'ProductDetail',
                    'conditions' => array('ProductDetail.id = Product.product_detail_id'),
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'product_families',
                    'alias' => 'ProductFamily',
                    'conditions' => array('ProductFamily.id = ProductDetail.product_family_id'),
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'product_categories',
                    'alias' => 'ProductCategory',
                    'conditions' => array('ProductCategory.id = ProductFamily.product_category_id'),
                ),
            ),
            'group' => array('Product.id')
        );
        return $this->find('all', $conditions);
    }

    function getTotalSaleAmount($yyyymm, $division_id=0, $area_id=0, $section_id=0){
        $condition = "AcquisitionProduct.acquisition_month = ". $yyyymm;
        if($section_id > 0){
            $condition .= " AND AcquisitionProduct.division_id = " .$division_id. " AND AcquisitionProduct.area_id = " .$area_id. " AND AcquisitionProduct.section_id = " .$section_id;
        }
        else if($area_id > 0){
            $condition .= " AND AcquisitionProduct.division_id = " .$division_id. " AND AcquisitionProduct.area_id = " .$area_id;
        }
        else if($division_id > 0){
            $condition .= " AND AcquisitionProduct.division_id = " .$division_id;
        }
        $condition .= " AND ";
        $conditions = array(
            'conditions' => array($condition."(Product.start_month = (SELECT MAX(start_month) FROM products AS p WHERE p.id = Product.id AND start_month <= $yyyymm))"),
            'fields' => array(
                'sum(Product.price) as totalPrice'
            ),
            'joins' => array(
                array(
                    'type' => 'LEFT',
                    'table' => 'products',
                    'alias' => 'Product',
                    'conditions' => array('Product.id = AcquisitionProduct.product_id'),
                )
            )
        );
        return $this->find('first', $conditions);
    }
}
