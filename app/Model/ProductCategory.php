<?php
App::uses('AppModel', 'Model');

class ProductCategory extends AppModel {
    var $hasMany = array('ProductFamily');


    function getPointHistory($startMonth) {

        $productCategories = $this->find('all', array(
            'fields' => array(
                'ProductCategory.id',
                'ProductCategory.name',
                'AcquisitionType.id',
                'AcquisitionType.acquisition_type',
                'AcquisitionPointHistory.point'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'acquisition_types',
                    'alias' => 'AcquisitionType',
                    'conditions' => array(
                        'ProductCategory.id = AcquisitionType.product_category_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'acquisition_point_histories',
                    'alias' => 'AcquisitionPointHistory',
                    'conditions' => array(
                        'AcquisitionType.id = AcquisitionPointHistory.acquisition_type_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => '(select acquisition_type_id, MAX(start_month) max_start_month from acquisition_point_histories where start_month <='. $startMonth .' group by acquisition_type_id)',
                    'alias' => 'subTbl',
                    'conditions' => array(
                        'AcquisitionPointHistory.start_month = subTbl.max_start_month',
                        'AcquisitionPointHistory.acquisition_type_id = subTbl.acquisition_type_id'
                    )
                )
            ),
            'order' => array(
                'ProductCategory.id'
            )
        ));
        $data = array();
        $acquisitionTypeId = array();
        $acquisitionType = array();
        $point = array();
        foreach ($productCategories as $idx => $productCategory) {

            $acquisitionTypeId[] = $productCategory['AcquisitionType']['id'];
            $acquisitionType[] = $productCategory['AcquisitionType']['acquisition_type'];
            $point[] = $productCategory['AcquisitionPointHistory']['point'];

            if ($idx == count($productCategories) -1 || $productCategory['ProductCategory']['name'] != $productCategories[$idx+1]['ProductCategory']['name']) {
                $data[] = array(
                    'id' => $productCategory['ProductCategory']['id'],
                    'name' => $productCategory['ProductCategory']['name'],
                    'acquisitionTypeId' =>  $acquisitionTypeId,
                    'acquisitionType' => $acquisitionType,
                    'point' => $point
                );
                $acquisitionTypeId = array();
                $acquisitionType = array();
                $point = array();
            }
        }
        return $data;
    }
}
