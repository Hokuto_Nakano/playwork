<?php
App::uses('AppModel', 'Model');

class Staff extends AppModel {
//    var $belongsTo = array('StaffHistory');

    function auth($id, $pass){
        $conditions = array("Staff.login_id" => $id, "Staff.password" => $pass, "Staff.disabled_flg" => 0);
        $data = $this->find('first', array('conditions' => $conditions));
        if(!$data) return false;
        return $data['Staff']['id'];
    }

    function findAll($param) {
        $staff = $this->find('all', $param);
        return $staff;
    }

    function getAuthority($loginId) {
        $data = $this->findByLoginId($loginId);
        return $data['Staff']['authority'];
    }

    function getRankingApStaffs($startMonth, $divisionId) {

        $conditions = array(

            'fields' => array(
                'Staff.*',
                'StaffHistory.*',
                'JobType.type',
                'Division.name'
            ),
            'order' => 'Division.id, Staff.id',
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'staff_histories',
                    'alias' => 'StaffHistory',
                    'conditions' => array(
                        'Staff.id = StaffHistory.staff_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                    'alias' => 'max_month_StaffHistory',
                    'conditions' => array(
                        'max_month_StaffHistory.staff_id = StaffHistory.staff_id',
                        'max_month_StaffHistory.max_start_month = StaffHistory.start_month'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'sections',
                    'alias' => 'Section',
                    'conditions' => array(
                        'Section.id = StaffHistory.section_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array(
                        'Area.id = Section.area_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'divisions',
                    'alias' => 'Division',
                    'conditions' => array(
                        'Division.id = Area.division_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'job_types',
                    'alias' => 'JobType',
                    'conditions' => array(
                        'JobType.id = StaffHistory.job_type_id'
                    )
                )
            ));

        if ($divisionId) {
            $conditions['conditions'] = array(
                'Division.id' => $divisionId
            );
        }

        $staffs = $this->find('all', $conditions);

        $data = array();
        foreach ($staffs as $idx => $staff) {
            $data[] = array(
                'staffId' => $staff['Staff']['id'],
                'staffNo' => $staff['Staff']['staff_no'],
                'staffName' => $staff['StaffHistory']['name'],
                'jobType' => $staff['JobType']['type'],
                'division' => $staff['Division']['name']
            );
        }
        return $data;
    }

    function getRankingApOverhangsStaffs($startMonth, $divisionId) {

        $conditions = array(

            'fields' => array(
                'Staff.*',
                'StaffHistory.*',
                'StaffPointGoal.*',
                'JobType.type',
                'Division.name',
                'Attendance.total_work_hours',
                'ifnull(StaffCountUp.cnt,0) count'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'staff_histories',
                    'alias' => 'StaffHistory',
                    'conditions' => array(
                        'Staff.id = StaffHistory.staff_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                    'alias' => 'max_month_StaffHistory',
                    'conditions' => array(
                        'max_month_StaffHistory.staff_id = StaffHistory.staff_id',
                        'max_month_StaffHistory.max_start_month = StaffHistory.start_month'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'sections',
                    'alias' => 'Section',
                    'conditions' => array(
                        'Section.id = StaffHistory.section_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'areas',
                    'alias' => 'Area',
                    'conditions' => array(
                        'Area.id = Section.area_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'divisions',
                    'alias' => 'Division',
                    'conditions' => array(
                        'Division.id = Area.division_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'job_types',
                    'alias' => 'JobType',
                    'conditions' => array(
                        'JobType.id = StaffHistory.job_type_id'
                    )
                ),
                array(
                    'type' => 'LEFT',
                    'table' => '(select staff_id, sec_to_time(sum(time_to_sec(work_hours))) total_work_hours from attendances where attendance_month = '. $startMonth .' group by staff_id)',
                    'alias' => 'Attendance',
                    'conditions' => array(
                        'Attendance.staff_id = Staff.id'
                    )
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'staff_point_goals',
                    'alias' => 'StaffPointGoal',
                    'conditions' => array(
                        'StaffPointGoal.staff_id = StaffHistory.id',
                        'StaffPointGoal.month' => $startMonth
                    )
                ),
                array(
                    'type' => 'LEFT',
                    'table' => "(select staff_id, count(*) cnt from staff_count_ups where date_format(recorded_date, '%Y%m') = ". $startMonth ." group by staff_id, date_format(recorded_date, '%Y%m'), contract_status having contract_status = '解約')",
                    'alias' => 'StaffCountUp',
                    'conditions' => array(
                        'StaffCountUp.staff_id = StaffHistory.staff_id'
                    )
                )
            ));

        if ($divisionId) {
            $conditions['conditions'] = array(
                'Division.id' => $divisionId
            );
        }

        $staffs = $this->find('all', $conditions);

        $data = array();
        foreach ($staffs as $idx => $staff) {
            $totalWorkHours = $staff['Attendance']['total_work_hours'] ? $staff['Attendance']['total_work_hours'] : '00:00:00';
            $array = explode(":", $totalWorkHours);
            $hour = $array[0];
            $minute = round($array[1]/60,2);
            $totalWorkHours = $hour + $minute;

            $data[] = array(
                'staffId' => $staff['Staff']['id'],
                'staffNo' => $staff['Staff']['staff_no'],
                'staffName' => $staff['StaffHistory']['name'],
                'jobType' => $staff['JobType']['type'],
                'totalWorkHours' => $totalWorkHours,
                'kaiyakuCount' => $staff[0]['count'],
                'division' => $staff['Division']['name'],
                'goalPoint' => $staff['StaffPointGoal']['point'] ? $staff['StaffPointGoal']['point'] : 0
            );
        }
        return $data;
    }

    function getSupportStaffs($startMonth) {

        $conditions = array(

            'fields' => array(
                'Staff.*',
                'StaffHistory.*',
                'JobType.type',
                'Attendance.total_work_hours'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'staff_histories',
                    'alias' => 'StaffHistory',
                    'conditions' => array(
                        'Staff.id = StaffHistory.staff_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where start_month <= '. $startMonth .' and disabled_flg = 0 group by staff_id)',
                    'alias' => 'max_month_StaffHistory',
                    'conditions' => array(
                        'max_month_StaffHistory.staff_id = StaffHistory.staff_id',
                        'max_month_StaffHistory.max_start_month = StaffHistory.start_month'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'job_types',
                    'alias' => 'JobType',
                    'conditions' => array(
                        'JobType.id = StaffHistory.job_type_id'
                    )
                ),
                array(
                    'type' => 'LEFT',
                    'table' => '(select staff_id, sec_to_time(sum(time_to_sec(work_hours))) total_work_hours from attendances where attendance_month = '. $startMonth .' group by staff_id)',
                    'alias' => 'Attendance',
                    'conditions' => array(
                        'Attendance.staff_id = Staff.id'
                    )
                )
            ));

        $staffs = $this->find('all', $conditions);

        $data = array();
        foreach ($staffs as $idx => $staff) {
            $totalWorkHours = $staff['Attendance']['total_work_hours'] ? $staff['Attendance']['total_work_hours'] : '00:00:00';
            $array = explode(":", $totalWorkHours);
            $hour = $array[0];
            $minute = round($array[1]/60,2);
            $totalWorkHours = $hour + $minute;

            $data[] = array(
                'staffId' => $staff['Staff']['id'],
                'staffName' => $staff['StaffHistory']['name'],
                'jobType' => $staff['JobType']['type'],
                'totalWorkHours' => $totalWorkHours
            );
        }
        return $data;
    }

}
