<?php
App::uses('AppModel', 'Model');

class PlValue extends AppModel {

    function getList($yyyymm, $division_id=0, $area_id=0, $section_id=0){
        $condition = "PlValue.month = ". $yyyymm;
        if($section_id > 0){
            $condition .= " AND PlValue.section_id = " .$section_id;
        }
        else if($area_id > 0){
            $condition .= " AND PlValue.area_id = " .$area_id. " AND PlValue.section_id = 0";
        }
        else if($division_id > 0){
            $condition .= " AND PlValue.division_id = " .$division_id. " AND PlValue.area_id = 0 AND PlValue.section_id = 0";
        }
        $condition .= " AND ";
        $conditions = array(
            'conditions' => array($condition."(PlItem.start_month = (SELECT MAX(pli.start_month) FROM pl_items AS pli WHERE pli.id = PlItem.id AND pli.start_month <= $yyyymm))"),
            'order' => array('PlCategory.calc_order ASC', 'PlCategory.show_order ASC', 'PlItem.calc ASC', 'PlItem.id ASC'),
            'fields' => array(
                'PlValue.id', 'PlValue.value', 'PlCategory.id', 'PlCategory.category_name', 'PlItem.id', 'PlItem.item_name', 'PlItem.calc', 'PlItem.start_month'
            ),
            'joins' => array(
                array(
                    'type' => 'LEFT',
                    'table' => 'pl_items',
                    'alias' => 'PlItem',
                    'conditions' => array('PlItem.id = PlValue.item_id'),
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'pl_categories',
                    'alias' => 'PlCategory',
                    'conditions' => array('PlCategory.id = PlItem.category_id'),
                ),
            ),
        );
        $plvalues = $this->find('all', $conditions);
        return $plvalues;
    }

    function getById($id){
        $conditions = array(
            'conditions' => array("PlValue.id = ". $id),
            'fields' => array(
                'PlCategory.id', 'PlCategory.category_name', 'PlItem.id', 'PlItem.item_name', 'PlItem.calc', 'PlItem.start_month', 'PlValue.*'
            ),
            'joins' => array(
                array(
                    'type' => 'LEFT',
                    'table' => 'pl_items',
                    'alias' => 'PlItem',
                    'conditions' => array('PlItem.id = PlValue.item_id'),
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'pl_categories',
                    'alias' => 'PlCategory',
                    'conditions' => array('PlCategory.id = PlItem.category_id'),
                ),
            ),
        );
        $plvalue = $this->find('first', $conditions);
        return $plvalue;
    }

    function getTotalValues($yyyymm, $division_id=0, $area_id=0, $section_id=0){
        $condition = "PlValue.month = ". $yyyymm;
        if($section_id > 0){
            $condition .= " AND PlValue.section_id = " .$section_id;
        }
        else if($area_id > 0){
            $condition .= " AND PlValue.area_id = " .$area_id. " AND PlValue.section_id = 0";
        }
        else if($division_id > 0){
            $condition .= " AND PlValue.division_id = " .$division_id. " AND PlValue.area_id = 0 AND PlValue.section_id = 0";
        }
        $conditions = array(
            'conditions' => array($condition),
            'fields' => array(
                'sum(PlValue.value) as sumValue', 'PlItem.category_id'
            ),
            'joins' => array(
                array(
                    'type' => 'LEFT',
                    'table' => 'pl_items',
                    'alias' => 'PlItem',
                    'conditions' => array('PlItem.id = PlValue.item_id'),
                )
            ),
            'group' => array('PlItem.category_id')
        );
        $result = array();
        $plvalues = $this->find('all', $conditions);
        foreach($plvalues as $plvalue){
            $result[$plvalue["PlItem"]["category_id"]] = $plvalue[0]["sumValue"];
        }
        return $result;
    }
}
