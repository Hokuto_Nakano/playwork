<?php
App::uses('AppModel', 'Model');
App::import('Model', 'Area');

class Section extends AppModel {
    //var $hasOne = array('Staff');
    var $belongsTo = array('Area');
    // var $belongsTo = array(
    //     'Area' => array(
    //         'className' => 'Area',
    //         'foreignKey' => 'area_id'
    //     ),
    //     'Division' => array(
    //         'className' => 'Division',
    //         'conditions' => 'Area.division_id = Division.id',
    //         'foreignKey' => ''
    //     )
    // );

    function getFirstSections() {
        $this->Area = new Area();

        $area = $this->Area->find('first', array(
            'fields' => array(
                'id'
            ),
            'order' => 'id'
        ));

        $sections = $this->find('all', array(
            'conditions' => array(
                'area_id' => $area['Area']['id']
            ),
            'fields' => array(
                'id', 'name'
            ),
            'order' => 'id'
        ));

        $ret = array();
        foreach ($sections as $index => $section) {
            $ret[] = array(
                'id' => $section['Section']['id'],
                'name' => $section['Section']['name']
            );
        }
        return $ret;

    }

    function getAllSimpleSections() {
        $sections = $this->find('all', array(
            'fields' => array(
                'Section.id', 'Section.name'
            )
        ));
        $ret = array();
        foreach ($sections as $index => $section) {
            $ret[] = array(
                'id' => $section['Section']['id'],
                'name' => $section['Section']['name']
            );
        }
        return $ret;
    }

    function getAllSimpleSectionsByAreaId($areaId) {
        $sections = $this->find('all', array(
            'conditions' => array(
                'area_id' => $areaId
            ),
            'fields' => array(
                'Section.id', 'Section.name'
            )
        ));
        $ret = array();
        foreach ($sections as $index => $section) {
            $ret[] = array(
                'id' => $section['Section']['id'],
                'name' => $section['Section']['name']
            );
        }
        return $ret;
    }

    function getAllSimpleSectionsById($sectionId) {
        $sections = $this->find('all', array(
            'conditions' => array(
                'Section.id' => $sectionId
            ),
            'fields' => array(
                'Section.id', 'Section.name'
            )
        ));
        $ret = array();
        foreach ($sections as $index => $section) {
            $ret[] = array(
                'id' => $section['Section']['id'],
                'name' => $section['Section']['name']
            );
        }
        return $ret;
    }
}
