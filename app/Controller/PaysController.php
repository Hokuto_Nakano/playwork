<?php
App::uses('AuthController', 'Controller');

class PaysController extends AuthController {

    public $uses = array('StaffHistory', 'StaffPay', 'StaffIncentive', 'Incentive', 'IncentiveRule', 'StaffCountUp', 'ProductCategory');

    public function index($yyyymm = null){
        $this->set("header", "給与");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }

        if($yyyymm == 'download') {
            $this->download();
        } else {
            $this->set("year", Util::GetYYYY($yyyymm));
            $this->set("month", Util::GetMM($yyyymm));

            $this->getData($yyyymm);
        }
    }

    function api($id) {

        if ($this->request->is('post') && $this->request->is('ajax')) {

            if ($id == 'import') {

                $month = $this->request->data('month');

                // アップロードファイルを取得
                $csvFile = $this->request->params['form']['csvFile'];

                // アップロードファイルエラーチェック
                $this->checkUploadError($csvFile);

                if ($csvFile['size'] <= 0) {
                    throw new InternalErrorException();
                }

                // CSVデータを取得
                $tmpName = $csvFile['tmp_name'];
                $this->loadModel('CsvParser');
                $csvData = $this->CsvParser->getCsvData($tmpName);

                $errorMessage = '';
                foreach ($csvData as $rowIndex => $data) {
                    $staffNo = $data[0];
                    $name = $data[1];
                    $pay = $data[2];
                    $incentive = $data[3];

                    $staffHistory = $this->StaffHistory->find('first', array(
                        'fields' => array(
                            'StaffHistory.id',
                        ),
                        'joins' => array(
                            array(
                                'type' => 'INNER',
                                'table' => '(select staff_id, MAX(start_month) max_start_month from staff_histories where name = \''. $name .'\' and disabled_flg = 0 and start_month <= '. $month .')',
                                'alias' => 'sub_staff_history',
                                'conditions' => array(
                                    'sub_staff_history.max_start_month = StaffHistory.start_month',
                                    'sub_staff_history.staff_id = StaffHistory.staff_id'
                                )
                            ),
                            array(
                                'type' => 'INNER',
                                'table' => '(select id from staffs where staff_no = '. $staffNo .')',
                                'alias' => 'sub_staff',
                                'conditions' => array(
                                    'sub_staff.id = StaffHistory.staff_id'
                                )
                            )
                        )
                    ));

                    if (!$staffHistory) {
                        $errorMessage .= '登録対象のスタッフが見つかりません。[社員番号='. $staffNo .', 社員名='. $name .']<br>';
                        continue;
                    }

                    $staffHistoryId = $staffHistory['StaffHistory']['id'];

                    // 存在チェック
                    $staffPay = $this->StaffPay->find('first', array(
                        'conditions' => array(
                            'month' => $month,
                            'staff_id' => $staffHistoryId
                        )
                    ));

                    $paysData = array(
                        'month' => $month,
                        'staff_id' => $staffHistoryId,
                        'pay' => $pay,
                        'incentive' => $incentive
                    );

                    if ($staffPay) {
                        // 更新
                        $paysData['id'] = $staffPay['StaffPay']['id'];
                    } else {
                        // 登録
                        $this->StaffPay->create();
                    }

                    if (!$this->StaffPay->save($paysData)) {
                        throw new InternalErrorException('登録に失敗しました。');
                    }
                }
            }

            if ($errorMessage) {
                $this->responseFailure($errorMessage);
            }
            $this->responseSuccess();
        }
    }

    function getData($startMonth) {

        // デフォルト値
        $DEFAULT_OFFSET = 0;
        $DEFAULT_LIMIT = NUMBER_OF_DISPLAYED;

        // オフセット、リミット取得
        $offset = null;
        $limit = null;
        if (isset($this->request->query['offset'])) {
            $offset = $this->request->query['offset'];
        } else {
            $offset = $DEFAULT_OFFSET;
        }
        if (isset($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        } else {
            $limit = $DEFAULT_LIMIT;
        }

        // SQL実行
        $staffs = $this->StaffHistory->find('all', array(
            'conditions' => array(
                'StaffHistory.start_month <=' => $startMonth
            ),
            'fields' => array(
                'StaffHistory.id', 'StaffHistory.name', 'StaffPay.incentive', 'StaffPay.pay', 'Attendance.total_work_hours'
            ),
            'joins' => $this->getJoinConditions($startMonth),
            'offset' => $offset,
            'limit' => $limit,
            'group' => array(
                'StaffHistory.id'
            )
        ));

        $count = $this->StaffHistory->find('count', array(
            'conditions' => array(
                'StaffHistory.start_month <=' => $startMonth
            ),
            'fields' => array(
                'StaffHistory.id', 'StaffHistory.name'
            ),
            'joins' => $this->getJoinConditions($startMonth),
            'group' => array(
                'StaffHistory.id'
            )
        ));

        foreach ($staffs as $idx => $staff) {

            $incentives = $this->Incentive->find('all', array(
                'fields' => array(
                    'Incentive.id', 'Incentive.incentive_name', 'StaffIncentive.*'
                ),
                'conditions' => array(
                    'start_month <=' => $startMonth,
                    'disabled_flg' => 0
                ),
                'joins' => array(
                    array(
                        'type' => 'LEFT',
                        'table' => '(select staff_id, incentive_id, sum(incentive) staff_incentive, month from staff_incentives where staff_id = '. $staff['StaffHistory']['id'] .' and (month = 0 or month = '. $startMonth .') group by staff_id, incentive_id, month)',
                        'alias' => 'StaffIncentive',
                        'conditions' => array(
                            'StaffIncentive.incentive_id = Incentive.id'
                        )
                    ),
                ),
                'order' => array(
                    'Incentive.id'
                )
            ));
            $staffs[$idx]['StaffIncentive'] = array();
            foreach ($incentives as $incentive) {
                $staffs[$idx]['StaffIncentive'][] = $incentive['StaffIncentive']['staff_incentive'] ? $incentive['StaffIncentive']['staff_incentive'] : 0;
            }

        }

        $incentiveNames = $this->Incentive->getIncentiveNames($startMonth);

        if ($staffs && $count) {
            $this->set('incentiveNames', $incentiveNames);
            $this->set('staff', $staffs);
            $this->set('count', $count);
            $this->set('offset', $offset);
            $this->set('limit', $limit);
            $this->set('page', floor(($offset + $limit) / $limit));
            $this->set('totalPage', ceil($count / $limit));
        } else {
            $this->set('incentiveNames', $incentiveNames);
            $this->set('count', 0);
            $this->set('offset', $DEFAULT_OFFSET);
            $this->set('limit', $DEFAULT_LIMIT);
            $this->set('page', 0);
            $this->set('totalPage', 0);
        }
    }

    function getJoinConditions($startMonth) {
        $joinCondition = array(
            array(
                'type' => 'INNER',
                'table' => '(select staff_id, MAX(staff_histories.start_month) max_start_month from staff_histories where start_month <='. $startMonth .' and disabled_flg = 0  group by staff_id)',
                'alias' => 'subTbl',
                'conditions' => array(
                    'StaffHistory.start_month = subTbl.max_start_month',
                    'StaffHistory.staff_id = subTbl.staff_id'
                )
            ),
            array(
                'type' => 'LEFT',
                'table' => 'staff_pays',
                'alias' => 'StaffPay',
                'conditions' => array(
                    'StaffPay.staff_id = StaffHistory.id',
                    'StaffPay.month' => $startMonth
                )
            ),
            array(
                'type' => 'LEFT',
                'table' => '(select staff_id, sec_to_time(sum(time_to_sec(work_hours))) total_work_hours from attendances where attendance_month = '. $startMonth .' group by staff_id)',
                'alias' => 'Attendance',
                'conditions' => array(
                    'Attendance.staff_id = StaffHistory.staff_id'
                )
            )
//            array(
//                'type' => 'LEFT',
//                'table' => '(select staff_id, incentive_id, sum(incentive) staff_incentive, month from staff_incentives where month = '. $startMonth .' group by staff_id, incentive_id, month)',
//                'alias' => 'StaffIncentive',
//                'conditions' => array(
//                    'StaffIncentive.staff_id = StaffHistory.id'
//                )
//            ),
//            array(
//                'type' => 'LEFT',
//                'table' => 'incentives',
//                'alias' => 'Incentive',
//                'conditions' => array(
//                    'Incentive.id = StaffIncentive.incentive_id'
//                )
//            ),
        );
        return $joinCondition;
    }

    function download() {

        // レスポンスヘッダ
        $this->response->header('Content-Disposition: attachment; filename=pay.csv');

        // ファイルの情報とファイルの内容を設定
        $this->autoRender = false;
        $this->response->type('csv');
        $this->response->file(WWW_ROOT.'files/pay.csv');
        $this->response->download('pay.csv');

    }

    function checkUploadError($csvFile) {

        $messages = null;
        switch($csvFile['error']) {
            case UPLOAD_ERR_OK:
                //値: 0; この場合のみ、ファイルあり
                break;

            case UPLOAD_ERR_INI_SIZE:
                //値: 1; アップロードされたファイルは、php.ini の upload_max_filesize ディレクティブの値を超えています（post_max_size, upload_max_filesize）
                $messages = 'アップロードされたファイルが大きすぎます。' . ini_get('upload_max_filesize') . '以下のファイルをアップロードしてください。';
                break;

            case UPLOAD_ERR_FORM_SIZE:
                //値: 2; アップロードされたファイルは、HTML フォームで指定された MAX_FILE_SIZE を超えています。
                $messages = 'アップロードされたファイルが大きすぎます。' . ($_POST['MAX_FILE_SIZE'] / 1000) . 'KB以下のファイルをアップロードしてください。';
                break;

            case UPLOAD_ERR_PARTIAL:
                //値: 3; アップロードされたファイルは一部のみしかアップロードされていません。
                $messages = 'アップロードに失敗しています（通信エラー）。もう一度アップロードをお試しください。';
                break;

            case UPLOAD_ERR_NO_FILE:
                //値: 4; ファイルはアップロードされませんでした。（この場合のみ、ファイルがないことを表している）
                //$messages = 'ファイルをアップロードしてください';
                break;

            case UPLOAD_ERR_NO_TMP_DIR:
                //値: 6; テンポラリフォルダがありません。PHP 4.3.10 と PHP 5.0.3 で導入されました。
                $messages = 'アップロードに失敗しています（システムエラー）。もう一度アップロードをお試しください。';
                break;

            default:
                //UPLOAD_ERR_CANT_WRITE 値: 7; ディスクへの書き込みに失敗しました。PHP 5.1.0 で導入されました。
                //UPLOAD_ERR_EXTENSION 値: 8; ファイルのアップロードが拡張モジュールによって停止されました。 PHP 5.2.0 で導入されました。
                //何かおかしい
                $messages = 'アップロードファイルをご確認ください。';
                break;
        }

        if ($messages) {
            $this->log($messages);
            throw new BadRequestException($messages);
        }
    }


}
