<?php
App::uses('AuthController', 'Controller');

class AcquisitionTypesController extends AuthController {

    public $uses = array('AcquisitionType', 'AcquisitionPointHistory', 'ProductCategory');

    public function index($yyyymm = null){
        $this->set("header", "商材獲得区分");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $acquisitionTypes = $this->AcquisitionType->getList($yyyymm);
        $this->set("acquisitionTypes", $acquisitionTypes);
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));
    }

    public function api($id = null){
        if($this->request->is('get')){
        }
        else if($this->request->is('post')){
            if(!$id){ //新規
                if($this->AcquisitionType->findByProduct_category_idAndAcquisition_type($this->request->data['category_id'], $this->request->data['acquisition_type'])){
                    $this->responseFailure('獲得区分はすでに使用されています');
                }

                $data = array(
                    'product_category_id' => $this->request->data['category_id'],
                    'acquisition_type' => $this->request->data['acquisition_type']
                );
                $this->AcquisitionType->create();
                if(!$this->AcquisitionType->save($data)){
                    throw new InternalErrorException();
                }

                $acquisition_type_id = $this->AcquisitionType->getLastInsertID();

                $data = array(
                    'acquisition_type_id' => $acquisition_type_id,
                    'point' => $this->request->data['point'],
                    'start_month' => $this->request->data['start_month']
                );
                $this->AcquisitionPointHistory->create();
                if(!$this->AcquisitionPointHistory->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
            else{
                $acquisitionType = $this->AcquisitionType->findByIdAndAcquisition_type($id, $this->request->data['acquisition_type']);
                if(!$acquisitionType){
                    $data = array(
                        'id' => $id,
                        'product_category_id' => $this->request->data['category_id'],
                        'acquisition_type' => $this->request->data['acquisition_type']
                    );
                    if(!$this->AcquisitionType->save($data)){
                        throw new InternalErrorException();
                    }
                }

                $acquisitionPoint = $this->AcquisitionPointHistory->findByAcquisition_type_idAndStart_month($id, $this->request->data['start_month']);
                if(!$acquisitionPoint){
                    $this->AcquisitionPointHistory->create();
                    $data = array(
                        'acquisition_type_id' => $id,
                        'point' => $this->request->data['point'],
                        'start_month' => $this->request->data['start_month']
                    );
                }
                else{
                    $data = array(
                        'id' => $acquisitionPoint['AcquisitionPointHistory']['id'],
                        'acquisition_type_id' => $id,
                        'point' => $this->request->data['point'],
                        'start_month' => $this->request->data['start_month']
                    );
                }
                if(!$this->AcquisitionPointHistory->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
        }
        else if($this->request->is('delete')){
            try{
                if(!$this->AcquisitionType->delete($id)){
                    $this->responseFailure('削除に失敗しました');
                }
            }
            catch(Exception $e){
                $this->responseFailure('削除に失敗しました');
            }

            $this->responseSuccess();
        }
    }

    public function partial($id = null, $yyyymm = null){
        $acquisitionTypeId = $id;
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        if($acquisitionTypeId == 'new'){
            $this->set('acquisitionType', array(
                "id" => 0,
                "acquisition_type" => "",
                "point" => ""
            ));
            $categories = $this->ProductCategory->find('all', array('order' => array('name ASC')));
            $this->set('categories', $categories);
            if($categories){
                $this->set('category', $categories[0]['ProductCategory']);
            }
            else{
                $this->responseFailure('商材カテゴリが一つもありません');
            }
        }
        else if(is_numeric($acquisitionTypeId)){
            $acquisitionType = $this->AcquisitionType->findById($acquisitionTypeId);
            if(!$acquisitionType) throw new NotFoundException();

            //$acquisitionPoint = $this->AcquisitionPointHistory->findByAcquisitionTypeIdAndStartMonth($acquisitionTypeId, $yyyymm);
            $acquisitionPoint = $this->AcquisitionPointHistory->find("first", array(
                'conditions' => array(
                    'acquisition_type_id' => $acquisitionTypeId,
                    "start_month = (SELECT MAX(aph.start_month) FROM acquisition_point_histories AS aph WHERE aph.acquisition_type_id = AcquisitionPointHistory.acquisition_type_id AND aph.start_month <= $yyyymm)"
                )
            ));
            $this->set('acquisitionPoint', $acquisitionPoint["AcquisitionPointHistory"]);
            $this->set('acquisitionType', array(
                "id" => $acquisitionType["AcquisitionType"]["id"],
                "acquisition_type" => $acquisitionType["AcquisitionType"]["acquisition_type"],
                "point" => $acquisitionPoint["AcquisitionPointHistory"]["point"],
                'start_month' => $acquisitionPoint["AcquisitionPointHistory"]["start_month"]
            ));
            $category = $this->ProductCategory->findById($acquisitionType["AcquisitionType"]["product_category_id"]);
            $this->set('category', $category["ProductCategory"]);
            $this->set('categories', array());
        }
        else{
            throw new BadRequestException();
        }
    }
}
