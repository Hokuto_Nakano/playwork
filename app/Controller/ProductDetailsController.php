<?php
App::uses('AuthController', 'Controller');

class ProductDetailsController extends AuthController {

    public $uses = array('ProductCategory', 'ProductFamily', 'ProductDetail', 'Product');

    public function api($id = null){
        if($this->request->is('post')){
            //$this->log($this->request->data, LOG_DEBUG);
            if(!$id){
                if(!$this->request->data['product_category_id']){
                    $data = array(
                        'name' => $this->request->data['product_category_name'],
                        'short_name' => $this->request->data['product_category_short_name']
                    );
                    $this->ProductCategory->create();
                    if(!$this->ProductCategory->save($data)){
                        throw new InternalErrorException();
                    }
                    $product_category_id = $this->ProductCategory->getLastInsertID();
                }
                else{
                    $product_category_id = $this->request->data['product_category_id'];
                }

                if(!$this->request->data['product_family_id']){
                    $data = array(
                        'product_category_id' => $product_category_id,
                        'name' => $this->request->data['product_family_name'],
                        'short_name' => $this->request->data['product_family_short_name']
                    );
                    $this->ProductFamily->create();
                    if(!$this->ProductFamily->save($data)){
                        throw new InternalErrorException();
                    }
                    $product_family_id = $this->ProductFamily->getLastInsertID();
                }
                else{
                    $product_family_id = $this->request->data['product_family_id'];
                }

                $data = array(
                    'product_family_id' => $product_family_id
                );
                $this->ProductDetail->create();
                if(!$this->ProductDetail->save($data)){
                    throw new InternalErrorException();
                }

                $data = array(
                    'product_detail_id' => $this->ProductDetail->getLastInsertID(),
                    'product_detail_name' => $this->request->data['product_detail_name'],
                    'product_detail_short_name' => $this->request->data['product_detail_short_name'],
                    'price' => $this->request->data['price'],
                    'start_month' => $this->request->data['start_month']
                );
                $this->Product->create();
                if(!$this->Product->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
            else{
                if(!$this->ProductDetail->findById($id)){
                    throw new InternalErrorException();
                }

                $product = $this->Product->findByProduct_detail_idAndStart_month($id, $this->request->data['start_month']);
                if($product){
                    if(($product['Product']['product_detail_name'] != $this->request->data['product_detail_name']) && $this->Product->findByProduct_detail_name($this->request->data['product_detail_name'])){
                        $this->responseFailure('商材はすでに使用されています');
                    }

                    $data = array(
                        'id' => $product['Product']['id'],
                        'product_detail_id' => $id,
                        'product_detail_name' => $this->request->data['product_detail_name'],
                        'product_detail_short_name' => $this->request->data['product_detail_short_name'],
                        'price' => $this->request->data['price'],
                        'start_month' => $this->request->data['start_month']
                    );
                }
                else{
                    $this->Product->create();
                    $data = array(
                        'product_detail_id' => $id,
                        'product_detail_name' => $this->request->data['product_detail_name'],
                        'product_detail_short_name' => $this->request->data['product_detail_short_name'],
                        'price' => $this->request->data['price'],
                        'start_month' => $this->request->data['start_month']
                    );
                }
                if(!$this->Product->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
        }
        // 削除処理
        else if($this->request->is('delete')){
            try{
                if(!$this->Product->delete($id)){
                    $this->responseFailure('削除に失敗しました');
                }
            }
            catch(Exception $e){
                $this->responseFailure('削除に失敗しました');
            }

            $this->responseSuccess();
        }
    }

    public function partial($id = null, $yyyymm = null){
        $productId = $id;
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        if($productId == 'new'){
            $product = $this->Product->createNewProduct();
            $this->set('product', $product);
            $categories = $this->ProductCategory->find('all', array('order' => array('name ASC')));
            $this->set('categories', $categories);
        }
        else if(is_numeric($productId)){
            $product = $this->Product->getProductById($productId);
            if(!$product) throw new NotFoundException();

            $this->set('product', $product);
            $categories = $this->ProductCategory->find('all', array('order' => array('name ASC')));
            $this->set('categories', $categories);
        }
        else{
            throw new BadRequestException();
        }
    }
}
