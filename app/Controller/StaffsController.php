<?php
App::uses('AuthController', 'Controller');

class StaffsController extends AuthController {

    public $uses = array('Division', 'Area', 'Section', 'CallCenter', 'StaffType', 'ShiftType', 'JobType', 'Staff', 'StaffHistory', 'Company');

    public function index($yyyymm = null){
        $this->set('header', 'スタッフ情報');

        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }

        if($yyyymm == 'download') {
            $this->download();
        } else {
            $this->set('year', Util::GetYYYY($yyyymm));
            $this->set('month', Util::GetMM($yyyymm));
            $this->getData($yyyymm);
        }

    }

    public function api($id = null){
        if($this->request->is('get')){

            // エリア取得
            if($id == 'get_area') {
                $divisionId = $this->request->query('divisionId');

                $data = $this->Area->find('all', array(
                    'conditions' => array(
                        'Area.division_id' => $divisionId
                    ),
                    'order' => 'Area.id',
                    'fields'=>array('Area.id', 'Area.name')
                ));

                if($data) {
                    $this->responseJson($data);
                    $this->responseSuccess();
                } else {
                    $this->responseFailure('エリアが見つかりません');
                }
            }

            // 部署取得
            if ($id == 'get_section') {
                $areaId = $this->request->query('areaId');

                $data = $this->Section->find('all', array(
                    'conditions' => array(
                        'Section.area_id' => $areaId
                    ),
                    'order' => 'Section.id',
                    'fields'=>array('Section.id', 'Section.name')
                ));

                if($data) {
                    $this->responseJson($data);
                    $this->responseSuccess();
                } else {
                    $this->responseFailure('部署が見つかりません');
                }
            }
        }

        if($this->request->is('post')){
            if(!$id){
                // 登録
                if($this->Staff->findByLoginId($this->request->data['login_id'])){
                    $this->responseFailure('ログインIDはすでに使用されています');
                }
                // 登録処理
                $this->Staff->create();
                $this->StaffHistory->create();
                $staffData = array(
                    'staff_no' => $this->request->data['staff_no'],
                    'login_id' => $this->request->data['login_id'],
                    'password' => $this->request->data['password'],
                    'outside_attendance_id' => $this->request->data['outside_attendance_id'],
                    'hire_int_date' => $this->request->data['hire_int_date']
                );

                if($this->Staff->save($staffData)) {
                    $staffId = $this->Staff->getLastInsertID();
                    $historyData = array(
                        'staff_id' => $staffId,
                        'section_id' => $this->request->data['section_id'],
                        'job_type_id' => $this->request->data['job_type_id'],
                        'staff_type_id' => $this->request->data['staff_type_id'],
                        'call_center_id' => $this->request->data['call_center_id'],
                        'shift_type_id' => $this->request->data['shift_type_id'],
                        'company_id' => $this->request->data['company_id'],
                        'name' => $this->request->data['name'],
                        'start_month' => substr($this->request->data['hire_int_date'], 0, 6)
                    );

                    if($this->StaffHistory->save($historyData)) {

                        //チャットシステムのユーザー更新呼び出し
                        $userId = $staffId;
                        $user = $this->Staff->findById($userId);
                        $profile = $this->StaffHistory->getProfile($userId, Util::GetThisMonth());
                        Util::RegistChatInfo($userId, $profile["staffName"], $profile["affiliation"], $user["Staff"]["login_id"], $user["Staff"]["password"]);

                        $this->responseSuccess();
                    }else{
                        throw new InternalErrorException();
                    }
                }
                else{
                    throw new InternalErrorException();
                }

            } else if($id == 'disabled') {

                $historyId = $this->request->data['historyId'];
                $history = $this->StaffHistory->findById($historyId);
                $profile = $this->StaffHistory->getProfile($history["StaffHistory"]["staff_id"], Util::GetThisMonth());

                // 無効
                $this->debug('無効フラグON [historyId='.$historyId.']');

                $data = array(
                    'id' => $historyId,
                    'disabled_flg' => true
                );

                if($this->StaffHistory->save($data)) {


                    $data2 = array(
                        'id' => $history["StaffHistory"]["staff_id"],
                        'disabled_flg' => true
                    );
                    if($this->Staff->save($data2)){
                        //チャットシステムのユーザー更新呼び出し
                        Util::RegistChatInfo($history["StaffHistory"]["staff_id"], $profile["staffName"], $profile["affiliation"], "", "");
                        $this->responseSuccess();
                    }
                    else{
                        $this->responseFailure('無効化に失敗しました。[staffId='.$history["StaffHistory"]["staff_id"].']');
                    }
                } else {
                    $this->responseFailure('無効化に失敗しました。[historyId='.$historyId.']');
                }


            } else if($id == 'import'){
                // スタッフデータインポート
                $this->debug('スタッフデータインポート');

                // スタッフデータを取得
                $csvHeader = $this->request->data['csvHeader'];
                $csvData = $this->request->data['csvData'];

                // スタッフデータ登録
                for($i = 0; $i < count($csvData); $i++) {

                    // 社員番号
                    $staffNo = $csvData[$i][0];
                    // ログインID
                    $loginId = $csvData[$i][1];
                    // パスワード
                    $password = $csvData[$i][2];
                    // 外部勤怠システムID
                    $outsideAttendanceId = $csvData[$i][3];
                    // 事業部
                    $division = $csvData[$i][4];
                    // エリア
                    $area = $csvData[$i][5];
                    // 部署
                    $section = $csvData[$i][6];
                    // 職種区分
                    $jobType = $csvData[$i][7];
                    // 社員区分
                    $staffType = $csvData[$i][8];
                    // コールセンター
                    $callCenter = $csvData[$i][9];
                    // シフト区分
                    $shiftType = $csvData[$i][10];
                    // 会社名
                    $company = $csvData[$i][11];
                    // 氏名
                    $name = $csvData[$i][12];
                    // 入社日
                    $hireIntDate = $csvData[$i][13];

                    // 登録・更新するスタッフ情報
                    $staffData = array(
                        'staff_no' => $staffNo,
                        'login_id' => $loginId,
                        'password' => $password,
                        'outside_attendance_id' => $outsideAttendanceId,
                        'hire_int_date' => $hireIntDate
                    );

                    // スタッフ情報存在チェック
                    $staffByNo = $this->Staff->findByStaffNo($staffNo);
                    $staffByLoginId = $this->Staff->findByLoginId($loginId);

                    $updateFlg = false;
                    if ($staffByNo) {
                        // 更新
                        $staffData['id'] = $staffByNo['Staff']['id'];
                        $this->debug('スタッフデータ 更新 [staffNoあり]');
                        $updateFlg = true;
                    } else if ($staffByLoginId){
                        // 更新
                        $staffData['id'] = $staffByLoginId['Staff']['id'];
                        $this->debug('スタッフデータ 更新 [LoginIdあり]');
                        $updateFlg = true;
                    } else {
                        // 登録
                        $this->Staff->create();
                        $this->debug('スタッフデータ 新規登録');
                    }

                    $errorMessage = null;
                    if (!$staffNo) {
                        $errorMessage .= '社員番号が見つかりません。';
                    } else if (!$loginId) {
                        $errorMessage .= 'ログインIDが見つかりません。';
                    } else if (!$password && !$updateFlg) {
                        $errorMessage .= 'パスワードが見つかりません。';
                    } else if (!$outsideAttendanceId) {
                        $errorMessage .= '外部勤怠システムIDが見つかりません。';
                    } else if (!$division) {
                        $errorMessage .= '事業部が見つかりません。';
                    } else if (!$area) {
                        $errorMessage .= 'エリアが見つかりません。';
                    } else if (!$section) {
                        $errorMessage .= '部署が見つかりません。';
                    } else if (!$jobType) {
                        $errorMessage .= '職種区分が見つかりません。';
                    } else if (!$staffType) {
                        $errorMessage .= '社員区分が見つかりません。';
                    } else if (!$callCenter) {
                        $errorMessage .= 'コールセンター区分が見つかりません。';
                    } else if (!$shiftType) {
                        $errorMessage .= 'シフト区分が見つかりません。';
                    } else if (!$company) {
                        $errorMessage .= '会社名が見つかりません。';
                    } else if (!$name) {
                        $errorMessage .= '名前が見つかりません。';
                    } else if (!$hireIntDate) {
                        $errorMessage .= '入社日が見つかりません。';
                    }

                    if ($errorMessage) {
                        $this->responseFailure($errorMessage);
                    }

                    // パスワードは変更しない
                    if ($updateFlg && !$password) {
                        $staffData['password'] = $staffByNo['Staff']['password'];
                    }

                    // スタッフ情報登録・更新
                    $result1 = $this->Staff->save($staffData);

                    $divisionData = $this->Division->findByName($division);
                    $divisionId = isset($divisionData['Division']['id']) ? $divisionData['Division']['id'] : 0;

                    $areaData = $this->Area->find('first', array(
                       'conditions' => array(
                           'Area.name' => $area,
                           'Area.division_id' => $divisionId
                       ),
                        'fields'=>array('Area.id')
                    ));
                    $areaId = isset($areaData['Area']['id']) ? $areaData['Area']['id'] : 0;

                    $sectionData = $this->Section->find('first', array(
                        'conditions' => array(
                            'Section.name' => $section,
                            'Section.area_id' => $areaId
                        )
                    ));

                    $jobTypeData = $this->JobType->find('first', array(
                        'conditions' => array('JobType.type' => $jobType)
                    ));
                    $staffTypeData = $this->StaffType->find('first', array(
                        'conditions' => array('StaffType.type' => $staffType)
                    ));
                    $callCenterData = $this->CallCenter->find('first', array(
                        'conditions' => array('CallCenter.name' => $callCenter)
                    ));
                    $shiftTypeData = $this->ShiftType->find('first', array(
                        'conditions' => array('ShiftType.type' => $shiftType)
                    ));
                    $companyData = $this->Company->find('first', array(
                        'conditions' => array('Company.name' => $company)
                    ));

                    $staffId = $this->Staff->getLastInsertID();

                    if (!$divisionData) {
                        $errorMessage = '事業部が不正です。';
                    } else if (!$areaData) {
                        $errorMessage = 'エリアが不正です。';
                    } else if (!$sectionData) {
                        $errorMessage = '部署が不正です。';
                    } else if (!$jobTypeData) {
                        $errorMessage = '職種区分が不正です。';
                    } else if (!$staffTypeData) {
                        $errorMessage = '職員区分が不正です。';
                    } else if (!$callCenterData) {
                        $errorMessage = 'コールセンター区分が不正です。';
                    } else if (!$shiftTypeData) {
                        $errorMessage = 'シフト区分が不正です。';
                    } else if (!$companyData) {
                        $errorMessage = '会社名が不正です。';
                    }

                    if ($errorMessage) {
                        $this->log($errorMessage);
                        $this->responseFailure($errorMessage);
                    }

                    // 登録・更新するスタッフ履歴のデータ
                    $historyData = array(
                        'staff_id' => $staffId,
                        'section_id' => $sectionData['Section']['id'],
                        'job_type_id' => $jobTypeData['JobType']['id'],
                        'staff_type_id' => $staffTypeData['StaffType']['id'],
                        'call_center_id' => $callCenterData['CallCenter']['id'],
                        'shift_type_id' => $shiftTypeData['ShiftType']['id'],
                        'company_id' => $companyData['Company']['id'],
                        'name' => $name,
                        'start_month' => substr($hireIntDate, 0, 6)
                    );

                    // スタッフ情報が更新の場合、表示年月で登録・更新する
                    if ($staffByNo) {
                        $historyData['staff_id'] = $staffByNo['Staff']['id'];
                        $historyData['start_month'] = $this->request->data['startMonth'];
                        $staffId = $staffByNo['Staff']['id'];
                    } else if ($staffByLoginId) {
                        $historyData['staff_id'] = $staffByLoginId['Staff']['id'];
                        $historyData['start_month'] = $this->request->data['startMonth'];
                        $staffId = $staffByLoginId['Staff']['id'];
                    }

                    // スタッフ履歴存在チェック
                    $historyByStartMonth = $this->StaffHistory->find('first', array(
                        'conditions' => array(
                            'staff_id' => $staffId,
                            'start_month' => $this->request->data['startMonth']
                        )
                    ));

                    // 表示年月のデータが登録されている場合
                    if ($historyByStartMonth) {
                        // 更新
                        $historyData['id'] = $historyByStartMonth['StaffHistory']['id'];
                        $this->debug('スタッフ履歴データ 更新');
                    } else {
                        // 登録
                        $this->StaffHistory->create();
                        $this->debug('スタッフデータ 新規登録');
                    }

                    $result2 = $this->StaffHistory->save($historyData);
                    if (!$result1 || !$result2) {
                        $this->responseFailure('スタッフ登録に失敗しました。');
                    }

                    //チャットシステムのユーザー更新呼び出し
                    $user = $this->Staff->findById($staffId);
                    $profile = $this->StaffHistory->getProfile($staffId, Util::GetThisMonth());
                    Util::RegistChatInfo($staffId, $profile["staffName"], $profile["affiliation"], $user["Staff"]["login_id"], $user["Staff"]["password"]);

                }

                $this->responseSuccess();

            } else {

                // 履歴登録か履歴更新か
                $data = $this->StaffHistory->find('all', array(
                    'conditions'=>array(
                        'and' => array(
                            'StaffHistory.staff_id' => $this->request->data['staff_id'],
                            'StaffHistory.start_month' => $this->request->data['req_start_month']
                        )),
                    'fields'=>array('id')
                ));

                // 更新
                $staffData = array(
                    'id' => $this->request->data['staff_id'],
                    'staff_no' => $this->request->data['staff_no'],
                    'outside_attendance_id' => $this->request->data['outside_attendance_id'],
                    'hire_int_date' => $this->request->data['hire_int_date']
                );

                if(isset($this->request->data['password'])){
                    $staffData["password"] = $this->request->data['password'];
                }

                $historyData = array(
                    'staff_id' => $this->request->data['staff_id'],
                    'section_id' => $this->request->data['section_id'],
                    'job_type_id' => $this->request->data['job_type_id'],
                    'staff_type_id' => $this->request->data['staff_type_id'],
                    'call_center_id' => $this->request->data['call_center_id'],
                    'shift_type_id' => $this->request->data['shift_type_id'],
                    'company_id' => $this->request->data['company_id'],
                    'name' => $this->request->data['name'],
                    'start_month' => $this->request->data['req_start_month']
                );

                // データが存在すれば更新
                if($data) {
                    // 更新はidと適用月をセット
                    $this->debug('スタッフ履歴更新：staff_id='.$this->request->data['staff_id']);
                    $historyData['id'] = $this->request->data['history_id'];
                } else {
                    // 登録
                    $this->debug('スタッフ履歴登録');
                    $this->StaffHistory->create();
                }

                if($this->Staff->save($staffData) && $this->StaffHistory->save($historyData)) {

                    //チャットシステムのユーザー更新呼び出し
                    $userId = $this->request->data['staff_id'];
                    $user = $this->Staff->findById($userId);
                    $profile = $this->StaffHistory->getProfile($userId, Util::GetThisMonth());
                    Util::RegistChatInfo($userId, $profile["staffName"], $profile["affiliation"], $user["Staff"]["login_id"], $user["Staff"]["password"]);

                    $this->responseSuccess();
                } else {
                    $this->responseFailure();
                }
                exit();
            }
        }
        // 削除処理
        if($this->request->is('delete')){
            $staffHistory = $this->StaffHistory->findById($id);
            $staff_id = $staffHistory['Staff']['id'];

            if($this->StaffHistory->delete($id)){
                // staffHistoryが全て削除された場合はstaffも削除
                $tmp = $this->StaffHistory->findByStaffId($staff_id);
                if(empty($tmp)) {
                    if($this->Staff->delete($staff_id)){

                        //チャットシステムのユーザー更新呼び出し
                        Util::RegistChatInfo($staff_id, "", "", "", "");

                        $this->responseSuccess();
                    }
                } else {
                    $this->responseSuccess();
                }
            }
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $divisions = $this->Division->find('all', array('order' => 'id'));
            $areas = $this->Area->find('all', array(
                'conditions' => array('Area.division_id' => $divisions[0]['Division']['id']),
                'order' => 'Area.id'
            ));
            $sections = $this->Section->find('all', array(
                'conditions' => array('Section.area_id' => $areas[0]['Area']['id']),
                'order' => 'Section.id'
            ));
            $staffTypes = $this->StaffType->find('all', array('order' => 'id'));
            $jobTypes = $this->JobType->find('all', array('order' => 'id'));
            $shiftTypes = $this->ShiftType->find('all', array('order' => 'id'));
            $callCenters = $this->CallCenter->find('all', array('order' => 'id'));
            $companies = $this->Company->find('all', array('order' => 'id'));

            $this->set('id', 0);
            $this->set('history_id', '');
            $this->set('staff_no', '');
            $this->set('staff_id', '');
            $this->set('login_id', '');
            $this->set('password', '');
            $this->set('outside_attendance_id', '');
            $this->set('divisions', $divisions);
            $this->set('division_short_name', '');
            $this->set('areas', $areas);
            $this->set('area_short_name', '');
            $this->set('sections', $sections);
            $this->set('section_name', '');
            $this->set('section_short_name', '');
            $this->set('jobtypes', $jobTypes);
            $this->set('stafftypes', $staffTypes);
            $this->set('callcenters', $callCenters);
            $this->set('shifttypes', $shiftTypes);
            $this->set('companies', $companies);
            $this->set('name', '');
            $this->set('hire_int_date', '');
            $this->set('start_month', '');
            $this->set('updateFlg', false);
            $this->set('division_id', -1);
            $this->set('area_id', -1);
            $this->set('section_id', -1);
            $this->set('jobtype_id', -1);
            $this->set('stafftype_id', -1);
            $this->set('callcenter_id', -1);
            $this->set('shifttype_id', -1);
            $this->set('company_id', -1);
            $this->set('adminFlg', $this->Session->read('authority') == 10);

        }
        else if(is_numeric($id)){

            // 個別取得
            $staffHistory = $this->StaffHistory->findById($id);
            $staffTypes = $this->StaffType->find('all', array('order' => 'id'));
            $jobTypes = $this->JobType->find('all', array('order' => 'id'));
            $shiftTypes = $this->ShiftType->find('all', array('order' => 'id'));
            $callCenters = $this->CallCenter->find('all', array('order' => 'id'));
            $companies = $this->Company->find('all', array('order' => 'id'));

            $divisions = $this->Division->find('all', array('order' => 'Division.id'));
            $areas = $this->Area->find('all', array('order' => 'Area.id'));
            $sections = $this->Section->find('all', array('order' => 'Section.id'));
            $sectionByHistory = $this->Section->find('first', array('conditions' => array('Section.id' => $staffHistory['Section']['id'])));
            if($sectionByHistory){
                $divisionByHistory = $this->Division->find('first', array('conditions' => array('Division.id' => $sectionByHistory['Area']['division_id'])));
            }
            else{
                $sectionByHistory = array('Area' => array('id' => 0, 'name' => ''));
                $divisionByHistory = array('Division' => array('id' => 0, 'name' => ''));
            }

            if(!$staffHistory) throw new NotFoundException();
            $this->set('id', $id);
            $this->set('history_id', $staffHistory['StaffHistory']['id']);
            $this->set('staff_id', $staffHistory['StaffHistory']['staff_id']);
            $this->set('staff_no', $staffHistory['Staff']['staff_no']);
            $this->set('login_id', $staffHistory['Staff']['login_id']);
            $this->set('password', $staffHistory['Staff']['password']);
            $this->set('outside_attendance_id', $staffHistory['Staff']['outside_attendance_id']);
            $this->set('name', $staffHistory['StaffHistory']['name']);
            $this->set('hire_int_date', $staffHistory['Staff']['hire_int_date']);
            $this->set('divisions', $divisions);
            $this->set('division_id', $divisionByHistory['Division']['id']);
            $this->set('division_name', $divisionByHistory['Division']['name']);
            $this->set('areas', $areas);
            $this->set('area_id', $sectionByHistory['Area']['id']);
            $this->set('area_name', $sectionByHistory['Area']['name']);
            $this->set('sections', $sections);
            $this->set('section_id', $staffHistory['Section']['id']);
            $this->set('section_name', $staffHistory['Section']['name']);
            $this->set('jobtypes', $jobTypes);
            $this->set('jobtype_id', $staffHistory['JobType']['id']);
            $this->set('jobtype_type', $staffHistory['JobType']['type']);
            $this->set('stafftypes', $staffTypes);
            $this->set('stafftype_id', $staffHistory['StaffType']['id']);
            $this->set('stafftype_type', $staffHistory['StaffType']['type']);
            $this->set('callcenters', $callCenters);
            $this->set('callcenter_id', $staffHistory['CallCenter']['id']);
            $this->set('callcenter_name', $staffHistory['CallCenter']['name']);
            $this->set('shifttypes', $shiftTypes);
            $this->set('shifttype_id', $staffHistory['ShiftType']['id']);
            $this->set('shifttype_type', $staffHistory['ShiftType']['type']);
            $this->set('companies', $companies);
            $this->set('company_id', $staffHistory['Company']['id']);
            $this->set('company_name', $staffHistory['Company']['name']);
            $this->set('start_month', $staffHistory['StaffHistory']['start_month']);
            $this->set('updateFlg', true);
            $this->set('adminFlg', $this->Session->read('authority') == 10);


        }
        else{
            throw new BadRequestException();
        }


    }

    function getData($startMonth) {

        // デフォルト値
        $DEFAULT_OFFSET = 0;
        $DEFAULT_LIMIT = NUMBER_OF_DISPLAYED;

        // オフセット、リミット取得
        $offset = null;
        $limit = null;
        if(isset($this->request->query['offset'])){
            $offset = $this->request->query['offset'];
        } else {
            $offset = $DEFAULT_OFFSET;
        }
        if(isset($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        } else {
            $limit = $DEFAULT_LIMIT;
        }

        // SQL実行
        $staff = $this->StaffHistory->getList($startMonth, $offset, $limit);
        $count = $this->StaffHistory->getCount($startMonth);

        if($staff && $count){
            $this->set('staff', $staff);
            $this->set('count', $count[0][0]['count']);
            $this->set('offset', $offset);
            $this->set('limit', $limit);
            $this->set('page', floor(($offset + $limit) / $limit));
            $this->set('totalPage', ceil($count[0][0]['count'] / $limit));
        } else {
            $this->set('count', 0);
            $this->set('offset', $DEFAULT_OFFSET);
            $this->set('limit', $DEFAULT_LIMIT);
            $this->set('page', 0);
            $this->set('totalPage', 0);
        }
    }

    function download() {

        // レスポンスヘッダ
        $this->response->header('Content-Disposition: attachment; filename=staff.csv');

        // ファイルの情報とファイルの内容を設定
        $this->autoRender = false;
        $this->response->type('csv');
        $this->response->file(WWW_ROOT.'files/staff.csv');
        $this->response->download('staff.csv');

    }

    function debug($message){
        $this->log($message, LOG_DEBUG);
    }
}
