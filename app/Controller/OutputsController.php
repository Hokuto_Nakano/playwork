<?php
App::uses('AuthController', 'Controller');

class OutputsController extends AuthController {

    public $uses = array('Division', 'Area', 'Section');

    public function index($yyyymm = null, $divisionId = 0, $areaId = 0, $sectionId = 0){
        $this->set("header", "ファイル出力");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $divisions = $this->Division->getAllSimpleDivisions();
        $this->set("divisions", $divisions);
        $this->set("divisionId", $divisionId);

        $areas = $this->Area->find('all', array(
            'conditions' => array('Area.division_id' => $divisionId),
            'order' => 'Area.id'
        ));

        $sections = $this->Section->find('all', array(
            'conditions' => array('Section.area_id' => $areaId),
            'order' => 'Section.id'
        ));

        $this->set("areas", $areas);
        $this->set("areaId", $areaId);
        $this->set("sections", $sections);
        $this->set("sectionId", $sectionId);


    }
}
