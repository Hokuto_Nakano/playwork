<?php
App::uses('AuthController', 'Controller');

class CallCentersController extends AuthController {
    
    public $uses = array('CallCenter');

    public function index(){
    }

    public function api($id = null){
        if($this->request->is('get')){

            if(!$id){
                // 一覧
                $conditions = array('order' => array('id' => 'asc'));
                $callCenter = $this->CallCenter->find('all', $conditions);

                $data = null;
                if($callCenter){
                    $response = array('result'=>'OK', 'data'=>$callCenter);
                }
                else{
                    $response = array('result'=>'NG');
                }

                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            } else {
                // 個別取得
                $callCenter = $this->CallCenter->findById($id);
                $data = null;
                if($callCenter){
                    $response = array('result'=>'OK', 'data'=>$callCenter);
                }
                else{
                    $response = array('result'=>'NG');
                }
                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            }
        }

        if($this->request->is('post')){
            if(!$id){
                //$this->log($this->request->data['division_name'], LOG_DEBUG);
                if($this->CallCenter->findByName($this->request->data['name'])){
                    $this->responseFailure('コールセンター名はすでに使用されています');
                }
                // 登録処理
                $this->CallCenter->create();
                if($this->CallCenter->save($this->request->data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
            else{
                //$this->log($this->request->data['division_id'], LOG_DEBUG);
                $callCenter = $this->CallCenter->findById($id);
                if($this->CallCenter->findByName($this->request->data['name'])){
                    $this->responseFailure('コールセンター名はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'name' => $this->request->data['name'],
                );
                $this->log($data, LOG_DEBUG);
                if($this->CallCenter->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        // 削除処理
        if($this->request->is('delete')){
            if($this->CallCenter->delete($id)){
                $this->responseSuccess();
            }
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $this->set('id', 0);
            $this->set('name', '');
        }
        else if(is_numeric($id)){
            $data = $this->CallCenter->findById($id);
            if(!$data) throw new NotFoundException();

            $this->set('id', $id);
            $this->set('name', $data['CallCenter']['name']);
        }
        else{
            throw new BadRequestException();
        }
    }
}
