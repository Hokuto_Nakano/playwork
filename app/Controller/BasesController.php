<?php
App::uses('AuthController', 'Controller');

class BasesController extends AuthController {
    public $uses = array('Division', 'Area', 'Section');

    public function index(){
        $bases = $this->Division->getBaseList();
        $this->set("bases", $bases);
    }
}
