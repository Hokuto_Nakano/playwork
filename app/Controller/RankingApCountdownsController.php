<?php
App::uses('AuthController', 'Controller');

class RankingApCountdownsController extends AuthController {

    public $uses = array('Goal', 'StaffCountUp', 'CountItem', 'OutputJson');

    private $key = "RankingApCountdowns";

    public function index($yyyymm = null, $isUpdate = 0){
        $this->set("header", "目標件数カウントダウン");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $outputJson = $this->OutputJson->findByKeyAndMonthAndDivisionId($this->key, $yyyymm, 0);

        if(!$isUpdate && $outputJson){
            $json = $outputJson["OutputJson"]["json"];
            $data = json_decode($json, true);
            $modified = $outputJson["OutputJson"]["modified"];
        }
        else{
            $data = $this->getData($yyyymm);
            $json = json_encode($data);
            $newData = array(
                "key" => $this->key,
                "month" => $yyyymm,
                "json" => $json
            );
            if($outputJson){
                $newData["id"] = $outputJson["OutputJson"]["id"];
            }

            if($this->OutputJson->save($newData)){
                $id = $this->OutputJson->getLastInsertID();
                $id = $id ? $id : $newData["id"];
                $updateData = $this->OutputJson->findById($id);
                $modified = $updateData["OutputJson"]["modified"];
            }
        }

        $this->set('goals', $data['goals']);
        $this->set('goalTotal', $data['goalTotal']);
        $this->set('graph', $data['graph']);
        $this->set("modified", date("Y/m/d H:i", strtotime($modified)));
    }

    public function excel($yyyymm = null, $divisionId = 0){
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $book = new PHPExcel();
        $book->getActiveSheet()->setTitle('目標件数カウントダウン');
        $sheet = $book->getActiveSheet();

        $data = $this->getData($yyyymm, $divisionId);

        // 目標獲得件数
        $sheet->setCellValueByColumnAndRow(0, 1, '目標獲得件数');
        $sheet->setCellValueByColumnAndRow(1, 1, $data['goalTotal']);
        $sheet->getStyleByColumnAndRow(1, 1)->getFont()->setSize(18);
        $sheet->getStyleByColumnAndRow(1, 1)->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 1, 'Countdown!');

        // ヘッダー
        $headers = array(
            '事業部（エリア名）', '目標', '必要件数', '実績', '達成率'
        );

        foreach ($headers as $colNum => $header) {
            $sheet->setCellValueByColumnAndRow($colNum, 3, $header);
        }

        foreach ($data['goals'] as $rowNum => $goal) {
            $colNum=0;
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4, $goal['divisionName']. '(' . $goal['areaName'] .')');
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4, $goal['goalCount']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4, $goal['necessary']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4, $goal['achievement']);
            $sheet->setCellValueByColumnAndRow($colNum, $rowNum+4, round($goal['rate'], 1) .'%');
        }

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="ranking_ap_countdowns.xlsx"');

        $writer = PHPExcel_IOFactory::createWriter($book, 'Excel2007');
        $writer->save('php://output');
        exit;
    }

    function getData($month) {

        // 後確のcount_item_idを取得
//        $itemName = 'アポイント数';
//        $countItem = $this->CountItem->findByItemName($itemName);
//        if($countItem){
//            $countItemId = $countItem['CountItem']['id'];
//        }
//        $maekakuId = $this->CountItem->getMaekakuId();
        $atokakuId = $this->CountItem->getAtokakuId();
        $goals = $this->Goal->getList($month);

        $goalTotal = 0;

        foreach ($goals as $idx => $goal) {
//            $staffCountUp = $this->StaffCountUp->getTotalAreaCount($month, $goal['areaId'], $countItemId);
            $staffCountUp = $this->StaffCountUp->getAcquisitionAreaCount($month, $goal['areaId'], $atokakuId);

            // 実績
            $goals[$idx]['achievement'] = $staffCountUp['count'];

            // 必要件数
            $goals[$idx]['necessary'] = $goal['goalCount'] - $staffCountUp['count'];

            // 達成率
            $goals[$idx]['rate'] = 0;
            if ($staffCountUp['count']) {
                $goals[$idx]['rate'] = round(($staffCountUp['count'] / $goals[$idx]['necessary']) * 100, 1);
            }

            // 目標獲得件数
            $goalTotal += $goal['goalCount'];
        }

        if($atokakuId){
            $graphs = $this->StaffCountUp->getCountDownGraph($month, $atokakuId);
        }
        else{
            $graphs = array();
        }

        $averageCount = $goalTotal / date('t', strtotime($month.'01'));

        // 1ヶ月分ループ
        $date = new DateTime();
        $date->setDate(date('Y', strtotime($month.'01')), date('m', strtotime($month.'01')), date('d', strtotime($month.'01')));
        $ymd = $date->format('Ymd');

        $graphData = array();
        $goalCount = $averageCount;
        $lastCount = 0;
        for($i = 1; $i < date('t', strtotime($month.'01')) + 1; $i++) {
            $datetime = new DateTime($ymd);
            $data = array(
                'date' => $datetime->format('Y-m-d'),
                'averageCount' => floor($goalCount),
                'goalCount' => 0
            );

//            if ($i >= date('d')) {
//                $data['goalCount'] = null;
//            } else {
                foreach ($graphs as $idx => $graph) {
                    $key = array_search($ymd, $graph);
                    if ($key) {
                        $lastCount += $graph['count'];
                        $data['goalCount'] = $lastCount;
                        break;
                    } else {
                        $data['goalCount'] = $lastCount;
                    }
//                }
            }

            $graphData[] = $data;
            $ymd += 1;
            $goalCount += $averageCount;

        }

        $result = array(
            'goals' => $goals,
            'goalTotal' => $goalTotal,
            'graph' => json_encode($graphData)
        );

        return $result;
    }

}
