<?php
App::uses('ApiController', 'Controller');

class ProgressController extends ApiController {

    public $uses = array('ProductCategory', 'CountItem', 'StaffCountUp', 'StaffPointGoal', 'StaffHistory');

    public function index(){
    }

    public function api($staffId = 0, $month = 0){

        if($this->request->is('get')){

            if (!$staffId || !$month) {
                $this->response->statusCode(400);
                $this->responseFailure('社員IDまたは対象年月が不正です。');
            }

            try {
                $data = $this->getData($staffId, $month);
                $this->responseJson($data);
            } catch (Exception $e) {
                $this->response->statusCode(500);
                $this->log($e->getTraceAsString());
                $this->responseFailure($e->getMessage());
            }
        }

    }

    function getData($staffId, $month) {

        $maekakuId = $this->CountItem->getMaekakuId();
        $atokakuId = $this->CountItem->getAtokakuId();

        $productCategories = $this->ProductCategory->getPointHistory($month);

        $bp = 0;
        foreach ($productCategories as $productCategory) {
            foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                $data = $this->StaffCountUp->getAcquisitionCount($staffId, $productCategory['id'], $acquisitionTypeId, $month, $maekakuId, $atokakuId);
                $bp += $data['bp'];
            }
        }

        // 最新
        $staffHistory = $this->StaffHistory->find('first', array(
            'fields' => array(
                'StaffHistory.*'
            ),
            'conditions' => array(
                'StaffHistory.staff_id' => $staffId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, max(start_month) max_start_month from staff_histories where staff_id ='. $staffId .' and start_month <=' . $month .' group by staff_id)',
                    'alias' => 'sub_tbl',
                    'conditions' => array(
                        'sub_tbl.staff_id = StaffHistory.staff_id',
                        'sub_tbl.max_start_month = StaffHistory.start_month'
                    )
                )
            )
        ));

        // 社員情報が見つからない場合はオールゼロ
        if (!$staffHistory) {
            $data = array(
                'bp' => 0,
                'goalBp' =>  0,
                'progress' =>  0,
                'totalBp' => 0
            );
            $this->responseJson($data);
        }

        $historyId = $staffHistory['StaffHistory']['id'];

        $goalBp = $this->StaffPointGoal->findFirst(array(
            'fields' => array(
                'StaffPointGoal.point'
            ),
            'conditions' => array(
                'staff_id' => $historyId
            )
        ));


        $staffHistory = $this->StaffHistory->find('first', array(
            'fields' => array(
                'StaffHistory.*'
            ),
            'conditions' => array(
                'StaffHistory.staff_id' => $staffId
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, min(start_month) max_start_month from staff_histories where staff_id ='. $staffId .' and start_month <=' . $month .' group by staff_id)',
                    'alias' => 'sub_tbl',
                    'conditions' => array(
                        'sub_tbl.staff_id = StaffHistory.staff_id',
                        'sub_tbl.max_start_month = StaffHistory.start_month'
                    )
                )
            )
        ));

        // 履歴にある一番古い年月を取得
        $startMonth = $staffHistory['StaffHistory']['start_month'];
        $startDateTime = new DateTime($startMonth.'01');
        $totalBp = 0;

        // 指定年月までの累計を計算
        while($startDateTime->format('Ym') <= $month) {

            $productCategories = $this->ProductCategory->getPointHistory($startDateTime->format('Ym'));

            foreach ($productCategories as $productCategory) {
                foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                    $data = $this->StaffCountUp->getAcquisitionCount($staffId, $productCategory['id'], $acquisitionTypeId, $startDateTime->format('Ym'), $maekakuId, $atokakuId);
                    $totalBp += $data['bp'];
                }
            }

            // 翌月
            $startDateTime->modify('+1 month');
        }

        $data = array(
            'bp' => $bp,
            'goalBp' => $goalBp ? $goalBp['point'] : 0,
            'progress' => $goalBp ? round(($bp / ($goalBp['point'] / date('t') * date('d'))) * 100, 1) : 0,
            'totalBp' => $totalBp
        );

        return $data;
    }

}
