<?php
App::uses('AuthController', 'Controller');

class AcquisitionsController extends AuthController {

    public $uses = array('Acquisition', 'Product');

    public function index(){
        $conditions = array('order' => array('Product.name' => 'asc', 'Acquisition.type' => 'asc'));
        $acquisitions = $this->Acquisition->find('all', $conditions);
        $this->set("acquisitions", $acquisitions);
    }

    public function api($id = null){
        if($this->request->is('get')){

            if(!$id){
                // 一覧
                $conditions = array('order' => array('Product.name' => 'asc', 'Acquisition.type' => 'asc'));
                $acquisitions = $this->Acquisition->find('all', $conditions);

                if($acquisitions){
                    $this->responseJson($acquisitions);
                }
                else{
                    $this->responseFailure();
                }
            } else {
                // 個別取得
                $acquisition = $this->Acquisition->findById($id);
                if($acquisition){
                    $this->responseJson($acquisition);
                }
                else{
                    $this->responseFailure();
                }
            }
        }

        if($this->request->is('post')){
            if(!$id){
                if($this->Acquisition->findByType($this->request->data['type'])){
                    $this->responseFailure('獲得区分はすでに使用されています');
                }
                // 登録処理
                $this->Acquisition->create();
                if(!$this->Acquisition->save($this->request->data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
            else{
                $acquisition = $this->Acquisition->findById($id);
                if(($acquisition['Acquisition']['type'] != $this->request->data['type']) && $this->Acquisition->findByType($this->request->data['type'])){
                    $this->responseFailure('獲得区分はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'product_id' => $this->request->data['product_id'],
                    'type' => $this->request->data['type'],
                    'bp' => $this->request->data['bp']
                );
                if(!$this->Acquisition->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
        }
        else if($this->request->is('delete')){
            if(!$this->Acquisition->delete($id)){
                throw new InternalErrorException();
            }
            $this->responseSuccess();
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $this->set('id', 0);
            $this->set('type', '');
            $this->set('bp', '');

            $products = $this->Product->find('all', array('field' => array('id', 'name'), 'order' => array('name ASC')));
            if($products){
                $this->set('products', $products);
                $selectProduct = $products[0]['Product'];
                $this->set('selectProduct', $selectProduct);
            }
            else{
                $this->set('products', array());
                $this->set('selectProduct', array(
                    'id' => 0,
                    'name' => ''
                ));
            }
        }
        else if(is_numeric($id)){
            $data = $this->Acquisition->findById($id);
            if(!$data) throw new NotFoundException();

            $this->set('id', $id);
            $this->set('type', $data['Acquisition']['type']);
            $this->set('bp', $data['Acquisition']['bp']);

            $product = $this->Product->findById($data['Acquisition']['product_id']);
            $selectProduct = $product['Product'];
            $this->set('selectProduct', $selectProduct);

            $products = $this->Product->find('all', array('field' => array('id', 'name'), 'order' => array('name ASC')));
            $this->set('products', $products);
        }
        else{
            throw new BadRequestException();
        }
    }
}
