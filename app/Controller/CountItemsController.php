<?php
App::uses('AuthController', 'Controller');

class CountItemsController extends AuthController {

    public $uses = array('CountItem');

    public function index(){
        $this->set("header", "カウント項目設定");
        $countItems = $this->CountItem->find('all');
        $this->set("countItems", $countItems);
    }

    public function api($id = null){
        if($this->request->is('post')){
            if(!$id){ //新規
                if($this->CountItem->findByItemName($this->request->data['item_name'])){
                    $this->responseFailure('この項目名はすでに使用されています');
                }

                $data = array(
                    'item_name' => $this->request->data['item_name']
                );
                $this->CountItem->create();
                if(!$this->CountItem->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
            else{
                $data = array(
                    'id' => $id,
                    'item_name' => $this->request->data['item_name']
                );
                if(!$this->CountItem->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
        }
        else if($this->request->is('delete')){
            try{
                if(!$this->CountItem->delete($id)){
                    $this->responseFailure('削除に失敗しました');
                }
            }
            catch(Exception $e){
                $this->responseFailure('削除に失敗しました');
            }

            $this->responseSuccess();
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $this->set('countItem', array(
                "id" => 0,
                "item_name" => ""
            ));
        }
        else if(is_numeric($id)){
            $countItem = $this->CountItem->findById($id);
            if(!$countItem) throw new NotFoundException();

            $this->set('countItem', array(
                "id" => $id,
                "item_name" => $countItem["CountItem"]["item_name"]
            ));
        }
        else{
            throw new BadRequestException();
        }
    }
}
