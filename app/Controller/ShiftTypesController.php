<?php
App::uses('AuthController', 'Controller');

class ShiftTypesController extends AuthController {

    public $uses = array('ShiftType');

    public function index(){
    }

    public function api($id = null){
        if($this->request->is('get')){

            if(!$id){
                // 一覧
                $conditions = array('order' => array('id' => 'asc'));
                $shiftType = $this->ShiftType->find('all', $conditions);

                $data = null;
                if($shiftType){
                    $response = array('result'=>'OK', 'data'=>$shiftType);
                }
                else{
                    $response = array('result'=>'NG');
                }

                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            } else {
                // 個別取得
                $shiftType = $this->ShiftType->findById($id);
                $data = null;
                if($shiftType){
                    $response = array('result'=>'OK', 'data'=>$shiftType);
                }
                else{
                    $response = array('result'=>'NG');
                }
                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            }
        }

        if($this->request->is('post')){
            if(!$id){
                // 登録
                if($this->ShiftType->findByType($this->request->data['type'])){
                    $this->responseFailure('シフト区分はすでに使用されています');
                }
                // 登録処理
                $this->ShiftType->create();
                if($this->ShiftType->save($this->request->data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
            else{
                // 更新
                $type = $this->request->data['type'];
                $is_break = $this->request->data['is_break'];
                $shiftType = $this->ShiftType->findByType($this->request->data['type']);
                if(($shiftType['ShiftType']['type'] == $type) && ($shiftType['ShiftType']['is_break'] == $is_break)){
                    $this->responseFailure('シフト区分、休憩有無フラグはすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'type' => $type,
                    'is_break' => $is_break,
                );
                if($this->ShiftType->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        // 削除処理
        if($this->request->is('delete')){
            if($this->ShiftType->delete($id)){
                $this->responseSuccess();
            }
        }
    }

    public function partial($id = null){

        if($id == 'new'){
            $this->set('id', 0);
            $this->set('type', '');
        }
        else if(is_numeric($id)){
            $data = $this->ShiftType->findById($id);
            if(!$data) throw new NotFoundException();

            $this->set('id', $id);
            $this->set('type', $data['ShiftType']['type']);
            if($data['ShiftType']['is_break'] == true){
                $this->set('is_break', 1);
                $this->set('is_break_label', '休憩あり');
            } else {
                $this->set('is_break', 0);
                $this->set('is_break_label', '休憩なし');
            }
        }
        else{
            throw new BadRequestException();
        }
    }
}
