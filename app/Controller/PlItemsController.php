<?php
App::uses('AuthController', 'Controller');

class PlItemsController extends AuthController {

    public $uses = array('PlItem', 'PlCategory');

    public function index($yyyymm = null){
        $this->set("header", "PL項目設定");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $plItems = $this->PlItem->getList($yyyymm);
        $this->set("plitems", $plItems);
        $pl_calc = Configure::read("pl_calc");
        $this->set("pl_calc", $pl_calc);
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));
    }

    public function api($id = null){
        if($this->request->is('get')){
        }
        else if($this->request->is('post')){
            if(!is_numeric($this->request->data['start_month'])){
                $this->responseFailure('適用開始月は数字で入力してください');
            }
            if(!$id){ //新規
                if($this->PlItem->findByItemName($this->request->data['item_name'])){
                    $this->responseFailure('この項目名はすでに使用されています');
                }

                $data = array(
                    'category_id' => $this->request->data['category_id'],
                    'item_name' => $this->request->data['item_name'],
                    'calc' => $this->request->data['calc'],
                    'start_month' => $this->request->data['start_month']
                );
                $this->PlItem->create();
                if(!$this->PlItem->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
            else{
                $data = array(
                    'id' => $id,
                    'category_id' => $this->request->data['category_id'],
                    'item_name' => $this->request->data['item_name'],
                    'calc' => $this->request->data['calc'],
                    'start_month' => $this->request->data['start_month']
                );
                if(!$this->PlItem->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
        }
        else if($this->request->is('delete')){
            try{
                if(!$this->PlItem->delete($id)){
                    $this->responseFailure('削除に失敗しました');
                }
            }
            catch(Exception $e){
                $this->responseFailure('削除に失敗しました');
            }

            $this->responseSuccess();
        }
    }

    public function partial($id = null, $yyyymm = null){
        $plItemId = $id;
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $pl_calc = Configure::read("pl_calc");
        $this->set("pl_calc", $pl_calc);

        $this->set("categories", $this->PlCategory->find('all'));

        if($plItemId == 'new'){
            $this->set('plItem', array(
                "id" => 0,
                "category_id" => "1",
                "item_name" => "",
                "calc" => "plus",
                "start_month" => $yyyymm
            ));
        }
        else if(is_numeric($plItemId)){
            $plItem = $this->PlItem->getById($plItemId);
            if(!$plItem) throw new NotFoundException();

            $this->set('plItem', array(
                "id" => $plItemId,
                "category_id" => $plItem["PlCategory"]["id"],
                "item_name" => $plItem["PlItem"]["item_name"],
                "calc" => $plItem["PlItem"]["calc"],
                "start_month" => $plItem["PlItem"]["start_month"]
            ));

        }
        // else if(is_numeric($acquisitionTypeId)){
        //     $acquisitionType = $this->AcquisitionType->findById($acquisitionTypeId);
        //     if(!$acquisitionType) throw new NotFoundException();
        //
        //     //$acquisitionPoint = $this->AcquisitionPointHistory->findByAcquisitionTypeIdAndStartMonth($acquisitionTypeId, $yyyymm);
        //     $acquisitionPoint = $this->AcquisitionPointHistory->find("first", array(
        //         'conditions' => array(
        //             'acquisition_type_id' => $acquisitionTypeId,
        //             "start_month = (SELECT MAX(aph.start_month) FROM acquisition_point_histories AS aph WHERE aph.acquisition_type_id = AcquisitionPointHistory.acquisition_type_id AND aph.start_month <= $yyyymm)"
        //         )
        //     ));
        //     $this->set('acquisitionPoint', $acquisitionPoint["AcquisitionPointHistory"]);
        //     $this->set('acquisitionType', array(
        //         "id" => $acquisitionType["AcquisitionType"]["id"],
        //         "acquisition_type" => $acquisitionType["AcquisitionType"]["acquisition_type"],
        //         "point" => $acquisitionPoint["AcquisitionPointHistory"]["point"],
        //         'start_month' => $acquisitionPoint["AcquisitionPointHistory"]["start_month"]
        //     ));
        //     $category = $this->ProductCategory->findById($acquisitionType["AcquisitionType"]["product_category_id"]);
        //     $this->set('category', $category["ProductCategory"]);
        //     $this->set('categories', array());
        // }
        // else{
        //     throw new BadRequestException();
        // }
    }
}
