<?php
App::uses('AuthController', 'Controller');

class InputFormatsController extends AuthController
{

    public $uses = array('ProductCategory', 'ProductCsvTableMap', 'ProductCsvColumnMap', "Acquisition", 'StaffHistory', 'CountCondition', 'StaffIncentive');

    public function index($yyyymm = null, $id = null) {

        $this->set("header", "獲得データ");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        // 商材カテゴリを取得
        $this->getProductCategory();

        // 一覧表示
        $this->getData($id);
    }

    public function api($id = null) {

        if ($this->request->is('get')) {
        }

        if ($this->request->is('post')) {

            if ($id == 'input_table') {
                // input table登録
                $productCategory = $this->request->data['productCategory'];

                // 最新のtable mapsを取得
                $tableMaps = $this->ProductCsvTableMap->find('first', array(
                    'conditions' => array(
                        'product_category_id' => $productCategory,
                        'disabled_flg' => 0
                    ),
                    'fields' => 'MAX(start_month) as max_start_month'
                ));

                // 登録対象のtable mapsを取得
                $params = array(
                    'conditions' => array(
                        'ProductCsvTableMap.product_category_id' => $productCategory,
                        'ProductCsvTableMap.start_month' => $tableMaps[0]['max_start_month'],
                        'ProductCsvTableMap.disabled_flg' => 0
                    ),
                    'fields' => array('ProductCsvColumnMap.product_input_column_name',
                        'ProductCsvColumnMap.product_csv_column_name',
                        'ProductCsvTableMap.product_input_table_name',
                        'ProductCsvTableMap.common_key_columns'),
                    'joins' => array(
                        array(
                            'type' => 'INNER',
                            'table' => 'product_csv_column_maps',
                            'alias' => 'ProductCsvColumnMap',
                            'conditions' => array('ProductCsvColumnMap.table_map_id = ProductCsvTableMap.id')
                        ),

                    ),
                    'order' => array('ProductCsvColumnMap.id' => 'asc')
                );

                $data = $this->ProductCsvTableMap->find('all', $params);

                if ($data) {

                    // アップロードファイルを取得
                    $csvFile = $this->request->params['form']['csvFile'];

                    // アップロードファイルエラーチェック
                    $this->checkUploadError($csvFile);

                    if ($csvFile['size'] <= 0) {
                        $this->response->statusCode(400);
                        return 'CSVファイルが不正です。';
                    }

                    $tmpName = $csvFile['tmp_name'];

                    $this->loadModel('CsvParser');

                    // CSVデータを取得
                    $csvHeader = $this->CsvParser->getCsvHeader($tmpName);
                    $csvData = $this->CsvParser->getCsvData($tmpName);

                    list($inputTableName, $commonKeyColumn) = '';
                    list($inputColumn, $csvColumn, $csvHeaderRow) = array(array(), array(), array());
                    $necessaryIdx = array();
                    for ($i = 0; $i < count($data); $i++) {
                        $inputTableName = $data[$i]['ProductCsvTableMap']['product_input_table_name'];
                        $commonKeyColumn = $data[$i]['ProductCsvTableMap']['common_key_columns'];

                        $inputColumnName = $data[$i]['ProductCsvColumnMap']['product_input_column_name'];
                        array_push($inputColumn, $inputColumnName);

                        $csvColumnName = $data[$i]['ProductCsvColumnMap']['product_csv_column_name'];
                        array_push($csvColumn, $csvColumnName);

                        // CSV実データのうち、必要なカラム
                        for ($j = 0; $j < count($csvHeader); $j++) {
                            if ($csvHeader[$j] == $csvColumnName) {
                                array_push($necessaryIdx, $j);
                                array_push($csvHeaderRow, $csvHeader[$j]);
                                break;
                            }
                        }
                    }

                    // ユニークキーカラムを配列にする
                    $commonKeyColumns = explode(',', $commonKeyColumn);

                    // 必要な実データをコピーする
                    $csvRows = array();
                    for ($i = 0; $i < count($csvData); $i++) {
                        $csvRow = array();
                        for ($j = 0; $j < count($csvData[$i]); $j++) {
                            for ($k = 0; $k < count($necessaryIdx); $k++) {
                                // 必要なカラムであるかチェック
                                if ($necessaryIdx[$k] != $j) {
                                    continue;
                                }
                                array_push($csvRow, $csvData[$i][$j]);
                            }
                        }
                        array_push($csvRows, $csvRow);
                    }

                    // ユニークキーカラムのインデックスを取得
                    $necessaryIdx = array();
                    for ($i = 0; $i < count($commonKeyColumns); $i++) {
                        for ($j = 0; $j < count($csvHeaderRow); $j++) {
                            if ($commonKeyColumns[$i] == $csvHeaderRow[$j]) {
                                $this->debug('一致！' . $commonKeyColumns[$i] . 'と' . $csvHeaderRow[$j]);
                                array_push($necessaryIdx, $j);
                                break;
                            }
                        }
                    }

                    // ユニークキーカラムの値を取得
                    $commonKeyRow = array();
                    for ($i = 0; $i < count($csvRows); $i++) {
                        $commonKey = '';
                        for ($j = 0; $j < count($necessaryIdx); $j++) {
                            $commonKey .= $csvRows[$i][$j] . ',';
                        }
                        array_push($commonKeyRow, $this->deleteLastComma($commonKey));
                    }

                    // 登録対象年月以外の計上日は除外
                    $month = $this->request->data('month');
                    $recordedDateNames = $this->getRecordedDate($productCategory);

                    $tmpCsvRows = array();
                    $tmpCommonKeyRow = array();
                    foreach ($csvRows as $index => $csvRow) {
                        foreach ($recordedDateNames as $recordedDateName) {
                            $idx = array_search($recordedDateName, $csvHeader);

                            if (!is_numeric($idx)) {
                                continue;
                            }
                            $dateTime = new DateTime($csvRow[$idx]);

                            if ($dateTime->format('Ym') == $month) {
                                $tmpCsvRows[] = $csvRow;
                                $tmpCommonKeyRow[] = $commonKeyRow[$index];
                                break;
                            }
                        }
                    }
                    $csvRows = $tmpCsvRows;
                    $commonKeyRow = $tmpCommonKeyRow;

                    try {
                        // ユニークキーが設定されている場合は、更新の可能性あり（delete-insertとする）
                        $this->selectDelete($commonKeyRow, $inputTableName);

                        // レコード登録
                        $this->insertInputTable($commonKeyRow, $inputTableName, $inputColumn, $csvRows);

                        //獲得データ集計
                        $this->Acquisition->aggregate($inputTableName, $month);

                        // インセンティブ計算
                        // $this->StaffIncentive->calcIncentive($month);
                        exec('php ' . APP . 'Console' . DS . 'cake.php incentive_calc ' . $month . ' > /dev/null &');

                    } catch (Exception $e) {
                        $this->log($e->getTraceAsString());
                        $this->response->statusCode(500);
                        return $e->getMessage();
                    }

                    $this->responseSuccess();
                }
            }
        }

        if ($this->request->is('delete')) {

            // 削除対象のIDと商材カテゴリIDを取得
            $id = $this->request->data('id');
            $productCategoryId = $this->request->data['productCategoryId'];

            // 最新のtable mapsを取得
            $tableMaps = $this->ProductCsvTableMap->find('first', array(
                'conditions' => array(
                    'product_category_id' => $productCategoryId,
                    'disabled_flg' => 0
                ),
                'fields' => 'MAX(start_month) as max_start_month'
            ));

            // 登録対象のtable mapsを取得
            $params = array(
                'conditions' => array(
                    'ProductCsvTableMap.product_category_id' => $productCategoryId,
                    'ProductCsvTableMap.start_month' => $tableMaps[0]['max_start_month'],
                    'ProductCsvTableMap.disabled_flg' => 0
                ),
                'fields' => array(
                    'ProductCsvTableMap.product_input_table_name'
                )
            );

            $productCsvTableMap = $this->ProductCsvTableMap->find('first', $params);

            $tableName = $productCsvTableMap['ProductCsvTableMap']['product_input_table_name'];

            // IDの最後のカンマを削除
            $id = $this->deleteLastComma($id);

            // 削除
            try {

                $this->delete($tableName, $id);

            } catch (Exception $e) {
                $this->log($e->getTraceAsString());
                $this->response->statusCode(500);
                return $e->getMessage();
            }

            $this->responseSuccess();
        }
    }

    public function getPdo() {

        $host = '127.0.0.1';
        $user = 'widsadmin';
        $password = 'wids5';
        $dbName = 'backoffice_widsley';

        // エンコーディング設定
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );
        // データベースに接続
        $pdo = new PDO(
            "mysql:host=$host;dbname=$dbName",
            $user,
            $password,
            $options
        );

        return $pdo;
    }

    public function selectDelete($commonKeyRow, $tableName) {

        if (!$commonKeyRow) return;

        try {
            $pdo = $this->getPdo();

            $registKey = '';
            for ($i = 0; $i < count($commonKeyRow); $i++) {
                $registKey .= '\'' . $commonKeyRow[$i] . '\',';
            }
            $registKey = $this->deleteLastComma($registKey);

            // データを取得
            $query = 'select id from ' . $tableName . ' where regist_key in (' . $registKey . ');';


            foreach ($pdo->query($query) as $row) {
                $id = $row['id'];

                $query = 'delete from ' . $tableName . ' where id=' . $id;
                $pdo->query($query);
            }

        } catch (PDOException $e) {
            throw new InternalErrorException($e->getMessage());
        }
    }

    public function insertInputTable($commonKeyRow, $tableName, $inputColumn, $csvRows) {

        try {
            $pdo = $this->getPdo();

            $sqlInputColumn = '';
            for ($i = 0; $i < count($inputColumn); $i++) {
                $sqlInputColumn .= $inputColumn[$i] . ', ';
            }
            $sqlInputColumn = $this->deleteLastComma($sqlInputColumn);

            for ($i = 0; $i < count($csvRows); $i++) {
                $insertData = '';
                for ($j = 0; $j < count($csvRows[$i]); $j++) {
                    $insertData .= '\'' . $csvRows[$i][$j] . '\',';
                }
                $insertData = $this->deleteLastComma($insertData);

                // 登録
                $insert = 'insert into ' . $tableName . ' (regist_key, ' . $sqlInputColumn . ') values (\'' . $commonKeyRow[$i] . '\',' . $insertData . ');';
                $flag = $pdo->query($insert);

                if (!$flag) {
                    throw new InternalErrorException($tableName . 'へのinsertに失敗しました。');
                }
            }

        } catch (PDOException $e) {
            throw new InternalErrorException($e->getMessage());
        }
    }

    public function selectInputTable($tableName, $offset, $limit) {

        $pdo = $this->getPdo();
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        $sql = 'select * from ' . $tableName . ' LIMIT :offset, :limit;';

        $this->debug($sql . ' [offset=' . $offset . ', limit=' . $limit . ']');

        $stmt = $pdo->prepare($sql);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll();

    }

    public function selectCount($tableName) {

        $pdo = $this->getPdo();

        $sql = 'select count(*) as count from ' . $tableName;

        $stmt = $pdo->query($sql);

        $count = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $count[0]['count'];

    }

    function deleteLastComma($value) {
        $idx = strrpos($value, ',');
        if ($idx) {
            return $value = substr($value, 0, $idx);
        }
        return $value;
    }

    function getProductCategory() {
        $this->debug('商材カテゴリを取得');
        $data = $this->ProductCategory->find('all');

        //if (!$data) throw new NotFoundException('商材カテゴリが見つかりません');

        $this->set('productCategories', $data);
    }

    function getData($id) {

        if (!$id) {
            $data = $this->ProductCategory->find('first');
            if($data){
                $id = $data['ProductCategory']['id'];
            }
        }

        // 最新のtable mapsを取得
        $tableMaps = $this->ProductCsvTableMap->find('first', array(
            'conditions' => array(
                'product_category_id' => $id,
                'disabled_flg' => 0
            ),
            'fields' => 'MAX(start_month) as max_start_month'
        ));

        // table mapsが見つからなければ処理終了
        if (!$tableMaps[0]['max_start_month']) {
            $this->set('selected', $id);
            $this->set('count', 0);
            $this->set('offset', 0);
            $this->set('limit', 0);
            $this->set('page', 0);
            $this->set('totalPage', 0);
            return;
        }

        $params = array(
            'conditions' => array(
                'ProductCsvTableMap.product_category_id' => $id,
                'ProductCsvTableMap.start_month' => $tableMaps[0]['max_start_month'],
                'ProductCsvTableMap.disabled_flg' => 0
            ),
            'fields' => array(
                'ProductCsvTableMap.id',
                'ProductCsvTableMap.product_input_table_name',
            )
        );

        $data = $this->ProductCsvTableMap->find('first', $params);

        $tableMapId = $data['ProductCsvTableMap']['id'];
        $params = array(
            'conditions' => array(
                'ProductCsvColumnMap.table_map_id' => $tableMapId
            ),
            'fields' => array(
                'ProductCsvColumnMap.product_input_column_name',
                'ProductCsvColumnMap.product_csv_column_name'
            )
        );

        $columnMapData = $this->ProductCsvColumnMap->find('all', $params);

        $inputTableName = $data['ProductCsvTableMap']['product_input_table_name'];

        // オフセット、リミット取得
        $offset = $limit = null;
        if (isset($this->request->query['offset'])) {
            $offset = $this->request->query['offset'];
        } else {
            $offset = 0;
        }
        if (isset($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        } else {
            $limit = 100;
        }
        // データと件数を取得
        $inputTableData = $this->selectInputTable($inputTableName, $offset, $limit);
        $count = $this->selectCount($inputTableName);

        if ($inputTableData) {
            $this->set('inputTableData', $inputTableData);
            $this->set('columnMapData', $columnMapData);
            $this->set('count', $count);
            $this->set('offset', $offset);
            $this->set('limit', $limit);
            $this->set('page', floor(($offset + $limit) / $limit));
            $this->set('totalPage', ceil($count / $limit));
        } else {
            $this->set('count', 0);
            $this->set('offset', $offset);
            $this->set('limit', $limit);
            $this->set('page', 0);
            $this->set('totalPage', 0);
        }
        $this->set('selected', $id);

    }

    function checkUploadError($csvFile)
    {

        $messages = null;
        switch ($csvFile['error']) {
            case UPLOAD_ERR_OK:
                //値: 0; この場合のみ、ファイルあり
                break;

            case UPLOAD_ERR_INI_SIZE:
                //値: 1; アップロードされたファイルは、php.ini の upload_max_filesize ディレクティブの値を超えています（post_max_size, upload_max_filesize）
                $messages = 'アップロードされたファイルが大きすぎます。' . ini_get('upload_max_filesize') . '以下のファイルをアップロードしてください。';
                break;

            case UPLOAD_ERR_FORM_SIZE:
                //値: 2; アップロードされたファイルは、HTML フォームで指定された MAX_FILE_SIZE を超えています。
                $messages = 'アップロードされたファイルが大きすぎます。' . ($_POST['MAX_FILE_SIZE'] / 1000) . 'KB以下のファイルをアップロードしてください。';
                break;

            case UPLOAD_ERR_PARTIAL:
                //値: 3; アップロードされたファイルは一部のみしかアップロードされていません。
                $messages = 'アップロードに失敗しています（通信エラー）。もう一度アップロードをお試しください。';
                break;

            case UPLOAD_ERR_NO_FILE:
                //値: 4; ファイルはアップロードされませんでした。（この場合のみ、ファイルがないことを表している）
                //$messages = 'ファイルをアップロードしてください';
                break;

            case UPLOAD_ERR_NO_TMP_DIR:
                //値: 6; テンポラリフォルダがありません。PHP 4.3.10 と PHP 5.0.3 で導入されました。
                $messages = 'アップロードに失敗しています（システムエラー）。もう一度アップロードをお試しください。';
                break;

            default:
                //UPLOAD_ERR_CANT_WRITE 値: 7; ディスクへの書き込みに失敗しました。PHP 5.1.0 で導入されました。
                //UPLOAD_ERR_EXTENSION 値: 8; ファイルのアップロードが拡張モジュールによって停止されました。 PHP 5.2.0 で導入されました。
                //何かおかしい
                $messages = 'アップロードファイルをご確認ください。';
                break;
        }

        if ($messages) {
            $this->log($messages);
            throw new BadRequestException($messages);
        }
    }

    function getRecordedDate($productCategoryId) {

        // 最新のtable mapsを取得
        $tableMaps = $this->ProductCsvTableMap->find('first', array(
            'conditions' => array(
                'product_category_id' => $productCategoryId,
                'disabled_flg' => 0
            ),
            'fields' => array(
                'id', 'MAX(start_month) as max_start_month'
            )
        ));
        $tableMapId =$tableMaps['ProductCsvTableMap']['id'];

        $countConditions = $this->CountCondition->find('all', array(
            'conditions' => array(
                'table_map_id' => $tableMapId,
                'product_category_id' => $productCategoryId
            )
        ));

        // 計上日のカラム名を取得
        $csvColumnNames = array();
        foreach ($countConditions as $countCondition) {
            $dateColumnName = $countCondition['CountCondition']['date_column_name'];

            $columnMaps = $this->ProductCsvColumnMap->find('first', array(
                'conditions' => array(
                    'table_map_id' => $tableMapId,
                    'product_input_column_name' => $dateColumnName
                )
            ));
            $csvColumnNames[] = $columnMaps['ProductCsvColumnMap']['product_csv_column_name'];
        }

        return $csvColumnNames;
    }

    function delete($tableName, $id) {

        try {
            $pdo = $this->getPdo();

            $query = 'delete from ' . $tableName . ' where id in (' . $id .')';
            $pdo->query($query);

        } catch (PDOException $e) {
            throw new InternalErrorException($e->getMessage());
        }
    }

    function debug($message)
    {
        $this->log($message, LOG_DEBUG);
    }



}
