<?php
App::uses('AuthController', 'Controller');

class ProductsController extends AuthController {

    public $uses = array('ProductCategory', 'Product');

    public function index($yyyymm = null){
        $this->set("header", "商材情報");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $products = $this->Product->getList($yyyymm);
        $this->set("products", $products);
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));
    }

    public function api($id = null){
        if($this->request->is('get')){
            if(!$id){
                // 一覧
                $products = $this->Product->getLatestList();

                if($products){
                    $this->responseJson($products);
                }
                else{
                    $this->responseFailure();
                }
            } else {
                // 個別取得
                $product = $this->ProductCategory->findById($id);
                if($product){
                    $this->responseJson($product);
                }
                else{
                    $this->responseFailure();
                }
            }
        }
    }
}
