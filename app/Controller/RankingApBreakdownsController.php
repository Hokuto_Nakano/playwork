<?php
App::uses('AuthController', 'Controller');

class RankingApBreakdownsController extends AuthController {

    public $uses = array('ProductCategory', 'Staff', 'StaffCountUp', 'Division', 'CountItem', 'OutputJson');

    private $key = "RankingApBreakdowns";

    public function index($yyyymm = null, $divisionId = 0, $isUpdate = 0){
        $this->set("header", "アポラン（内訳）");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $outputJson = $this->OutputJson->findByKeyAndMonthAndDivisionId($this->key, $yyyymm, $divisionId);

        if(!$isUpdate && $outputJson){
            $json = $outputJson["OutputJson"]["json"];
            $data = json_decode($json, true);
            $modified = $outputJson["OutputJson"]["modified"];
        }
        else{
            $data = $this->getData($yyyymm, $divisionId);
            $json = json_encode($data);
            $newData = array(
                "key" => $this->key,
                "month" => $yyyymm,
                "division_id" => $divisionId,
                "json" => $json
            );
            if($outputJson){
                $newData["id"] = $outputJson["OutputJson"]["id"];
            }

            if($this->OutputJson->save($newData)){
                $id = $this->OutputJson->getLastInsertID();
                $id = $id ? $id : $newData["id"];
                $updateData = $this->OutputJson->findById($id);
                $modified = $updateData["OutputJson"]["modified"];
            }
        }

        $this->set('productCategories', $data['productCategories']);
        $this->set('staffs', $data['staffs']);
        $this->set('divisions', $data['divisions']);
        $this->set('divisionId', $data['divisionId']);
        $this->set('colspan', $data['colspan']);
        $this->set("modified", date("Y/m/d H:i", strtotime($modified)));
    }

    public function excel($yyyymm = null, $divisionId = 0){
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $book = new PHPExcel();
        $book->getActiveSheet()->setTitle('アポラン（内訳）');
        $sheet = $book->getActiveSheet();

        $data = $this->getData($yyyymm, $divisionId);

        // ヘッダー共通
        $headerCommon = array(
            '社員番号', '名前', '階級', '事業部', '合計BP'
        );
        foreach ($headerCommon as $colNum => $header) {
            $sheet->setCellValueByColumnAndRow($colNum, 1, $header);

            // セルを結合
            $sheet->mergeCellsByColumnAndRow($colNum, 1, $colNum, 3);

            // 垂直方向を中央揃え
            $sheet->getStyleByColumnAndRow($colNum, 1)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        }

        // ヘッダー
        $colNum = count($headerCommon);
        $acquisitionCol = 0;
        $pointCol = 0;
        foreach ($data['productCategories'] as $productCategory) {
            // ヘッダー最上部
            $sheet->setCellValueByColumnAndRow($colNum + $acquisitionCol, 1, $productCategory['name']);

            // ヘッダー中部
            foreach ($productCategory['acquisitionType'] as $acquisitionType) {
                $sheet->setCellValueByColumnAndRow($colNum + $acquisitionCol, 2, $acquisitionType);
                $acquisitionCol++;
            }

            // ヘッダー最下部
            foreach ($productCategory['point'] as $point)  {
                $sheet->setCellValueByColumnAndRow($colNum + $pointCol, 3, $point.'BP');
                $pointCol++;
            }
        }

        foreach ($data["staffs"] as $rowNum => $staff) {
            $colNum=0;
            $sheet->setCellValueExplicitByColumnAndRow($colNum++, $rowNum+4, $staff['staffNo'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4, $staff['staffName']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4, $staff['jobType']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4, $staff['division']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4, $staff['totalBp']);

            foreach ($staff['count'] as $count) {
                foreach ($count as $totalCount) {
                    $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4, $totalCount);
                }
            }
        }

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="ranking_ap_breakdowns.xlsx"');

        $writer = PHPExcel_IOFactory::createWriter($book, 'Excel2007');
        $writer->save('php://output');
        exit;
    }

    function getData($startMonth, $divisionId) {

        // 前確と後確のcount_item_idを取得
//        $itemName = '獲得件数';
        $maekakuId = $this->CountItem->getMaekakuId();
        $atokakuId = $this->CountItem->getAtokakuId();

        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        $staffs = $this->Staff->getRankingApStaffs($startMonth, $divisionId);

        foreach ($staffs as $idx => $staff) {
            $totalCounts = array();
            $totalBp = 0;
            foreach ($productCategories as $productCategory) {
                $totalCount = array();
                foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                    $data = $this->StaffCountUp->getAcquisitionCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth, $maekakuId, $atokakuId);
                    $totalCount[] = $data['totalCount'];
                    $totalBp += $data['bp'];
                }
                $totalCounts[] = $totalCount;
            }
            $staffs[$idx]['totalBp'] = $totalBp;
            $staffs[$idx]['count'] = $totalCounts;
        }

        // 事業部
        $divisions = $this->Division->getAllSimpleDivisions();

        // データはありません
        $colspan = 0;
        foreach ($productCategories as $productCategory) {
            $colspan += count($productCategory['acquisitionType']);
        }

        $result = array(
            'productCategories' => $productCategories,
            'staffs' => $staffs,
            'divisions' => $divisions,
            'divisionId' => $divisionId,
            'colspan' => $colspan + 5
        );

        return $result;
    }
}
