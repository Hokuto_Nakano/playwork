<?php
App::uses('AuthController', 'Controller');

class ReportMonthliesController extends AuthController {

    public $uses = array('ProductCategory', 'StaffHistory', 'StaffCountUp', 'Division', 'CountItem', 'Goal', 'OutputJson');

    private $key = "ReportMonthlies";

    public function index($yyyymm = null, $divisionId = 0, $isUpdate = 0){
        $this->set("header", "月報");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $outputJson = $this->OutputJson->findByKeyAndMonthAndDivisionId($this->key, $yyyymm, $divisionId);

        if(!$isUpdate && $outputJson){
            $json = $outputJson["OutputJson"]["json"];
            $data = json_decode($json, true);
            $modified = $outputJson["OutputJson"]["modified"];
        }
        else{
            $data = $this->getData($yyyymm, $divisionId);
            $json = json_encode($data);
            $newData = array(
                "key" => $this->key,
                "month" => $yyyymm,
                "division_id" => $divisionId,
                "json" => $json
            );
            if($outputJson){
                $newData["id"] = $outputJson["OutputJson"]["id"];
            }

            if($this->OutputJson->save($newData)){
                $id = $this->OutputJson->getLastInsertID();
                $id = $id ? $id : $newData["id"];
                $updateData = $this->OutputJson->findById($id);
                $modified = $updateData["OutputJson"]["modified"];
            }
        }

        $this->set('productCategories', $data['productCategories']);
        $this->set('goals', $data['goals']);
        $this->set('divisions', $data['divisions']);
        $this->set('divisionId', $data['divisionId']);
        $this->set('colspan', $data['colspan']);
        $this->set('totalDivisionData', $data['totalDivisionData']);
        $this->set('totalAreaData', $data['totalAreaData']);
        $this->set("modified", date("Y/m/d H:i", strtotime($modified)));
    }

    public function excel($yyyymm = null, $divisionId = 0){
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $book = new PHPExcel();
        $book->getActiveSheet()->setTitle('月報');
        $sheet = $book->getActiveSheet();

        $data = $this->getData($yyyymm, $divisionId);

        // ヘッダー共通
        $headerCommon = array(
            '事業部', 'エリア', '部署', 'BP', '人数', 'BP平均値', '獲得件数', '件数平均値', 'BP内訳'
        );
        $colIdx = 0;
        foreach ($headerCommon as $colNum => $header) {
            $sheet->setCellValueByColumnAndRow($colNum + $colIdx, 1, $header);

            if ($colNum == 3) {
                $sheet->setCellValueByColumnAndRow($colNum + $colIdx++, 3, '目標');
                $sheet->setCellValueByColumnAndRow($colNum + $colIdx++, 3, '実績');
                $sheet->setCellValueByColumnAndRow($colNum + $colIdx, 3, '達成率');
                // セルを結合
                $sheet->mergeCellsByColumnAndRow($colNum, 1, $colNum + $colIdx, 2);
                // 垂直方向を中央揃え
                $sheet->getStyleByColumnAndRow($colNum, 1)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            } else if ($colNum < 8) {
                // セルを結合
                $sheet->mergeCellsByColumnAndRow($colNum + $colIdx, 1, $colNum + $colIdx, 3);

                // 垂直方向を中央揃え
                $sheet->getStyleByColumnAndRow($colNum + $colIdx, 1)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            }
        }

        // ヘッダー
        $colNum = count($headerCommon) - 1;
        $acquisitionTypeCol = 0;
        foreach ($data['productCategories'] as $productCategory) {
            // ヘッダー中部
            $sheet->setCellValueByColumnAndRow($colNum + $colIdx + $acquisitionTypeCol, 2, $productCategory['name']);

            // ヘッダー最下部
            foreach ($productCategory['acquisitionType'] as $acquisitionType) {
                $sheet->setCellValueByColumnAndRow($colNum + $colIdx + $acquisitionTypeCol, 3, $acquisitionType);
                $acquisitionTypeCol++;
            }
        }

        $divisionIdx = 0;
        $areaIdx = 0;

        $divisionRow = 0;
        $areaRow = 0;
        foreach ($data["goals"] as $rowNum => $goal) {
            $colNum=0;
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['divisionName']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['areaName']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['sectionName']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['goalBp'].'BP');
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['totalBp'].'BP');
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['achievementRate'].'%');
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['staffCount']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['averageBp']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['allCount'].'件');
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $goal['averageCount']);

            foreach ($goal['totalCount'] as $count) {
                foreach ($count as $totalCount) {
                    $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+4 + $areaRow + $divisionRow, $totalCount);
                }
            }

            // エリア出力
            if ($rowNum == count($data["goals"])-1 || ($data["goals"][$rowNum+1]['areaId'] != $goal['areaId']) ) {

                $areaRow++;
                $areaNum = 0;
                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalAreaData'][$areaIdx]['divisionName']);
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalAreaData'][$areaIdx]['areaName']);
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, '');
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalAreaData'][$areaIdx]['goalBp'].'BP');
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalAreaData'][$areaIdx]['totalBp'].'BP');
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalAreaData'][$areaIdx]['achievementRate'].'%');
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalAreaData'][$areaIdx]['staffCount']);
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalAreaData'][$areaIdx]['averageBp']);
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalAreaData'][$areaIdx]['allCount'].'件');
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalAreaData'][$areaIdx]['averageCount']);
                $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                foreach ($data['totalAreaData'][$areaIdx]['totalCount'] as $count) {
                    foreach ($count as $totalCount) {
                        $sheet->setCellValueByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow, $totalCount);
                        $sheet->getStyleByColumnAndRow($areaNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f5f5f5');
                        $sheet->getStyleByColumnAndRow($areaNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);
                    }
                }
                $areaIdx++;
            }

            // 事業部出力
            if ($rowNum == count($data["goals"])-1 || ($data["goals"][$rowNum+1]['divisionId'] != $goal['divisionId']) ) {

                $divisionRow++;
                $divisionNum = 0;
                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalDivisionData'][$divisionIdx]['divisionName']);
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, '');
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, '');
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalDivisionData'][$divisionIdx]['goalBp'].'BP');
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalDivisionData'][$divisionIdx]['totalBp'].'BP');
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalDivisionData'][$divisionIdx]['achievementRate'].'%');
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalDivisionData'][$divisionIdx]['staffCount']);
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalDivisionData'][$divisionIdx]['averageBp']);
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalDivisionData'][$divisionIdx]['allCount'].'件');
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, $data['totalDivisionData'][$divisionIdx]['averageCount']);
                $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);

                foreach ($data['totalDivisionData'][$divisionIdx]['totalCount'] as $count) {
                    foreach ($count as $totalCount) {
                        $sheet->setCellValueByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow, $totalCount);
                        $sheet->getStyleByColumnAndRow($divisionNum, $rowNum+4 + $areaRow + $divisionRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9edf7');
                        $sheet->getStyleByColumnAndRow($divisionNum++, $rowNum+4 + $areaRow + $divisionRow)->getFont()->setBold(true);
                    }
                }
                $divisionIdx++;
            }
        }

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="report_monthlies.xlsx"');

        $writer = PHPExcel_IOFactory::createWriter($book, 'Excel2007');
        $writer->save('php://output');
        exit;
    }

    function getData($startMonth, $divisionId) {

        // 前確と後確のcount_item_idを取得
//        $itemName = '獲得件数';
        $maekakuId = $this->CountItem->getMaekakuId();
        $atokakuId = $this->CountItem->getAtokakuId();

        // 商材カテゴリ
        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        // 目標値
        $goals = $this->Goal->getReportMonthly($startMonth, $divisionId);

        foreach ($goals as $idx => $goal) {
            $totalCounts = array();
            $totalBp = 0;
            $allCount = 0;
            foreach ($productCategories as $productCategory) {
                $totalCount = array();
                foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                    //$data = $this->StaffCountUp->getTotalSectionCount($startMonth, $goal['sectionId'], $productCategory['id'], $acquisitionTypeId, $countItemId);
                    $data = $this->StaffCountUp->getAcquisitionSectionCount($startMonth, $goal['sectionId'], $productCategory['id'], $acquisitionTypeId, $maekakuId, $atokakuId);
                    $totalCount[] = $data['count'];
                    $allCount += $data['count'];
                    $totalBp += $data['bp'];
                }
                $totalCounts[] = $totalCount;
            }

            // 所属人数を取得
            $staffCount = $this->StaffHistory->getStaffCount($startMonth, $goal['sectionId']);
            $goals[$idx]['staffCount'] = $staffCount;

            // 実績BP
            $goals[$idx]['totalBp'] = $totalBp;

            // 商材ごとの獲得件数
            $goals[$idx]['totalCount'] = $totalCounts;

            // 全ての獲得件数
            $goals[$idx]['allCount'] = $allCount;

            // BP平均値
            $goals[$idx]['averageBp'] = 0;
            if ($totalBp && $staffCount) {
                $goals[$idx]['averageBp'] = round($totalBp / $staffCount, 1);
            }

            // 件数平均値
            $goals[$idx]['averageCount'] = 0;
            if ($totalBp && $allCount) {
                $goals[$idx]['averageCount'] = round($totalBp / $allCount, 1);
            }

            // 達成率 = 実績BP/(目標BP/総稼働日数*経過日数)
            $goals[$idx]['achievementRate'] = 0;
            if ($goal['goalBp']) {
                $goals[$idx]['achievementRate'] = round(($totalBp / ($goal['goalBp'] / date('t') * date('d'))) * 100, 1);
            }

        }

        // 事業部・エリアごとの合計・平均
        $totalDivisionData = $this->getTotalDivisionData($goals, $productCategories);
        $totalAreaData = $this->getTotalAreaData($goals, $productCategories);

        // 事業部
        $divisions = $this->Division->getAllSimpleDivisions();

        $colspan = 0;
        foreach ($productCategories as $productCategory) {
            $colspan += count($productCategory['acquisitionType']);
        }

        $data = array(
            'productCategories' => $productCategories,
            'goals' => $goals,
            'divisions' => $divisions,
            'divisionId' => $divisionId,
            'colspan' => $colspan,
            'totalDivisionData' => $totalDivisionData,
            'totalAreaData' => $totalAreaData
        );

        return $data;
    }

    function getTotalAreaData($goals, $productCategories) {

        $areas = $this->grouping($goals, 'areaId');
        $data = array();

        foreach ($areas as $areaId => $sections) {
            $goalBp = $totalBp = $achievementRate = $staffCount = $averageBp = $allCount = $averageCount = $count = 0;

            // BP内訳の空配列作成
            $counts = array();
            foreach ($productCategories as $productCategory) {
                $count = array();
                foreach ($productCategory['acquisitionType'] as $acquisitionType) {
                    $count[] = 0;
                }
                $counts[] = $count;
            }

            foreach ($sections as $section) {
                $divisionId = $section['divisionId'];
                $divisionName = $section['divisionName'];
                $areaName = $section['areaName'];
                $goalBp += $section['goalBp'];
                $totalBp += $section['totalBp'];
                $achievementRate += $section['achievementRate'];

                $staffCount += $section['staffCount'];
                $averageBp += $section['averageBp'];
                $allCount += $section['allCount'];
                $averageCount += $section['averageCount'];

                // BP内訳
                foreach ($section['totalCount'] as $i => $totalCounts) {
                    foreach ($totalCounts as $j => $totalCount) {
                        $counts[$i][$j] = $counts[$i][$j] + $totalCount;
                    }
                }
            }

            $achievementRate = round($achievementRate / count($sections), 1);
            $averageBp = round($averageBp / count($sections), 1);
            $averageCount = round($averageCount / count($sections), 1);

            $data[] = array(
                'divisionId' => $divisionId,
                'divisionName' => $divisionName,
                'areaId' => $areaId,
                'areaName' => $areaName,
                'goalBp' => $goalBp,
                'totalBp' => $totalBp,
                'achievementRate' => $achievementRate,
                'staffCount' => $staffCount,
                'averageBp' => $averageBp,
                'allCount' => $allCount,
                'averageCount' => $averageCount,
                'totalCount' => $counts
            );
        }
        return $data;
    }

    function getTotalDivisionData($goals, $productCategories) {

        $divisions = $this->grouping($goals, 'divisionId');
        $data = array();

        foreach ($divisions as $divisionId => $sections) {
            $goalBp = $totalBp = $achievementRate = $staffCount = $averageBp = $allCount = $averageCount = $count = 0;

            // BP内訳の空配列作成
            $counts = array();
            foreach ($productCategories as $productCategory) {
                $count = array();
                foreach ($productCategory['acquisitionType'] as $acquisitionType) {
                    $count[] = 0;
                }
                $counts[] = $count;
            }

            foreach ($sections as $section) {
                $divisionId = $section['divisionId'];
                $divisionName = $section['divisionName'];
                $goalBp += $section['goalBp'];
                $totalBp += $section['totalBp'];
                $achievementRate += $section['achievementRate'];

                $staffCount += $section['staffCount'];
                $averageBp += $section['averageBp'];
                $allCount += $section['allCount'];
                $averageCount += $section['averageCount'];

                // BP内訳
                foreach ($section['totalCount'] as $i => $totalCounts) {
                    foreach ($totalCounts as $j => $totalCount) {
                        $counts[$i][$j] = $counts[$i][$j] + $totalCount;
                    }
                }
            }

            $achievementRate = round($achievementRate / count($sections), 1);
            $averageBp = round($averageBp / count($sections), 1);
            $averageCount = round($averageCount / count($sections), 1);

            $data[] = array(
                'divisionId' => $divisionId,
                'divisionName' => $divisionName,
                'goalBp' => $goalBp,
                'totalBp' => $totalBp,
                'achievementRate' => $achievementRate,
                'staffCount' => $staffCount,
                'averageBp' => $averageBp,
                'allCount' => $allCount,
                'averageCount' => $averageCount,
                'totalCount' => $counts
            );
        }

        return $data;

    }

    function grouping($array, $key) {
        $data = array();

        foreach($array as $value) {
            $group = $value[$key];

            if (!isset($data[$group])) {
                $data[$group] = array();
            }

            $data[$group][] = $value;
        }

        return $data;
    }
}
