<?php
App::uses('AuthController', 'Controller');

class ChatController extends AuthController {

    public $uses = array('Staff', 'StaffHistory');

    function index(){
        // $this->layout = "";

        //チャットシステムのユーザー更新呼び出し
        //戻りのurlにリダイレクト
        $userId = $this->Session->read('userId');
        $user = $this->Staff->findById($userId);
        $profile = $this->StaffHistory->getProfile($userId, Util::GetThisMonth());
        $redirect_url = Util::RegistChatInfo($userId, $profile["staffName"], $profile["divisionName"], $user["Staff"]["login_id"], $user["Staff"]["password"]);
        if($redirect_url){
            $this->redirect($redirect_url);
        }
    }
}
