<?php
App::uses('AuthController', 'Controller');

class AttendancesController extends AuthController
{

    public $uses = array('Attendance', 'AttendanceCsvMap', 'AttendanceCalc', 'Staff', 'StaffHistory', 'StaffIncentive');

    public function index($yyyymm = null){

        $this->set('header', '勤怠データ');

        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }

        $this->set('year', Util::GetYYYY($yyyymm));
        $this->set('month', Util::GetMM($yyyymm));

        // 一覧表示
        $this->getData($yyyymm);

    }

    public function api($id = null)
    {
        if ($this->request->is('get')) {

             if (!$id) {
                // 存在チェック
                $data = $this->AttendanceCalc->find('all');

                if ($data) {
                    $response = array('result' => 'OK', 'response' => $data);
                } else {
                    $response = array('result' => 'OK');
                }

                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            }
        }
        // 登録
        if ($this->request->is('post')) {

            if ($id == 'reg'){

                // 年月
                $startMonth = $this->request->data('startMonth');

                // 勤怠データ登録
                $csvHeader = $this->request->data['csvHeader'];
                $csvData = $this->request->data['csvData'];

                // 勤怠設定取得
                $attendanceCalc = $this->AttendanceCalc->find('first');

                // 勤怠CSVマップ取得
                $AttendanceCsvMap = $this->AttendanceCsvMap->find('first');

                // ヘッダーを解析して勤怠データで取得すべきインデックスを取得
                list(
                    $attendanceIdIdx, $nameIdx, $dateIdx, $attendanceDateIdx, $leaveDateIdx, $attendanceTimeIdx,
                    $leaveTimeIdx, $attendanceDatetimeIdx, $leaveDatetimeIdx, $workHoursIdx, $breakHoursIdx,
                    $breakIntimeIdx, $breakOuttimeIdx
                    ) = null;
                for($i = 0; $i < count($csvHeader); $i++) {
                    // 外部ユーザーID
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['other_id_column']) {
                        $attendanceIdIdx = $i;
                    }
                    // 氏名
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['name_column']) {
                        $nameIdx = $i;
                    }
                    // 年月日
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['date_column']) {
                        $dateIdx = $i;
                    }
                    // 出勤日
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['attendance_date_column']) {
                        $attendanceDateIdx = $i;
                    }
                    // 退勤日
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['leave_date_column']) {
                        $leaveDateIdx = $i;
                    }
                    // 出勤時刻
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['attendance_time_column']) {
                        $attendanceTimeIdx = $i;
                    }
                    // 退勤時刻
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['leave_time_column']) {
                        $leaveTimeIdx = $i;
                    }
                    // 出勤日時
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['attendance_datetime_column']) {
                        $attendanceDatetimeIdx = $i;
                    }
                    // 退勤日時
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['leave_datetime_column']) {
                        $leaveDatetimeIdx = $i;
                    }
                    // 実働時間
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['work_hours_column']) {
                        $workHoursIdx = $i;
                    }
                    // 休憩時間
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['break_hours_column']) {
                        $breakHoursIdx = $i;
                    }
                    // 休憩入り
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['break_in_time_column']) {
                        $breakIntimeIdx = $i;
                    }
                    // 休憩戻り
                    if ($csvHeader[$i] == $AttendanceCsvMap['AttendanceCsvMap']['break_out_time_column']) {
                        $breakOuttimeIdx = $i;
                    }
                }

                list($attendanceMonth, $date, $attendanceDate, $attendanceTime,
                    $leaveTime, $attendanceDatetime, $leaveDatetime, $workHours, $breakHours, $breakIntime, $breakOuttime) = null;

                $errorMessages = array();

                // 勤怠データ登録
                for($i = 0; $i < count($csvData); $i++) {
                    // 外部ユーザーIDと氏名は必須
                    $attendanceId = $csvData[$i][$attendanceIdIdx];
                    $name = $csvData[$i][$nameIdx];

                    // スタッフ情報を取得
                    $staff = $this->Staff->findByOutsideAttendanceId($attendanceId);

                    // スタッフが存在しない場合エラーエッセージを追加
                    if (!$staff) {
                        $errorMessage = '対象の社員が存在しません。['.$AttendanceCsvMap['AttendanceCsvMap']['other_id_column'].'='. $attendanceId .', 氏名='. $name.']';
                        // エラーメッセージの重複はしないようにする
                        if (!is_numeric(array_search($errorMessage, $errorMessages))) {
                            $errorMessages[] = $errorMessage;
                        }
                        continue;
                    }

                    $staffId = $staff['Staff']['id'];

                    // 年月日
                    if (is_numeric($dateIdx)) {
                        $date = $csvData[$i][$dateIdx];
                    }
                    // 出勤日
                    if ($attendanceDateIdx) {
                        $attendanceDate = date('Ymd', strtotime($csvData[$i][$attendanceDateIdx]));
                    } else if ($date) {
                        $attendanceDate = date('Ymd', strtotime($date));
                    }

                    // 出勤時刻
                    if ($attendanceTimeIdx) {
                        if ($attendanceDateIdx) {
                            $attendanceTime = date('Y/m/d', strtotime($attendanceDate)).' '.$csvData[$i][$attendanceTimeIdx];
                        } else if ($date){
                            $attendanceTime = $date.' '.$csvData[$i][$attendanceTimeIdx];
                        }
                        // 計算後のカラムが選択されている場合、そのまま。
                        if (strpos($csvHeader[$attendanceTimeIdx], '計算後')) {
                            $attendanceDatetime = $attendanceTime;
                        }else{
                            // 計算後のカラムが選択されていない場合は計算
                            $attendanceDatetime = $this->getOutputAttendanceDatetime(
                                $attendanceTime,
                                $attendanceCalc['AttendanceCalc']['attendance_time_unit'],
                                $attendanceCalc['AttendanceCalc']['attendance_time_rounding']
                            );
                        }

                    }
                    // 退勤時刻
                    if ($leaveTimeIdx) {
                        if ($attendanceDateIdx) {
                            $leaveTime = date('Y/m/d', strtotime($attendanceDate)).' '.$csvData[$i][$leaveTimeIdx];
                        } else if($date) {
                            $leaveTime = $date.' '.$csvData[$i][$leaveTimeIdx];
                        }
                        // 計算後のカラムが選択されている場合、そのまま。
                        if (strpos($csvHeader[$leaveTimeIdx], '計算後')) {
                            $leaveDatetime = $leaveTime;
                        } else {
                            // 計算後のカラムが選択されていない場合は計算
                            $leaveDatetime = $this->getOutputLeaveDatetime(
                                $leaveTime,
                                $attendanceCalc['AttendanceCalc']['attendance_time_unit'],
                                $attendanceCalc['AttendanceCalc']['attendance_time_rounding'],
                                $attendanceCalc['AttendanceCalc']['max_leave_time']
                            );
                        }
                    }

                    // 出勤日時
                    if ($attendanceDatetimeIdx) {
                        $attendanceDatetime = $csvData[$i][$attendanceDatetimeIdx];
                        // 計算後のカラムが選択されている場合、そのまま。
                        if (strpos($csvHeader[$attendanceDatetimeIdx], '計算後')) {
                            $attendanceTime = $attendanceDatetime;
                        } else {
                            // 計算後のカラムが選択されていない場合は計算
                            $attendanceDatetime = $this->getOutputAttendanceDatetime(
                                $attendanceDatetime,
                                $attendanceCalc['AttendanceCalc']['attendance_time_unit'],
                                $attendanceCalc['AttendanceCalc']['attendance_time_rounding']
                            );
                        }
                    }

                    // 退勤日時
                    if ($leaveDatetimeIdx) {
                        $leaveDatetime = $csvData[$i][$leaveDatetimeIdx];
                        // 計算後のカラムが選択されている場合、そのまま。
                        if (strpos($csvHeader[$leaveDatetimeIdx], '計算後')) {
                            $leaveTime = $leaveDatetime;
                        } else {
                            // 計算後のカラムが選択されていない場合は計算
                            $leaveDatetime = $this->getOutputLeaveDatetime(
                                $leaveDatetime,
                                $attendanceCalc['AttendanceCalc']['leave_time_unit'],
                                $attendanceCalc['AttendanceCalc']['leave_time_rounding'],
                                $attendanceCalc['AttendanceCalc']['max_leave_time']
                            );
                         }
                    }

                    // 出勤月
                    if ($attendanceDate) {
                        $attendanceMonth = date('Ym', strtotime($attendanceDate));
                    } else if ($date) {
                        $attendanceMonth = date('Ym', strtotime($date));
                    }

                    // 休憩入り時間
                    if ($breakIntimeIdx) {
                        $breakIntime = $csvData[$i][$breakIntimeIdx];
                    }
                    // 休憩戻り時間
                    if ($breakOuttimeIdx) {
                        $breakOuttime = $csvData[$i][$breakOuttimeIdx];
                    }
                    // 休憩時間
                    if ($breakHoursIdx) {
                        $breakHours = $csvData[$i][$breakHoursIdx];
                    } else {
                        // 休憩入りと休憩戻りがある場合はそれから計算する
                        if ($breakIntime && $breakOuttime) {
                            $breakHours = $this->calcBreakTime($breakIntime, $breakOuttime);
                            $breakHours = $this->getBreakTime($breakHours,
                                $attendanceCalc['AttendanceCalc']['break_time_unit'],
                                $attendanceCalc['AttendanceCalc']['break_time_rounding']
                                );

                        } else {
                            // 休憩入りと休憩戻りがない場合は1時間に固定
                            $breakHours = '01:00:00';
                        }
                    }
                    // 実働時間
                    if ($workHoursIdx) {
                        $workHours = $csvData[$i][$workHoursIdx];
                    } else {
                        // 休憩時間から計算する
                        $workHours = $this->diffTime($attendanceDatetime, $leaveDatetime, $breakHours);
                    }

                    $this->debug('staffId='.$staffId);
                    // 実働時間は
                    $attendance = array(
                        'staff_id' => $staffId,
                        'attendance_month' => $attendanceMonth, // yyyymm
                        'attendance_date' => $attendanceDate, // yyyymmdd
                        'input_attendance_datetime' => $attendanceTime,
                        'input_leave_datetime' => $leaveTime,
                        'output_attendance_datetime' => $attendanceDatetime,
                        'output_leave_datetime' => $leaveDatetime,
                        'work_hours_org' => $workHours,
                        'work_hours' => $workHours,
                        'break_hours' => $breakHours
                    );

                    // 存在チェック
                    $data = $this->Attendance->find('first', array(
                        'conditions' => array(
                            'staff_id' => $staffId,
                            'attendance_date' => $attendanceDate
                        )
                    ));

                    if($data) {
                        // 更新
                        $attendance['id'] = $data['Attendance']['id'];
                    } else {
                        // 登録
                        $this->Attendance->create();
                    }
                    $this->Attendance->save($attendance);

                }

                // インセンティブ更新
                // $this->StaffIncentive->calcIncentive($startMonth);
                exec('php ' . APP . 'Console' . DS . 'cake.php incentive_calc ' . $startMonth . ' > /dev/null &');

                if ($errorMessages) {
                    $this->log($errorMessages, LOG_WARNING);
                    $this->responseFailure($errorMessages);
                }

                $this->responseSuccess();

            } else if ($id == 'update') {

                $data = $this->request->data('attendances');
                $startMonth = $this->request->data('startMonth');

                for($i = 0; $i < count($data); $i++) {

                    list($inputAttendanceDatetime, $inputLeaveDatetime, $outputAttendanceDatetime, $outputLeaveDatetime) = null;

                    $staffId = $data[$i]['staffId'];
                    $attendanceDate = $data[$i]['attendanceDate'];

                    if(isset($data[$i]['inputAttendanceDatetime'])) {
                        $inputAttendanceDatetime = $data[$i]['inputAttendanceDatetime'];
                    }
                    if(isset($data[$i]['inputLeaveDatetime'])) {
                        $inputLeaveDatetime = $data[$i]['inputLeaveDatetime'];
                    }
                    if(isset($data[$i]['outputAttendanceDatetime'])) {
                        $outputAttendanceDatetime = $data[$i]['outputAttendanceDatetime'];
                    }
                    if(isset($data[$i]['outputLeaveDatetime'])) {
                        $outputLeaveDatetime = $data[$i]['outputLeaveDatetime'];
                    }
                    $overTime = $data[$i]['overTime'];

                    // 出勤時間（計算前）がある
                    // 退勤時間（計算前）がある
                    // 出勤時間（計算後）がある
                    // 退勤時間（計算後）がある




                    // 残業時間の入力がない場合は次のデータを処理
                    if ($overTime == '00:00:00') {
                    //    continue;
                    }

                    $attendanceData = $this->Attendance->find('first', array(
                        'conditions' => array(
                            'Attendance.staff_id' => $staffId,
                            'Attendance.attendance_date' => $attendanceDate
                        ),
                        'fields' => array(
                            'Attendance.id',
                            'Attendance.input_attendance_datetime',
                            'Attendance.input_leave_datetime',
                            'Attendance.output_attendance_datetime',
                            'Attendance.output_leave_datetime',
                            'Attendance.work_hours_org',
                            'Attendance.break_hours',
                            'Attendance.over_time'
                        ),
                    ));

                    // パラメータがundefinedかつDBの残業時間と同じ場合は次のデータを処理
                    $dbOverTime = $attendanceData['Attendance']['over_time'];
                    if (!$inputAttendanceDatetime && !$inputLeaveDatetime && !$outputAttendanceDatetime && !$outputLeaveDatetime && ($dbOverTime == $overTime)) {
                        continue;
                    }

                    list($attendanceTime, $leaveTime, $attendanceDatetime, $leaveDatetime) = null;

                    // 計算前の出勤時間、退勤時間が入力されている場合は計算する
                    if ($inputAttendanceDatetime && $inputLeaveDatetime) {

                        // 勤怠設定を取得
                        $attendanceCalc = $this->AttendanceCalc->find('first');

                        $attendanceTime = date('Y/m/d', strtotime($attendanceDate)).' '.$inputAttendanceDatetime;
                        $leaveTime = date('Y/m/d', strtotime($attendanceDate)).' '.$inputLeaveDatetime;
                        $attendanceDatetime = $this->getOutputAttendanceDatetime(
                            $attendanceTime,
                            $attendanceCalc['AttendanceCalc']['attendance_time_unit'],
                            $attendanceCalc['AttendanceCalc']['attendance_time_rounding']
                        );
                        $leaveDatetime = $this->getOutputLeaveDatetime(
                            $leaveTime,
                            $attendanceCalc['AttendanceCalc']['attendance_time_unit'],
                            $attendanceCalc['AttendanceCalc']['attendance_time_rounding'],
                            $attendanceCalc['AttendanceCalc']['max_leave_time']
                        );
                    }

                    // 計算後の出勤時間、退勤時間が入力されていればそれを使用して実働時間を計算する
                    $breakHours = $attendanceData['Attendance']['break_hours'];
                    if ($outputAttendanceDatetime && $outputLeaveDatetime &&
                        ($outputAttendanceDatetime != '00:00:00') && ($outputLeaveDatetime != '00:00:00')) {
                        // 実働時間を計算する
                        $attendanceTime = date('Y/m/d', strtotime($attendanceDate)).' '.$outputAttendanceDatetime;
                        $leaveTime = date('Y/m/d', strtotime($attendanceDate)).' '.$outputLeaveDatetime;
                        $attendanceDatetime = date('Y/m/d', strtotime($attendanceDate)).' '.$outputAttendanceDatetime;
                        $leaveDatetime = date('Y/m/d', strtotime($attendanceDate)).' '.$outputLeaveDatetime;
                    }

                    // undefinedのものは入力されていない＝DBにデータが存在する
                    if (!$inputAttendanceDatetime && !$inputLeaveDatetime && !$outputAttendanceDatetime && !$outputLeaveDatetime) {
                        $attendanceTime = $attendanceData['Attendance']['input_attendance_datetime'];
                        $leaveTime = $attendanceData['Attendance']['input_leave_datetime'];
                        $attendanceDatetime = $attendanceData['Attendance']['output_attendance_datetime'];
                        $leaveDatetime = $attendanceData['Attendance']['output_leave_datetime'];
                    }

                    $dbWorkHoursOrg = null;
                    if ($attendanceDatetime && $leaveDatetime) {
                        // 残業時間を含まない実働時間
                        $dbWorkHoursOrg = $this->diffTime($attendanceDatetime, $leaveDatetime, $breakHours);
                    } else {
                        $dbWorkHoursOrg = $attendanceData['Attendance']['work_hours_org'];
                    }

                    // 計算後の残業時間
                    $workHours = $this->calcWorkHours($dbWorkHoursOrg, $overTime);

                    // 更新
                    $updateData = array(
                        'id' => $attendanceData['Attendance']['id'],
                        'input_attendance_datetime' => $attendanceTime,
                        'input_leave_datetime' => $leaveTime,
                        'output_attendance_datetime' => $attendanceDatetime,
                        'output_leave_datetime' => $leaveDatetime,
                        'work_hours' => $workHours,
                        'over_time' => $overTime
                    );

                    if (!$this->Attendance->save($updateData)) {
                        $this->responseFailure('勤怠データの更新に失敗しました。[staffId='.$staffId.']');
                    }
                }
                // $this->StaffIncentive->calcIncentive($startMonth);
                exec('php ' . APP . 'Console' . DS . 'cake.php incentive_calc ' . $startMonth . ' > /dev/null &');
                $this->responseSuccess();
            }
        }

    }

    function getRoundingLabel($rounding) {
        switch ($rounding) {
            case 'floor':
                return '切り捨て';
            case 'round':
                return '四捨五入';
            case 'ceil':
                return '切り上げ';
            default:
                return '';
        }
    }

    function getOutputAttendanceDatetime($datetimeString, $unit, $rounding) {

        $datetime = new DateTime($datetimeString);

        // 00:00:00の場合はそのまま返す
        $time = $datetime->format('H:i:s');
        if ($time == '00:00:00') {
            return $datetime->format('Y-m-d H:i:s');
        }

        $hour = $datetime->format('H');
        $minute = $datetime->format('i');

        switch ($unit) {
            case 15:
                switch ($rounding) {
                    case 'floor':
                        // 切り捨て
                        if ($minute < 15) {
                            $minute = 0;
                        } else if ($minute >= 15 && $minute < 30){
                            $minute = 15;
                        } else if ($minute >= 31 && $minute < 45){
                            $minute = 30;
                        } else if ($minute >= 46 && $minute < 60){
                            $minute = 45;
                        }
                        break;
                    case 'round':
                        // 四捨五入
                        if ($minute >= 0 && $minute < 7) {
                            $minute = 0;
                        } else if ($minute >= 8 && $minute < 22){
                            $minute = 15;
                        } else if ($minute >= 22 && $minute < 38){
                            $minute = 30;
                        } else if ($minute >= 38 && $minute < 54){
                            $minute = 45;
                        } else {
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    case 'ceil':
                        // 切り上げ
                        if ($minute >= 0 && $minute < 15) {
                            $minute = 15;
                        } else if ($minute >= 15 && $minute < 30){
                            $minute = 30;
                        } else if ($minute >= 30 && $minute < 45){
                            $minute = 45;
                        } else {
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case 30:
                switch ($rounding) {
                    case 'floor':
                        // 切り捨て
                        if ($minute < 30) {
                            $minute = 30;
                        } else {
                            $minute = 0;
                            $hour++;
                        }
                        break;
                    case 'round':
                        // 四捨五入
                        if ($minute > 0 && $minute < 15) {
                            $minute = 0;
                        } else if ($minute > 15 && $minute < 45) {
                            $minute = 30;
                        } else {
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    case 'ceil':
                        // 切り上げ
                        if ($minute > 30 && $minute < 60) {
                            // 30分〜59分は30分とする
                            $minute = 30;
                        } else {
                            // 30分以下は0分とする
                            $minute = 0;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case 60:
                switch ($rounding) {
                    case 'floor':
                        // 切り捨て
                        $minute = 0;
                        $hour++;
                        break;
                    case 'round':
                        // 四捨五入
                        if ($minute >= 30) {
                            $minute = 0;
                            $hour++;
                        } else {
                            $minute = 0;
                        }
                        break;
                    case 'ceil':
                        // 切り上げ
                        $minute = 0;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        $datetime->setTime($hour, $minute);
        return $datetime->format('Y-m-d H:i:s');
    }

    function getOutputLeaveDatetime($datetimeString, $unit, $rounding, $maxLeaveTime) {

        $datetime = new DateTime($datetimeString);

        // 00:00:00の場合はそのまま返す
        $time = $datetime->format('H:i:s');
        if ($time == '00:00:00') {
            return $datetime->format('Y-m-d H:i:s');
        }

        $hour = $datetime->format('H');
        $minute = $datetime->format('i');
        $maxHour = substr($maxLeaveTime, 0, 2);

        // 退勤時間が最大退勤時間を超えていた場合
        if($hour >= $maxHour) {
            $datetime->setTime($maxHour, 0);
            return $datetime->format('Y-m-d H:i:s');
        }

        switch ($unit) {
            case 15:
                switch ($rounding) {
                    case 'floor':
                        // 切り捨て
                        if ($minute < 15) {
                            $minute = 0;
                        } else if ($minute >= 15 && $minute < 30){
                            $minute = 15;
                        } else if ($minute >= 31 && $minute < 45){
                            $minute = 30;
                        } else if ($minute >= 46 && $minute < 60){
                            $minute = 45;
                        }
                        break;
                    case 'round':
                        // 四捨五入
                        if ($minute >= 0 && $minute < 7) {
                            $minute = 0;
                        } else if ($minute >= 8 && $minute < 22){
                            $minute = 15;
                        } else if ($minute >= 22 && $minute < 38){
                            $minute = 30;
                        } else if ($minute >= 38 && $minute < 54){
                            $minute = 45;
                        } else {
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    case 'ceil':
                        // 切り上げ
                        if ($minute >= 0 && $minute < 15) {
                            $minute = 15;
                        } else if ($minute >= 15 && $minute < 30){
                            $minute = 30;
                        } else if ($minute >= 30 && $minute < 45){
                            $minute = 45;
                        } else {
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case 30:
                switch ($rounding) {
                    case 'floor':
                        // 切り捨て
                        if ($minute < 30) {
                            $minute = 0;
                        } else {
                            $minute = 30;
                        }
                        break;
                    case 'round':
                        // 四捨五入
                        if ($minute >= 0 && $minute <= 15) {
                            $minute = 0;
                        } else if ($minute > 15 && $minute < 45) {
                            $minute = 30;
                        } else {
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    case 'ceil':
                        // 切り上げ
                        if ($minute >= 0 && $minute < 30) {
                            // 0分〜29分は30分とする
                            $minute = 30;
                        } else {
                            // 30分以上は60分とする
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case 60:
                switch ($rounding) {
                    case 'floor':
                        // 切り捨て
                        $minute = 0;
                        break;
                    case 'round':
                        // 四捨五入
                        if ($minute >= 30) {
                            $hour++;
                            $minute = 0;
                        } else {
                            $minute = 0;
                        }
                        break;
                    case 'ceil':
                        // 切り上げ
                        $hour++;
                        $minute = 0;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        $datetime->setTime($hour, $minute);
        return $datetime->format('Y-m-d H:i:s');
    }

    function diffTime($start, $end, $breakTime) {

        $startSec = strtotime($start);
        $time = explode(':', $breakTime);

        // 1時間以上の場合
        if($time[0] > 0) {
            $endSec = date(strtotime($end.'-'.$time[0].' hour'));
        } else if ($time[1] > 0) {
            // 1分〜59分の場合
            $endSec = date(strtotime($end.'-'.$time[1].' minute'));
        } else {
            // 休憩なしの場合
            $endSec = strtotime($end);
        }

        $diff = $endSec - $startSec;

        return gmdate('H:i:s',$diff);

    }

    function calcBreakTime($breakIntime, $breakOuttime) {

        $startSec = strtotime($breakIntime);
        $endSec = strtotime($breakOuttime);
        $diff = $endSec - $startSec;

        return gmdate('H:i:s',$diff);

    }

    function getBreakTime($breakTime, $unit, $rounding) {

        $today = date('Y-m-d');

        $datetime = new DateTime($today.' '.$breakTime);

        $hour = $datetime->format('H');
        $minute = $datetime->format('i');

        switch ($unit) {
            case 15:
                switch ($rounding) {
                    case 'floor':
                        // 切り捨て
                        if ($minute < 15) {
                            $minute = 0;
                        } else if ($minute >= 15 && $minute < 30){
                            $minute = 15;
                        } else if ($minute >= 31 && $minute < 45){
                            $minute = 30;
                        } else if ($minute >= 46 && $minute < 60){
                            $minute = 45;
                        }
                        break;
                    case 'round':
                        // 四捨五入
                        if ($minute >= 0 && $minute < 7) {
                            $minute = 0;
                        } else if ($minute >= 8 && $minute < 22){
                            $minute = 15;
                        } else if ($minute >= 22 && $minute < 38){
                            $minute = 30;
                        } else if ($minute >= 38 && $minute < 54){
                            $minute = 45;
                        } else {
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    case 'ceil':
                        // 切り上げ
                        if ($minute >= 0 && $minute < 15) {
                            $minute = 15;
                        } else if ($minute >= 15 && $minute < 30){
                            $minute = 30;
                        } else if ($minute >= 30 && $minute < 45){
                            $minute = 45;
                        } else {
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case 30:
                switch ($rounding) {
                    case 'floor':
                        // 切り捨て
                        if ($minute < 30) {
                            $minute = 0;
                        } else {
                            $minute = 30;
                        }
                        break;
                    case 'round':
                        // 四捨五入
                        if ($minute >= 0 && $minute <= 15) {
                            $minute = 0;
                        } else if ($minute > 15 && $minute < 45) {
                            $minute = 30;
                        } else {
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    case 'ceil':
                        // 切り上げ
                        if ($minute >= 0 && $minute < 30) {
                            // 0分〜29分は30分とする
                            $minute = 30;
                        } else {
                            // 30分以上は60分とする
                            $hour++;
                            $minute = 0;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case 60:
                switch ($rounding) {
                    case 'floor':
                        // 切り捨て
                        $minute = 0;
                        break;
                    case 'round':
                        // 四捨五入
                        if ($minute >= 30) {
                            $hour++;
                        } else {
                            $minute = 0;
                        }
                        break;
                    case 'ceil':
                        // 切り上げ
                        $hour++;
                        $minute = 0;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        $datetime->setTime($hour, $minute);

        return $datetime->format('H:i:s');
    }

    function calcWorkHours($workHours, $overTime) {

        $calcWorkHours = date('Y-m-d').' '.$workHours;

        $time = explode(':', $overTime);

        // 1時間以上の場合
        if($time[0] > 0) {
            $calcWorkHours = date('Y-m-d H:i:s', strtotime($calcWorkHours.'+'.$time[0].' hour'));
        }
        // 1分以上の場合
        if ($time[1] > 0) {
            $calcWorkHours = date('Y-m-d H:i:s', strtotime($calcWorkHours.'+'.$time[1].' minute'));
        }

        return $calcWorkHours;
    }

    function getData($yyyymm) {
        // 一覧表示

        $attendanceMonth = $yyyymm;

        $this->debug('勤怠管理 一覧表示 パラメータ:' . $attendanceMonth);

        // 条件
        $conditions = array(
            'conditions' => array('Attendance.attendance_month' => $attendanceMonth),
            'fields' => array('DISTINCT Attendance.attendance_date', 'Attendance.*', 'StaffHistory.name'),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'staff_histories',
                    'alias' => 'StaffHistory',
                    'conditions' => array(
                        'StaffHistory.staff_id = Staff.id')
                ),
            ),
            'order' => array('Attendance.staff_id' => 'asc', 'Attendance.attendance_date' => 'asc')
        );
        $data = $this->Attendance->find('all', $conditions);

        if ($data) {
            $this->set('attendanceData', $data);
        }

    }

    function debug($message)
    {
        $this->log($message, LOG_DEBUG);
    }
}
