<?php
App::uses('AuthController', 'Controller');

class RankingSupportsController extends AuthController
{

    public $uses = array('Staff', 'CountItem', 'StaffCountUp', 'ProductCategory', 'Ranking', 'OutputJson');

    private $key = "RankingSupports";

    public function index($yyyymm = null, $isUpdate = 0) {

        $this->set("header", "サポート処理件数");
        if (!$yyyymm) {
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $outputJson = $this->OutputJson->findByKeyAndMonthAndDivisionId($this->key, $yyyymm, 0);

        if(!$isUpdate && $outputJson){
            $json = $outputJson["OutputJson"]["json"];
            $data = json_decode($json, true);
            $modified = $outputJson["OutputJson"]["modified"];
        }
        else{
            $data = $this->getData($yyyymm);
            $json = json_encode($data);
            $newData = array(
                "key" => $this->key,
                "month" => $yyyymm,
                "json" => $json
            );
            if($outputJson){
                $newData["id"] = $outputJson["OutputJson"]["id"];
            }

            if($this->OutputJson->save($newData)){
                $id = $this->OutputJson->getLastInsertID();
                $id = $id ? $id : $newData["id"];
                $updateData = $this->OutputJson->findById($id);
                $modified = $updateData["OutputJson"]["modified"];
            }
        }

        $this->set('productCategories', $data['productCategories']);
        $this->set('staffs', $data['staffs']);
        $this->set('colspan', $data['colspan']);
        $this->set("modified", date("Y/m/d H:i", strtotime($modified)));
    }

    public function excel($yyyymm = null, $divisionId = 0){
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $book = new PHPExcel();
        $book->getActiveSheet()->setTitle('サポート処理件数');
        $sheet = $book->getActiveSheet();

        $data = $this->getData($yyyymm);

        // ヘッダー
        $headers = array(
            '順位', '前日比', '名前', '役職',
        );
        foreach ($headers as $colNum => $header) {
            $sheet->setCellValueByColumnAndRow($colNum, 1, $header);
        }
        $colNum = count($headers);
        foreach ($data['productCategories'] as $productCategory) {
            $sheet->setCellValueByColumnAndRow($colNum++, 1, $productCategory['name']);
        }
        $sheet->setCellValueByColumnAndRow($colNum++, 1, '合計');
        $sheet->setCellValueByColumnAndRow($colNum, 1, '稼働時間');

        foreach ($data['staffs'] as $rowNum => $staff) {
            $colNum=0;
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['currentRanking']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['previousDay']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['staffName']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['jobType']);
            foreach ($staff['count'] as $count) {
                $sheet->setCellValueByColumnAndRow($colNum++, $rowNum + 2, $count);
            }
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['totalCount']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['totalWorkHours'].'時間');
        }

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="ranking_supports.xlsx"');

        $writer = PHPExcel_IOFactory::createWriter($book, 'Excel2007');
        $writer->save('php://output');
        exit;
    }

    function getData($month) {

        // サポート処理件数のcount_item_idを取得
        $itemName = 'サポート処理件数';
        $countItem = $this->CountItem->findByItemName($itemName);
        if($countItem){
            $countItemId = $countItem['CountItem']['id'];
        }

        // 商材カテゴリ
        $productCategories = $this->ProductCategory->getPointHistory($month);

        $staffs = $this->Staff->getSupportStaffs($month, $countItem);
        $sort = array();

        foreach ($staffs as $idx => $staff) {
            $counts = array();
            $totalCount = 0;
            foreach ($productCategories as $productCategory) {
                $productCategoryId = $productCategory['id'];
                $data = $this->StaffCountUp->getSupportCount($month, $countItemId, $staff['staffId'], $productCategoryId);
                $counts[] = $data['count'];
                $totalCount += $data['count'];
            }
            $staffs[$idx]['count'] = $counts;
            $staffs[$idx]['totalCount'] = $totalCount;

            // ソートのキーは獲得BP
            $sort[$idx] = $totalCount;
        }

        // ソート
        if ($staffs) {
            array_multisort($sort, SORT_DESC, $staffs);
        }

        // 合計が0でないスタッフを表示
        $staffs = array_filter($staffs, function($staff) {
            return $staff['totalCount'] > 0;
        });

        // ランキングを更新
        $staffs = $this->updateRanking($staffs, $month);

        $result = array(
            'productCategories' => $productCategories,
            'staffs' => $staffs,
            'colspan' => count($productCategories) + 6
        );

        return $result;
    }

    function updateRanking($staffs, $month) {

        $sameCount = 0;
        $currentRanking = 1;

        // 順位を更新（全体の場合）
        foreach ($staffs as $idx => $staff) {

            $ranking = $this->Ranking->findByStaffIdAndRankingTypeAndMonth($staff['staffId'], 1, $month);

            // 同位を考慮
            if ($idx != 0 && ($staff['totalCount'] == $staffs[$idx-1]['totalCount'])) {
                $sameCount++;
            } else {
                $sameCount = 0;
            }

            $rank = $currentRanking - $sameCount;

            $staffs[$idx]['currentRanking'] = $rank;

            if ($ranking) {
                $data = array(
                    'id' => $ranking['Ranking']['id'],
                    'staff_id' => $staff['staffId'],
                    'ranking_type' => 1,
                    'ranking' => $rank,
                    'last_ranking' => $ranking['Ranking']['ranking'],
                    'month' => $month
                );
            } else {
                $data = array(
                    'staff_id' => $staff['staffId'],
                    'ranking' => $rank,
                    'ranking_type' => 1,
                    'last_ranking' => 0,
                    'month' => $month
                );
                $this->Ranking->create();
            }

            if (!$this->Ranking->save($data)) {
                throw new InternalErrorException();
            }

            // 前日より上位
            if ($rank < $data['last_ranking']) {
                $staffs[$idx]['previousDay'] = '↑';
            } else if ($rank == $data['last_ranking']) {
                // 同位
                $staffs[$idx]['previousDay'] = '→';
            } else if ($data['last_ranking'] == 0) {
                // 初回
                $staffs[$idx]['previousDay'] = '↑';
            } else {
                // 前日より下位
                $staffs[$idx]['previousDay'] = '↓';
            }
            $currentRanking++;
        }

        return $staffs;
    }

}
