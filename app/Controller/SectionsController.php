<?php
App::uses('AuthController', 'Controller');

class SectionsController extends AuthController {
    public $uses = array('Section', 'Area', 'Division');

    public function api($id = null){
        if($this->request->is('post')){
            if($id == null){
                if($this->Section->findByNameAndArea_id($this->request->data['section_name'], $this->request->data['area_id'])){
                    $this->responseFailure('部署名はすでに使用されています');
                }
                if(!$this->request->data['division_name']){
                    $this->responseFailure('事業部が入力されていません');
                }
                if(!$this->request->data['area_name']){
                    $this->responseFailure('エリアが入力されていません');
                }

                if(!$this->request->data['division_id']){
                    $data = array(
                        'name' => $this->request->data['division_name'],
                        'short_name' => $this->request->data['division_short_name']
                    );
                    $this->Division->create();
                    if(!$this->Division->save($data)){
                        throw new InternalErrorException();
                    }
                    $division_id = $this->Division->getLastInsertID();
                }
                else{
                    $division_id = $this->request->data['division_id'];
                }

                if(!$this->request->data['area_id']){
                    $data = array(
                        'division_id' => $division_id,
                        'name' => $this->request->data['area_name'],
                        'short_name' => $this->request->data['area_short_name']
                    );
                    $this->Area->create();
                    if(!$this->Area->save($data)){
                        throw new InternalErrorException();
                    }
                    $area_id = $this->Area->getLastInsertID();
                }
                else{
                    $area_id = $this->request->data['area_id'];
                }

                $data = array(
                    'area_id' => $area_id,
                    'name' => $this->request->data['section_name'],
                    'short_name' => $this->request->data['section_short_name']
                );
                $this->Section->create();
                if(!$this->Section->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
            else{
                $area = $this->Section->findById($id);
                if(($area['Section']['name'] != $this->request->data['section_name']) && $this->Section->findByNameAndArea_id($this->request->data['section_name'], $this->request->data['area_id'])){
                    $this->responseFailure('部署名はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'area_id' => $this->request->data['area_id'],
                    'name' => $this->request->data['section_name'],
                    'short_name' => $this->request->data['section_short_name']
                );
                if(!$this->Section->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
        }
        else if($this->request->is('delete')){
            try{
                if(!$this->Section->delete($id)){
                    $this->responseFailure('削除に失敗しました');
                }
            }
            catch(Exception $e){
                $this->responseFailure('削除に失敗しました');
            }

            $this->responseSuccess();
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $initialObj = array(
                'id' => 0,
                'name' => '',
                'short_name' => '',
            );
            $this->set('section', $initialObj);
            $this->set('selectDivision', $initialObj);
            $this->set('selectArea', $initialObj);

            $divisions = $this->Division->find('all', array('order' => array('name ASC')));
            $this->set('divisions', $divisions);
        }
        else if(is_numeric($id)){
            $data = $this->Section->findById($id);
            if(!$data) throw new NotFoundException();

            $section = $data['Section'];
            $this->set('section', $section);

            $divisions = $this->Division->find('all', array('order' => array('name ASC')));
            $this->set('divisions', $divisions);

            $areas = $this->Area->find('all', array(
                'conditions' => array('Area.id' => $section['area_id']),
                'order' => array('Area.name ASC')
            ));
            $this->set('areas', $areas);

            $area = $this->Area->findById($section['area_id']);
            $selectArea = $area['Area'];
            $this->set('selectArea', $selectArea);

            $division = $this->Division->findById($selectArea['division_id']);
            $selectDivision = $division['Division'];
            $this->set('selectDivision', $selectDivision);
        }
        else{
            throw new BadRequestException();
        }
    }
}
