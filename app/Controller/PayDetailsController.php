<?php
App::uses('AuthController', 'Controller');

class PayDetailsController extends AuthController {

    public $uses = array(
        'StaffHistory',
        'StaffType',
        'StaffPay',
        'StaffPointGoal',
        'StaffIncentive'
    );

    public function index($id = null, $yyyymm = null){
        $this->set('header', '給与・インセンティブ詳細');

        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));
        $this->getData($id, $yyyymm);
    }

    function api($id = null) {

        if ($this->request->is('post') && $this->request->is('ajax')) {
            $historyId = $this->request->data('staffHistoryId');
            $bp = $this->request->data('bp');
            $incentive = $this->request->data('incentive');
            $startMonth = $this->request->data('startMonth');

            $staffPointGoalData = array(
                'month' => $startMonth,
                'staff_id' => $historyId,
                'point' => $bp,
            );

            // 存在チェック
            $staffPointGoal = $this->StaffPointGoal->findByMonthAndStaffId($startMonth, $historyId);
            if ($staffPointGoal) {
                $staffPointGoalData['id'] = $staffPointGoal['StaffPointGoal']['id'];
            } else {
                $this->StaffPointGoal->create();
            }

            if (!$this->StaffPointGoal->save($staffPointGoalData)) {
                throw new InternalErrorException();
            }

            $staffPayData = array(
                'month' => $startMonth,
                'staff_id' => $historyId,
                'incentive' => $incentive
            );

            // 存在チェック
            $staffPay = $this->StaffPay->findByMonthAndStaffId($startMonth, $historyId);
            if ($staffPay) {
                $staffPayData['id'] = $staffPay['StaffPay']['id'];
            } else {
                $this->StaffPay->create();
            }

            if (!$this->StaffPay->save($staffPayData)) {
                throw new InternalErrorException();
            }

            $this->responseSuccess();
        }
    }

    function getData($id, $startMonth) {

        // SQL実行
        $staff = $this->StaffHistory->find('first', array(
            'conditions' => array(
                'StaffHistory.id' => $id,
                'StaffHistory.start_month <=' => $startMonth
            ),
            'fields' => array(
                'StaffHistory.id', 'StaffHistory.name', 'StaffHistory.staff_id',
                'StaffPay.pay', 'StaffPointGoal.point', 'StaffPay.incentive', 'Attendance.total_work_hours'
            ),
            'joins' => $this->getJoinConditions($startMonth)
        ));

        $staffId = 0;
        if ($staff) {
            $staffId = $staff['StaffHistory']['id'];
        }

        $staffIncentives = $this->StaffIncentive->find('all', array(
            'conditions' => array(
                'staff_id' => $staffId,
                '(month = 0 or month =' . $startMonth . ')'
            ),
            'fields' => array(
                'StaffIncentive.month', 'sum(StaffIncentive.incentive) incentive', 'Incentive.hourly_plus'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'incentives',
                    'alias' => 'Incentive',
                    'conditions' => array(
                        'Incentive.id = StaffIncentive.incentive_id'
                    )
                )
            ),
            'group' => array(
                'Incentive.hourly_plus'
            )
        ));

        $staffIncentivesBreakdowns = $this->StaffIncentive->find('all', array(
            'conditions' => array(
                'staff_id' => $staffId,
                '(month = 0 or month =' . $startMonth . ')'
            ),
            'fields' => array(
                'StaffIncentive.month', 'StaffIncentive.incentive', 'Incentive.incentive_name', 'Incentive.hourly_plus'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'incentives',
                    'alias' => 'Incentive',
                    'conditions' => array(
                        'Incentive.id = StaffIncentive.incentive_id'
                    )
                )
            )
        ));

        $this->set('staffHistoryId', $id);
        $this->set('startYear', substr($startMonth, 0, 4));
        $this->set('startMonth', substr($startMonth, 4, 2));
        $this->set('staffIncentives', $staffIncentives);
        $this->set('staffIncentivesBreakdowns', $staffIncentivesBreakdowns);
        if (isset($staff['StaffHistory']['name'])) {
            $this->set('name', $staff['StaffHistory']['name']);
        } else {
            $this->set('name', '');
        }
        if (isset($staff['StaffPay']['pay'])) {
            $this->set('pay', $staff['StaffPay']['pay']);
        } else {
            $this->set('pay', 0);
        }
        if (isset($staff['Attendance']['total_work_hours'])) {
            $this->set('totalWorkHours', $staff['Attendance']['total_work_hours']);

            $times = explode(':', $staff['Attendance']['total_work_hours']);
            $times[1] = $times[1] / 60;

            $result = $staff['StaffPay']['pay'] * $times[0] + ($staff['StaffPay']['pay'] * $times[1]);

            $this->set('payCalc', floor($result));
        } else {
            $this->set('totalWorkHours', '00:00:00');
            $this->set('payCalc', 0);
        }
        if (isset($staff['StaffPay']['incentive'])) {
            $this->set('incentive', $staff['StaffPay']['incentive']);
        } else {
            $this->set('incentive', 0);
        }
        if (isset($staff['StaffPointGoal']['point'])) {
            $this->set('bp', $staff['StaffPointGoal']['point']);
        } else {
            $this->set('bp', 0);
        }
    }

    function getJoinConditions($startMonth) {
        $joinCondition = array(
            array(
                'type' => 'INNER',
                'table' => '(select staff_id, MAX(staff_histories.start_month) max_start_month from staff_histories where start_month <='. $startMonth .' and disabled_flg = 0 group by staff_id)',
                'alias' => 'subTbl',
                'conditions' => array(
                    'StaffHistory.start_month = subTbl.max_start_month',
                    'StaffHistory.staff_id = subTbl.staff_id'
                )
            ),
            array(
                'type' => 'LEFT',
                'table' => 'staff_pays',
                'alias' => 'StaffPay',
                'conditions' => array(
                    'StaffPay.staff_id = StaffHistory.id',
                    'StaffPay.month' => $startMonth
                )
            ),
            array(
                'type' => 'LEFT',
                'table' => 'staff_point_goals',
                'alias' => 'StaffPointGoal',
                'conditions' => array(
                    'StaffPointGoal.staff_id = StaffHistory.id',
                    'StaffPointGoal.month' => $startMonth
                )
            ),
            array(
                'type' => 'LEFT',
                'table' => '(select staff_id, sec_to_time(sum(time_to_sec(work_hours))) total_work_hours from attendances where attendance_month = '. $startMonth .' group by staff_id)',
                'alias' => 'Attendance',
                'conditions' => array(
                    'Attendance.staff_id = StaffHistory.staff_id'
                )
            )
        );
        return $joinCondition;
    }
}
