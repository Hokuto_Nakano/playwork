<?php
App::uses('AuthController', 'Controller');

class StaffTypesController extends AuthController {

    public $uses = array('StaffType');

    public function index(){
    }

    public function api($id = null){
        if($this->request->is('get')){

            if(!$id){
                // 一覧
                $conditions = array('order' => array('id' => 'asc'));
                $staffType = $this->StaffType->find('all', $conditions);

                $data = null;
                if($staffType){
                    $response = array('result'=>'OK', 'data'=>$staffType);
                }
                else{
                    $response = array('result'=>'NG');
                }

                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            } else {
                // 個別取得
                $staffType = $this->StaffType->findById($id);
                $data = null;
                if($staffType){
                    $response = array('result'=>'OK', 'data'=>$staffType);
                }
                else{
                    $response = array('result'=>'NG');
                }
                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            }
        }

        if($this->request->is('post')){
            if(!$id){
                //$this->log($this->request->data['division_name'], LOG_DEBUG);
                if($this->StaffType->findByType($this->request->data['type'])){
                    $this->responseFailure('社員区分はすでに使用されています');
                }
                // 登録処理
                $this->StaffType->create();
                if($this->StaffType->save($this->request->data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
            else{
                //$this->log($this->request->data['division_id'], LOG_DEBUG);
                if($this->StaffType->findByType($this->request->data['type'])){
                    $this->responseFailure('社員区分はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'type' => $this->request->data['type'],
                );
                if($this->StaffType->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        // 削除処理
        if($this->request->is('delete')){
            if($this->StaffType->delete($id)){
                $this->responseSuccess();
            }
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $this->set('id', 0);
            $this->set('type', '');
        }
        else if(is_numeric($id)){
            $data = $this->StaffType->findById($id);
            if(!$data) throw new NotFoundException();

            $this->set('id', $id);
            $this->set('type', $data['StaffType']['type']);
        }
        else{
            throw new BadRequestException();
        }
    }
}
