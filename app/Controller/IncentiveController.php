<?php
App::uses('ApiController', 'Controller');

class IncentiveController extends ApiController {

    public $uses = array('ProductCategory', 'CountItem', 'StaffCountUp', 'StaffIncentive', 'StaffHistory');

    public function index(){
    }

    public function api($staffId = 0, $month = 0){

        if($this->request->is('get')){

            if (!$staffId || !$month) {
                $this->response->statusCode(400);
                $this->responseFailure('社員IDまたは対象年月が不正です。');
            }

            try {
                $data = $this->getData($staffId, $month);
                $this->responseJson($data);
            } catch (Exception $e) {
                $this->response->statusCode(500);
                $this->log($e->getTraceAsString());
                $this->responseFailure($e->getMessage());
            }
        }

    }

    function getData($staffId, $month) {

        $staffHistory = $this->StaffHistory->find('first', array(
            'conditions' => array(
                'StaffHistory.staff_id' => $staffId,
                'StaffHistory.start_month <=' => $month
            ),
            'fields' => array(
                'StaffPay.pay', 'StaffPay.incentive', 'Attendance.total_work_hours'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, MAX(staff_histories.start_month) max_start_month from staff_histories where start_month <='. $month .' and disabled_flg = 0 group by staff_id)',
                    'alias' => 'subTbl',
                    'conditions' => array(
                        'StaffHistory.start_month = subTbl.max_start_month',
                        'StaffHistory.staff_id = subTbl.staff_id'
                    )
                ),
                array(
                    'type' => 'LEFT',
                    'table' => 'staff_pays',
                    'alias' => 'StaffPay',
                    'conditions' => array(
                        'StaffPay.staff_id = StaffHistory.id',
                        'StaffPay.month' => $month
                    )
                ),
                array(
                    'type' => 'LEFT',
                    'table' => '(select staff_id, sec_to_time(sum(time_to_sec(work_hours))) total_work_hours from attendances where attendance_month = '. $month .' group by staff_id)',
                    'alias' => 'Attendance',
                    'conditions' => array(
                        'Attendance.staff_id = StaffHistory.staff_id'
                    )
                )
            )
        ));

        if (!$staffHistory) {
            $data = array(
                'salary' => 0,
                'pay' => 0,
                'incentive' => 0,
                'hourlyIncentive' => 0,
                'monthlyIncentive' => 0,
                'totalWorkHours' => '00:00:00',
            );
            return $data;
        }

        $staffIncentives = $this->StaffIncentive->find('all', array(
            'conditions' => array(
                'StaffHistory.staff_id' => $staffId,
                '(month = 0 or month =' . $month . ')'
            ),
            'fields' => array(
                'StaffIncentive.month', 'sum(StaffIncentive.incentive) incentive', 'Incentive.hourly_plus'
            ),
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'table' => 'staff_histories',
                    'alias' => 'StaffHistory',
                    'conditions' => array(
                        'StaffIncentive.staff_id = StaffHistory.id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => '(select staff_id, MAX(staff_histories.start_month) max_start_month from staff_histories where start_month <='. $month .' and disabled_flg = 0 group by staff_id)',
                    'alias' => 'subTbl',
                    'conditions' => array(
                        'StaffHistory.start_month = subTbl.max_start_month',
                        'StaffHistory.staff_id = subTbl.staff_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'table' => 'incentives',
                    'alias' => 'Incentive',
                    'conditions' => array(
                        'Incentive.id = StaffIncentive.incentive_id'
                    )
                )
            ),
            'group' => array(
                'Incentive.hourly_plus'
            )
        ));

        $hourlyIncentive = 0;
        $monthlyIncentive = 0;

        foreach ($staffIncentives as $staffIncentive) {
            $hourlyPlus = $staffIncentive['Incentive']['hourly_plus'];
            $incentive = $staffIncentive[0]['incentive'];

            if ($hourlyPlus) {
                // 時給
                $hourlyIncentive = $incentive;
            } else {
                // 月給
                $monthlyIncentive = $incentive;
            }
        }

        // 時給
        $pay = $staffHistory['StaffPay']['pay'] ? $staffHistory['StaffPay']['pay'] : 0;

        // 稼働時間
        $totalWorkHours = $staffHistory['Attendance']['total_work_hours'];
        if (!$totalWorkHours) {
            $totalWorkHours = '00:00:00';
        }
        $times = explode(':', $totalWorkHours);

        // 突発的インセンティブ
        $incentive = $staffHistory['StaffPay']['incentive'] ? $staffHistory['StaffPay']['incentive'] : 0;

        // 時給x稼働時間
        $monthlyPay = floor(($pay * $times[0]) + ($pay * $times[1] / 60));

        // 時給インセンx稼働時間
        $totalHourlyIncentive = floor(($hourlyIncentive * $times[0]) + ($hourlyIncentive * $times[1] / 60));

        // 総支給額＝時給x稼働時間 + 時給インセンx稼働時間 + 月給インセン
        $salary = $monthlyPay + $totalHourlyIncentive + $monthlyIncentive;

        $data = array(
            'salary' => $salary,
            'pay' => $pay,
            'incentive' => $incentive,
            'hourlyIncentive' => $hourlyIncentive,
            'monthlyIncentive' => $monthlyIncentive,
            'totalWorkHours' => $totalWorkHours
        );
        return $data;
    }

}
