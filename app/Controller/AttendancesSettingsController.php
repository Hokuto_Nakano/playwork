<?php
App::uses('AuthController', 'Controller');

class AttendancesSettingsController extends AuthController {

    public $uses = array('AttendanceCalc', 'AttendanceCsvMap');

    public function index($yyyymm = null){
        if ($yyyymm == 'contents') {
            $this->render('contents');
        }

        // 一覧表示
        $this->getData();
    }

    public function api($id = null){

        if ($this->request->is('get')) {

            if (!$id) {
                // 存在チェック
                $data = $this->AttendanceCalc->find('first');

                if ($data) {
                    $response = array('result' => 'OK', 'response' => $data);
                } else {
                    $response = array('result' => 'OK');
                }

                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            }
        }

        if($this->request->is('post')) {

            if ($id == 'headers') {
                // 画面入力値
                $input = $this->request->data['input'];

                list(
                    $other_id_column, $name_column, $date_column, $attendance_date_column,
                    $leave_date_column, $attendance_time_column, $leave_time_column,
                    $attendance_datetime_column, $leave_datetime_column, $work_hours_column,
                    $break_hours_column, $break_in_time_column, $break_out_time_column
                    ) = null;

                for ($i = 0; $i < count($input); $i++) {
                    $name = $input[$i]['name'];
                    $value = $input[$i]['value'];

                    if ($value != 'none') {
                        // 可変変数
                        ${$value} = $this->getLabelName($name);
                    }
                }

                $csvMapsData = array(
                    'other_id_column' => $other_id_column,
                    'name_column' => $name_column,
                    'date_column' => $date_column,
                    'attendance_date_column' => $attendance_date_column,
                    'leave_date_column' => $leave_date_column,
                    'attendance_time_column' => $attendance_time_column,
                    'leave_time_column' => $leave_time_column,
                    'attendance_datetime_column' => $attendance_datetime_column,
                    'leave_datetime_column' => $leave_datetime_column,
                    'work_hours_column' => $work_hours_column,
                    'break_hours_column' => $break_hours_column,
                    'break_in_time_column' => $break_in_time_column,
                    'break_out_time_column' => $break_out_time_column
                );

                // 存在チェック
                $data = $this->AttendanceCsvMap->find('first');

                $response = array();
                if ($data) {
                    // 存在していた場合、更新
                    $csvMapsData['id'] = $data['AttendanceCsvMap']['id'];
                    $response['status'] = 'update';
                } else {
                    // 存在していない場合、登録
                    $this->AttendanceCsvMap->create();
                    $response['status'] = 'insert';
                }

                $this->debug($csvMapsData);

                // 登録処理
                if($this->AttendanceCsvMap->save($csvMapsData)) {
                    $this->responseJson($response);
                    $this->response->send();
                    exit();
                } else {
                    throw new InternalErrorException();
                }

            } else if (!$id) {
                $this->debug('勤怠設定：登録処理');

                // 登録処理
                $this->AttendanceCalc->create();
                if($this->AttendanceCalc->save($this->request->data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }

                $response = array('result' => 'OK');
                $this->responseJson($response);
                $this->response->send();
                exit();

            } else {

                $this->debug('勤怠設定：更新処理');

                $data = array(
                    'id' => $id,
                    'attendance_time_unit' => $this->request->data['attendance_time_unit'],
                    'attendance_time_rounding' => $this->request->data['attendance_time_rounding'],
                    'leave_time_unit' => $this->request->data['leave_time_unit'],
                    'leave_time_rounding' => $this->request->data['leave_time_rounding'],
                    'break_time_unit' => $this->request->data['break_time_unit'],
                    'break_time_rounding' => $this->request->data['break_time_rounding'],
                    'max_leave_time' => $this->request->data['max_leave_time']
                );

                if($this->AttendanceCalc->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
                $response = array('result' => 'OK');
                $this->responseJson($response);
                $this->response->send();
                exit();
            }

        }
    }

    public function partial($id = null)
    {
        if ($id == 'new') {
            $this->set('attendanceTimeUnit', $this->getUnit());
            $this->set('attendanceTimeRounding', $this->getRounding());
            $this->set('leaveTimeUnit', $this->getUnit());
            $this->set('leaveTimeRounding', $this->getRounding());
            $this->set('breakTimeUnit', $this->getUnit());
            $this->set('breakTimeRounding', $this->getRounding());
            $this->set('maxLeaveTime', '18:00');

        } else if (is_numeric($id)) {
            $data = $this->AttendanceCalc->findById($id);
            if (!$data) throw new NotFoundException();

            $this->set('id', $id);
            $this->set('attendanceTimeUnit', $this->getUnit());
            $this->set('attendanceTimeRounding', $this->getRounding());
            $this->set('leaveTimeUnit', $this->getUnit());
            $this->set('leaveTimeRounding', $this->getRounding());
            $this->set('breakTimeUnit', $this->getUnit());
            $this->set('breakTimeRounding', $this->getRounding());

            $this->set('attendanceTimeUnitSelected', $data['AttendanceCalc']['attendance_time_unit']);
            $this->set('attendanceTimeRoundingSelected', $data['AttendanceCalc']['attendance_time_rounding']);
            $this->set('leaveTimeUnitSelected', $data['AttendanceCalc']['leave_time_unit']);
            $this->set('leaveTimeRoundingSelected', $data['AttendanceCalc']['leave_time_rounding']);
            $this->set('breakTimeUnitSelected', $data['AttendanceCalc']['break_time_unit']);
            $this->set('breakTimeRoundingSelected', $data['AttendanceCalc']['break_time_rounding']);
            $maxLeaveTime = $data['AttendanceCalc']['max_leave_time'];
            $this->set('maxLeaveTime', substr($maxLeaveTime , 0 , strlen($maxLeaveTime) - 3));

        } else {
            throw new BadRequestException();
        }
    }

    function getUnit() {
        return array(
            '15' => '15分',
            '30' => '30分',
            '60' => '60分'
        );
    }

    function getRounding() {
        return array(
            'floor' => '切り捨て',
            'round' => '四捨五入',
            'ceil' => '切り上げ'
        );
    }

    function getLabelName($key){
        $attendanceCsvColumns = $this->request->data['headers']['csvColumns'];

        for ($i = 0; $i < count($attendanceCsvColumns); $i++) {
            for ($j = 0; $j < count($attendanceCsvColumns[$i]); $j++){
                $columnKey = $attendanceCsvColumns[$i]['key'];
                if ($key == $columnKey) {
                    return $attendanceCsvColumns[$i]['name'];
                }
            }
        }
        $this->log('[ERROR] Not found key = '.$key);
        throw new InternalErrorException('[ERROR] Column name is not found. ['.$key.']');
    }

    function getData() {
        $data = $this->AttendanceCsvMap->find('first');

        if($data) {
            $this->set('attendanceCsvMaps', $data);
        }
    }

    function debug($message)
    {
        $this->log($message, LOG_DEBUG);
    }
}
