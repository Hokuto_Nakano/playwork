<?php
App::uses('AuthController', 'Controller');

class CompaniesController extends AuthController {

    public $uses = array('Company');

    public function index(){
    }

    public function api($id = null){
        if($this->request->is('get')){

            if(!$id){
                // 一覧
                $data = $this->Company->find('all', array(
                    'order' => array('id' => 'asc')
                ));
                $this->responseJson($data);
            } else {
                // 個別取得
                $data = $this->Company->findById($id);
                $this->responseJson($data);
            }
        }

        if($this->request->is('post')){
            if(!$id){
                if($this->Company->findByName($this->request->data['name'])){
                    $this->responseFailure('会社名はすでに使用されています');
                }
                // 登録処理
                $this->Company->create();
                if($this->Company->save($this->request->data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
            else{
                if($this->Company->findByName($this->request->data['name'])){
                    $this->responseFailure('会社名はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'name' => $this->request->data['name'],
                );
                if($this->Company->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        // 削除処理
        if($this->request->is('delete')){
            if($this->Company->delete($id)) {
                $this->responseSuccess();
            } else {
                $this->responseFailure();
            }
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $this->set('id', 0);
            $this->set('name', '');
        }
        else if(is_numeric($id)){
            $data = $this->Company->findById($id);
            if(!$data) throw new NotFoundException();

            $this->set('id', $id);
            $this->set('name', $data['Company']['name']);
        }
        else{
            throw new BadRequestException();
        }
    }
}
