<?php
App::uses('AuthController', 'Controller');

class JobTypesController extends AuthController {

    public $uses = array('JobType');

    public function index(){
    }

    public function api($id = null){
        if($this->request->is('get')){

            if(!$id){
                // 一覧
                $conditions = array('order' => array('id' => 'asc'));
                $jobType = $this->JobType->find('all', $conditions);

                $data = null;
                if($jobType){
                    $response = array('result'=>'OK', 'data'=>$jobType);
                }
                else{
                    $response = array('result'=>'NG');
                }

                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            } else {
                // 個別取得
                $jobType = $this->JobType->findById($id);
                $data = null;
                if($jobType){
                    $response = array('result'=>'OK', 'data'=>$jobType);
                }
                else{
                    $response = array('result'=>'NG');
                }
                $this->response->body(json_encode($response));
                $this->response->send();
                exit();
            }
        }

        if($this->request->is('post')){
            if(!$id){
                // 登録
                if($this->JobType->findByType($this->request->data['type'])){
                    $this->responseFailure('社員区分はすでに使用されています');
                }
                // 登録処理
                $this->JobType->create();
                if($this->JobType->save($this->request->data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
            else{
                // 更新
                if($this->JobType->findByType($this->request->data['type'])){
                    $this->responseFailure('職種区分はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'type' => $this->request->data['type'],
                );
                if($this->JobType->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        // 削除処理
        if($this->request->is('delete')){
            if($this->JobType->delete($id)){
                $this->responseSuccess();
            }
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $this->set('id', 0);
            $this->set('type', '');
        }
        else if(is_numeric($id)){
            $data = $this->JobType->findById($id);
            if(!$data) throw new NotFoundException();

            $this->set('id', $id);
            $this->set('type', $data['JobType']['type']);
        }
        else{
            throw new BadRequestException();
        }
    }
}
