<?php
App::uses('AuthController', 'Controller');

class IncentiveRulesController extends AuthController {

    public $uses = array("Incentive", "IncentiveRule", 'Division', 'Area', 'Section', 'JobType', 'StaffType', 'StaffIncentive');

    public function index($incentiveId = null, $yyyymm = null, $divisionId = 0, $areaId = 0, $sectionId = 0){

        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }

        $incentive = $this->Incentive->getById($incentiveId);
        if(!$incentive) throw new BadRequestException();

        $this->set("header", $incentive["incentive_name"]);
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));
        $incentiveTarget = array();
        switch($incentive['target_type']) {
            case 'notype':
                break;
            case 'job':
                $jobTypes = $this->JobType->find('all', array('order' => 'id'));
                foreach ($jobTypes as $jobType) {
                    $incentiveTarget[$jobType['JobType']['id']] = $jobType['JobType']['type'];
                }

                break;
            case 'staff':
                $staffTypes = $this->StaffType->find('all', array('order' => 'id'));
                foreach ($staffTypes as $staffType) {
                    $incentiveTarget[$staffType['StaffType']['id']] = $staffType['StaffType']['type'];
                }
                break;
        }

        // 初期表示状態
        $rankingRangeName = null;
        $divisions = null;
        $areas = null;
        $sections = null;
        if (!$divisionId) {
            if ($incentive['ranking_range'] == 'division') {
                $divisionId = $this->Division->getFirstDivisionId();
                $divisions = $this->Division->getAllSimpleDivisions();
                $rankingRangeName = '事業部';
            } else if ($incentive['ranking_range'] == 'area') {
                $divisionId = $this->Division->getFirstDivisionId();
                $divisions = $this->Division->getAllSimpleDivisions();
                $areas = $this->Area->getAllSimpleAreasByDivisionId($divisionId);
                $rankingRangeName = 'エリア';
                $areaId = $areas[0]['id'];
            } else if ($incentive['ranking_range'] == 'section') {
                $divisionId = $this->Division->getFirstDivisionId();
                $divisions = $this->Division->getAllSimpleDivisions();
                $areas = $this->Area->getAllSimpleAreasByDivisionId($divisionId);
                if ($areas) {
                    $areaId = $areas[0]['id'];
                }
                $sections = $this->Section->getAllSimpleSectionsByAreaId($areaId);
                if ($sections) {
                    $sectionId = $sections[0]['id'];
                }
                $rankingRangeName = '部署';
            }
        } else if ($divisionId && !$areaId) {
            if ($incentive['ranking_range'] == 'division') {
                $divisions = $this->Division->getAllSimpleDivisions();
                $rankingRangeName = '事業部';
            } else if ($incentive['ranking_range'] == 'area') {
                $divisions = $this->Division->getAllSimpleDivisions();
                $areas = $this->Area->getAllSimpleAreasByDivisionId($divisionId);
                if ($areas) {
                    $areaId = $areas[0]['id'];
                }
                $rankingRangeName = 'エリア';
            } else if ($incentive['ranking_range'] == 'section') {
                $divisions = $this->Division->getAllSimpleDivisions();
                $areas = $this->Area->getAllSimpleAreasByDivisionId($divisionId);
                if ($areas) {
                    $areaId = $areas[0]['id'];
                }
                $sections = $this->Section->getAllSimpleSectionsByAreaId($areaId);
                if ($sections) {
                    $sectionId = $sections[0]['id'];
                }
                $rankingRangeName = '部署';
            }
        } else if ($divisionId && $areaId && !$sectionId) {
            if ($incentive['ranking_range'] == 'area') {
                $divisions = $this->Division->getAllSimpleDivisions();
                $areas = $this->Area->getAllSimpleAreasByDivisionId($divisionId);
                $rankingRangeName = 'エリア';
            } else if ($incentive['ranking_range'] == 'section') {
                $divisions = $this->Division->getAllSimpleDivisions();
                $areas = $this->Area->getAllSimpleAreasByDivisionId($divisionId);
                $sections = $this->Section->getAllSimpleSectionsByAreaId($areaId);
                if ($sections) {
                    $sectionId = $sections[0]['id'];
                }
                $rankingRangeName = '部署';
            }
        } else if ($divisionId && $areaId && $sectionId) {
            $divisions = $this->Division->getAllSimpleDivisions();
            $areas = $this->Area->getAllSimpleAreasByDivisionId($divisionId);
            $sections = $this->Section->getAllSimpleSectionsByAreaId($areaId);
            $rankingRangeName = '部署';
        }

        $incentiveRules = $this->IncentiveRule->getAllByIncentiveId($incentiveId, $divisionId, $areaId, $sectionId, $yyyymm);

        if ($incentiveRules) {

            // 事業部・エリア・部
            if ($incentive['ranking_range'] == 'division') {
                $divisionId = $incentiveRules[0]['division_id'];
            } else if ($incentive['ranking_range'] == 'area') {
                $divisionId = $incentiveRules[0]['division_id'];
                $areaId = $incentiveRules[0]['area_id'];
            } else if ($incentive['ranking_range'] == 'section') {
                $divisionId = $incentiveRules[0]['division_id'];
                $areaId = $incentiveRules[0]['area_id'];
                $sectionId = $incentiveRules[0]['section_id'];
            }

            // 事業部・エリア・部の選択肢
            $divisions = $areas = $sections = array();
            if ($incentive['ranking_range'] == 'division') {
                $divisions = $this->Division->getAllSimpleDivisions();
                $rankingRangeName = '事業部';
            } else if ($incentive['ranking_range'] == 'area') {
                $divisions = $this->Division->getAllSimpleDivisions();
                $areas = $this->Area->getAllSimpleAreasByDivisionId($divisionId);
                $rankingRangeName = 'エリア';
            } else if ($incentive['ranking_range'] == 'section') {
                $divisions = $this->Division->getAllSimpleDivisions();
                $areas = $this->Area->getAllSimpleAreasByDivisionId($divisionId);
                $sections = $this->Section->getAllSimpleSectionsByAreaId($areaId);
                $rankingRangeName = '部';
            } else {
                $rankingRangeName = '';
            }

            $incentiveCounts = array();
            $incentivePays = array();
            $pay = array();
            $nextTarget = null;
            $count = 0;
            foreach ($incentiveRules as $index => $incentiveRule) {

                switch($incentive['target_type']) {
                    case 'notype':
                        $target = 0;
                        break;
                    case 'job':
                        $target = $incentiveRule['job_type_id'];
                        if (count($incentiveRules) != $index + 1) {
                            $nextTarget = $incentiveRules[$index+1]['job_type_id'];
                        }
                        break;
                    case 'staff':
                        $target = $incentiveRule['staff_type_id'];
                        if (count($incentiveRules) != $index + 1) {
                            $nextTarget = $incentiveRules[$index+1]['staff_type_id'];
                        }
                        break;
                }

                if ($count == 0) {
                    $incentiveCounts[] = array(
                        'rank' => $incentiveRule['rank_name'],
                        'count' => $incentiveRule['count']
                    );
                }

                if ($index != 0 && ($nextTarget != $target)) {
                    $count++;
                }

                $pay[] = $incentiveRule['incentive'];

                if ($index != 0 && ($nextTarget != $target) || $index == count($incentiveRules)-1) {
                    $incentivePays[] = array(
                        'target' => $target,
                        'pays' => $pay
                    );
                    $pay = array();
                }

            }

        } else {
            // 初期値を設定
            $incentiveCounts = array(
                "0" => array("rank" => "1位", "count" => "0"),
                "1" => array("rank" => "2位", "count" => "0"),
                "2" => array("rank" => "3位", "count" => "0"),
                "3" => array("rank" => "4位", "count" => "0"),
                "4" => array("rank" => "5位", "count" => "0")
            );
            $incentivePays = array(
                array(
                    "target" => "-1",
                    "pays" => array(
                        "0" => "0", "1" => "0", "2" => "0", "3" => "0", "4" => "0"
                    )
                )
            );
            // 区分なしは1行、社員区分・職種区分は2行
            if ($incentive['target_type'] != 'notype') {
                $incentivePays[] = array(
                    "target" => "-1",
                    "pays" => array(
                        "0" => "0", "1" => "0", "2" => "0", "3" => "0", "4" => "0"
                    )
                );
            }
        }

        $this->set("incentive", $incentive);
        $this->set("incentiveRules", $incentiveRules);
        $this->set("incentive_id", $incentiveId);
        $this->set('rankingRangeName', $rankingRangeName);
        $this->set('divisions', $divisions);
        $this->set('areas', $areas);
        $this->set('sections', $sections);
        $this->set('divisionId', $divisionId);
        $this->set('areaId', $areaId);
        $this->set('sectionId', $sectionId);

        $this->set('incentiveCounts', $incentiveCounts);
        $this->set('incentivePays', $incentivePays);
        $this->set('incentiveTarget', $incentiveTarget);
        $this->set('incentiveTerm', $incentive['incentive_term']);

        $ranking = $incentiveCounts[count($incentiveCounts)-1]['rank'];
        $this->set('ranking', str_replace('位','',$ranking));


    }

    public function api($incentive_id = null, $yyyymm = null){

        if($this->request->is('get')) {
            // エリア取得
            if($incentive_id == 'get_area') {
                $divisionId = $this->request->query('divisionId');

                $data = $this->Area->find('all', array(
                    'conditions' => array(
                        'Area.division_id' => $divisionId
                    ),
                    'order' => 'Area.id',
                    'fields'=>array('Area.id', 'Area.name')
                ));

                if($data) {
                    $this->responseJson($data);
                    $this->responseSuccess();
                } else {
                    $this->responseFailure('エリアが見つかりません');
                }
            }

            // 部署取得
            if ($incentive_id == 'get_section') {
                $areaId = $this->request->query('areaId');

                $data = $this->Section->find('all', array(
                    'conditions' => array(
                        'Section.area_id' => $areaId
                    ),
                    'order' => 'Section.id',
                    'fields'=>array('Section.id', 'Section.name')
                ));

                if($data) {
                    $this->responseJson($data);
                    $this->responseSuccess();
                } else {
                    $this->responseFailure('部署が見つかりません');
                }
            }
        } else if($this->request->is('post')){

            $divisionId = $this->request->data('divisionId') ? $this->request->data('divisionId') : 0;
            $areaId = $this->request->data('areaId') ? $this->request->data('areaId') : 0;
            $sectionId = $this->request->data('sectionId') ? $this->request->data('sectionId') : 0;
            $rankNames = $this->request->data('rank');
            $counts = $this->request->data('count');
            $targets = $this->request->data('target');
            $pays = $this->request->data('pays');

            $jobTypeId = 0;
            $staffTypeId = 0;

            // delete-insert
            $this->IncentiveRule->deleteAll(array(
                'incentive_id' => $incentive_id,
                'division_id' => $divisionId,
                'area_id' => $areaId,
                'section_id' => $sectionId,
                'start_month' => $yyyymm
            ), false);

            $incentive = $this->Incentive->getById($incentive_id);

            $index = 0;
            $targetIndex = 0;
            foreach($pays as $pay) {

                if ($index == count($rankNames)) {
                    $index = 0;
                    $targetIndex++;
                }

                switch($incentive['target_type']) {
                    case 'notype':
                        break;
                    case 'job':
                        $jobTypeId = $targets[$targetIndex];
                        break;
                    case 'staff':
                        $staffTypeId = $targets[$targetIndex];
                        break;
                }

                $data = array(
                    'incentive_id' => $incentive_id,
                    'division_id' => $divisionId,
                    'area_id' => $areaId,
                    'section_id' => $sectionId,
                    'staff_type_id' => $staffTypeId,
                    'job_type_id' => $jobTypeId,
                    'rank_name' => $rankNames[$index],
                    'count' => $counts[$index] ? $counts[$index] : 0,
                    'incentive' => $pay,
                    'start_month' => $yyyymm
                );

                $this->IncentiveRule->create();
                if (!$this->IncentiveRule->save($data)){
                    throw new InternalErrorException();
                }
                $index++;
            }

            // スタッフインセンティブを再計算
            // $this->StaffIncentive->calcIncentive($yyyymm);
            exec('php ' . APP . 'Console' . DS . 'cake.php incentive_calc ' . $yyyymm . ' > /dev/null &');

            $this->responseJson(array(
                'divisionId' => $divisionId,
                'areaId' => $areaId,
                'sectionId' => $sectionId
            ));

        }
        else if($this->request->is('delete')){
        }
    }
}
