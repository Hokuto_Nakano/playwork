<?php
App::uses('AuthController', 'Controller');

class LogoutController extends AuthController {
    public $components = array('Session');

    public function index(){
        $this->Session->destroy();
        $this->redirect('https://' . env('SERVER_NAME') . '/login');
    }
}
