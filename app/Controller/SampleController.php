<?php
App::uses('AuthController', 'Controller');
App::uses('SampleLib', 'Lib');

class SampleController extends AuthController {
    public function index(){
        $sample = new SampleLib();
        $this->set("message", $sample->getSampleLib());
    }
}
