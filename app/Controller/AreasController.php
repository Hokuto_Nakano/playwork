<?php
App::uses('AuthController', 'Controller');

class AreasController extends AuthController {
    public $uses = array('Area', 'Division');

    public function api($id = null){
        if($this->request->is('post')){
            if(!$id){
                if($this->Area->findByNameAndDivision_id($this->request->data['area_name'], $this->request->data['division_id'])){
                    $this->responseFailure('エリア名はすでに使用されています');
                }

                $data = array(
                    'division_id' => $this->request->data['division_id'],
                    'name' => $this->request->data['area_name'],
                    'short_name' => $this->request->data['area_short_name']
                );

                $this->Area->create();
                if(!$this->Area->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
            else{
                $area = $this->Area->findById($id);
                if(($area['Area']['name'] != $this->request->data['area_name']) && $this->Area->findByNameAndDivision_id($this->request->data['area_name'], $this->request->data['division_id'])){
                    $this->responseFailure('エリア名はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'division_id' => $this->request->data['division_id'],
                    'name' => $this->request->data['area_name'],
                    'short_name' => $this->request->data['area_short_name']
                );
                if($this->Area->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        else if($this->request->is('delete')){
            try{
                if(!$this->Area->delete($id)){
                    $this->responseFailure('削除に失敗しました');
                }
            }
            catch(Exception $e){
                $this->responseFailure('削除に失敗しました');
            }

            $this->responseSuccess();
        }
    }

    public function partial($id = null){
        if(is_numeric($id)){
            $data = $this->Area->findById($id);
            if(!$data) throw new NotFoundException();

            $area = $data['Area'];
            $this->set('area', $area);

            $division = $this->Division->findById($area['division_id']);
            $division = $division['Division'];
            $this->set('division', $division);

            $divisions = $this->Division->find('all', array('order' => array('name ASC')));
            $this->set('divisions', $divisions);
        }
        else{
            throw new BadRequestException();
        }
    }
}
