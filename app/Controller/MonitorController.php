<?php
App::uses('AuthController', 'Controller');

class MonitorController extends AuthController {

    function index(){
        $this->redirect(array('controller' => 'rankingApOverhangs', 'action' => 'index', Util::GetThisMonth(), 0, 1));
    }
}
