<?php
App::uses('AuthController', 'Controller');

class ProductFamiliesController extends AuthController {
    public $uses = array('ProductFamily', 'ProductCategory');

    public function api($id = null){
        if($this->request->is('post')){
            if(!$id){
                if($this->ProductFamily->findByName($this->request->data['product_family_name'])){
                    $this->responseFailure('商品ファミリ名はすでに使用されています');
                }

                $data = array(
                    'name' => $this->request->data['product_family_name'],
                    'short_name' => $this->request->data['product_family_short_name']
                );
                $this->ProductFamily->create();
                if($this->ProductFamily->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
            else{
                $family = $this->ProductFamily->findById($id);
                if(($family['ProductFamily']['name'] != $this->request->data['product_family_name']) && $this->ProductFamily->findByName($this->request->data['product_family_name'])){
                    $this->responseFailure('商品ファミリ名はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'name' => $this->request->data['product_family_name'],
                    'short_name' => $this->request->data['product_family_short_name']
                );
                if($this->ProductFamily->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        else if($this->request->is('delete')){
            try{
                if(!$this->ProductFamily->delete($id)){
                    $this->responseFailure('削除に失敗しました');
                }
            }
            catch(Exception $e){
                $this->responseFailure('削除に失敗しました');
            }

            $this->responseSuccess();
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $this->set('family', array(
                'id' => 0,
                'name' => '',
                'short_name' => '',
            ));
        }
        else if(is_numeric($id)){
            $data = $this->ProductFamily->findById($id);
            if(!$data) throw new NotFoundException();

            $family = $data['ProductFamily'];
            $this->set('family', $family);

            $category = $this->ProductCategory->findById($family['product_category_id']);
            $category = $category['ProductCategory'];
            $this->set('category', $category);
        }
        else{
            throw new BadRequestException();
        }
    }
}
