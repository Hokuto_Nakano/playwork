<?php
App::uses('AppController', 'Controller');

class SessionController extends AppController {
    public $uses = array('Staff');
    public $components = array('Session');

    public function api(){
        if($this->request->is('post')){
            $this->response->type('json');

            $loginid = $this->request->data['loginid'];
            $password = $this->request->data['password'];
            $userId = $this->Staff->auth($loginid, $password);
            if(!$userId){
                $this->response->statusCode(401);
                $this->response->body(json_encode(array('result'=>'NG')));
                $this->response->send();
                exit();
            }
            else{
                $this->Session->write('isloged', !!$userId);
                $this->Session->write('userId', $userId);
                $this->Session->write('loginId', $loginid);
                $this->Session->write('authority', $this->Staff->getAuthority($loginid));
                $this->response->body(json_encode(array('result'=>'OK')));
                $this->response->send();
                exit();
            }
        }
        else{
            throw new BadRequestException();
        }
    }
}
