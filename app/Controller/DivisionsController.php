<?php
App::uses('AuthController', 'Controller');

class DivisionsController extends AuthController {
    public $uses = array('Division');

    public function api($id = null){
        if($this->request->is('post')){
            if(!$id){
                if($this->Division->findByName($this->request->data['division_name'])){
                    $this->responseFailure('事業部名はすでに使用されています');
                }

                $data = array(
                    'name' => $this->request->data['division_name'],
                    'short_name' => $this->request->data['division_short_name']
                );
                $this->Division->create();
                if($this->Division->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
            else{
                $division = $this->Division->findById($id);
                if(($division['Division']['name'] != $this->request->data['division_name']) && $this->Division->findByName($this->request->data['division_name'])){
                    $this->responseFailure('事業部名はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'name' => $this->request->data['division_name'],
                    'short_name' => $this->request->data['division_short_name']
                );
                if($this->Division->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        else if($this->request->is('delete')){
            try{
                if(!$this->Division->delete($id)){
                    $this->responseFailure('削除に失敗しました');
                }
            }
            catch(Exception $e){
                $this->responseFailure('削除に失敗しました');
            }

            $this->responseSuccess();
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $this->set('division', array(
                'id' => 0,
                'name' => '',
                'short_name' => '',
            ));
        }
        else if(is_numeric($id)){
            $data = $this->Division->findById($id);
            if(!$data) throw new NotFoundException();

            $division = $data['Division'];
            $this->set('division', $division);
        }
        else{
            throw new BadRequestException();
        }
    }
}
