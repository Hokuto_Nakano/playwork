<?php
App::uses('AppController', 'Controller');
App::import('Model', 'Staff');

class ApiController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();

        $this->response->header('Access-Control-Allow-Origin: *');
        if($this->request->method() == 'OPTIONS'){
            $this->response->header('Access-Control-Allow-Headers', $headers);
            $this->response->header('Access-Control-Allow-Methods', empty($method) ? 'GET, POST, PUT, DELETE' : $method);
            $this->response->header('Access-Control-Allow-Credentials', 'true');
        }

        if(!isset($_SERVER['PHP_AUTH_USER'])){
            $this->response->statusCode(401);
            $this->responseFailure('Unauthorizedddd');
        }
        $this->Staff = new Staff();
        if(!$this->Staff->findByLoginIdAndPassword($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])){
            $this->response->statusCode(401);
            $this->responseFailure('Unauthorized');
        }
    }

    protected function responseSuccess(){
        $this->response->body(json_encode(array('status'=>'OK')));
        $this->response->send();
        exit();
    }

    protected function responseFailure($message = null){
        $data = null;
        if($message){
            $data = array('status'=>'NG', 'message'=>$message);
        }
        else{
            $data = array('status'=>'NG');
        }

        $this->response->body(json_encode($data));
        $this->response->send();
        exit();
    }

    protected function responseJson($data){
        if(!isset($data) && !is_array($data)){
            throw new NotFoundException();
        }
        else{
            $this->response->body(json_encode(array('status'=>'OK', 'data'=>$data)));
            $this->response->send();
            exit();
        }
    }
}
