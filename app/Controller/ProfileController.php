<?php
App::uses('ApiController', 'Controller');

class ProfileController extends ApiController {

    public $uses = array('StaffHistory');

    public function index(){
    }

    public function api($staffId = 0, $month = 0){

        if($this->request->is('get')){

            if (!$staffId || !$month) {
                $this->response->statusCode(400);
                $this->responseFailure('社員IDまたは対象年月が不正です。');
            }

            try {
                $data = $this->StaffHistory->getProfile($staffId, $month);
                $this->responseJson($data);
            } catch (Exception $e) {
                $this->response->statusCode(500);
                $this->log($e->getTraceAsString());
                $this->responseFailure($e->getMessage());
            }
        }

    }

}
