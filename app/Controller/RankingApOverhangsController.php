<?php
App::uses('AuthController', 'Controller');

class RankingApOverhangsController extends AuthController {

    public $uses = array('ProductCategory', 'Staff', 'StaffCountUp', 'StaffIncentive', 'Division', 'Attendance', 'Ranking', 'CountItem', 'OutputJson');

    private $key = "RankingApOverhangs";

    public function index($yyyymm = null, $divisionId = 0, $isMonitor = 0, $isUpdate = 0){
        $this->set("header", "Daily Appointer Ranking");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $outputJson = $this->OutputJson->findByKeyAndMonthAndDivisionId($this->key, $yyyymm, $divisionId);

        if(!$isUpdate && $outputJson){
            $json = $outputJson["OutputJson"]["json"];
            $data = json_decode($json, true);
            $modified = $outputJson["OutputJson"]["modified"];
        }
        else{
            $data = $this->getData($yyyymm, $divisionId);
            $json = json_encode($data);
            $newData = array(
                "key" => $this->key,
                "month" => $yyyymm,
                "division_id" => $divisionId,
                "json" => $json
            );
            if($outputJson){
                $newData["id"] = $outputJson["OutputJson"]["id"];
            }

            if($this->OutputJson->save($newData)){
                $id = $this->OutputJson->getLastInsertID();
                $id = $id ? $id : $newData["id"];
                $updateData = $this->OutputJson->findById($id);
                $modified = $updateData["OutputJson"]["modified"];
            }
        }

        // 指定件数で絞り込む
        $data["staffs"] = $this->breakdown($data["staffs"]);

        $this->set('staffs', $data["staffs"]);
        $this->set('divisions', $data["divisions"]);
        $this->set('divisionId', $data['divisionId']);
        $this->set("modified", date("Y/m/d H:i", strtotime($modified)));

        $this->set("isMonitor", $isMonitor);
        if(!!$isMonitor) $this->layout = 'monitor';
    }

    public function excel($yyyymm = null, $divisionId = 0, $outputId = 0){
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $book = new PHPExcel();
        $book->getActiveSheet()->setTitle('アポラン（貼出用）');
        $sheet = $book->getActiveSheet();

        $apOverhang = new RankingApOverhangsController();
        $data = $this->getData($yyyymm, $divisionId);

        $headers = array(
            "順位","前日比","名前","RANK","目標BP","獲得BP","進捗","BP平均値","稼働時間","着地想定BP","当月内取消","暫定インセン","時間インセン","職級","事業部"
        );

        foreach ($headers as $colNum => $header) {
            $sheet->setCellValueByColumnAndRow($colNum, 1, $header);
        }

        foreach ($data["staffs"] as $rowNum => $staff) {
            $colNum=0;
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['currentRanking']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['previousDay']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['staffName']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['rank']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['goalPoint']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['totalBp']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['progress']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['averageBp']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['totalWorkHours']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['landingBp']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['kaiyakuCount']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['provisionalIncentive']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['hourlyIncentive']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['jobType']);
            $sheet->setCellValueByColumnAndRow($colNum++, $rowNum+2, $staff['division']);
        }

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="ranking_ap_overhangs.xlsx"');

        $writer = PHPExcel_IOFactory::createWriter($book, 'Excel2007');
        $writer->save('php://output');
        exit;
    }

    function getData($startMonth, $divisionId) {

        // 前確と後確のcount_item_idを取得
//        $itemName = '獲得件数';
        $maekakuId = $this->CountItem->getMaekakuId();
        $atokakuId = $this->CountItem->getAtokakuId();

        $productCategories = $this->ProductCategory->getPointHistory($startMonth);

        $staffs = $this->Staff->getRankingApOverhangsStaffs($startMonth, $divisionId);
        $sort = array();

        foreach ($staffs as $idx => $staff) {
            $totalBp = 0;
            foreach ($productCategories as $productCategory) {
                foreach ($productCategory['acquisitionTypeId'] as $acquisitionTypeId) {
                    $data = $this->StaffCountUp->getAcquisitionCount($staff['staffId'], $productCategory['id'], $acquisitionTypeId, $startMonth, $maekakuId, $atokakuId);
                    $totalBp += $data['bp'];
                }
            }
            $staffs[$idx]['totalBp'] = $totalBp;

            // ランク
            $rank = $this->getRank($totalBp);
            $staffs[$idx]['rank'] = $rank;

            // 進捗 = 獲得BP/(目標BP/稼働日数*経過日数)
            if ($totalBp && $staff['goalPoint']) {
                $staffs[$idx]['progress'] = round(($totalBp / ($staff['goalPoint'] / date('t') * date('d'))) * 100, 1);
            } else {
                $staffs[$idx]['progress'] = 0;
            }
            // 平均BP = 獲得BP/(稼働時間/8)
            if ($totalBp && $staff['totalWorkHours']) {
                $staffs[$idx]['averageBp'] = round($totalBp/($staff['totalWorkHours']/8), 1);
            } else {
                $staffs[$idx]['averageBp'] = 0;
            }
            // 着地想定BP = 獲得BP/経過日数*稼働日数
            if ($totalBp) {
                $staffs[$idx]['landingBp'] = round($totalBp/date('d')*date('t'), 1);
            } else {
                $staffs[$idx]['landingBp'] = 0;
            }

            // 暫定インセンティブ、時給インセンティブ
            $incentives = $this->StaffIncentive->getIncentive($staff['staffId']);

            if (!$incentives) {
                $staffs[$idx]['hourlyIncentive'] = 0;
                $staffs[$idx]['provisionalIncentive'] = 0;
            }

            foreach ($incentives as $incentive) {
                if ($incentive['hourlyPlus']) {
                    // 時給上乗せ
                    $staffs[$idx]['hourlyIncentive'] = $incentive['incentive'];
                    if (!isset($staffs[$idx]['provisionalIncentive']) || !$staffs[$idx]['provisionalIncentive']) {
                        $staffs[$idx]['provisionalIncentive'] = 0;
                    }

                } else {
                    // 暫定インセンティブ
                    if (!isset($staffs[$idx]['hourlyIncentive']) || !$staffs[$idx]['hourlyIncentive']) {
                        $staffs[$idx]['hourlyIncentive'] = 0;
                    }
                    $staffs[$idx]['provisionalIncentive'] = $incentive['incentive'];
                }
            }

            // ソートのキーは獲得BP
            $sort[$idx] = $totalBp;
        }

        // ソート
        if ($staffs) {
            array_multisort($sort, SORT_DESC, $staffs);
        }

        // 獲得BPが0でないスタッフを表示
        $staffs = array_filter($staffs, function($staff) {
            return $staff['totalBp'] > 0;
        });

        // ランキングを更新
        $staffs = $this->updateRanking($staffs, $divisionId, $startMonth);

        // 事業部
        $divisions = $this->Division->getAllSimpleDivisions();

        $result = array(
            "staffs" => $staffs,
            "divisions" => $divisions,
            "divisionId" => $divisionId
        );

        return $result;

    }

    function breakdown($staffs) {

        $count = count($staffs);

        // デフォルト値
        $DEFAULT_OFFSET = 0;
        $DEFAULT_LIMIT = NUMBER_OF_APPOINT_RANKING;

        // オフセット、リミット取得
        $offset = null;
        $limit = null;
        if(isset($this->request->query['offset'])){
            $offset = $this->request->query['offset'];
        } else {
            $offset = $DEFAULT_OFFSET;
        }
        if(isset($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        } else {
            $limit = $DEFAULT_LIMIT;
        }

        // 表示対象のデータ
        $data = array();
        foreach ($staffs as $idx => $staff) {
            if ($offset <= $idx && $idx < ($offset + $limit)) {
                $data[] = $staff;
            }
        }

        $this->set('count', $count);
        $this->set('offset', $offset);
        $this->set('limit', $limit);
        $this->set('page', floor(($offset + $limit) / $limit));
        $this->set('totalPage', ceil($count / $limit));

        return $data;
    }

    function updateRanking($staffs, $divisionId, $startMonth) {

        $sameCount = 0;
        $currentRanking = 1;

        if ($divisionId) {

            // 順位を更新（事業部の場合）
            foreach ($staffs as $idx => $staff) {

                $ranking = $this->Ranking->findByStaffIdAndRankingTypeAndMonth($staff['staffId'], 0, $startMonth);

                // 同位を考慮
                if ($idx != 0 && ($staff['totalBp'] == $staffs[$idx-1]['totalBp'])) {
                    $sameCount++;
                } else {
                    $sameCount = 0;
                }
                $rank = $currentRanking - $sameCount;
                $staffs[$idx]['currentRanking'] = $rank;

                if ($ranking) {
                    $data = array(
                        'id' => $ranking['Ranking']['id'],
                        'ranking_type' => 0,
                        'staff_id' => $staff['staffId'],
                        'division_ranking' => $rank,
                        'division_last_ranking' => $ranking['Ranking']['division_ranking'],
                        'month' => $startMonth
                    );
                } else {
                    $data = array(
                        'staff_id' => $staff['staffId'],
                        'ranking_type' => 0,
                        'division_ranking' => $rank,
                        'division_last_ranking' => 0,
                        'month' => $startMonth
                    );
                    $this->Ranking->create();
                }

                if (!$this->Ranking->save($data)) {
                    throw new InternalErrorException();
                }

                // 前日より上位
                if ($rank < $data['division_last_ranking']) {
                    $staffs[$idx]['previousDay'] = '↑';
                } else if ($rank == $data['division_last_ranking']) {
                    // 同位
                    $staffs[$idx]['previousDay'] = '→';
                } else if ($data['division_last_ranking'] == 0) {
                    // 初回
                    $staffs[$idx]['previousDay'] = '↑';
                } else{
                    // 前日より下位
                    $staffs[$idx]['previousDay'] = '↓';
                }
                $currentRanking++;
            }

        } else {

            // 順位を更新（全体の場合）
            foreach ($staffs as $idx => $staff) {

                $ranking = $this->Ranking->findByStaffIdAndRankingTypeAndMonth($staff['staffId'], 0, $startMonth);

                // 同位を考慮
                if ($idx != 0 && ($staff['totalBp'] == $staffs[$idx-1]['totalBp'])) {
                    $sameCount++;
                } else {
                    $sameCount = 0;
                }

                $rank = $currentRanking - $sameCount;
                $staffs[$idx]['currentRanking'] = $rank;

                if ($ranking) {
                    $data = array(
                        'id' => $ranking['Ranking']['id'],
                        'ranking_type' => 0,
                        'staff_id' => $staff['staffId'],
                        'ranking' => $rank,
                        'last_ranking' => $ranking['Ranking']['ranking'],
                        'month' => $startMonth
                    );
                } else {
                    $data = array(
                        'staff_id' => $staff['staffId'],
                        'ranking_type' => 0,
                        'ranking' => $rank,
                        'last_ranking' => 0,
                        'month' => $startMonth
                    );
                    $this->Ranking->create();
                }

                if (!$this->Ranking->save($data)) {
                    throw new InternalErrorException();
                }

                // 前日より上位
                if ($rank < $data['last_ranking']) {
                    $staffs[$idx]['previousDay'] = '↑';
                } else if ($rank == $data['last_ranking']) {
                    // 同位
                    $staffs[$idx]['previousDay'] = '→';
                } else if ($data['last_ranking'] == 0) {
                    // 初回
                    $staffs[$idx]['previousDay'] = '↑';
                }  else {
                    // 前日より下位
                    $staffs[$idx]['previousDay'] = '↓';
                }
                $currentRanking++;
            }
        }

        return $staffs;
    }

    function compare($a, $b) {
        if ($a > $b) {
            return 1;
        } else {
            return 0;
        }
    }

    function getRank($totalBp) {
        if ($totalBp < 0.6) {
            return 'G';
        } else if ($totalBp < 0.8) {
            return 'F';
        } else if ($totalBp < 1) {
            return 'E';
        } else if ($totalBp < 1.8) {
            return 'D';
        } else if ($totalBp < 2.2) {
            return 'C';
        } else if ($totalBp < 2.6) {
            return 'B';
        } else if ($totalBp < 3) {
            return 'A';
        } else if ($totalBp >= 3) {
            return 'S';
        } else {
            return '-';
        }
    }
}
