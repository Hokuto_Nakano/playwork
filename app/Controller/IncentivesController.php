<?php
App::uses('AuthController', 'Controller');

class IncentivesController extends AuthController {
    public function index($yyyymm = null){
        $this->set("header", "インセンティブ設定");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $incentives = $this->Incentive->getList($yyyymm);
        $this->set("incentives", $incentives);

        $aggregate_periods = Configure::read("aggregate_period");
        $this->set("aggregate_periods", $aggregate_periods);

        $incentive_terms = Configure::read("incentive_term_bp");
        if (INCENTIVE_COUNT_TYPE == 1) {
            $incentive_terms = Configure::read("incentive_term_count");
        }
        $this->set("incentive_terms", $incentive_terms);

        $ranking_ranges = Configure::read("ranking_range");
        $this->set("ranking_ranges", $ranking_ranges);

        $target_types = Configure::read("target_type");
        $this->set("target_types", $target_types);

        $hourly_pluses = Configure::read("hourly_plus");
        $this->set("hourly_pluses", $hourly_pluses);
    }

    public function api($id = null){
        if($this->request->is('post')){
            if(!$id){
                // 登録
                $data = array(
                    "incentive_name" => $this->request->data['incentive_name'],
                    "aggregate_period" => $this->request->data['aggregate_period'],
                    "incentive_term" => $this->request->data['incentive_term'],
                    "ranking_range" => $this->request->data['incentive_term'] == "rank" ? $this->request->data['ranking_range'] : null,
                    "target_type" => $this->request->data['target_type'] ? $this->request->data['target_type'] : null,
                    "hourly_plus" => isset($this->request->data['hourly_plus']) ? $this->request->data['hourly_plus'] : 0,
                    "start_month" => $this->request->data['start_month']
                );
                $this->Incentive->create();
                if(!$this->Incentive->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            } else {
                // 更新
                $data = array(
                    "id" => $id,
                    "incentive_name" => $this->request->data['incentive_name'],
                    "aggregate_period" => $this->request->data['aggregate_period'],
                    "incentive_term" => $this->request->data['incentive_term'],
                    "ranking_range" => $this->request->data['incentive_term'] == "rank" ? $this->request->data['ranking_range'] : null,
                    "target_type" => $this->request->data['target_type'] ? $this->request->data['target_type'] : null,
                    "hourly_plus" => $this->request->data['hourly_plus'] ? $this->request->data['hourly_plus'] : 0,
                    "start_month" => $this->request->data['start_month']
                );

                if(!$this->Incentive->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
        }
        else if($this->request->is('delete')){
            if($id){
                $incentive = $this->Incentive->findById($id);
                if(!$incentive) throw new BadRequestException();

                $data = array(
                    "id" => $id,
                    "disabled_flg" => 1
                );
                if(!$this->Incentive->save($data)){
                    throw new InternalErrorException();
                }

                $this->responseSuccess();
            }
        }
    }

    public function partial($id = null, $yyyymm = null){
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        if($id == 'new'){
            $incentive = array(
                "id" => "",
                "incentive_name" => "",
                "disabled_flg" => 0
            );
        }
        else if(is_numeric($id)){
            $incentive = $this->Incentive->getById($id);
        }
        else{
            throw new BadRequestException();
        }

        $this->set("incentive", $incentive);

        $aggregate_periods = Configure::read("aggregate_period");
        $this->set("aggregate_periods", $aggregate_periods);

        $incentive_terms = Configure::read("incentive_term_bp");
        if (INCENTIVE_COUNT_TYPE == 1) {
            $incentive_terms = Configure::read("incentive_term_count");
        }
        $this->set("incentive_terms", $incentive_terms);

        $ranking_ranges = Configure::read("ranking_range");
        $this->set("ranking_ranges", $ranking_ranges);

        $target_types = Configure::read("target_type");
        $this->set("target_types", $target_types);

        $hourly_pluses = Configure::read("hourly_plus");
        $this->set("hourly_pluses", $hourly_pluses);
    }
}
