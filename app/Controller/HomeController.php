<?php
App::uses('AuthController', 'Controller');

class HomeController extends AuthController {

    var $uses = array('Staff');

    public function index(){
        $this->set("header", "Back Office");

        $authority = $this->Session->read('authority');
        if ($authority == 0) {
            // ログインIDから社員IDを取得
            $loginId = $this->Session->read('loginId');
            $staff = $this->Staff->find('first', array(
                'fields' => array(
                    'Staff.*', 'StaffHistory.*'
                ),
                'conditions' => array(
                    'login_id' => $loginId
                ),
                'joins' => array(
                    array(
                        'type' => 'INNER',
                        'table' => 'staff_histories',
                        'alias' => 'StaffHistory',
                        'conditions' => array(
                            'StaffHistory.staff_id = Staff.id'
                        )
                    ),
                    array(
                        'type' => 'INNER',
                        'table' => '(select staff_id, MAX(staff_histories.start_month) max_start_month from staff_histories where start_month <='. Util::GetThisMonth() .' and disabled_flg = 0 group by staff_id)',
                        'alias' => 'subTbl',
                        'conditions' => array(
                            'StaffHistory.start_month = subTbl.max_start_month'
                        )
                    ),
                )
            ));
            $staffHistoryId = $staff['StaffHistory']['id'];

            $this->redirect('/pay_details/'. $staffHistoryId .'/'. Util::GetThisMonth());
        }
    }
}
