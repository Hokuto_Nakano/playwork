<?php
App::uses('AuthController', 'Controller');

class GoalsController extends AuthController {
    public $uses = array('Division', 'Goal');

    public function index($yyyymm = null){

        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }

        $bases = $this->Division->getBaseGoalList($yyyymm);

        $this->set("bases", $bases);
        $this->set('header', '拠点目標設定');
        $this->set('year', Util::GetYYYY($yyyymm));
        $this->set('month', Util::GetMM($yyyymm));
    }

    function api() {

        // 目標設定
        if ($this->request->is('post') && $this->request->is('ajax')) {

            $divisionIds = $this->request->data('divisionId');
            $areaIds = $this->request->data('areaId');
            $sectionIds = $this->request->data('sectionId');
            $goalCounts = $this->request->data('goalCount');
            $goalBps = $this->request->data('goalBp');
            $month = $this->request->data('month');

            foreach ($goalCounts as $idx => $goalCount) {

                $areaId = $areaIds[$idx]['value'] ? $areaIds[$idx]['value'] : 0;
                $sectionId = $sectionIds[$idx]['value'] ? $sectionIds[$idx]['value'] : 0;

                $data = array(
                    'month' => $month,
                    'goal_count' => $goalCounts[$idx]['value'],
                    'goal_bp' => $goalBps[$idx]['value'],
                    'division_id' => $divisionIds[$idx]['value'],
                    'area_id' => $areaId,
                    'section_id' => $sectionId
                );

                $goal = $this->Goal->findByMonthAndDivisionIdAndAreaIdAndSectionId($month, $divisionIds[$idx]['value'], $areaId, $sectionId);

                if ($goal) {
                    $data['id'] = $goal['Goal']['id'];
                } else {
                    $this->Goal->create();
                }

                if (!$this->Goal->save($data)) {
                    throw new InternalErrorException();
                }
            }
            $this->responseSuccess();
        }
    }
}
