<?php
App::uses('AuthController', 'Controller');

class ProfitLossesController extends AuthController {

    public $uses = array('PlValue', 'PlCategory', 'PlItem', 'Division', 'Area', 'Section', 'AcquisitionProduct', 'StaffType', 'OutputJson');

    private $key = "ProfitLosses";

    public function index($yyyymm = null, $isUpdate = 0){
        $this->set("header", "PL");
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $isUpdate = !isset($this->request->query['update']) ? 0 : $this->request->query['update'];

        $division_id = !isset($this->request->query['division_id']) ? 0 : $this->request->query['division_id'];
        $area_id = !isset($this->request->query['area_id']) ? 0 : $this->request->query['area_id'];
        $section_id = !isset($this->request->query['section_id']) ? 0 : $this->request->query['section_id'];
        $outputJson = $this->OutputJson->findByKeyAndMonthAndDivisionIdAndAreaIdAndSectionId($this->key, $yyyymm, $division_id, $area_id, $section_id);

        if(!$isUpdate && $outputJson){
            $json = $outputJson["OutputJson"]["json"];
            $data = json_decode($json, true);
            $modified = $outputJson["OutputJson"]["modified"];
        }
        else{
            $data = $this->getData($yyyymm, $division_id, $area_id, $section_id);
            $json = json_encode($data);
            $newData = array(
                "key" => $this->key,
                "month" => $yyyymm,
                "division_id" => $division_id,
                "area_id" => $area_id,
                "section_id" => $section_id,
                "json" => $json
            );
            if($outputJson){
                $newData["id"] = $outputJson["OutputJson"]["id"];
            }

            if($this->OutputJson->save($newData)){
                $id = $this->OutputJson->getLastInsertID();
                $id = $id ? $id : $newData["id"];
                $updateData = $this->OutputJson->findById($id);
                $modified = $updateData["OutputJson"]["modified"];
            }
        }

        $this->set("division_id", $data['division_id']);
        $this->set("area_id", $data['area_id']);
        $this->set("section_id", $data['section_id']);
        $this->set('divisions', $data['divisions']);
        $this->set('areas', $data['areas']);
        $this->set('sections', $data['sections']);
        $this->set("plValues", $data['plValues']);
        $this->set("sales", $data['sales']);
        $this->set("totalSale", $data['totalSale']);
        $this->set("costs", $data['costs']);
        $this->set("totalCost", $data['totalCost']);
        $this->set("profitA", $data['profitA']);
        $this->set("totalFree", $data['totalFree']);
        $this->set("profitB", $data['profitB']);
        $this->set("modified", date("Y/m/d H:i", strtotime($modified)));
    }

    public function getData($yyyymm, $division_id, $area_id, $section_id) {

        $divisions = $this->Division->find('all', array('order' => 'id'));
        // $division_id = !isset($this->request->query['division_id']) ? 0 : $this->request->query['division_id'];

        $areas = $this->Area->find('all', array(
            'conditions' => array('Area.division_id' => $division_id),
            'order' => 'Area.id'
        ));
        // $area_id = !isset($this->request->query['area_id']) ? 0 : $this->request->query['area_id'];

        $sections = $this->Section->find('all', array(
            'conditions' => array('Section.area_id' => $area_id),
            'order' => 'Section.id'
        ));
        // $section_id = !isset($this->request->query['section_id']) ? 0 : $this->request->query['section_id'];
        $totalValues = $this->PlValue->getTotalValues($yyyymm, $division_id, $area_id, $section_id);

        $saleAmount = $this->AcquisitionProduct->getTotalSaleAmount($yyyymm, $division_id, $area_id, $section_id);
        $totalSale = isset($saleAmount[0]) ? $saleAmount[0]["totalPrice"] : 0;
        $totalSale += isset($totalValues["1"]) ? $totalValues["1"] : 0;

        $totalCost = 0;
        $costs = $this->StaffType->getPayments($yyyymm, $division_id, $area_id, $section_id);
        foreach($costs as $key => $cost){
            $totalCost += $cost;
        }
        $totalCost += isset($totalValues["2"]) ? $totalValues["2"] : 0;

        $profitA = $totalSale - $totalCost;

        $totalFree = isset($totalValues["3"]) ? $totalValues["3"] : 0;

        $profitB = $profitA - $totalFree;

        $data = array(
            "division_id" => $division_id,
            "area_id" => $area_id,
            "section_id" => $section_id,
            "divisions" => $divisions,
            "areas" => $areas,
            "sections" => $sections,
            "plValues" => $this->PlValue->getList($yyyymm, $division_id, $area_id, $section_id),
            "sales" => $this->AcquisitionProduct->getGroupBySalesAmount($yyyymm, $division_id, $area_id, $section_id),
            "totalSale" => $totalSale,
            "costs" => $costs,
            "totalCost" => $totalCost,
            "profitA" => $profitA,
            "totalFree" => $totalFree,
            "profitB" => $profitB,

        );

        return $data;
    }

    public function api($yyyymm, $id = null){
        if($this->request->is('post')){
            $itemId = $this->request->data["item_id"];
            $plItem = $this->PlItem->findById($itemId);
            $value = $this->request->data["value"];
            if($plItem["PlItem"]["calc"] == "minus"){
                $value *= -1;
            }

            if(!$id){
                // 登録処理
                $data = array(
                    'item_id' => $itemId,
                    'division_id' => $this->request->data["division_id"],
                    'area_id' => $this->request->data["area_id"],
                    'section_id' => $this->request->data["section_id"],
                    'value' => $value,
                    'month' => $yyyymm
                );
                $this->PlValue->create();
                if($this->PlValue->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
            else{
                $data = array(
                    'id' => $id,
                    'item_id' => $itemId,
                    'division_id' => $this->request->data["division_id"],
                    'area_id' => $this->request->data["area_id"],
                    'section_id' => $this->request->data["section_id"],
                    'value' => $value,
                    'month' => $yyyymm
                );
                if($this->PlValue->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        // 削除処理
        if($this->request->is('delete')){
            if($this->PlValue->delete($id)){
                $this->responseSuccess();
            }
        }
    }

    public function partial($id = null, $yyyymm = null){
        $plValueId = $id;
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $base_name = "";

        $division_id = !$this->request->query['division_id'] ? 0 : $this->request->query['division_id'];
        $division = $this->Division->findById($division_id);
        if($division){
            $base_name = $division["Division"]["name"];
        }
        $area_id = !isset($this->request->query['area_id']) ? 0 : $this->request->query['area_id'];
        $area = $this->Area->findById($area_id);
        if($area){
            $base_name .= " ".$area["Area"]["name"];
        }
        $section_id = !isset($this->request->query['section_id']) ? 0 : $this->request->query['section_id'];
        $section = $this->Section->findById($section_id);
        if($section){
            $base_name .= " ".$section["Section"]["name"];
        }

        $base_name = !$base_name ? "全社用の値として登録します" : $base_name ." 用の値として登録します";
        $this->set("base_name", $base_name);

        $this->set("plItems", $this->PlItem->getList($yyyymm));

        if($plValueId == 'new'){
            $this->set('plValue', array(
                "id" => 0,
                "category_id" => "1",
                "item_id" => "1",
                "item_name" => "",
                "value" => "0",
                "division_id" => $division_id,
                "area_id" => $area_id,
                "section_id" => $section_id,
                "start_month" => $yyyymm
            ));
        }
        else if(is_numeric($plValueId)){
            $plValue = $this->PlValue->getById($plValueId);
            if(!$plValue) throw new NotFoundException();

            $value =  $plValue["PlValue"]["value"];
            if($plValue["PlItem"]["calc"] == "minus"){
                $value *= -1;
            }
            $this->set('plValue', array(
                "id" => $plValueId,
                "category_id" => $plValue["PlCategory"]["id"],
                "category_name" => $plValue["PlCategory"]["category_name"],
                "item_id" => $plValue["PlItem"]["id"],
                "item_name" => $plValue["PlItem"]["item_name"],
                "value" => $value,
                "division_id" => $plValue["PlValue"]["division_id"],
                "area_id" => $plValue["PlValue"]["area_id"],
                "section_id" => $plValue["PlValue"]["section_id"],
                "start_month" => $plValue["PlItem"]["start_month"]
            ));

        }
    }

    public function excel($yyyymm = null){
        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set("year", Util::GetYYYY($yyyymm));
        $this->set("month", Util::GetMM($yyyymm));

        $book = new PHPExcel();
        $book->getActiveSheet()->setTitle('PL');
        $sheet = $book->getActiveSheet();

        $data = $this->getData($yyyymm);

        // 売上
        $sheet->setCellValueByColumnAndRow(0, 1, '売上');
        $sheet->setCellValueByColumnAndRow(1, 1, $data['totalSale'].'円');
        $sheet->getStyleByColumnAndRow(0, 1)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(0, 1)->getFont()->setSize(16);
        $sheet->getStyleByColumnAndRow(1, 1)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(1, 1)->getFont()->setSize(16);

        $totalRows = 2;
        foreach($data['sales'] as $rowIdx => $sale) {
            $sheet->setCellValueByColumnAndRow(0, $totalRows, $sale[0]["product_name"]);
            $sheet->setCellValueByColumnAndRow(1, $totalRows, $sale[0]["sumPrice"].'円');
            $totalRows ++;
        }

        foreach($data['plValues'] as $plValue) {
            if($plValue["PlCategory"]["id"] == 1) {
                $sheet->setCellValueByColumnAndRow(0, $totalRows, $plValue["PlItem"]["item_name"]);
                $sheet->setCellValueByColumnAndRow(1, $totalRows, $plValue["PlValue"]["value"].'円');
                $totalRows++;
            }
        }

        // 一行開ける
        $totalRows++;

        // 費用
        $sheet->setCellValueByColumnAndRow(0, $totalRows, '費用');
        $sheet->setCellValueByColumnAndRow(1, $totalRows, $data['totalCost'].'円');
        $sheet->getStyleByColumnAndRow(0, $totalRows)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(0, $totalRows)->getFont()->setSize(16);
        $sheet->getStyleByColumnAndRow(1, $totalRows)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(1, $totalRows)->getFont()->setSize(16);
        $totalRows++;

        foreach($data['costs'] as $staffType => $cost) {
            $sheet->setCellValueByColumnAndRow(0, $totalRows, '人件費：'.$staffType);
            $sheet->setCellValueByColumnAndRow(1, $totalRows, $cost.'円');
            $totalRows++;
        }

        foreach($data['plValues'] as $plValue) {
            if($plValue["PlCategory"]["id"] == 2) {
                $sheet->setCellValueByColumnAndRow(0, $totalRows, $plValue["PlItem"]["item_name"]);
                $sheet->setCellValueByColumnAndRow(1, $totalRows, $plValue["PlValue"]["value"].'円');
                $totalRows++;
            }
        }

        // 一行開ける
        $totalRows++;

        // 利益A
        $sheet->setCellValueByColumnAndRow(0, $totalRows, '利益A');
        $sheet->setCellValueByColumnAndRow(1, $totalRows, $data['profitA'].'円');
        $sheet->getStyleByColumnAndRow(0, $totalRows)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(0, $totalRows)->getFont()->setSize(16);
        $sheet->getStyleByColumnAndRow(1, $totalRows)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(1, $totalRows)->getFont()->setSize(16);
        $totalRows += 2;

        // 自由項目
        $sheet->setCellValueByColumnAndRow(0, $totalRows, '自由項目');
        $sheet->setCellValueByColumnAndRow(1, $totalRows, $data['totalFree'].'円');
        $sheet->getStyleByColumnAndRow(0, $totalRows)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(0, $totalRows)->getFont()->setSize(16);
        $sheet->getStyleByColumnAndRow(1, $totalRows)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(1, $totalRows)->getFont()->setSize(16);
        $totalRows++;

        foreach($data['plValues'] as $plValue) {
            if($plValue["PlCategory"]["id"] == 3) {
                $sheet->setCellValueByColumnAndRow(0, $totalRows, $plValue["PlItem"]["item_name"]);
                $sheet->setCellValueByColumnAndRow(1, $totalRows, $plValue["PlValue"]["value"].'円');
                $totalRows++;
            }
        }
        $totalRows ++;

        // 利益B
        $sheet->setCellValueByColumnAndRow(0, $totalRows, '利益B');
        $sheet->setCellValueByColumnAndRow(1, $totalRows, $data['profitB'].'円');
        $sheet->getStyleByColumnAndRow(0, $totalRows)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(0, $totalRows)->getFont()->setSize(16);
        $sheet->getStyleByColumnAndRow(1, $totalRows)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(1, $totalRows)->getFont()->setSize(16);

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="profit_loss.xlsx"');

        $writer = PHPExcel_IOFactory::createWriter($book, 'Excel2007');
        $writer->save('php://output');
        exit;
    }
}
