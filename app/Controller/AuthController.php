<?php
App::uses('AppController', 'Controller');

class AuthController extends AppController {
    public $components = array('Session');

    public function beforeFilter() {
        parent::beforeFilter();
        if(!$this->Session->read('isloged')){
            $this->response->statusCode(401);
            $this->redirect('/login');
        }
    }
}
