<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::import('vendor', 'Util');

require ROOT . '/app/Vendor/vendor/autoload.php';

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public function index(){
        throw new NotFoundException();
    }

    public function beforeFilter(){
        parent::beforeFilter();

        //$this->log($this->request->params['controller']."/".$this->request->params['action'], LOG_DEBUG);

        $action = $this->request->params['action'];
        if($action == 'api' || $action == 'partial'){
//            if(!$this->request->is('ajax')) {
//                throw new NotFoundException();
//            }
        }
        if($action == 'api'){
            $this->response->type('json');
            $this->autoRender = false; //viewなし
//            $this->response->header('Access-Control-Allow-Origin: *');
//            $this->response->header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        }
        if($action == 'partial'){
            $this->layout = ""; //layoutなし
        }
    }

    protected function responseSuccess(){
        $this->response->body(json_encode(array('result'=>'OK')));
        $this->response->send();
        exit();
    }

    protected function responseFailure($message = null){
        $data = null;
        if($message){
            $data = array('result'=>'NG', 'message'=>$message);
        }
        else{
            $data = array('result'=>'NG');
        }

        $this->response->body(json_encode($data));
        $this->response->send();
        exit();
    }

    protected function responseJson($data){
        if(!isset($data) && !is_array($data)){
            throw new NotFoundException();
        }
        else{
            $this->response->body(json_encode(array('result'=>'OK', 'data'=>$data)));
            $this->response->send();
            exit();
        }
    }
}
