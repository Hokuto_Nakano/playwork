<?php
App::uses('AuthController', 'Controller');

class FormatsController extends AuthController
{

    public $uses = array('CountItem', 'Product','ProductCategory','ProductCsvTableMap', 'ProductCsvColumnMap', 'CountCondition', 'StaffCondition', 'AcquisitionType', 'ProductCsvStatusMap', 'ProductCsvPlanMap', 'ProductCsvAcquisitionMap');

    public function index($yyyymm = null){

        $this->set('header', 'フォーマット管理');

        if(!$yyyymm){
            $yyyymm = Util::GetThisMonth();
        }
        $this->set('year', Util::GetYYYY($yyyymm));
        $this->set('month', Util::GetMM($yyyymm));

        // 一覧表示
        $this->getData($yyyymm);

        switch($yyyymm) {
            case 'csvtable':
                $this->render('csvtable');
                break;
            case 'acquisitiontable':
                $this->render('acquisitiontable');
                break;
            case 'statustable':
                $this->render('statustable');
                break;
            case 'plantable':
                $this->render('plantable');
            break;
            case 'counttable':
                $this->render('counttable');
                break;
            case 'stafftable':
                $this->render('stafftable');
                break;
        }

    }

    public function api($id = null)
    {
        if ($this->request->is('get')) {

            if (is_numeric($id)) {
                // 獲得区分の取得
                $this->debug('獲得区分を取得');

                $data = $this->AcquisitionType->find('all', array(
                    'conditions' => array('product_category_id' => $id)
                ));

                if ($data) {
                    $this->responseJson($data);
                    $this->responseSuccess();
                } else {
                    $this->responseFailure();
                }
                exit();
            } else if ($id == 'plans') {
                $this->debug('商材ファミリーと商材プラン項目を取得');
                $startMonth = $this->request->query('startMonth');
                $productCategoryId = $this->request->query('productCategoryId');
                $data = $this->Product->getProductByProductCategoryId($productCategoryId, $startMonth);

                if ($data) {
                    $this->responseJson($data);
                    $this->responseSuccess();
                } else {
                    $this->responseFailure();
                }
                exit();
            } else if ($id == 'count_items') {
                $this->debug('カウント項目を取得');
                $data = $this->CountItem->find('all');

                if ($data) {
                    $this->responseJson($data);
                    $this->responseSuccess();
                } else {
                    $this->responseFailure();
                }
                exit();
            } else if ($id == 'check_exist') {
                $this->debug('check_exist');

                $data = $this->ProductCsvTableMap->find('first', array(
                    'conditions' => array(
                        'product_category_id' => $this->request->query('productCategoryId'),
                        'disabled_flg' => 0,
                        'start_month' => $this->request->query('startMonth')
                )));

                if ($data) {
                    $this->responseFailure();
                } else {
                    $this->responseSuccess();
                }

            } else if ($id == 'update') {

                $this->debug('フォーマット管理：更新 データ取得');

                // tableMapIdを取得
                $tableMapId = $this->request->query('tableMapId');

                // CsvTableMapsを取得
                $csvTableMaps = $this->ProductCsvTableMap->findById($tableMapId);
                $productCategoryId = $csvTableMaps['ProductCsvTableMap']['product_category_id'];

                // CsvColumnMapsを取得
                $csvColumnMaps = $this->ProductCsvColumnMap->find('all', array(
                    'conditions' => array(
                        'ProductCsvColumnMap.table_map_id' => $tableMapId
                    )
                ));
                // CsvAcquisitionMapを取得
                $csvAcquisitionMaps = $this->ProductCsvAcquisitionMap->find('all', array(
                    'conditions' => array(
                        'ProductCsvAcquisitionMap.table_map_id' => $tableMapId
                    )
                ));
                // CsvStatusMapsを取得
                $csvStatusMaps = $this->ProductCsvStatusMap->find('all', array(
                    'conditions' => array(
                        'ProductCsvStatusMap.table_map_id' => $tableMapId
                    )
                ));
                // CsvPlanMapsを取得
                $csvPlanMaps = $this->ProductCsvPlanMap->find('all', array(
                    'fields' => array(
                        'ProductCsvPlanMap.*', 'ProductFamily.*', 'Product.*'
                    ),
                    'conditions' => array(
                        'ProductCsvPlanMap.table_map_id' => $tableMapId
                    ),
                    'joins' => array(
                        array(
                            'type' => 'INNER',
                            'table' => 'products',
                            'alias' => 'Product',
                            'conditions' => array(
                                'Product.id = ProductCsvPlanMap.product_id'
                            )
                        ),
                        array(
                            'type' => 'INNER',
                            'table' => 'product_details',
                            'alias' => 'ProductDetail',
                            'conditions' => array(
                                'ProductDetail.id = Product.product_detail_id'
                            )
                        ),
                        array(
                            'type' => 'INNER',
                            'table' => 'product_families',
                            'alias' => 'ProductFamily',
                            'conditions' => array(
                                'ProductFamily.id = ProductDetail.product_family_id'
                            )
                        )
                )));
                // CountConditionを取得
                $countConditions = $this->CountCondition->find('all', array(
                    'conditions' => array(
                        'CountCondition.table_map_id' => $tableMapId,
                        'CountCondition.product_category_id' => $productCategoryId
                    )
                ));
                // AcquisitionTypeを取得
                $acquisitionTypes = $this->AcquisitionType->find('all', array(
                    'conditions' => array(
                        'AcquisitionType.product_category_id' => $productCategoryId
                    )
                ));

                // StaffConditionを取得
                $countConditionList = array();
                for($i = 0; $i < count($countConditions); $i++) {
                    $countConditionId = $countConditions[$i]['CountCondition']['id'];
                    array_push($countConditionList, $countConditionId);
                }

                $staffConditions = $this->StaffCondition->find('all', array(
                    'conditions' => array(
                        'StaffCondition.count_condition_id' => $countConditionList,
                        'StaffCondition.product_category_id' => $productCategoryId
                    ),
                    'fields' => array(
                        'StaffCondition.*'
                    )
                ));

                // レスポンス
                if ($csvTableMaps && $csvColumnMaps && $csvAcquisitionMaps && $csvStatusMaps
                     && $csvPlanMaps && $countConditions && $acquisitionTypes && $staffConditions
                    ) {
                    $data = array(
                        $csvTableMaps,
                        $csvColumnMaps,
                        $csvAcquisitionMaps,
                        $csvStatusMaps,
                        $csvPlanMaps,
                        $countConditions,
                        $acquisitionTypes,
                        $staffConditions
                    );
                    $this->responseJson($data);
                    $this->responseSuccess();

                } else {
                    $this->responseFailure('データが見つかりませんでした。[tableMapId='.$id.']');
                }
            }
        }

        if ($this->request->is('post')) {
            if (is_numeric($id) && $id > 197001) {
                $this->debug('登録処理');

                $startMonth = $id;

                // 連想配列に変換
                $data1 = json_decode($this->request->data['data1'], true);
                $condition1 = json_decode($this->request->data['condition1'], true);
                $condition2 = json_decode($this->request->data['condition2'], true);
                $csvColumns = json_decode($this->request->data['csvColumns'], true);
                $acquisitions = json_decode($this->request->data['acquisitions'], true);
                $statusData = json_decode($this->request->data['statusData'], true);
                $planData = json_decode($this->request->data['planData'], true);
                $categoryId = json_decode($this->request->data['categoryId'], true);

                // 更新の場合のIDを取得
                list($tableMapId, $columnMapsIds, $statusMapIds, $planMapIds, $acquisitionMapIds, $countConditionIds, $staffConditionIds) = 0;
                if (isset($this->request->data['productCsvTableMapId'])) {
                    $tableMapId = $this->request->data['productCsvTableMapId'];
                }
                if (isset($this->request->data['productCsvColumnMapIds'])) {
                    $columnMapsIds = $this->request->data['productCsvColumnMapIds'];
                }
                if (isset($this->request->data['productCsvStatusMapIds'])) {
                    $statusMapIds = $this->request->data['productCsvStatusMapIds'];
                }
                if (isset($this->request->data['productCsvAcquisitionMapIds'])) {
                    $acquisitionMapIds = $this->request->data['productCsvAcquisitionMapIds'];
                }
                if (isset($this->request->data['productCsvPlanMapIds'])) {
                    $planMapIds = $this->request->data['productCsvPlanMapIds'];
                }
                if (isset($this->request->data['countConditionIds'])) {
                    $countConditionIds = $this->request->data['countConditionIds'];
                }
                if (isset($this->request->data['staffConditionIds'])) {
                    $staffConditionIds = $this->request->data['staffConditionIds'];
                }

                // データ型取得
                $dataTypes = array();
                foreach ($data1['dataTypes'] as $key) {
                    foreach ($key as $value) {
                        array_push($dataTypes, $value);
                    }
                }

                // テーブルを新規作成しデータ登録
                $tableName = $this->createTable($dataTypes);

                // common_key_columnsの生成
                $commonKey = '';
                for ($a = 0; $a < count($data1['uniqKeys']); $a++) {
                    $commonKey .= $data1['uniqKeys'][$a]['uniqColumn'];
                    if ((count($data1['uniqKeys']) - 1) != $a) {
                        $commonKey .= ',';
                    }
                }

                // name_collecting_key
                $nameCollectingKey = 'column'.$data1['nameCollectingKeys'][0]['nameCollectingRow'];

                // 商材CSVテーブルマップの登録
                $data = array(
                    'product_category_id' => $categoryId,
                    'product_input_table_name' => $tableName,
                    'common_key_columns' => $commonKey,
                    'name_collecting' => $nameCollectingKey,
                    'start_month' => $startMonth
                );
                if ($tableMapId != 0) {
                    // 更新
                    $data['id'] = $tableMapId;
                } else {
                    // 新規登録
                    $this->ProductCsvTableMap->create();
                }
                $this->ProductCsvTableMap->save($data);


                // 商材CSVカラムマップの登録
                $dataTypes = $data1['dataTypes'];
                $count = 0;
                foreach ($data1['columnNames'] as $key) {
                    foreach ($key as $value) {
                        $index = key($key);
                        $inputTableColumnName = 'column' . $index;

                        $data = array(
                            'table_map_id' => $this->ProductCsvTableMap->getLastInsertID(),
                            'product_input_column_name' => $inputTableColumnName,
                            'product_csv_column_name' => $value,
                            'data_type' => $dataTypes[$count]['row'],
                            'start_month' => $startMonth
                        );
                        if ($columnMapsIds != 0) {
                            // 更新
                            $data['id'] = $columnMapsIds[$count];
                            $data['table_map_id'] = $tableMapId;
                        } else {
                            // 新規登録
                            $this->ProductCsvColumnMap->create();
                        }

                        $this->ProductCsvColumnMap->save($data);
                        $count++;
                    }
                }

                // 獲得区分カラム
                $acquisitionTypeColumnNames = array();

                // product_csv_acquisition_mapsテーブルの登録
                for ($i = 0; $i < count($acquisitions); $i++) {
                    for ($j = 0; $j < count($acquisitions[$i]); $j++) {
                        $name = $acquisitions[$i][$j]['name'];
                        switch($name) {
                            case 'type':
                                $acquisitionTypeId = $acquisitions[$i][$j]['value'];
                                break;
                            case 'csv_column':
                                $index = $acquisitions[$i][$j]['value'];
                                $csvColumn = $csvColumns[$index];
                                array_push($acquisitionTypeColumnNames, $csvColumn);
                                $inputColumn = 'column'.$index;
                                break;
                            case 'conditions':
                                $conditions = $acquisitions[$i][$j]['value'];
                                break;
                            case 'compare':
                                $compare = $acquisitions[$i][$j]['value'];
                                break;
                            default:
                                break;
                        }
                    }
                    $idx = strrpos($csvColumn, ',');
                    if ($idx) {
                        $csvColumn = substr($csvColumn, 0, $idx);
                    }
                    $data = array(
                        'table_map_id' => $this->ProductCsvTableMap->getLastInsertID(),
                        'acquisition_type_id' => $acquisitionTypeId,
                        'product_input_column_name' => $inputColumn,
                        'product_csv_column_name' => $csvColumn,
                        'comparison_condition' => $conditions,
                        'comparison_string' => $compare
                    );
                    if ($acquisitionMapIds != 0) {
                        // 更新
                        $data['id'] = $acquisitionMapIds[$i];
                        $data['table_map_id'] = $tableMapId;
                    } else {
                        // 新規登録
                        $this->ProductCsvAcquisitionMap->create();
                    }
                    $this->ProductCsvAcquisitionMap->save($data);
                }

                // product_csv_status_mapsテーブルの登録
                for ($i = 0; $i < count($statusData); $i++) {
                    for ($j = 0; $j < count($statusData[$i]); $j++) {
                        $name = $statusData[$i][$j]['name'];
                        switch($name) {
                            case 'status':
                                $status = $statusData[$i][$j]['value'];
                                break;
                            case 'csv_column':
                                $index = $statusData[$i][$j]['value'];
                                $csvColumn = $csvColumns[$index];
                                $inputColumn = 'column'.$index;
                                break;
                            case 'conditions':
                                $conditions = $statusData[$i][$j]['value'];
                                break;
                            case 'compare':
                                $compare = $statusData[$i][$j]['value'];
                                break;
                            default:
                                break;
                        }
                    }
                    $data = array(
                        'table_map_id' => $this->ProductCsvTableMap->getLastInsertID(),
                        'product_input_column_name' => $inputColumn,
                        'product_csv_column_name' => $csvColumn,
                        'comparison_condition' => $conditions,
                        'comparison_string' => $compare,
                        'contract_status' => $status
                    );
                    if ($statusMapIds != 0) {
                        // delete-insert
                        for ($k = 0; $k < count($statusMapIds); $k++) {
                            $this->ProductCsvStatusMap->delete($statusMapIds[$k]);
                        }
                        $data['table_map_id'] = $tableMapId;
                    }
                    // 新規登録
                    $this->ProductCsvStatusMap->create();
                    $this->ProductCsvStatusMap->save($data);
                }

                // product_csv_plan_mapsテーブルの登録
                for ($i = 0; $i < count($planData); $i++) {
                    for ($j = 0; $j < count($planData[$i]); $j++) {
                        $name = $planData[$i][$j]['name'];
                        switch($name) {
                            case 'productId':
                                $productId = $planData[$i][$j]['value'];
                                break;
                            case 'csv_column':
                                $index = $planData[$i][$j]['value'];
                                $csvColumn = $csvColumns[$index];
                                $inputColumn = 'column'.$index;
                                break;
                            case 'conditions':
                                $conditions = $planData[$i][$j]['value'];
                                break;
                            case 'compare':
                                $compare = $planData[$i][$j]['value'];
                                break;
                            default:
                                break;
                        }
                    }
                    $data = array(
                        'table_map_id' => $this->ProductCsvTableMap->getLastInsertID(),
                        'product_id' => $productId,
                        'product_input_column_name' => $inputColumn,
                        'product_csv_column_name' => $csvColumn,
                        'comparison_condition' => $conditions,
                        'comparison_string' => $compare
                    );
                    if ($planMapIds != 0) {
                        // delete-insert
                        for ($k = 0; $k < count($planMapIds); $k++) {
                            $this->ProductCsvPlanMap->delete($planMapIds[$k]);
                        }
                        $data['table_map_id'] = $tableMapId;
                    }
                    // 新規登録
                    $this->ProductCsvPlanMap->create();
                    $this->ProductCsvPlanMap->save($data);
                }

                // カウント条件の前にカラム名を取得
                if(!$tableMapId) {
                    $tableMapId = $this->ProductCsvTableMap->getLastInsertID();
                }
                $tableMapData = $this->ProductCsvColumnMap->find('all', array(
                    'conditions' => array('table_map_id' => $tableMapId)
                ));
                $inputColumnNames = array();
                $csvColumnNames = array();
                for($a = 0; $a < count($tableMapData); $a++) {
                    array_push($inputColumnNames, $tableMapData[$a]['ProductCsvColumnMap']['product_input_column_name']);
                    array_push($csvColumnNames, $tableMapData[$a]['ProductCsvColumnMap']['product_csv_column_name']);
                }

                // カウント条件の登録 delete-insert
                for ($i = 0; $i < count($condition1); $i++) {
                    $csvColumnValueArray = array();
                    $conditionsValueArray = array();
                    $compareValueArray = array();
                    for ($j = 0; $j < count($condition1[$i]); $j++) {
                        $name = $condition1[$i][$j]['name'];
                        switch ($name) {
                            case 'count_items':
                                $countItemsValue = $condition1[$i][$j]['value'];
                                break;
                            case 'recorded_date':
                                $index = $condition1[$i][$j]['value'];
                                $recordedDateValue = $csvColumns[$index];
                                break;
                            case 'csv_column':
                                $index = $condition1[$i][$j]['value'];
                                array_push($csvColumnValueArray, $csvColumns[$index]);
                                break;
                            case 'conditions':
                                array_push($conditionsValueArray, $condition1[$i][$j]['value']);
                                break;
                            case 'compare':
                                array_push($compareValueArray, $condition1[$i][$j]['value']);
                                break;
                        }
                    }

                    $identifyUserValue = null;
                    $countItemLabelValue = null;

                    $staffNoArray = array();
                    $staffNameArray = array();
                    $staffConditionArray = array();
                    if (count($condition2) > $i) {
                        for ($m = 0; $m < count($condition2[$i]); $m++) {
                            $name = $condition2[$i][$m]['name'];
                            switch ($name) {
                                case 'count_item_label':
                                    $countItemLabelValue = $condition2[$i][$m]['value'];
                                    if ($countItemsValue != $countItemLabelValue) {
                                        continue;
                                    }
                                    break;
                                case 'identify-user':
                                    $index = $condition2[$i][$m]['value'];
                                    $identifyUserValue = $csvColumns[$index];
                                    break;
                                case 'conditions':
                                    $condition = $condition2[$i][$m]['value'];
                                    array_push($staffConditionArray, $condition);
                                    break;
                                case 'staff':
                                    $value = $condition2[$i][$m]['value'];
                                    // 社員番号の場合
                                    if ($value == 'staffNo') {
                                        $staffNo = $identifyUserValue;
                                        array_push($staffNoArray, $staffNo);
                                        array_push($staffNameArray, '');
                                    } else {
                                        // 氏名の場合
                                        $staffName = $identifyUserValue;
                                        array_push($staffNoArray, '');
                                        array_push($staffNameArray, $staffName);
                                    }
                                    break;
                            }
                        }
                    }

                    $tmpCountItemId = '';
                    for ($k = 0; $k < count($csvColumnValueArray); $k++) {
                        $data = array(
                            'table_map_id' => $this->ProductCsvTableMap->getLastInsertID(),
                            'product_category_id' => $categoryId,
                            'count_item_id' => $countItemsValue,
                            'date_column_name' => $this->getColumn($inputColumnNames, $csvColumnNames, $recordedDateValue),
                            'data_column_name' => $this->getColumn($inputColumnNames, $csvColumnNames, $csvColumnValueArray[$k]),
                            'comparison_condition' => $conditionsValueArray[$k],
                            'comparison_string' => $compareValueArray[$k]
                        );
                        // delete-insert
                        if ($staffConditionIds != 0 && $countConditionIds != 0) {
                            // staff_conditionを先に削除
                            for ($l = 0; $l < count($staffConditionIds); $l++) {
                                $staffConditionId = $staffConditionIds[$l];
                                $this->StaffCondition->delete($staffConditionId);
                            }
                            // count_conditionを後に削除
                            for ($l = 0; $l < count($countConditionIds); $l++) {
                                $countConditionId = $countConditionIds[$l];
                                $this->CountCondition->delete($countConditionId);
                            }
                            // 更新
                            $data['table_map_id'] =  $tableMapId;
                        } else {
                            // 新規
                            $data['table_map_id'] = $this->ProductCsvTableMap->getLastInsertID();
                        }

                        // 新規登録
                        $this->CountCondition->create();
                        $this->CountCondition->save($data);

                        // カウントコンディションID
                        $countConditionId = $this->CountCondition->getLastInsertID();

                        if (count($condition2) > $i) {
                            if ($tmpCountItemId != $countItemsValue) {
                                for ($m = 0; $m < count($staffConditionArray); $m++) {
                                    $staffConditionData = array(
                                        'count_condition_id' => $countConditionId,
                                        'product_category_id' => $categoryId,
                                        'user_id_column_name' => $this->getColumn($inputColumnNames, $csvColumnNames, $staffNoArray[$m]),
                                        'user_name_column_name' => $this->getColumn($inputColumnNames, $csvColumnNames, $staffNameArray[$m]),
                                        'comparison_condition' => $staffConditionArray[$m],
                                    );
                                    $this->StaffCondition->create();
                                    $this->StaffCondition->save($staffConditionData);
                                }
                            }

                            $tmpCountItemId = $countItemsValue;
                        }
                    }
                }

                $this->responseSuccess();

            } else if($id == 'disabled') {

                $tableMapId = $this->request->data['tableMapId'];

                // 無効
                $this->debug('フォーマット履歴無効化');
                $this->debug('無効フラグON [tableMapId=' . $tableMapId . ']');

                $data = array(
                    'id' => $tableMapId,
                    'disabled_flg' => true
                );

                if ($this->ProductCsvTableMap->save($data)) {
                    $this->responseSuccess();
                } else {
                    $this->responseFailure('無効化に失敗しました。[tableMapId=' . $tableMapId . ']');
                }
            }
        }
        // 削除処理
        if ($this->request->is('delete')) {

            if($id) {
                $this->debug('フォーマット履歴削除（ID：'.$id.'）');

                $data = $this->ProductCsvTableMap->findById($id);

                if ($data) {
                    $tableName = $data['ProductCsvTableMap']['product_input_table_name'];
                    $this->deleteTable($tableName);
                } else {
                    throw NotFoundException();
                }


                $this->ProductCsvPlanMap->deleteAll(array('ProductCsvPlanMap.table_map_id' => $id), false);
                $this->ProductCsvAcquisitionMap->deleteAll(array('ProductCsvAcquisitionMap.table_map_id' => $id), false);
                $this->ProductCsvStatusMap->deleteAll(array('ProductCsvStatusMap.table_map_id' => $id), false);
                $this->ProductCsvColumnMap->deleteAll(array('ProductCsvColumnMap.table_map_id' => $id), false);
                $this->ProductCsvTableMap->delete($id);

                $this->responseSuccess();
            } else {
                $this->debug('削除：ID指定なし');
            }
        }
    }

    public function partial($id = null){

        if ($id == 'new') {
            $this->debug('フォーマット管理：商材カテゴリを取得');
            $data = $this->ProductCategory->find('all');

            if(!$data) throw new NotFoundException('商材カテゴリが見つかりません');

            $this->set('prooductCategories', $data);
            $this->set('updateFlg', false);

        } else if ($id == 'update'){
            $this->set('updateFlg', true);
        }

    }


    public function deleteTable($tableName){

        try {
            // データベースに接続
            $pdo = $this->getPdo();

            $query = 'drop table '.$tableName;
            $this->debug($query);
            $pdo->query($query);

        } catch (PDOException $e) {
            $this->log($e->getMessage());
        }

    }

    public function createTable($dataTypes)
    {
        $tableName = 'table_' . time();

        try {
            // データベースに接続
            $pdo = $this->getPdo();

            // カラム数
            $size = count($dataTypes);
            $column = '';
            $columnComma = '';
            for ($i = 0; $i < $size; $i++) {
                $dataType = $dataTypes[$i];
                switch($dataType) {
                    case 'varchar':
                        $column .= ' column' . $i . ' varchar(100) not null';
                        break;
                    case 'date':
                        $column .= ' column' . $i . ' date not null';
                        break;
                    case 'int':
                        $column .= ' column' . $i . ' int not null';
                        break;
                    case 'float':
                        $column .= ' column' . $i . ' float not null';
                        break;

                }
                $columnComma .= 'column' . $i;
                if ($i != $size - 1) {
                    $column .= ', ';
                    $columnComma .= ', ';
                }

            }

            $create = 'create table ' . $tableName
                . ' ('
                . 'id int(10) auto_increment primary key,'
                . 'regist_key varchar(255) not null,'
                . $column
                . ') engine=InnoDB default charset=utf8;';
            $this->debug($create);
            $pdo->query($create);

            return $tableName;

        } catch (PDOException $e) {
            $this->log($e->getMessage());
        }
    }

    function getData($yyyymm) {
        if (is_numeric($yyyymm) && $yyyymm > 197001) {

            $this->debug('フォーマット管理：一覧取得');
            $startMonth = $yyyymm;

            $dbo = $this->ProductCsvTableMap->getDataSource();
            $subQuery = $dbo->buildStatement(
                array(
                    'fields' => array('product_category_id', 'MAX(start_month) max_start_month'),
                    'table' => $dbo->fullTableName($this->ProductCsvTableMap),
                    'alias' => 'sub_tbl',
                    'conditions' => array('start_month <=' => $startMonth),
                    'group' => 'product_category_id'),    $this->ProductCsvTableMap);
            $subQuery = '(' . $subQuery . ')';

            // 一覧
            $params = array(
                'conditions' => array(
                    'ProductCsvTableMap.disabled_flg' => false
                ),
                'fields' => array('ProductCsvTableMap.id', 'ProductCsvTableMap.product_input_table_name',
                    'ProductCsvTableMap.common_key_columns', 'ProductCsvTableMap.name_collecting',
                    'ProductCategory.name',
                    'ProductCsvTableMap.modified'),
                'joins' => array(
                    array(
                        'type' => 'INNER',
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'conditions' => array('ProductCategory.id = ProductCsvTableMap.product_category_id')
                    ),
                    array(
                        'type' => 'INNER',
                        'table' => $subQuery,
                        'alias' => 'max_tbl',
                        'conditions' => array('ProductCsvTableMap.product_category_id = max_tbl.product_category_id and ProductCsvTableMap.start_month = max_tbl.max_start_month')
                    )
                ),
                'order' => array('ProductCsvTableMap.id' => 'asc')
            );

            $data = $this->ProductCsvTableMap->find('all', $params);

            if ($data) {
                $this->set('productCsvTableMap', $data);
            }

        }
    }

    function getPdo() {

        $host = '127.0.0.1';
        $user = 'widsadmin';
        $password = 'wids5';
        $dbName = 'backoffice_widsley';

        // エンコーディング設定
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );
        // データベースに接続
        $pdo = new PDO(
            "mysql:host=$host;dbname=$dbName",
            $user,
            $password,
            $options
        );
        return $pdo;
    }

    function getColumn ($inputColumnNames, $csvNames, $columnName) {
        // $columnNameと$csvNamesを比較して一致したindexで$inputColumnNamesを返す
        for ($i = 0; $i < count($csvNames); $i++) {
            if ($columnName == $csvNames[$i]) {
                return $inputColumnNames[$i];
            }
        }
        return '';
    }

    function debug($message)
    {
        $this->log($message, LOG_DEBUG);
    }
}
