<?php
App::uses('AuthController', 'Controller');

class ProductCategoriesController extends AuthController {
    public $uses = array('ProductCategory');

    public function api($id = null){
        if($this->request->is('post')){
            if(!$id){
                if($this->ProductCategory->findByName($this->request->data['product_category_name'])){
                    $this->responseFailure('商品カテゴリ名はすでに使用されています');
                }

                $data = array(
                    'name' => $this->request->data['product_category_name'],
                    'short_name' => $this->request->data['product_category_short_name']
                );
                $this->ProductCategory->create();
                if($this->ProductCategory->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
            else{
                $category = $this->ProductCategory->findById($id);
                if(($category['ProductCategory']['name'] != $this->request->data['product_category_name']) && $this->ProductCategory->findByName($this->request->data['product_category_name'])){
                    $this->responseFailure('商品カテゴリ名はすでに使用されています');
                }

                $data = array(
                    'id' => $id,
                    'name' => $this->request->data['product_category_name'],
                    'short_name' => $this->request->data['product_category_short_name']
                );
                if($this->ProductCategory->save($data)) {
                    $this->responseSuccess();
                }
                else{
                    throw new InternalErrorException();
                }
            }
        }
        else if($this->request->is('delete')){
            try{
                if(!$this->ProductCategory->delete($id)){
                    $this->responseFailure('削除に失敗しました');
                }
            }
            catch(Exception $e){
                $this->responseFailure('削除に失敗しました');
            }

            $this->responseSuccess();
        }
    }

    public function partial($id = null){
        if($id == 'new'){
            $this->set('category', array(
                'id' => 0,
                'name' => '',
                'short_name' => '',
            ));
        }
        else if(is_numeric($id)){
            $data = $this->ProductCategory->findById($id);
            if(!$data) throw new NotFoundException();

            $category = $data['ProductCategory'];
            $this->set('category', $category);
        }
        else{
            throw new BadRequestException();
        }
    }
}
