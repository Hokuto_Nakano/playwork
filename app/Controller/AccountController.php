<?php
App::uses('AuthController', 'Controller');

class AccountController extends AuthController {

    public $uses = array('Staff', 'StaffHistory');

    public function index(){
        $this->getData();
    }

    public function api($id = null){

        if ($this->request->is('post') && $this->request->is('ajax')) {

            $oldLoginId = $this->Session->read('loginId');
            $newLoginId = $this->request->data('loginId');
            $password = $this->request->data('password');

            // ログインID既存チェック
            $self = $this->Staff->findByLoginId($oldLoginId);
            $staffId = $self['Staff']['id'];

            $staff = $this->Staff->find('first', array(
                'conditions' => array(
                    'Staff.id <>' => $staffId,
                    'Staff.login_id' => $newLoginId
                )
            ));
            if ($staff) {
                $this->responseFailure('ログインIDは既に使用されています。[ログインID=' . $newLoginId .']');
            }

            $staffData = array(
                'id' => $staffId,
                'login_id' => $newLoginId,
                'password' => $password
            );
            if (!$this->Staff->save($staffData)) {
                throw new InternalErrorException();
            }
            $this->Session->write('loginId', $newLoginId);

            //チャットシステムのユーザー更新呼び出し
            $userId = $staffId;
            $user = $this->Staff->findById($userId);
            $profile = $this->StaffHistory->getProfile($userId, Util::GetThisMonth());
            Util::RegistChatInfo($userId, $profile["staffName"], $profile["affiliation"], $user["Staff"]["login_id"], $user["Staff"]["password"]);

            $this->responseSuccess();
        }
    }

    function getData() {

        $loginId = $this->Session->read('loginId');

        $staff = $this->Staff->findByLoginId($loginId);

        $password = $staff['Staff']['password'];

        $this->set('loginId', $loginId);
        $this->set('password', $password);

    }
}
