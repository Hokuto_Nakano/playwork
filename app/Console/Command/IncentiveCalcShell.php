<?php
class IncentiveCalcShell extends AppShell {

    public $uses = array('StaffIncentive');

    public function main() {
        $this->log("shell execute", LOG_DEBUG);
        $yyyymm = $this->args[0];
        $this->StaffIncentive->calcIncentive($yyyymm);
    }
}
