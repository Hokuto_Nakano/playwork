<?php

$config["aggregate_period"] = array(
    "monthly" => "月間",
    "total" => "累計"
);

$config["incentive_term_count"] = array(
    "count" => "獲得件数",
    "rank" => "ランキング"
);

$config["incentive_term_bp"] = array(
    "count" => "獲得BP",
    "rank" => "ランキング"
);

$config["ranking_range"] = array(
    "all" => "全体",
    "division" => "事業部",
    "area" => "エリア",
    "section" => "部"
);

$config["target_type"] = array(
    "notype" => "区分なし",
    "staff" => "社員区分",
    "job" => "職種区分"
);

$config["hourly_plus"] = array(
    "0" => "月給",
    "1" => "時給"
);

// PL計算用
$config["pl_calc"] = array(
    "plus" => "プラス",
    "minus" => "マイナス"
);


define("COMPARE_EQUAL", "equal");
define("COMPARE_NOTEQUAL", "notEqual");
define("COMPARE_INDEXOF", "indexOf");
define("COMPARE_NOTINDEXOF", "notIndexOf");

define("NUMBER_OF_DISPLAYED", 20);
define("NUMBER_OF_APPOINT_RANKING", 18);

define("INCENTIVE_TERM_COUNT", "count");
define("INCENTIVE_TERM_RANK", "rank");

define("RANKING_RANGE_ALL", "all");
define("RANKING_RANGE_DIVISION", "division");
define("RANKING_RANGE_AREA", "area");
define("RANKING_RANGE_SECTION", "section");

define("TARGET_TYPE_NOTYPE", "notype");
define("TARGET_TYPE_STAFF", "staff");
define("TARGET_TYPE_JOB", "job");

// ベストエフォート社:0（獲得BP）、ベストエフォート社以外:1（件数）
define("INCENTIVE_COUNT_TYPE", 0);
