# ************************************************************
# Sequel Pro SQL dump
# バージョン 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# ホスト: 127.0.0.1 (MySQL 5.1.73)
# データベース: backoffice_widsley
# 作成時刻: 2017-02-17 01:43:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# テーブルのダンプ acquisition_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acquisition_conditions`;

CREATE TABLE `acquisition_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `count_condition_id` int(10) unsigned NOT NULL COMMENT 'カウント条件ID',
  `acquisition_type_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '獲得区分カラム名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_acquisition_conditions_idx` (`count_condition_id`),
  CONSTRAINT `acquisition_conditions_ibfk_1` FOREIGN KEY (`count_condition_id`) REFERENCES `count_conditions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ acquisition_point_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acquisition_point_histories`;

CREATE TABLE `acquisition_point_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acquisition_type_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `point` float(3,1) NOT NULL DEFAULT '0.0' COMMENT 'BP重み',
  `start_month` int(6) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acquisition_type_id` (`acquisition_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ acquisition_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acquisition_products`;

CREATE TABLE `acquisition_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acquisition_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `product_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `acquisition_date` date NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ acquisition_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acquisition_types`;

CREATE TABLE `acquisition_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `acquisition_type` varchar(20) NOT NULL DEFAULT '' COMMENT '獲得区分',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category_id` (`product_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ acquisitions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acquisitions`;

CREATE TABLE `acquisitions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `acquisition_type_id` int(10) unsigned NOT NULL COMMENT '獲得区分ID',
  `acquisition_date` date NOT NULL,
  `contract_status` enum('契約','キャンセル','解約') NOT NULL DEFAULT '契約',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `division_id` int(10) unsigned NOT NULL COMMENT '事業部テーブルのid',
  `name` varchar(45) NOT NULL DEFAULT '' COMMENT 'エリア名',
  `short_name` varchar(20) DEFAULT NULL COMMENT 'エリア省略名',
  `responsible_staff_id` int(10) unsigned NOT NULL COMMENT '責任者ID（社員テーブルのid）',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `division_id` (`division_id`,`name`),
  KEY `fk_areas_divisions_idx` (`division_id`),
  CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ attendance_calcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendance_calcs`;

CREATE TABLE `attendance_calcs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attendance_time_unit` tinyint(4) NOT NULL COMMENT '出勤時間単位',
  `attendance_time_rounding` enum('round','floor','ceil') NOT NULL COMMENT '出勤時間端数処理',
  `leave_time_unit` tinyint(4) NOT NULL COMMENT '退勤時間単位',
  `leave_time_rounding` enum('round','floor','ceil') NOT NULL COMMENT '退勤時間端数処理',
  `break_time_unit` tinyint(4) NOT NULL COMMENT '休憩時間単位',
  `break_time_rounding` enum('round','floor','ceil') NOT NULL COMMENT '休憩時間端数処理',
  `max_leave_time` time NOT NULL COMMENT '最大退勤時間',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ attendance_csv_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendance_csv_maps`;

CREATE TABLE `attendance_csv_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `other_id_column` varchar(20) NOT NULL COMMENT '外部ユーザーIDカラム',
  `name_column` varchar(20) NOT NULL COMMENT '氏名カラム',
  `date_column` varchar(20) DEFAULT NULL COMMENT '日付カラム（出退勤共通）',
  `attendance_date_column` varchar(20) DEFAULT NULL COMMENT '出勤日カラム',
  `leave_date_column` varchar(20) DEFAULT NULL COMMENT '退勤日カラム',
  `attendance_time_column` varchar(20) DEFAULT NULL COMMENT '出勤時刻カラム',
  `leave_time_column` varchar(20) DEFAULT NULL COMMENT '退勤時刻カラム',
  `attendance_datetime_column` varchar(20) DEFAULT NULL COMMENT '出勤日時カラム',
  `leave_datetime_column` varchar(20) DEFAULT NULL COMMENT '退勤日時カラム',
  `work_hours_column` varchar(20) DEFAULT NULL COMMENT '実働時間カラム',
  `break_hours_column` varchar(20) DEFAULT NULL COMMENT '休憩時間カラム',
  `break_in_time_column` varchar(20) DEFAULT NULL COMMENT '休憩入り時刻カラム',
  `break_out_time_column` varchar(20) DEFAULT NULL COMMENT '休憩戻り時刻カラム',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ attendances
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendances`;

CREATE TABLE `attendances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `attendance_month` int(6) NOT NULL COMMENT '出勤月（yyyymm）',
  `attendance_date` int(11) NOT NULL COMMENT '出勤日（重複チェック、上書き更新に必要）',
  `input_attendance_datetime` datetime NOT NULL COMMENT '入力出勤日時',
  `input_leave_datetime` datetime NOT NULL COMMENT '入力退勤日時',
  `output_attendance_datetime` datetime NOT NULL COMMENT '計算後出勤日時',
  `output_leave_datetime` datetime NOT NULL COMMENT '計算後退勤日時',
  `work_hours_org` time NOT NULL COMMENT '実働時間（残業時間を含まない）',
  `work_hours` time NOT NULL COMMENT '実働時間(残業時間を含む)',
  `break_hours` time NOT NULL COMMENT '休憩時間(h)',
  `over_time` time NOT NULL COMMENT '残業時間',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_id` (`staff_id`,`attendance_date`),
  KEY `fk_attendances_staffs_idx` (`staff_id`),
  CONSTRAINT `fk_attendances_staffs` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ available_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `available_data`;

CREATE TABLE `available_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_name` varchar(45) NOT NULL COMMENT 'データ名',
  `teble_name` varchar(45) NOT NULL COMMENT 'テーブル名',
  `column_name` varchar(45) NOT NULL COMMENT 'カラム名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_name_UNIQUE` (`data_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ call_centers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `call_centers`;

CREATE TABLE `call_centers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT 'コールセンター名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `call_center_name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ companies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '会社名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ composite_count_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `composite_count_items`;

CREATE TABLE `composite_count_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `count_item_id` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid（自身の定義）',
  `calc_count_item_id1` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid',
  `available_data_id1` int(10) unsigned NOT NULL COMMENT '利用可能データテーブルのid',
  `arithmetic_operator` enum('plus','minus','dot','per') NOT NULL COMMENT '算術演算子',
  `calc_count_item_id2` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid',
  `available_data_id2` int(10) unsigned NOT NULL COMMENT '利用可能データテーブルのid',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `count_item_id` (`count_item_id`),
  CONSTRAINT `composite_count_items_ibfk_1` FOREIGN KEY (`count_item_id`) REFERENCES `count_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ count_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `count_conditions`;

CREATE TABLE `count_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL COMMENT '商材CSVテーブルマップテーブルのid',
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材カテゴリid',
  `count_item_id` int(10) unsigned NOT NULL COMMENT 'カウント項目id',
  `date_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '商材インプットテーブルの日付カラム名',
  `data_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '商材インプットテーブルの対象データカラム名',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') NOT NULL DEFAULT 'equal' COMMENT '比較条件',
  `comparison_string` varchar(20) NOT NULL DEFAULT '' COMMENT '比較文字列',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category_id` (`product_category_id`),
  KEY `count_item_id` (`count_item_id`),
  CONSTRAINT `count_conditions_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `count_conditions_ibfk_2` FOREIGN KEY (`count_item_id`) REFERENCES `count_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ count_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `count_items`;

CREATE TABLE `count_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(45) NOT NULL DEFAULT '' COMMENT 'アウトプット名',
  `composite_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '複合フラグ',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ divisions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `divisions`;

CREATE TABLE `divisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '' COMMENT '事業部名',
  `short_name` varchar(20) DEFAULT NULL COMMENT '事業部省略名',
  `responsible_staff_id` int(10) unsigned NOT NULL COMMENT '責任者ID（社員テーブルのid）',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `division_name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ goals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `goals`;

CREATE TABLE `goals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month` int(10) unsigned NOT NULL COMMENT '対象の年月',
  `goal_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '目標件数',
  `goal_bp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '目標BP',
  `division_id` int(10) NOT NULL DEFAULT '0' COMMENT '事業部ID',
  `area_id` int(10) NOT NULL DEFAULT '0' COMMENT 'エリアID',
  `section_id` int(10) NOT NULL DEFAULT '0' COMMENT '部署ID',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_rules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_rules`;

CREATE TABLE `incentive_rules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `incentive_id` int(10) unsigned NOT NULL DEFAULT '0',
  `division_id` int(10) NOT NULL DEFAULT '0',
  `area_id` int(10) NOT NULL DEFAULT '0',
  `section_id` int(10) NOT NULL DEFAULT '0',
  `staff_type_id` int(10) NOT NULL DEFAULT '0',
  `job_type_id` int(10) NOT NULL DEFAULT '0',
  `rank_name` varchar(20) NOT NULL DEFAULT '',
  `count` float NOT NULL,
  `incentive` int(11) NOT NULL,
  `start_month` int(6) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentives`;

CREATE TABLE `incentives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `incentive_name` varchar(20) NOT NULL DEFAULT '',
  `aggregate_period` enum('monthly','total') NOT NULL DEFAULT 'monthly',
  `incentive_term` enum('count','rank') NOT NULL DEFAULT 'count',
  `ranking_range` enum('all','division','area','section') DEFAULT NULL,
  `target_type` enum('staff','job','notype') NOT NULL DEFAULT 'notype',
  `disabled_flg` tinyint(1) NOT NULL DEFAULT '0',
  `hourly_plus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '時給に上乗せするか',
  `start_month` int(6) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ job_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_types`;

CREATE TABLE `job_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ monitor_contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_contents`;

CREATE TABLE `monitor_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_name` varchar(45) NOT NULL DEFAULT '',
  `show_flg` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ output_format_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `output_format_products`;

CREATE TABLE `output_format_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `format_id` int(10) unsigned NOT NULL COMMENT 'アウトプットフォーマットテーブルのid',
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材カテゴリid',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_output_formats_output_format_products_idk` (`format_id`),
  KEY `product_category_id` (`product_category_id`),
  CONSTRAINT `fk_output_formats_output_format_products` FOREIGN KEY (`format_id`) REFERENCES `output_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `output_format_products_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ output_formats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `output_formats`;

CREATE TABLE `output_formats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `format_name` varchar(45) NOT NULL DEFAULT '' COMMENT 'アウトプットフォーマット名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `format_name_UNIQUE` (`format_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_categories`;

CREATE TABLE `product_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '商材名',
  `short_name` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_csv_acquisition_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_acquisition_maps`;

CREATE TABLE `product_csv_acquisition_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL,
  `acquisition_type_id` int(10) unsigned NOT NULL,
  `product_input_column_name` varchar(45) NOT NULL,
  `product_csv_column_name` varchar(45) NOT NULL DEFAULT '',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') NOT NULL DEFAULT 'equal',
  `comparison_string` varchar(20) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_csv_column_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_column_maps`;

CREATE TABLE `product_csv_column_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL COMMENT '商材CSVテーブルマップテーブルのid',
  `product_input_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '商材インプットテーブルのカラム名',
  `product_csv_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '商材CSVのカラム名',
  `data_type` enum('INT','VARCHAR','DATE','FLOAT') NOT NULL DEFAULT 'VARCHAR' COMMENT 'データ型',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_csv_column_maps_product_csv_table_maps_idx` (`table_map_id`),
  CONSTRAINT `` FOREIGN KEY (`table_map_id`) REFERENCES `product_csv_table_maps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_csv_plan_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_plan_maps`;

CREATE TABLE `product_csv_plan_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_input_column_name` varchar(45) NOT NULL,
  `product_csv_column_name` varchar(45) NOT NULL DEFAULT '',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') NOT NULL DEFAULT 'equal',
  `comparison_string` varchar(50) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_csv_status_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_status_maps`;

CREATE TABLE `product_csv_status_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL,
  `product_input_column_name` varchar(45) NOT NULL,
  `product_csv_column_name` varchar(45) NOT NULL DEFAULT '',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') NOT NULL DEFAULT 'equal',
  `comparison_string` varchar(20) NOT NULL DEFAULT '',
  `contract_status` enum('契約','キャンセル','解約') NOT NULL DEFAULT '契約',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_csv_table_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_table_maps`;

CREATE TABLE `product_csv_table_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材テーブルのid',
  `product_input_table_name` varchar(20) NOT NULL COMMENT '商材インプットデータテーブル名',
  `common_key_columns` varchar(255) NOT NULL COMMENT '登録(追加・削除)共通キーカラム名（複数カラムのカンマ区切り）',
  `name_collecting` varchar(255) NOT NULL COMMENT '名寄せキーカラム',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `disabled_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_details`;

CREATE TABLE `product_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_family_id` int(10) unsigned NOT NULL COMMENT '商材I詳細D',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_family_id` (`product_family_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_families
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_families`;

CREATE TABLE `product_families` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '商材詳細名',
  `short_name` varchar(20) DEFAULT NULL COMMENT '商材詳細省略名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category_id` (`product_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_detail_id` int(10) unsigned NOT NULL COMMENT '商材I詳細D',
  `product_detail_name` varchar(50) NOT NULL DEFAULT '',
  `product_detail_short_name` varchar(20) DEFAULT NULL,
  `price` decimal(10,0) unsigned NOT NULL DEFAULT '0',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_detail_id` (`product_detail_id`,`start_month`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ rankings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rankings`;

CREATE TABLE `rankings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `ranking_type` tinyint(1) unsigned DEFAULT '0' COMMENT '0: アポラン（貼出用）1:サポート処理件数',
  `ranking` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最新の順位（全体）',
  `last_ranking` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '前回の順位（全体）',
  `division_ranking` int(10) NOT NULL DEFAULT '0' COMMENT '最新の順位（事業部指定）',
  `division_last_ranking` int(10) NOT NULL DEFAULT '0' COMMENT '前回の順位（事業部指定）',
  `month` int(11) NOT NULL DEFAULT '0' COMMENT 'ランキング対象月',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ranking_staffs` (`staff_id`),
  CONSTRAINT `fk_ranking_staffs` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `area_id` int(10) unsigned NOT NULL COMMENT 'エリアテーブルのid',
  `name` varchar(45) NOT NULL DEFAULT '' COMMENT '部署名',
  `short_name` varchar(20) DEFAULT NULL COMMENT '部署省略名',
  `responsible_staff_id` int(10) unsigned NOT NULL COMMENT '責任者ID（社員テーブルのid）',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sections_areas_idx` (`area_id`),
  CONSTRAINT `sections_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ shift_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shift_types`;

CREATE TABLE `shift_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL COMMENT 'シフト区分',
  `is_break` tinyint(1) NOT NULL DEFAULT '0' COMMENT '休憩ありなしフラグ',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shift_type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staff_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_conditions`;

CREATE TABLE `staff_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `count_condition_id` int(10) unsigned NOT NULL COMMENT 'カウントコンディションID',
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材カテゴリid',
  `user_id_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '外部ユーザーidカラム名',
  `user_name_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '氏名カラム名',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') NOT NULL DEFAULT 'equal' COMMENT '比較条件',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `count_condition_id` (`count_condition_id`),
  CONSTRAINT `staff_conditions_ibfk_1` FOREIGN KEY (`count_condition_id`) REFERENCES `count_conditions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staff_count_ups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_count_ups`;

CREATE TABLE `staff_count_ups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `count_item_id` int(10) unsigned NOT NULL COMMENT 'カウント項目ID',
  `acquisition_type_id` int(10) unsigned DEFAULT NULL COMMENT '獲得区分ID',
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員ID',
  `contract_status` enum('契約','キャンセル','解約') DEFAULT '契約',
  `recorded_date` date NOT NULL COMMENT '計上日',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staff_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_histories`;

CREATE TABLE `staff_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL COMMENT 'ログインID',
  `section_id` int(10) unsigned NOT NULL COMMENT '部署テーブルのid',
  `job_type_id` int(10) unsigned NOT NULL COMMENT '職種テーブルのid',
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `call_center_id` int(10) unsigned NOT NULL COMMENT 'コールセンターテーブルのid',
  `shift_type_id` int(10) unsigned NOT NULL COMMENT 'シフト区分テーブルのid',
  `company_id` int(10) unsigned NOT NULL COMMENT '会社テーブルのid',
  `name` varbinary(255) NOT NULL COMMENT '氏名',
  `start_month` int(6) NOT NULL DEFAULT '0' COMMENT '適用年月(yyyymm)',
  `disabled_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staff_incentives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_incentives`;

CREATE TABLE `staff_incentives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員ID',
  `incentive_id` int(10) unsigned NOT NULL DEFAULT '0',
  `incentive` decimal(10,0) NOT NULL COMMENT 'インセンティブ額',
  `month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staff_pays
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_pays`;

CREATE TABLE `staff_pays` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month` int(6) NOT NULL COMMENT '年月(yyyymm)',
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `pay` int(10) unsigned NOT NULL COMMENT '時給',
  `incentive` decimal(10,0) NOT NULL COMMENT 'インセンティブ額',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `month` (`month`,`staff_id`),
  KEY `fk_pays_staffs_idx` (`staff_id`),
  CONSTRAINT `fk_pays_staffs` FOREIGN KEY (`staff_id`) REFERENCES `staff_histories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staff_point_goals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_point_goals`;

CREATE TABLE `staff_point_goals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month` int(6) NOT NULL COMMENT '年月(yyyymm)',
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `point` int(10) unsigned NOT NULL COMMENT 'BP目標値',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `month` (`month`,`staff_id`),
  KEY `fk_goals_staffs_idx` (`staff_id`),
  CONSTRAINT `fk_goals_staffs` FOREIGN KEY (`staff_id`) REFERENCES `staff_histories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staff_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_types`;

CREATE TABLE `staff_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL COMMENT '社員区分',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staffs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staffs`;

CREATE TABLE `staffs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_id` varchar(45) NOT NULL COMMENT 'ログインID',
  `password` varchar(255) NOT NULL COMMENT 'パスワード',
  `staff_no` varchar(50) NOT NULL DEFAULT '' COMMENT '社員番号',
  `outside_attendance_id` varchar(50) NOT NULL DEFAULT '' COMMENT '外部勤怠システムID',
  `hire_int_date` int(11) NOT NULL DEFAULT '0' COMMENT '入社日(Unixタイムスタンプ)',
  `authority` int(2) NOT NULL DEFAULT '0' COMMENT '権限',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_no_UNIQUE` (`staff_no`),
  UNIQUE KEY `login_id_UNIQUE` (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
