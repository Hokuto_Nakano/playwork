-- Create syntax for TABLE 'pl_categories'
CREATE TABLE `pl_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) NOT NULL DEFAULT '' COMMENT 'PL分類名',
  `calc_order` tinyint(4) NOT NULL DEFAULT '0' COMMENT '計算順序',
  `show_order` tinyint(4) NOT NULL DEFAULT '0' COMMENT '表示順序',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_name` (`category_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'pl_items'
CREATE TABLE `pl_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL COMMENT 'PL分類ID',
  `item_name` varchar(50) NOT NULL DEFAULT '' COMMENT '項目名',
  `calc` enum('plus','minus') NOT NULL DEFAULT 'plus' COMMENT '計算方法',
  `start_month` int(6) NOT NULL DEFAULT '0' COMMENT '適用開始月（yyyymm）',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_name` (`item_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'pl_values'
CREATE TABLE `pl_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL COMMENT 'PL項目ID',
  `division_id` int(10) unsigned NOT NULL COMMENT '事業部ID',
  `area_id` int(10) unsigned NOT NULL COMMENT 'エリアID',
  `section_id` int(10) unsigned NOT NULL COMMENT '部署ID',
  `value` decimal(10,0) NOT NULL COMMENT '値',
  `month` int(6) NOT NULL DEFAULT '0' COMMENT '対象月',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE acquisition_products ADD division_id INT(10) unsigned NOT NULL DEFAULT 0 COMMENT '事業部ID' AFTER product_id;
ALTER TABLE acquisition_products ADD area_id INT(10) unsigned NOT NULL DEFAULT 0 COMMENT 'エリアID' AFTER division_id;
ALTER TABLE acquisition_products ADD section_id INT(10) unsigned NOT NULL DEFAULT 0 COMMENT '部署ID' AFTER area_id;
