ALTER TABLE acquisition_products ADD acquisition_month INT(6) NOT NULL DEFAULT 0 AFTER acquisition_date;
DROP TABLE composite_count_items;
ALTER TABLE areas DROP responsible_staff_id;
ALTER TABLE divisions DROP responsible_staff_id;
ALTER TABLE sections DROP responsible_staff_id;
