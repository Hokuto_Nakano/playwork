# ************************************************************
# Sequel Pro SQL dump
# バージョン 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# ホスト: 127.0.0.1 (MySQL 5.1.73)
# データベース: backoffice_widsley
# 作成時刻: 2017-01-14 05:38:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# テーブルのダンプ incentive_rules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_rules`;

CREATE TABLE `incentive_rules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `incentive_id` int(10) unsigned NOT NULL DEFAULT '0',
  `division_id` int(10) NOT NULL DEFAULT '0',
  `area_id` int(10) NOT NULL DEFAULT '0',
  `section_id` int(10) NOT NULL DEFAULT '0',
  `staff_type_id` int(10) NOT NULL DEFAULT '0',
  `job_type_id` int(10) NOT NULL DEFAULT '0',
  `rank_name` varchar(20) NOT NULL DEFAULT '',
  `count` float NOT NULL,
  `incentive` int(11) NOT NULL,
  `start_month` int(6) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentives`;

CREATE TABLE `incentives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `incentive_name` varchar(20) NOT NULL DEFAULT '',
  `aggregate_period` enum('monthly','total') NOT NULL DEFAULT 'monthly',
  `incentive_term` enum('count','rank') NOT NULL DEFAULT 'count',
  `ranking_range` enum('all','division','area','section') DEFAULT NULL,
  `target_type` enum('staff','job','notype') NOT NULL DEFAULT 'notype',
  `disabled_flg` tinyint(1) NOT NULL DEFAULT '0',
  `start_month` int(6) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
