# ************************************************************
# Sequel Pro SQL dump
# バージョン 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# ホスト: 127.0.0.1 (MySQL 5.1.73)
# データベース: backoffice_widsley
# 作成時刻: 2016-10-06 10:14:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# テーブルの削除 product_point_weights
DROP TABLE IF EXISTS `product_point_weights`;


# テーブルのダンプ acquisition_point_histories
# ------------------------------------------------------------

CREATE TABLE `acquisition_point_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acquisition_type_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `point` float(3,1) NOT NULL DEFAULT '0.0' COMMENT 'BP重み',
  `start_month` int(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `acquisition_type_id` (`acquisition_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ acquisition_types
# ------------------------------------------------------------

CREATE TABLE `acquisition_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `acquisition_type` varchar(20) NOT NULL DEFAULT '' COMMENT '獲得区分',
  PRIMARY KEY (`id`),
  KEY `product_category_id` (`product_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
