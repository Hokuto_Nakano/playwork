# ************************************************************
# Sequel Pro SQL dump
# バージョン 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# ホスト: 127.0.0.1 (MySQL 5.1.73)
# データベース: backoffice_widsley
# 作成時刻: 2016-08-09 04:20:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# テーブルのダンプ areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `division_id` int(10) unsigned NOT NULL COMMENT '事業部テーブルのid',
  `area_name` varchar(45) NOT NULL COMMENT 'エリア名',
  `abridged_name` varchar(20) DEFAULT NULL COMMENT 'エリア省略名',
  `responsible_staff_id` int(10) unsigned NOT NULL COMMENT '責任者ID（社員テーブルのid）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `area_name_UNIQUE` (`area_name`),
  KEY `fk_areas_divisions_idx` (`division_id`),
  CONSTRAINT `fk_areas_divisions` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;

INSERT INTO `areas` (`id`, `division_id`, `area_name`, `abridged_name`, `responsible_staff_id`)
VALUES
	(1,1,'第一エリア','第一OB第一',0);

/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ attendance_calcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendance_calcs`;

CREATE TABLE `attendance_calcs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attendance_time_unit` tinyint(4) NOT NULL COMMENT '出勤時間単位',
  `attendance_time_rounding` enum('round','floor','ceil') NOT NULL COMMENT '出勤時間端数処理',
  `leave_time_unit` tinyint(4) NOT NULL COMMENT '退勤時間単位',
  `leave_time_rounding` enum('round','floor','ceil') NOT NULL COMMENT '退勤時間端数処理',
  `max_leave_time` time NOT NULL COMMENT '最大退勤時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ attendance_csv_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendance_csv_maps`;

CREATE TABLE `attendance_csv_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `other_id_column` varchar(20) NOT NULL COMMENT '外部ユーザーIDカラム',
  `name_column` varchar(20) NOT NULL COMMENT '氏名カラム',
  `date_column` varchar(20) DEFAULT NULL COMMENT '日付カラム（出退勤共通）',
  `attendance_date_column` varchar(20) DEFAULT NULL COMMENT '出勤日カラム',
  `leave_date_column` varchar(20) DEFAULT NULL COMMENT '退勤日カラム',
  `attendance_time_column` varchar(20) DEFAULT NULL COMMENT '出勤時刻カラム',
  `leave_time_column` varchar(20) DEFAULT NULL COMMENT '退勤時刻カラム',
  `attendance_datetime_column` varchar(20) DEFAULT NULL COMMENT '出勤日時カラム',
  `leave_datetime_column` varchar(20) DEFAULT NULL COMMENT '退勤日時カラム',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ attendances
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendances`;

CREATE TABLE `attendances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `attendance_date` int(11) NOT NULL COMMENT '出勤日（重複チェック、上書き更新に必要）',
  `input_attendance_datetime` int(11) NOT NULL COMMENT '入力出勤日時',
  `input_leave_datetime` int(11) NOT NULL COMMENT '入力退勤日時',
  `output_attendance_datetime` int(11) NOT NULL COMMENT '計算後出勤日時',
  `output_leave_datetime` int(11) NOT NULL COMMENT '計算後退勤日時',
  `work_hours` time NOT NULL COMMENT '実働時間(h)',
  `break_hours` time NOT NULL COMMENT '休憩時間(h)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_id` (`staff_id`,`attendance_date`),
  KEY `fk_attendances_staffs_idx` (`staff_id`),
  CONSTRAINT `fk_attendances_staffs` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ available_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `available_data`;

CREATE TABLE `available_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_name` varchar(45) NOT NULL COMMENT 'データ名',
  `teble_name` varchar(45) NOT NULL COMMENT 'テーブル名',
  `column_name` varchar(45) NOT NULL COMMENT 'カラム名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_name_UNIQUE` (`data_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ bp_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bp_histories`;

CREATE TABLE `bp_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL COMMENT '日付',
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `bp` int(10) unsigned NOT NULL COMMENT 'BP累計',
  PRIMARY KEY (`id`),
  UNIQUE KEY `month_社員_idx` (`date`,`staff_id`),
  KEY `fk_goals_staffs_idx` (`staff_id`),
  CONSTRAINT `bp_histories_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ bp_total
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bp_total`;

CREATE TABLE `bp_total` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `bp` int(10) unsigned NOT NULL COMMENT 'BP累計',
  PRIMARY KEY (`id`),
  UNIQUE KEY `month_社員_idx` (`staff_id`),
  KEY `fk_goals_staffs_idx` (`staff_id`),
  CONSTRAINT `bp_total_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ call_centers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `call_centers`;

CREATE TABLE `call_centers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT 'コールセンター名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `call_center_name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `call_centers` WRITE;
/*!40000 ALTER TABLE `call_centers` DISABLE KEYS */;

INSERT INTO `call_centers` (`id`, `name`)
VALUES
	(2,'新宿'),
	(1,'池袋'),
	(3,'飯田橋');

/*!40000 ALTER TABLE `call_centers` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ composite_output_defines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `composite_output_defines`;

CREATE TABLE `composite_output_defines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `output_define_id` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid（自身の定義）',
  `calc_output_define_id1` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid',
  `available_data_id1` int(10) unsigned NOT NULL COMMENT '利用可能データテーブルのid',
  `arithmetic_operator` enum('plus','minus','dot','per') NOT NULL COMMENT '算術演算子',
  `calc_output_define_id2` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid',
  `available_data_id2` int(10) unsigned NOT NULL COMMENT '利用可能データテーブルのid',
  PRIMARY KEY (`id`),
  KEY `fk_composite_output_defines_output_defines_idx` (`output_define_id`),
  CONSTRAINT `fk_composite_output_defines_output_defines` FOREIGN KEY (`output_define_id`) REFERENCES `output_defines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ count_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `count_conditions`;

CREATE TABLE `count_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL COMMENT '商材テーブルのid',
  `register_type` enum('add','delete') NOT NULL COMMENT '登録タイプ（追加 or 削除）',
  `output_define_id` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid',
  `date_column_name` varchar(45) DEFAULT NULL COMMENT '商材インプットテーブルの日付カラム名',
  `data_column_name` varchar(45) DEFAULT NULL COMMENT '商材インプットテーブルの対象データカラム名',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') DEFAULT NULL COMMENT '比較条件',
  `comparison_string` varchar(20) DEFAULT NULL COMMENT '比較文字列',
  PRIMARY KEY (`id`),
  KEY `fk_count_conditions_output_defines_idx` (`output_define_id`),
  KEY `fk_count_conditions_products_idx` (`product_id`),
  CONSTRAINT `fk_count_conditions_output_defines` FOREIGN KEY (`output_define_id`) REFERENCES `output_defines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_count_conditions_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ divisions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `divisions`;

CREATE TABLE `divisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `division_name` varchar(45) NOT NULL COMMENT '事業部名',
  `abridged_name` varchar(20) DEFAULT NULL COMMENT '事業部省略名',
  `responsible_staff_id` int(10) unsigned NOT NULL COMMENT '責任者ID（社員テーブルのid）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `division_name_UNIQUE` (`division_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `divisions` WRITE;
/*!40000 ALTER TABLE `divisions` DISABLE KEYS */;

INSERT INTO `divisions` (`id`, `division_name`, `abridged_name`, `responsible_staff_id`)
VALUES
	(1,'第一アウトバンド事業部','第一アウトバンド',0);

/*!40000 ALTER TABLE `divisions` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ goals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `goals`;

CREATE TABLE `goals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month` int(11) NOT NULL COMMENT '年月',
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `bp` int(10) unsigned NOT NULL COMMENT 'BP目標値',
  PRIMARY KEY (`id`),
  UNIQUE KEY `month` (`month`,`staff_id`),
  KEY `fk_goals_staffs_idx` (`staff_id`),
  CONSTRAINT `fk_goals_staffs` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_bp
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_bp`;

CREATE TABLE `incentive_bp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `bp_over` int(10) unsigned NOT NULL COMMENT '範囲：この値以上',
  `bp_below` int(10) unsigned DEFAULT NULL COMMENT '範囲：この値未満',
  `incentive` decimal(10,2) NOT NULL COMMENT 'インセンティブ額',
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_bp_over
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_bp_over`;

CREATE TABLE `incentive_bp_over` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `award_name` varchar(45) NOT NULL DEFAULT '' COMMENT '賞の名前',
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `bp_over` int(10) unsigned NOT NULL COMMENT '範囲：この値以上',
  `incentive` decimal(10,2) NOT NULL COMMENT 'インセンティブ額',
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_bp_plus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_bp_plus`;

CREATE TABLE `incentive_bp_plus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `bp_every` int(10) unsigned NOT NULL COMMENT '刻み数',
  `incentive` decimal(10,2) NOT NULL COMMENT 'インセンティブ額',
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_bp_total
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_bp_total`;

CREATE TABLE `incentive_bp_total` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `bp` int(10) unsigned NOT NULL COMMENT 'この値を超えたとき',
  `incentive` decimal(10,2) NOT NULL COMMENT 'インセンティブ額',
  `after_hire_date` int(11) NOT NULL COMMENT '入社日範囲：この値以降',
  `before_hire_date` int(11) NOT NULL COMMENT '入社日範囲：この値以前',
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_hourly
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_hourly`;

CREATE TABLE `incentive_hourly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rank_name` char(1) NOT NULL DEFAULT '',
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `bp_over` decimal(10,1) unsigned DEFAULT NULL COMMENT '範囲：この値以上',
  `bp_below` decimal(10,1) unsigned DEFAULT NULL COMMENT '範囲：この値未満',
  `incentive` decimal(10,2) NOT NULL COMMENT 'インセンティブ額',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_name_UNIQUE` (`rank_name`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_hourly_down
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_hourly_down`;

CREATE TABLE `incentive_hourly_down` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `rank_name` char(1) NOT NULL DEFAULT '' COMMENT '時給インセンティブテーブルのrank_name',
  `month_count` tinyint(3) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `recovery_rank` char(1) NOT NULL DEFAULT '' COMMENT '時給インセンティブテーブルのrank_name',
  `incentive` decimal(10,2) NOT NULL COMMENT 'インセンティブ額',
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_hourly_limit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_hourly_limit`;

CREATE TABLE `incentive_hourly_limit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hour_below` int(10) unsigned NOT NULL COMMENT '◯時間未満',
  `incentive` decimal(10,2) NOT NULL COMMENT '上限インセンティブ額',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_ranks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_ranks`;

CREATE TABLE `incentive_ranks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `division_id` int(10) unsigned NOT NULL COMMENT '事業部テーブルのid',
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `rank` tinyint(3) unsigned NOT NULL COMMENT '順位',
  `incentive` decimal(10,2) NOT NULL COMMENT 'インセンティブ額',
  PRIMARY KEY (`id`),
  KEY `division_id` (`division_id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ job_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_types`;

CREATE TABLE `job_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `job_types` WRITE;
/*!40000 ALTER TABLE `job_types` DISABLE KEYS */;

INSERT INTO `job_types` (`id`, `type`)
VALUES
	(1,'AP'),
	(2,'AP（1ヶ月目）'),
	(3,'AP（2ヶ月目）'),
	(4,'GM'),
	(5,'Mgr'),
	(6,'Smgr'),
	(7,'TR'),
	(8,'一般'),
	(9,'一般（1ヶ月目）'),
	(10,'一般（2ヶ月目）'),
	(11,'代表取締役'),
	(12,'副統轄'),
	(13,'執行役員'),
	(14,'統轄'),
	(15,'部長');

/*!40000 ALTER TABLE `job_types` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ monitor_contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_contents`;

CREATE TABLE `monitor_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_name` varchar(45) NOT NULL DEFAULT '',
  `show_flg` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ output_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `output_data`;

CREATE TABLE `output_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `product_history_id` int(10) unsigned NOT NULL COMMENT '商材履歴テーブルのid',
  `date` int(11) NOT NULL COMMENT '日付',
  `output_define_id` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid',
  `data` double NOT NULL COMMENT 'アウトプットデータ',
  PRIMARY KEY (`id`),
  KEY `fk_output_data_staffs_idx` (`staff_id`),
  KEY `fk_output_data_output_defines_idx` (`output_define_id`),
  KEY `fk_output_data_product_histories_idx` (`product_history_id`),
  CONSTRAINT `fk_output_data_output_defines` FOREIGN KEY (`output_define_id`) REFERENCES `output_defines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_output_data_product_histories` FOREIGN KEY (`product_history_id`) REFERENCES `product_histories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_output_data_staffs` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ output_defines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `output_defines`;

CREATE TABLE `output_defines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `output_name` varchar(45) NOT NULL COMMENT 'アウトプット名',
  `composite_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '複合フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ output_format_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `output_format_products`;

CREATE TABLE `output_format_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `format_id` int(10) unsigned NOT NULL COMMENT 'アウトプットフォーマットテーブルのid',
  `product_id` int(10) unsigned NOT NULL COMMENT '商材テーブルのid',
  PRIMARY KEY (`id`),
  KEY `fk_output_formats_output_format_products_idk` (`format_id`),
  KEY `fk_producs_output_format_products_idk` (`product_id`),
  CONSTRAINT `fk_output_formats_output_format_products` FOREIGN KEY (`format_id`) REFERENCES `output_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producs_output_format_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ output_formats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `output_formats`;

CREATE TABLE `output_formats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `format_name` varchar(45) NOT NULL DEFAULT '' COMMENT 'アウトプットフォーマット名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `format_name_UNIQUE` (`format_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_csv_column_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_column_maps`;

CREATE TABLE `product_csv_column_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL COMMENT '商材CSVテーブルマップテーブルのid',
  `product_input_column_name` varchar(45) DEFAULT NULL COMMENT '商材インプットテーブルのカラム名',
  `product_csv_column_name` varchar(45) DEFAULT NULL COMMENT '商材CSVのカラム名',
  `data_type` enum('INT','VARCHAR','DATE','FLOAT') DEFAULT NULL COMMENT 'データ型',
  PRIMARY KEY (`id`),
  KEY `fk_product_csv_column_maps_product_csv_table_maps_idx` (`table_map_id`),
  CONSTRAINT `` FOREIGN KEY (`table_map_id`) REFERENCES `product_csv_table_maps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_csv_table_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_table_maps`;

CREATE TABLE `product_csv_table_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL COMMENT '商材テーブルのid',
  `register_type` enum('add','delete') NOT NULL COMMENT '登録タイプ（追加 or 削除）',
  `product_input_table_name` varchar(20) NOT NULL COMMENT '商材インプットデータテーブル名',
  `common_key_columns` varchar(255) NOT NULL COMMENT '登録(追加・削除)共通キーカラム名（複数カラムのカンマ区切り）',
  `user_id_column_name` varchar(45) DEFAULT NULL COMMENT '外部ユーザーidカラム名',
  `user_name_column_name` varchar(45) DEFAULT NULL COMMENT '外部氏名カラム名',
  PRIMARY KEY (`id`),
  KEY `fk_csv_table_maps_products_idx` (`product_id`),
  CONSTRAINT `fk_table_maps_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_histories`;

CREATE TABLE `product_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL COMMENT '商材テーブルのid',
  `product_category` varchar(45) NOT NULL DEFAULT '' COMMENT '商材分類名',
  `product_name` varchar(45) NOT NULL COMMENT '商材名',
  `abridged_name` varchar(20) DEFAULT NULL COMMENT '商材省略名',
  `sell_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '売単価',
  `cost` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'コスト',
  `bp_weight` float(2,1) NOT NULL DEFAULT '0.0' COMMENT 'BP重み',
  PRIMARY KEY (`id`),
  KEY `fk_product_histories_products_idx` (`product_id`),
  CONSTRAINT `fk_product_histories_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_history_id` int(10) unsigned NOT NULL COMMENT '商材履歴テーブルのid',
  PRIMARY KEY (`id`),
  KEY `fk_products_product_histories_idx` (`product_history_id`),
  CONSTRAINT `fk_products_product_histories` FOREIGN KEY (`product_history_id`) REFERENCES `product_histories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `area_id` int(10) unsigned NOT NULL COMMENT 'エリアテーブルのid',
  `section_name` varchar(45) NOT NULL COMMENT '部署名',
  `abridged_name` varchar(20) DEFAULT NULL COMMENT '部署省略名',
  `responsible_staff_id` int(10) unsigned NOT NULL COMMENT '責任者ID（社員テーブルのid）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `section_name_UNIQUE` (`section_name`),
  KEY `fk_sections_areas_idx` (`area_id`),
  CONSTRAINT `fk_sections_areas` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;

INSERT INTO `sections` (`id`, `area_id`, `section_name`, `abridged_name`, `responsible_staff_id`)
VALUES
	(1,1,'1部','1部',0);

/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ shift_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shift_types`;

CREATE TABLE `shift_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL COMMENT 'シフト区分',
  `is_break` tinyint(1) NOT NULL DEFAULT '0' COMMENT '休憩ありなしフラグ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shift_type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shift_types` WRITE;
/*!40000 ALTER TABLE `shift_types` DISABLE KEYS */;

INSERT INTO `shift_types` (`id`, `type`, `is_break`)
VALUES
	(1,'シフト1',1),
	(2,'シフト2',1),
	(3,'休憩なし',0);

/*!40000 ALTER TABLE `shift_types` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ staff_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_types`;

CREATE TABLE `staff_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL COMMENT '社員区分',
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `staff_types` WRITE;
/*!40000 ALTER TABLE `staff_types` DISABLE KEYS */;

INSERT INTO `staff_types` (`id`, `type`)
VALUES
	(3,'AP'),
	(2,'一般'),
	(4,'出向'),
	(5,'派遣'),
	(1,'責任者');

/*!40000 ALTER TABLE `staff_types` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ staffs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staffs`;

CREATE TABLE `staffs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_no` int(11) NOT NULL COMMENT '社員番号',
  `login_id` varchar(45) NOT NULL COMMENT 'ログインID',
  `password` varchar(255) NOT NULL COMMENT 'パスワード',
  `section_id` int(10) unsigned NOT NULL COMMENT '部署テーブルのid',
  `job_type_id` int(10) unsigned NOT NULL COMMENT '職種テーブルのid',
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `call_center_id` int(10) unsigned NOT NULL COMMENT 'コールセンターテーブルのid',
  `shift_type_id` int(10) unsigned NOT NULL COMMENT 'シフト区分テーブルのid',
  `name` varbinary(255) NOT NULL COMMENT '氏名',
  `hire_date` int(11) NOT NULL DEFAULT '0' COMMENT '入社日',
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_no_UNIQUE` (`staff_no`),
  UNIQUE KEY `login_id_UNIQUE` (`login_id`),
  KEY `fk_staffs_job_types_idx` (`job_type_id`),
  KEY `fk_staffs_staff_types_idx` (`staff_type_id`),
  KEY `fk_staffs_call_centers_idx` (`call_center_id`),
  KEY `fk_staffs_sections_idx` (`section_id`),
  KEY `fk_staffs_shift_types_idx` (`shift_type_id`),
  CONSTRAINT `fk_staffs_call_centers` FOREIGN KEY (`call_center_id`) REFERENCES `call_centers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_staffs_job_types` FOREIGN KEY (`job_type_id`) REFERENCES `job_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_staffs_sections` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_staffs_shift_types` FOREIGN KEY (`shift_type_id`) REFERENCES `shift_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_staffs_staff_types` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `staffs` WRITE;
/*!40000 ALTER TABLE `staffs` DISABLE KEYS */;

INSERT INTO `staffs` (`id`, `staff_no`, `login_id`, `password`, `section_id`, `job_type_id`, `staff_type_id`, `call_center_id`, `shift_type_id`, `name`, `hire_date`)
VALUES
	(10,1,'test','test',1,1,1,1,1,X'E38386E382B9E38388E382B9E382BFE38383E38395',0);

/*!40000 ALTER TABLE `staffs` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
