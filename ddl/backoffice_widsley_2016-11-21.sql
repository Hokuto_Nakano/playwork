# ************************************************************
# Sequel Pro SQL dump
# バージョン 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# ホスト: 127.0.0.1 (MySQL 5.1.73)
# データベース: backoffice_widsley
# 作成時刻: 2016-10-12 12:31:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# テーブルのダンプ acquisition_point_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acquisition_point_histories`;

CREATE TABLE `acquisition_point_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acquisition_type_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `point` float(3,1) NOT NULL DEFAULT '0.0' COMMENT 'BP重み',
  `start_month` int(6) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acquisition_type_id` (`acquisition_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acquisition_point_histories` WRITE;
/*!40000 ALTER TABLE `acquisition_point_histories` DISABLE KEYS */;

INSERT INTO `acquisition_point_histories` (`id`, `acquisition_type_id`, `point`, `start_month`)
VALUES
	(1,1,1.0,0),
	(2,2,0.6,0),
	(3,10,1.5,0),
	(4,4,0.9,0),
	(5,1,2.0,201609),
	(6,11,0.8,201610),
	(7,12,1.2,201611),
	(8,12,1.2,201612);

/*!40000 ALTER TABLE `acquisition_point_histories` ENABLE KEYS */;
UNLOCK TABLES;

# テーブルのダンプ acquisition_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acquisition_types`;

CREATE TABLE `acquisition_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `acquisition_type` varchar(20) NOT NULL DEFAULT '' COMMENT '獲得区分',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category_id` (`product_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acquisition_types` WRITE;
/*!40000 ALTER TABLE `acquisition_types` DISABLE KEYS */;

INSERT INTO `acquisition_types` (`id`, `product_category_id`, `acquisition_type`)
VALUES
	(1,4,'獲得'),
	(10,5,'新規'),
	(11,5,'転用'),
	(12,12,'転用');

/*!40000 ALTER TABLE `acquisition_types` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `division_id` int(10) unsigned NOT NULL COMMENT '事業部テーブルのid',
  `name` varchar(45) NOT NULL DEFAULT '' COMMENT 'エリア名',
  `short_name` varchar(20) DEFAULT NULL COMMENT 'エリア省略名',
  `responsible_staff_id` int(10) unsigned NOT NULL COMMENT '責任者ID（社員テーブルのid）',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `division_id` (`division_id`,`name`),
  KEY `fk_areas_divisions_idx` (`division_id`),
  CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;

INSERT INTO `areas` (`id`, `division_id`, `name`, `short_name`, `responsible_staff_id`)
VALUES
	(1,1,'第一エリア','第一OB第一',0),
	(5,1,'第二エリアあ','第二OB第二あ',0),
	(14,29,'第一エリア','WTS第一',0),
	(15,27,'第一エリア','アラ第一',0),
	(17,30,'サポート','サポート',0),
	(18,27,'第二エリア','アラ第二',0),
	(19,28,'推進事業部','',0),
	(23,32,'人事エリア','人事人事',0),
	(24,26,'第一エリア','第二OB第一',0),
	(25,37,'第一エリア','第四第一',0),
	(26,38,'新規事業部','',0);

/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ attendance_calcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendance_calcs`;

CREATE TABLE `attendance_calcs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attendance_time_unit` tinyint(4) NOT NULL COMMENT '出勤時間単位',
  `attendance_time_rounding` enum('round','floor','ceil') NOT NULL COMMENT '出勤時間端数処理',
  `leave_time_unit` tinyint(4) NOT NULL COMMENT '退勤時間単位',
  `leave_time_rounding` enum('round','floor','ceil') NOT NULL COMMENT '退勤時間端数処理',
  `break_time_unit` tinyint(4) NOT NULL COMMENT '休憩時間単位',
  `break_time_rounding` enum('round','floor','ceil') NOT NULL COMMENT '休憩時間端数処理',
  `max_leave_time` time NOT NULL COMMENT '最大退勤時間',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `attendance_calcs` WRITE;
/*!40000 ALTER TABLE `attendance_calcs` DISABLE KEYS */;

INSERT INTO `attendance_calcs` (`id`, `attendance_time_unit`, `attendance_time_rounding`, `leave_time_unit`, `leave_time_rounding`, `break_time_unit`, `break_time_rounding`, `max_leave_time`)
VALUES
	(1,30,'round',15,'round',15,'round','17:00:00');

/*!40000 ALTER TABLE `attendance_calcs` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ attendance_csv_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendance_csv_maps`;

CREATE TABLE `attendance_csv_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `other_id_column` varchar(20) NOT NULL COMMENT '外部ユーザーIDカラム',
  `name_column` varchar(20) NOT NULL COMMENT '氏名カラム',
  `date_column` varchar(20) DEFAULT NULL COMMENT '日付カラム（出退勤共通）',
  `attendance_date_column` varchar(20) DEFAULT NULL COMMENT '出勤日カラム',
  `leave_date_column` varchar(20) DEFAULT NULL COMMENT '退勤日カラム',
  `attendance_time_column` varchar(20) DEFAULT NULL COMMENT '出勤時刻カラム',
  `leave_time_column` varchar(20) DEFAULT NULL COMMENT '退勤時刻カラム',
  `attendance_datetime_column` varchar(20) DEFAULT NULL COMMENT '出勤日時カラム',
  `leave_datetime_column` varchar(20) DEFAULT NULL COMMENT '退勤日時カラム',
  `work_hours_column` varchar(20) DEFAULT NULL COMMENT '実働時間カラム',
  `break_hours_column` varchar(20) DEFAULT NULL COMMENT '休憩時間カラム',
  `break_in_time_column` varchar(20) DEFAULT NULL COMMENT '休憩入り時刻カラム',
  `break_out_time_column` varchar(20) DEFAULT NULL COMMENT '休憩戻り時刻カラム',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `attendance_csv_maps` WRITE;
/*!40000 ALTER TABLE `attendance_csv_maps` DISABLE KEYS */;

INSERT INTO `attendance_csv_maps` (`id`, `other_id_column`, `name_column`, `date_column`, `attendance_date_column`, `leave_date_column`, `attendance_time_column`, `leave_time_column`, `attendance_datetime_column`, `leave_datetime_column`, `break_hours_column`, `break_in_time_column`, `break_out_time_column`)
VALUES
	(4,'だこっくんID','氏名','年月日','年月日','年月日','出勤時刻','退勤時刻','','','',NULL,''),
	(5,'だこっくんID','氏名','年月日','年月日','年月日','出勤時刻','退勤時刻','','','',NULL,'');

/*!40000 ALTER TABLE `attendance_csv_maps` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ attendances
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendances`;

CREATE TABLE `attendances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `attendance_month` int(6) NOT NULL COMMENT '出勤月（yyyymm）',
  `attendance_date` int(11) NOT NULL COMMENT '出勤日（重複チェック、上書き更新に必要）',
  `input_attendance_datetime` datetime NOT NULL COMMENT '入力出勤日時',
  `input_leave_datetime` datetime NOT NULL COMMENT '入力退勤日時',
  `output_attendance_datetime` datetime NOT NULL COMMENT '計算後出勤日時',
  `output_leave_datetime` datetime NOT NULL COMMENT '計算後退勤日時',
  `work_hours_org` time NOT NULL COMMENT '実働時間（残業時間を含まない）',
  `work_hours` time NOT NULL COMMENT '実働時間(残業時間を含む)',
  `break_hours` time NOT NULL COMMENT '休憩時間(h)',
  `over_time` time NOT NULL COMMENT '残業時間',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_id` (`staff_id`,`attendance_date`),
  KEY `fk_attendances_staffs_idx` (`staff_id`),
  CONSTRAINT `fk_attendances_staffs` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ available_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `available_data`;

CREATE TABLE `available_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_name` varchar(45) NOT NULL COMMENT 'データ名',
  `teble_name` varchar(45) NOT NULL COMMENT 'テーブル名',
  `column_name` varchar(45) NOT NULL COMMENT 'カラム名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_name_UNIQUE` (`data_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ call_centers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `call_centers`;

CREATE TABLE `call_centers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT 'コールセンター名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `call_center_name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `call_centers` WRITE;
/*!40000 ALTER TABLE `call_centers` DISABLE KEYS */;

INSERT INTO `call_centers` (`id`, `name`)
VALUES
	(2,'新宿'),
	(1,'池袋'),
	(3,'飯田橋');

/*!40000 ALTER TABLE `call_centers` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ composite_count_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `composite_count_items`;

CREATE TABLE `composite_count_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `count_item_id` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid（自身の定義）',
  `calc_count_item_id1` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid',
  `available_data_id1` int(10) unsigned NOT NULL COMMENT '利用可能データテーブルのid',
  `arithmetic_operator` enum('plus','minus','dot','per') NOT NULL COMMENT '算術演算子',
  `calc_count_item_id2` int(10) unsigned NOT NULL COMMENT 'アウトプット定義テーブルのid',
  `available_data_id2` int(10) unsigned NOT NULL COMMENT '利用可能データテーブルのid',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `count_item_id` (`count_item_id`),
  CONSTRAINT `composite_count_items_ibfk_1` FOREIGN KEY (`count_item_id`) REFERENCES `count_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ count_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `count_conditions`;

CREATE TABLE `count_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL COMMENT '商材CSVテーブルマップテーブルのid',
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材カテゴリid',
  `count_item_id` int(10) unsigned NOT NULL COMMENT 'カウント項目id',
  `date_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '商材インプットテーブルの日付カラム名',
  `data_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '商材インプットテーブルの対象データカラム名',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') NOT NULL DEFAULT 'equal' COMMENT '比較条件',
  `comparison_string` varchar(20) NOT NULL DEFAULT '' COMMENT '比較文字列',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category_id` (`product_category_id`),
  KEY `count_item_id` (`count_item_id`),
  CONSTRAINT `count_conditions_ibfk_2` FOREIGN KEY (`count_item_id`) REFERENCES `count_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `count_conditions_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `count_conditions` WRITE;
/*!40000 ALTER TABLE `count_conditions` DISABLE KEYS */;

INSERT INTO `count_conditions` (`id`,`table_map_id`, `product_category_id`, `count_item_id`, `date_column_name`, `data_column_name`, `comparison_condition`, `comparison_string`)
VALUES
	(2,1,4,1,'作成日','外部システム顧客番号','notEqual',''),
	(3,1,5,1,'回収コール日','前確番号','notEqual',''),
	(4,1,5,1,'回収コール日','申込番号①','indexOf','OK');

/*!40000 ALTER TABLE `count_conditions` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ count_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `count_items`;

CREATE TABLE `count_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(45) NOT NULL DEFAULT '' COMMENT 'アウトプット名',
  `composite_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '複合フラグ',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `count_items` WRITE;
/*!40000 ALTER TABLE `count_items` DISABLE KEYS */;

INSERT INTO `count_items` (`id`, `item_name`, `composite_flg`)
VALUES
	(1,'アポイント数',0),
	(2,'前確数',0),
	(3,'後確OK数',0),
	(4,'携帯ヒアリング数',0),
	(5,'キャリア取次数',0),
	(6,'上位プラン付帯数',0),
	(7,'ISP付帯数',0),
	(8,'サポート処理件数',0),
	(9,'リトライ数',0),
	(10,'トスアップ数',0);

/*!40000 ALTER TABLE `count_items` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ divisions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `divisions`;

CREATE TABLE `divisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '' COMMENT '事業部名',
  `short_name` varchar(20) DEFAULT NULL COMMENT '事業部省略名',
  `responsible_staff_id` int(10) unsigned NOT NULL COMMENT '責任者ID（社員テーブルのid）',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `division_name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `divisions` WRITE;
/*!40000 ALTER TABLE `divisions` DISABLE KEYS */;

INSERT INTO `divisions` (`id`, `name`, `short_name`, `responsible_staff_id`)
VALUES
	(1,'第一アウトバンド事業部','',0),
	(26,'第二アウトバンド事業部','',0),
	(27,'アライアンス','アラ',0),
	(28,'推進事業部','',0),
	(29,'WTS事業部','WTS',0),
	(30,'サポート','サポート',0),
	(32,'人事部','',0),
	(35,'第三アウトバンド事業部','',0),
	(37,'第四アウトバウンド事業部','第四OB',0),
	(38,'新規事業部','',0);

/*!40000 ALTER TABLE `divisions` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ incentive_hourly
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_hourly`;

CREATE TABLE `incentive_hourly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rank_name` char(1) NOT NULL DEFAULT '',
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `point_over` decimal(10,1) unsigned NOT NULL COMMENT '範囲：この値以上',
  `point_below` decimal(10,1) unsigned NOT NULL COMMENT '範囲：この値未満',
  `incentive` decimal(10,0) NOT NULL COMMENT 'インセンティブ額',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_name_UNIQUE` (`rank_name`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_hourly_down
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_hourly_down`;

CREATE TABLE `incentive_hourly_down` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `rank_name` char(1) NOT NULL DEFAULT '' COMMENT '時給インセンティブテーブルのrank_name',
  `month_count` tinyint(3) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `recovery_rank` char(1) NOT NULL DEFAULT '' COMMENT '時給インセンティブテーブルのrank_name',
  `incentive` decimal(10,0) NOT NULL COMMENT 'インセンティブ額',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_hourly_limit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_hourly_limit`;

CREATE TABLE `incentive_hourly_limit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hour_below` int(10) unsigned NOT NULL COMMENT '◯時間未満',
  `incentive` decimal(10,0) NOT NULL COMMENT '上限インセンティブ額',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_point
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_point`;

CREATE TABLE `incentive_point` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `point_over` int(10) unsigned NOT NULL COMMENT '範囲：この値以上',
  `point_below` int(10) unsigned NOT NULL COMMENT '範囲：この値未満',
  `incentive` decimal(10,0) NOT NULL COMMENT 'インセンティブ額',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_point_over
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_point_over`;

CREATE TABLE `incentive_point_over` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `award_name` varchar(45) NOT NULL DEFAULT '' COMMENT '賞の名前',
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `point_over` int(10) unsigned NOT NULL COMMENT '範囲：この値以上',
  `incentive` decimal(10,0) NOT NULL COMMENT 'インセンティブ額',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_point_plus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_point_plus`;

CREATE TABLE `incentive_point_plus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `point_every` int(10) unsigned NOT NULL COMMENT '刻み数',
  `incentive` decimal(10,0) NOT NULL COMMENT 'インセンティブ額',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_point_total
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_point_total`;

CREATE TABLE `incentive_point_total` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `point` int(10) unsigned NOT NULL COMMENT 'この値を超えたとき',
  `incentive` decimal(10,0) NOT NULL COMMENT 'インセンティブ額',
  `after_hire_int_date` int(11) NOT NULL COMMENT '入社日範囲：この値以降',
  `before_hire_int_date` int(11) NOT NULL COMMENT '入社日範囲：この値以前',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ incentive_ranks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incentive_ranks`;

CREATE TABLE `incentive_ranks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `division_id` int(10) unsigned NOT NULL COMMENT '事業部テーブルのid',
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `rank` tinyint(3) unsigned NOT NULL COMMENT '順位',
  `incentive` decimal(10,0) NOT NULL COMMENT 'インセンティブ額',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `division_id` (`division_id`),
  KEY `staff_type_id` (`staff_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ job_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_types`;

CREATE TABLE `job_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `job_types` WRITE;
/*!40000 ALTER TABLE `job_types` DISABLE KEYS */;

INSERT INTO `job_types` (`id`, `type`)
VALUES
	(1,'AP'),
	(2,'AP（1ヶ月目）'),
	(3,'AP（2ヶ月目）'),
	(4,'GM'),
	(5,'Mgr'),
	(6,'Smgr'),
	(7,'TR'),
	(8,'一般'),
	(9,'一般（1ヶ月目）'),
	(10,'一般（2ヶ月目）'),
	(11,'代表取締役'),
	(12,'副統轄'),
	(13,'執行役員'),
	(14,'統轄'),
	(15,'部長');

/*!40000 ALTER TABLE `job_types` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ monitor_contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `monitor_contents`;

CREATE TABLE `monitor_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_name` varchar(45) NOT NULL DEFAULT '',
  `show_flg` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ output_format_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `output_format_products`;

CREATE TABLE `output_format_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `format_id` int(10) unsigned NOT NULL COMMENT 'アウトプットフォーマットテーブルのid',
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材カテゴリid',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_output_formats_output_format_products_idk` (`format_id`),
  KEY `product_category_id` (`product_category_id`),
  CONSTRAINT `output_format_products_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_output_formats_output_format_products` FOREIGN KEY (`format_id`) REFERENCES `output_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ output_formats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `output_formats`;

CREATE TABLE `output_formats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `format_name` varchar(45) NOT NULL DEFAULT '' COMMENT 'アウトプットフォーマット名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `format_name_UNIQUE` (`format_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ product_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_categories`;

CREATE TABLE `product_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '商材名',
  `short_name` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_categories` WRITE;
/*!40000 ALTER TABLE `product_categories` DISABLE KEYS */;

INSERT INTO `product_categories` (`id`, `name`, `short_name`)
VALUES
	(4,'ウォーターサーバー','水'),
	(5,'BEST光','BEST'),
	(12,'BEST_category','BEST_cate');

/*!40000 ALTER TABLE `product_categories` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ product_csv_acquisition_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_acquisition_maps`;

CREATE TABLE `product_csv_acquisition_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL,
  `acquisition_type_id` int(10) unsigned NOT NULL,
  `product_input_column_name` varchar(45) NOT NULL,
  `product_csv_column_name` varchar(45) NOT NULL DEFAULT '',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') NOT NULL DEFAULT 'equal',
  `comparison_string` varchar(20) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_csv_acquisition_maps` WRITE;
/*!40000 ALTER TABLE `product_csv_acquisition_maps` DISABLE KEYS */;

INSERT INTO `product_csv_acquisition_maps` (`id`, `table_map_id`, `acquisition_type_id`, `product_input_column_name`, `product_csv_column_name`, `comparison_condition`, `comparison_string`)
VALUES
	(1,1,1,'column0','作成日','notEqual',''),
	(2,1,2,'column0','作成日','indexOf',''),
	(3,2,10,'column17','獲得区分','equal','新規'),
	(4,2,11,'column17','獲得区分','equal','転用');

/*!40000 ALTER TABLE `product_csv_acquisition_maps` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ product_csv_column_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_column_maps`;

CREATE TABLE `product_csv_column_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL COMMENT '商材CSVテーブルマップテーブルのid',
  `product_input_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '商材インプットテーブルのカラム名',
  `product_csv_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '商材CSVのカラム名',
  `data_type` enum('INT','VARCHAR','DATE','FLOAT') NOT NULL DEFAULT 'VARCHAR' COMMENT 'データ型',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_csv_column_maps_product_csv_table_maps_idx` (`table_map_id`),
  CONSTRAINT `` FOREIGN KEY (`table_map_id`) REFERENCES `product_csv_table_maps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_csv_column_maps` WRITE;
/*!40000 ALTER TABLE `product_csv_column_maps` DISABLE KEYS */;

INSERT INTO `product_csv_column_maps` (`id`, `table_map_id`, `product_input_column_name`, `product_csv_column_name`, `data_type`)
VALUES
	(1,1,'column0','作成日','DATE'),
	(2,1,'column1','外部システム顧客番号','VARCHAR'),
	(3,1,'column2','トスアップAP名','VARCHAR'),
	(4,1,'column3','トスアップAP番号','VARCHAR'),
	(5,2,'column0','受注番号','VARCHAR'),
	(6,2,'column1','アポインター番号','VARCHAR'),
	(7,2,'column2','アポインター','VARCHAR'),
	(8,2,'column3','前確番号','VARCHAR'),
	(9,2,'column4','前確担当','VARCHAR'),
	(10,2,'column5','部署名','VARCHAR'),
	(11,2,'column6','エリア','VARCHAR'),
	(12,2,'column7','回収コール日','DATE'),
	(13,2,'column8','回収内容','VARCHAR'),
	(14,2,'column9','キャンセル日','DATE'),
	(15,2,'column10','電話サービス申込','VARCHAR'),
	(16,2,'column11','キャンペーン内容','VARCHAR'),
	(17,2,'column12','申込区分','VARCHAR'),
	(18,2,'column13','つながる機器補償フラグ','VARCHAR'),
	(19,2,'column14','くらしのお守りワイド','VARCHAR'),
	(20,2,'column15','NURO_解決サポート','VARCHAR'),
	(21,2,'column16','申込番号①','VARCHAR'),
	(22,2,'column17','獲得区分','VARCHAR');

/*!40000 ALTER TABLE `product_csv_column_maps` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ product_csv_status_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_status_maps`;

CREATE TABLE `product_csv_status_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_map_id` int(10) unsigned NOT NULL,
  `product_input_column_name` varchar(45) NOT NULL,
  `product_csv_column_name` varchar(45) NOT NULL DEFAULT '',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') NOT NULL DEFAULT 'equal',
  `comparison_string` varchar(20) NOT NULL DEFAULT '',
  `contract_status` enum('契約','キャンセル','解約') NOT NULL DEFAULT '契約',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_csv_status_maps` WRITE;
/*!40000 ALTER TABLE `product_csv_status_maps` DISABLE KEYS */;

INSERT INTO `product_csv_status_maps` (`id`, `table_map_id`, `product_input_column_name`, `product_csv_column_name`, `comparison_condition`, `comparison_string`, `contract_status`)
VALUES
	(1,1,'column0','作成日','indexOf','','キャンセル'),
	(2,1,'column0','作成日','indexOf','','解約'),
	(3,2,'column9','キャンセル日','notEqual','','キャンセル'),
	(4,2,'column12','申込区分','equal','解約','解約');

/*!40000 ALTER TABLE `product_csv_status_maps` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ product_csv_table_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_csv_table_maps`;

CREATE TABLE `product_csv_table_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材テーブルのid',
  `product_input_table_name` varchar(20) NOT NULL COMMENT '商材インプットデータテーブル名',
  `common_key_columns` varchar(255) NOT NULL COMMENT '登録(追加・削除)共通キーカラム名（複数カラムのカンマ区切り）',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `disabled_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_csv_table_maps` WRITE;
/*!40000 ALTER TABLE `product_csv_table_maps` DISABLE KEYS */;

INSERT INTO `product_csv_table_maps` (`id`, `product_category_id`, `product_input_table_name`, `common_key_columns`)
VALUES
	(1,4,'table_1476265097',','),
	(2,5,'table_1476268722',',');

/*!40000 ALTER TABLE `product_csv_table_maps` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ product_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_details`;

CREATE TABLE `product_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_family_id` int(10) unsigned NOT NULL COMMENT '商材I詳細D',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_family_id` (`product_family_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_details` WRITE;
/*!40000 ALTER TABLE `product_details` DISABLE KEYS */;

INSERT INTO `product_details` (`id`, `product_family_id`)
VALUES
	(8,2),
	(7,3),
	(1,4),
	(2,5),
	(5,5),
	(6,5);

/*!40000 ALTER TABLE `product_details` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ product_families
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_families`;

CREATE TABLE `product_families` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '商材詳細名',
  `short_name` varchar(20) DEFAULT NULL COMMENT '商材詳細省略名',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category_id` (`product_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_families` WRITE;
/*!40000 ALTER TABLE `product_families` DISABLE KEYS */;

INSERT INTO `product_families` (`id`, `product_category_id`, `name`, `short_name`)
VALUES
	(2,4,'CLYTIA','水CLY'),
	(3,5,'BEST光','BEST光'),
	(4,4,'アイディー','水アイディ'),
	(5,12,'BEST_fami','BEST_fam');

/*!40000 ALTER TABLE `product_families` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_detail_id` int(10) unsigned NOT NULL COMMENT '商材I詳細D',
  `product_detail_name` varchar(50) NOT NULL DEFAULT '',
  `product_detail_short_name` varchar(20) DEFAULT NULL,
  `price` decimal(10,0) unsigned NOT NULL DEFAULT '0',
  `start_month` int(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_detail_id` (`product_detail_id`,`start_month`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `product_detail_id`, `product_detail_name`, `product_detail_short_name`, `price`, `start_month`)
VALUES
	(1,1,'Mt.FUJI25(個人プラン)','Mt25個人',6000,201608),
	(2,1,'Mt.FUJI25(個人プラン)','Mt25個人',6500,201610),
	(3,2,'BEST','BEST',20,0),
	(4,2,'BEST_8月','BEST',2000,201608),
	(5,2,'BEST_10月','BEST',20000,201610),
	(8,5,'vvvffff','vvv',20,201610),
	(9,6,'BEST_family_plan','family_plan',20000,201610),
	(10,6,'BEST_family_planA','family_planA',18000,201611),
	(12,7,'BEST光12月版です','12月版',9000,201612),
	(13,7,'BEST光1月版です','1月版',10000,201701),
	(14,7,'BEST光2月版です','2月版',12000,201702),
	(15,7,'BEST光3月版です','3月版',13000,201703),
	(16,7,'BEST光4月版です','4月版',14000,201704),
	(17,7,'BEST光5月版です','5月版',15000,201705),
	(18,7,'BEST光6月版です','6月版',16000,201706),
	(19,7,'BEST光7月版です','7月版',16000,201707),
	(20,7,'BEST光8月版です','8月版',18000,201708),
	(21,7,'BEST光9月版です','9月版',19000,201709),
	(22,7,'BEST光10月版です','10月版',20000,201710),
	(23,7,'BEST光11月版です','11月版',11000,201711),
	(24,7,'BEST光12月版です','12月版',12000,201712),
	(25,5,'vvv','vvv',20,201710),
	(26,8,'7月','7',777,201707),
	(27,1,'Mt.FUJI25(7月プラン)','Mt25個人7月',7700,201707),
	(28,2,'BEST_9月','BEST',9999,201609);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `area_id` int(10) unsigned NOT NULL COMMENT 'エリアテーブルのid',
  `name` varchar(45) NOT NULL DEFAULT '' COMMENT '部署名',
  `short_name` varchar(20) DEFAULT NULL COMMENT '部署省略名',
  `responsible_staff_id` int(10) unsigned NOT NULL COMMENT '責任者ID（社員テーブルのid）',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sections_areas_idx` (`area_id`),
  CONSTRAINT `sections_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;

INSERT INTO `sections` (`id`, `area_id`, `name`, `short_name`, `responsible_staff_id`)
VALUES
	(2,5,'テスト部署','テスト部署',0),
	(9,1,'第一OB第一エリア','第一OB第一エリア',0),
	(10,1,'第一OB第一エリアB','第一第一B',0),
	(12,14,'2部','WTS第一2部',0),
	(13,15,'1部','',0),
	(14,17,'サポート','サポート',0),
	(17,19,'東北推進','',0),
	(20,14,'1部','WTS第一1部',0),
	(21,19,'推進事業部','推進事業部',0),
	(22,14,'3部','WTS第一3部',0),
	(24,23,'1部','',0),
	(25,24,'1部','第二OB第一1部',0),
	(26,25,'1部','第四第一1部',0),
	(27,26,'新規事業部','新規事業部',0);

/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ shift_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shift_types`;

CREATE TABLE `shift_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL COMMENT 'シフト区分',
  `is_break` tinyint(1) NOT NULL DEFAULT '0' COMMENT '休憩ありなしフラグ',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shift_type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shift_types` WRITE;
/*!40000 ALTER TABLE `shift_types` DISABLE KEYS */;

INSERT INTO `shift_types` (`id`, `type`, `is_break`)
VALUES
	(1,'シフト1',1),
	(2,'シフト2',1),
	(3,'休憩なし',0);

/*!40000 ALTER TABLE `shift_types` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ staff_costs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_costs`;

CREATE TABLE `staff_costs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分ID',
  `cost` decimal(10,0) unsigned NOT NULL DEFAULT '0' COMMENT 'コスト',
  `start_int_month` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '適用開始年月',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# テーブルのダンプ staff_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_conditions`;

CREATE TABLE `staff_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `count_condition_id` int(10) unsigned NOT NULL COMMENT 'カウントコンディションID',
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材カテゴリid',
  `user_id_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '外部ユーザーidカラム名',
  `user_name_column_name` varchar(45) NOT NULL DEFAULT '' COMMENT '氏名カラム名',
  `comparison_condition` enum('equal','notEqual','indexOf','notIndexOf') NOT NULL DEFAULT 'equal' COMMENT '比較条件',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `count_condition_id` (`count_condition_id`),
  CONSTRAINT `staff_conditions_ibfk_1` FOREIGN KEY (`count_condition_id`) REFERENCES `count_conditions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# テーブルのダンプ staff_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_histories`;

CREATE TABLE `staff_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL COMMENT 'ログインID',
  `section_id` int(10) unsigned NOT NULL COMMENT '部署テーブルのid',
  `job_type_id` int(10) unsigned NOT NULL COMMENT '職種テーブルのid',
  `staff_type_id` int(10) unsigned NOT NULL COMMENT '社員区分テーブルのid',
  `call_center_id` int(10) unsigned NOT NULL COMMENT 'コールセンターテーブルのid',
  `shift_type_id` int(10) unsigned NOT NULL COMMENT 'シフト区分テーブルのid',
  `name` varbinary(255) NOT NULL COMMENT '氏名',
  `start_month` int(6) NOT NULL DEFAULT '0' COMMENT '適用年月(yyyymm)',
  `disabled_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `staff_histories` WRITE;
/*!40000 ALTER TABLE `staff_histories` DISABLE KEYS */;

INSERT INTO `staff_histories` (`id`, `staff_id`, `section_id`, `job_type_id`, `staff_type_id`, `call_center_id`, `shift_type_id`, `name`, `start_month`)
VALUES
	(12,2,2,1,1,1,1,X'E382B5E383B3E38397E383ABE382B9E382BFE38383E38395',201608),
	(13,3,2,2,1,2,2,X'E38386E382B9E38388E382B9E382BFE38383E38395',201607),
	(14,2,9,1,1,1,1,X'E382B5E383B3E38397E383ABE382B9E382BFE38383E38395',201611),
	(15,4,2,1,1,1,1,X'E69DBEE4BA95',201610),
	(16,2,2,1,1,1,1,X'E382B5E383B3E38397E383ABE382B9E382BFE38383E38395',201610),
	(17,5,2,1,1,1,1,'kanno',201611),
	(18,6,2,1,1,1,1,'abe',201611),
	(19,7,2,1,1,1,1,'arai',201611),
	(20,8,2,1,1,1,1,'takahashi',201611);

/*!40000 ALTER TABLE `staff_histories` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ staff_point_goals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_point_goals`;

CREATE TABLE `staff_point_goals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month` int(6) NOT NULL COMMENT '年月(yyyymm)',
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員テーブルのid',
  `point` int(10) unsigned NOT NULL COMMENT 'BP目標値',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `month` (`month`,`staff_id`),
  KEY `fk_goals_staffs_idx` (`staff_id`),
  CONSTRAINT `fk_goals_staffs` FOREIGN KEY (`staff_id`) REFERENCES `staff_histories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ staff_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_types`;

CREATE TABLE `staff_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL COMMENT '社員区分',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `staff_types` WRITE;
/*!40000 ALTER TABLE `staff_types` DISABLE KEYS */;

INSERT INTO `staff_types` (`id`, `type`)
VALUES
	(3,'AP'),
	(2,'一般'),
	(4,'出向'),
	(5,'派遣'),
	(1,'責任者');

/*!40000 ALTER TABLE `staff_types` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ staffs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staffs`;

CREATE TABLE `staffs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_id` varchar(45) NOT NULL COMMENT 'ログインID',
  `password` varchar(255) NOT NULL COMMENT 'パスワード',
  `staff_no` varchar(50) NOT NULL DEFAULT '' COMMENT '社員番号',
  `outside_attendance_id` varchar(50) NOT NULL DEFAULT '' COMMENT '外部勤怠システムID',
  `hire_int_date` int(11) NOT NULL DEFAULT '0' COMMENT '入社日(Unixタイムスタンプ)',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_no_UNIQUE` (`staff_no`),
  UNIQUE KEY `login_id_UNIQUE` (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `staffs` WRITE;
/*!40000 ALTER TABLE `staffs` DISABLE KEYS */;

INSERT INTO `staffs` (`id`, `login_id`, `password`, `staff_no`, `outside_attendance_id`, `hire_int_date`)
VALUES
	(2,'sample','sample','0001','',201608),
	(3,'test','test','0002','',201607),
	(4,'matsui','matsui','55','',201608),
	(5,'kannno','kannno','19','9165',201611),
	(6,'abe','abe','6','9166',201611),
	(7,'arai','arai','39','9167',201611),
	(8,'takahashi','takahashi','24','9168',201611);

/*!40000 ALTER TABLE `staffs` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ table_1476265097
# ------------------------------------------------------------

DROP TABLE IF EXISTS `table_1476265097`;

CREATE TABLE `table_1476265097` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `regist_key` varchar(255) NOT NULL,
  `column0` varchar(100) NOT NULL,
  `column1` varchar(100) NOT NULL,
  `column2` varchar(100) NOT NULL,
  `column3` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ table_1476268722
# ------------------------------------------------------------

DROP TABLE IF EXISTS `table_1476268722`;

CREATE TABLE `table_1476268722` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `regist_key` varchar(255) NOT NULL,
  `column0` varchar(100) NOT NULL,
  `column1` varchar(100) NOT NULL,
  `column2` varchar(100) NOT NULL,
  `column3` varchar(100) NOT NULL,
  `column4` varchar(100) NOT NULL,
  `column5` varchar(100) NOT NULL,
  `column6` varchar(100) NOT NULL,
  `column7` varchar(100) NOT NULL,
  `column8` varchar(100) NOT NULL,
  `column9` varchar(100) NOT NULL,
  `column10` varchar(100) NOT NULL,
  `column11` varchar(100) NOT NULL,
  `column12` varchar(100) NOT NULL,
  `column13` varchar(100) NOT NULL,
  `column14` varchar(100) NOT NULL,
  `column15` varchar(100) NOT NULL,
  `column16` varchar(100) NOT NULL,
  `column17` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
