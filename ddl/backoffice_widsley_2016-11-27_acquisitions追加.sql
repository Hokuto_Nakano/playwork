# ************************************************************
# Sequel Pro SQL dump
# バージョン 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# ホスト: 127.0.0.1 (MySQL 5.1.73)
# データベース: backoffice_widsley
# 作成時刻: 2016-11-27 07:14:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# テーブルのダンプ acquisition_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acquisition_products`;

CREATE TABLE `acquisition_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acquisition_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `product_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `acquisition_date` date NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ acquisitions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acquisitions`;

CREATE TABLE `acquisitions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `acquisition_type_id` int(10) unsigned NOT NULL COMMENT '獲得区分ID',
  `acquisition_date` date NOT NULL,
  `contract_status` enum('契約','キャンセル','解約') NOT NULL DEFAULT '契約',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# テーブルのダンプ staff_count_ups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_count_ups`;

CREATE TABLE `staff_count_ups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(10) unsigned NOT NULL COMMENT '商材ID',
  `count_item_id` int(10) unsigned NOT NULL COMMENT 'カウント項目ID',
  `staff_id` int(10) unsigned NOT NULL COMMENT '社員ID',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
